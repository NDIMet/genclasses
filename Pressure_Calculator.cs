﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics;
//using MathNet.Numerics.LinearAlgebra;

namespace Pressure_Calculator
{
    class Pressure_Calculator
    {
        
        public double[] calculatePressureCoefficients(string sondeRawDataFile, string pressureReferenceFile)
        {
            //Get files in string[] format
            string[] sReferenceFile = System.IO.File.ReadAllLines(pressureReferenceFile);
            string[] sSondeFile = System.IO.File.ReadAllLines(sondeRawDataFile);

            //Declare number of data points for reference and sonde files(remove 5 lines for header info)
            int numberRefPoints = sReferenceFile.Length - 5;
            int numberSondePoints = sSondeFile.Length - 5;

            //Initialize reference data multi-dimensional array
            double[,] referenceData = new double[numberRefPoints, 2];

            //Read reference file information into arrays
            for (int i = 0; i < numberRefPoints; i++)
            {
                //Add 5 to start reading after header information
                string[] splitLine = sReferenceFile[i + 5].Split(',');

                double currentElapsedTime = double.Parse(splitLine[0]);
                double currentPressure = double.Parse(splitLine[1]);

                referenceData[i, 0] = currentElapsedTime;
                referenceData[i, 1] = currentPressure;
            }

            //Initialize sonde data arrays with length minus 5 lines for header information
            double[,] sondeRawData = new double[numberSondePoints, 5];

            //Parse each line of the sonde file to fill double arrays
            for (int i = 0; i < numberSondePoints; i++)
            {
                double sondeElapsedTime;
                double sondePressureCount;
                double sondePressureRefCount;
                double sondePressureTempCount;
                double sondePressureTempRefCount;

                try
                {
                    //Add 5 to start reading after header information
                    string[] splitLine = sSondeFile[i + 5].Split(',');

                    sondeElapsedTime = double.Parse(splitLine[0]);
                    sondePressureCount = double.Parse(splitLine[1]);
                    sondePressureRefCount = double.Parse(splitLine[2]);
                    sondePressureTempCount = double.Parse(splitLine[3]);
                    sondePressureTempRefCount = double.Parse(splitLine[4]);
                }
                catch
                {
                    sondeElapsedTime = 0.0;
                    sondePressureCount = 0.0;
                    sondePressureRefCount = 0.0;
                    sondePressureTempCount = 0.0;
                    sondePressureTempRefCount = 0.0;
                }

                sondeRawData[i, 0] = sondeElapsedTime;
                sondeRawData[i, 1] = sondePressureCount;
                sondeRawData[i, 2] = sondePressureRefCount;
                sondeRawData[i, 3] = sondePressureTempCount;
                sondeRawData[i, 4] = sondePressureTempRefCount;
            }

            //Filter sonde data points and record number of bad sonde packets
            int numberOfBadDataPoints = 0;
            for (int i = 0; i < numberSondePoints; i++)
            {
                //Criteria for good raw packet
                if ((sondeRawData[i, 1] != 0) && (sondeRawData[i, 1] < 800000)//Pressure Counts 
                    && (sondeRawData[i, 2] > 400000) && (sondeRawData[i, 2] < 800000)//Pressure Ref Counts
                    && (sondeRawData[i, 3] != 0) && (sondeRawData[i, 3] < 800000)//Temperature Counts
                    && (sondeRawData[i, 4] > 400000) && (sondeRawData[i, 4] < 800000))//Temperature Ref Counts
                {
                    //Intentionally left blank //Intentionally Left here after finding it.
                }
                else
                {
                    //Set Elapsed Time to 0.0 to identify as a bad data point
                    sondeRawData[i, 0] = 0.0;
                    numberOfBadDataPoints++;
                }
            }

            //Declare number of good data points
            int numberOfGoodSondePoints = numberSondePoints - numberOfBadDataPoints;

            //Build array of only good data packets and calculate pressure ratio and temperature
            double[,] filteredSondeData = new double[numberOfGoodSondePoints, 3];

            for (int i = 0, j = 0; i < numberSondePoints; i++)
            {
                if (sondeRawData[i, 0] != 0.0)
                {
                    filteredSondeData[j, 0] = sondeRawData[i, 0];
                    //Calculate Ratios
                    double pressureRatio = sondeRawData[i, 1] / sondeRawData[i, 2];
                    double temperatureRatio = sondeRawData[i, 3] / sondeRawData[i, 4];
                    //Add pressure ratio to array
                    filteredSondeData[j, 1] = pressureRatio;
                    //Calculate temperature
                    double Rtn = 6.04e3 * (1 - temperatureRatio) / temperatureRatio;
                    double calculatedTemperature = 1 / (1 / 3.375e3 * Math.Log(Rtn / 5e3) + 1 / 2.9815e2) - 273.15;
                    //Add temperature to array
                    filteredSondeData[j, 2] = calculatedTemperature;
                    //Increment only if good data packet
                    j++;
                }
            }

            double[,] timeSyncedData = new double[numberRefPoints, 5];
            int syncedDataPoints = 0;

            //Time sync sonde data to reference data
            for (int i = 0; i < numberRefPoints; i++)
            {
                timeSyncedData[i, 0] = referenceData[i, 0];//Elapsed Time
                timeSyncedData[i, 1] = referenceData[i, 1];//Reference Pressure

                timeSyncedData[i, 2] = 0.0;
                timeSyncedData[i, 3] = 0.0;
                timeSyncedData[i, 4] = 0.0;

                for (int j = 0; j < numberOfGoodSondePoints; j++)
                {
                    //System.Diagnostics.Debug.WriteLine(Math.Abs(timeSyncedData[i, 0] - filteredSondeData[j, 0]));

                    if (Math.Abs(timeSyncedData[i, 0] - filteredSondeData[j, 0]) < 2)
                    {
                        timeSyncedData[i, 2] = filteredSondeData[j, 0];//Sonde packet time
                        timeSyncedData[i, 3] = filteredSondeData[j, 1];//Pressure Ratio
                        timeSyncedData[i, 4] = filteredSondeData[j, 2];//Calculated Temperature
                        syncedDataPoints++;
                        break;
                    }
                    
                }
            }

            double[,] trimmedData = new double[syncedDataPoints, 5];

            for (int i = 0, j = 0; i < numberRefPoints; i++)
            {
                if ((timeSyncedData[i, 2] != 0.0) && (timeSyncedData[i, 3] != 0.0) && (timeSyncedData[i, 4] != 0.0))
                {
                    trimmedData[j, 0] = timeSyncedData[i, 0];
                    trimmedData[j, 1] = timeSyncedData[i, 1];
                    trimmedData[j, 2] = timeSyncedData[i, 2];
                    trimmedData[j, 3] = timeSyncedData[i, 3];
                    trimmedData[j, 4] = timeSyncedData[i, 4];
                    j++;
                }

            }

            timeSyncedData = trimmedData;

            System.IO.TextWriter tw = new System.IO.StreamWriter("Universal_CalcData.txt");
            for (int i = 0; i < timeSyncedData.Length / 5; i++) 
            {
                tw.Write(timeSyncedData[i, 0] + ",");
                tw.Write(timeSyncedData[i, 1] + ",");
                tw.Write(timeSyncedData[i, 2] + ",");
                tw.Write(timeSyncedData[i, 3] + ",");
                tw.Write(timeSyncedData[i, 4] + "\r\n");
            }
            tw.Close();

            //Build observation matrix
            double[,] dObservationMatrix = new double[syncedDataPoints, 10];

            for (int i = 0; i < syncedDataPoints; i++)
            {
                dObservationMatrix[i, 0] = Math.Pow(timeSyncedData[i, 3], 0) * Math.Pow(timeSyncedData[i, 4], 0); //p0T0
                dObservationMatrix[i, 1] = Math.Pow(timeSyncedData[i, 3], 0) * Math.Pow(timeSyncedData[i, 4], 1); //p0T1
                dObservationMatrix[i, 2] = Math.Pow(timeSyncedData[i, 3], 0) * Math.Pow(timeSyncedData[i, 4], 2); //p0T2
                dObservationMatrix[i, 3] = Math.Pow(timeSyncedData[i, 3], 0) * Math.Pow(timeSyncedData[i, 4], 3); //p0T3
                dObservationMatrix[i, 4] = Math.Pow(timeSyncedData[i, 3], 1) * Math.Pow(timeSyncedData[i, 4], 0); //p1T0
                dObservationMatrix[i, 5] = Math.Pow(timeSyncedData[i, 3], 1) * Math.Pow(timeSyncedData[i, 4], 1); //p1T1
                dObservationMatrix[i, 6] = Math.Pow(timeSyncedData[i, 3], 1) * Math.Pow(timeSyncedData[i, 4], 2); //p1T2
                dObservationMatrix[i, 7] = Math.Pow(timeSyncedData[i, 3], 2) * Math.Pow(timeSyncedData[i, 4], 0); //p2T0
                dObservationMatrix[i, 8] = Math.Pow(timeSyncedData[i, 3], 2) * Math.Pow(timeSyncedData[i, 4], 1); //p2T1
                dObservationMatrix[i, 9] = Math.Pow(timeSyncedData[i, 3], 3) * Math.Pow(timeSyncedData[i, 4], 0); //p3T0
            }

            double[] usedCoefficients = new double[10];
            try
            {

                MathNet.Numerics.LinearAlgebra.Double.SparseMatrix ObservationMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(dObservationMatrix);
                MathNet.Numerics.LinearAlgebra.Double.SparseMatrix ReferenceMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(syncedDataPoints, 1);

                for (int i = 0; i < syncedDataPoints; i++)
                {
                    ReferenceMatrix[i, 0] = timeSyncedData[i, 1];
                }

                MathNet.Numerics.LinearAlgebra.Double.SparseMatrix transposedO = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)ObservationMatrix.Transpose();
                MathNet.Numerics.LinearAlgebra.Double.SparseMatrix coefficientMatrix = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)(((transposedO.Multiply(ObservationMatrix)).Inverse()).Multiply(transposedO)).Multiply(ReferenceMatrix);

                /* The old dll
                Matrix ObservationMatrix = Matrix.Create(dObservationMatrix);
                Matrix ReferenceMatrix = new Matrix(syncedDataPoints, 1);
                for (int i = 0; i < syncedDataPoints; i++)
                {
                    ReferenceMatrix[i, 0] = timeSyncedData[i, 1];
                }

                Matrix transposedO = Matrix.Transpose(ObservationMatrix);

                Matrix coefficientMatrix = (((transposedO.Multiply(ObservationMatrix)).Inverse()).Multiply(transposedO)).Multiply(ReferenceMatrix);

                */

                for (int i = 0; i < usedCoefficients.Length; i++)
                {
                    usedCoefficients[i] = coefficientMatrix[i, 0];
                }

                double[] pressureCoefficients = new double[25];

                pressureCoefficients[0] = usedCoefficients[0];  //p0t0
                pressureCoefficients[1] = usedCoefficients[1];  //p0t1
                pressureCoefficients[2] = usedCoefficients[2];  //p0t2
                pressureCoefficients[3] = usedCoefficients[3];  //p0t3
                pressureCoefficients[4] = 0.0;                  //p0t4
                pressureCoefficients[5] = usedCoefficients[4];  //p1t0
                pressureCoefficients[6] = usedCoefficients[5];  //p1t1
                pressureCoefficients[7] = usedCoefficients[6];  //p1t2
                pressureCoefficients[8] = 0.0;                  //p1t3
                pressureCoefficients[9] = 0.0;                  //p1t4
                pressureCoefficients[10] = usedCoefficients[7]; //p2t0
                pressureCoefficients[11] = usedCoefficients[8]; //p2t1
                pressureCoefficients[12] = 0.0;                 //p2t2
                pressureCoefficients[13] = 0.0;                 //p2t3
                pressureCoefficients[14] = 0.0;                 //p2t4
                pressureCoefficients[15] = usedCoefficients[9]; //p3t0
                pressureCoefficients[16] = 0.0;                 //p3t1
                pressureCoefficients[17] = 0.0;                 //p3t2
                pressureCoefficients[18] = 0.0;                 //p3t3
                pressureCoefficients[19] = 0.0;                 //p3t4
                pressureCoefficients[20] = 0.0;                 //p4t0
                pressureCoefficients[21] = 0.0;                 //p4t1
                pressureCoefficients[22] = 0.0;                 //p4t2
                pressureCoefficients[23] = 0.0;                 //p4t3
                pressureCoefficients[24] = 0.0;                 //p4t4

                return pressureCoefficients;
            }
            catch
            {
                return null;
            }
        }

        public string analyzeCalibrationCoefficients(double[] coefficients, string sondeRawDataFile, string pressureReferenceFile) 
        {
            //Check coefficient array for proper length
            if ( coefficients==null || coefficients.Length != 25 ) 
            {
                return "FAIL--COEFFICIENTS";
            }

            //Get files in string[] format
            string[] sReferenceFile = System.IO.File.ReadAllLines(pressureReferenceFile);
            string[] sSondeFile = System.IO.File.ReadAllLines(sondeRawDataFile);

            //Declare number of data points for reference and sonde files(remove 5 lines for header info)
            int numberRefPoints = sReferenceFile.Length - 5;
            int numberSondePoints = sSondeFile.Length - 5;

            //Initialize reference data multi-dimensional array
            double[,] referenceData = new double[numberRefPoints, 3];

            //Read reference file information into arrays
            for (int i = 0; i < numberRefPoints; i++)
            {
                //Add 5 to start reading after header information
                string[] splitLine = sReferenceFile[i + 5].Split(',');

                double currentElapsedTime = double.Parse(splitLine[0]);
                double currentPressure = double.Parse(splitLine[1]);
                double currentTemperature = double.Parse(splitLine[2]);

                referenceData[i, 0] = currentElapsedTime;
                referenceData[i, 1] = currentPressure;
                referenceData[i, 2] = currentTemperature;
            }

            //Initialize sonde data arrays with length minus 5 lines for header information
            double[,] sondeRawData = new double[numberSondePoints, 5];

            //Parse each line of the sonde file to fill double arrays
            for (int i = 0; i < numberSondePoints; i++)
            {
                double sondeElapsedTime;
                double sondePressureCount;
                double sondePressureRefCount;
                double sondePressureTempCount;
                double sondePressureTempRefCount;

                try
                {
                    //Add 5 to start reading after header information
                    string[] splitLine = sSondeFile[i + 5].Split(',');

                    sondeElapsedTime = double.Parse(splitLine[0]);
                    sondePressureCount = double.Parse(splitLine[1]);
                    sondePressureRefCount = double.Parse(splitLine[2]);
                    sondePressureTempCount = double.Parse(splitLine[3]);
                    sondePressureTempRefCount = double.Parse(splitLine[4]);
                }
                catch
                {
                    sondeElapsedTime = 0.0;
                    sondePressureCount = 0.0;
                    sondePressureRefCount = 0.0;
                    sondePressureTempCount = 0.0;
                    sondePressureTempRefCount = 0.0;
                }

                sondeRawData[i, 0] = sondeElapsedTime;
                sondeRawData[i, 1] = sondePressureCount;
                sondeRawData[i, 2] = sondePressureRefCount;
                sondeRawData[i, 3] = sondePressureTempCount;
                sondeRawData[i, 4] = sondePressureTempRefCount;
            }

            //Filter sonde data points and record number of bad sonde packets
            int numberOfBadDataPoints = 0;
            for (int i = 0; i < numberSondePoints; i++)
            {
                //Criteria for good raw packet
                if ((sondeRawData[i, 1] != 0) && (sondeRawData[i, 1] < 800000)//Pressure Counts 
                    && (sondeRawData[i, 2] > 400000) && (sondeRawData[i, 2] < 800000)//Pressure Ref Counts
                    && (sondeRawData[i, 3] != 0) && (sondeRawData[i, 3] < 800000)//Temperature Counts
                    && (sondeRawData[i, 4] > 400000) && (sondeRawData[i, 4] < 800000))//Temperature Ref Counts
                {
                    //Intentionally left blank
                }
                else
                {
                    //Set Elapsed Time to 0.0 to identify as a bad data point
                    sondeRawData[i, 0] = 0.0;
                    numberOfBadDataPoints++;
                }
            }

            //Declare number of good data points
            int numberOfGoodSondePoints = numberSondePoints - numberOfBadDataPoints;

            //Build array of only good data packets and calculate pressure ratio and temperature
            double[,] filteredSondeData = new double[numberOfGoodSondePoints, 3];

            for (int i = 0, j = 0; i < numberSondePoints; i++)
            {
                if (sondeRawData[i, 0] != 0.0)
                {
                    filteredSondeData[j, 0] = sondeRawData[i, 0];
                    //Calculate Ratios
                    double pressureRatio = sondeRawData[i, 1] / sondeRawData[i, 2];
                    double temperatureRatio = sondeRawData[i, 3] / sondeRawData[i, 4];
                    //Add pressure ratio to array
                    filteredSondeData[j, 1] = pressureRatio;
                    //Calculate temperature
                    double Rtn = 6.04e3 * (1 - temperatureRatio) / temperatureRatio;
                    double calculatedTemperature = 1 / (1 / 3.375e3 * Math.Log(Rtn / 5e3) + 1 / 2.9815e2) - 273.15;
                    //Add temperature to array
                    filteredSondeData[j, 2] = calculatedTemperature;
                    //Increment only if good data packet
                    j++;
                }
            }

            double[,] timeSyncedData = new double[numberRefPoints, 6];
            int syncedDataPoints = 0;

            //Time sync sonde data to reference data
            for (int i = 0; i < numberRefPoints; i++)
            {
                timeSyncedData[i, 0] = referenceData[i, 0];//Elapsed Time
                timeSyncedData[i, 1] = referenceData[i, 1];//Reference Pressure
                timeSyncedData[i, 5] = referenceData[i, 2];//Reference Temperature

                timeSyncedData[i, 2] = 0.0;
                timeSyncedData[i, 3] = 0.0;
                timeSyncedData[i, 4] = 0.0;

                for (int j = i; j < numberOfGoodSondePoints; j++)
                {
                    if (Math.Abs(timeSyncedData[i, 0] - filteredSondeData[j, 0]) < 1)
                    {
                        timeSyncedData[i, 2] = filteredSondeData[j, 0];//Sonde packet time
                        timeSyncedData[i, 3] = filteredSondeData[j, 1];//Pressure Ratio
                        timeSyncedData[i, 4] = filteredSondeData[j, 2];//Calculated Temperature
                        syncedDataPoints++;
                        break;
                    }
                    else { }
                }
            }

            double[,] trimmedData = new double[syncedDataPoints, 6];

            for (int i = 0, j = 0; i < numberRefPoints; i++)
            {
                if ((timeSyncedData[i, 2] != 0.0) && (timeSyncedData[i, 3] != 0.0) && (timeSyncedData[i, 4] != 0.0))
                {
                    trimmedData[j, 0] = timeSyncedData[i, 0];
                    trimmedData[j, 1] = timeSyncedData[i, 1];
                    trimmedData[j, 2] = timeSyncedData[i, 2];
                    trimmedData[j, 3] = timeSyncedData[i, 3];
                    trimmedData[j, 4] = timeSyncedData[i, 4];
                    trimmedData[j, 5] = timeSyncedData[i, 5];
                    j++;
                }

            }

            timeSyncedData = trimmedData;

            //Check mean and standard deviation of calculated pressures
            double[] calculatedPressureError = new double[syncedDataPoints];
            double[] calculatedTemperatureError = new double[syncedDataPoints];

            for (int i = 0; i < syncedDataPoints; i++)
            {
                double referencePressure = timeSyncedData[i, 1];
                double referenceTemperature = timeSyncedData[i, 5];
                double pressureRatio = timeSyncedData[i, 3];
                double calculatedTemperature = timeSyncedData[i, 4];

                double calculatedPressure = coefficients[0] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[1] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[2] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[3] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 3) +
                    coefficients[5] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[6] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[7] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[10] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[11] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[15] * Math.Pow(pressureRatio, 3) * Math.Pow(calculatedTemperature, 0);

                calculatedPressureError[i] = calculatedPressure - referencePressure;
                calculatedTemperatureError[i] = calculatedTemperature - referenceTemperature;
            }

            double meanOfError = calculateMean(calculatedPressureError);
            double stDev = calculateStDev(calculatedPressureError);
            double RMSerror = Math.Sqrt(Math.Pow(meanOfError, 2) + Math.Pow(stDev, 2));

            double meanOfTempError = calculateMean(calculatedTemperatureError);
            double stDevOfTempError = calculateStDev(calculatedTemperatureError);
            double TempRMSError = Math.Sqrt(Math.Pow(meanOfTempError, 2) + Math.Pow(stDevOfTempError, 2));

            if (RMSerror > 0.3) 
            {
                return "FAIL--RMS="+RMSerror.ToString("0.000")+","+meanOfTempError.ToString("0.000");
            }

            return "PASS--RMS="+RMSerror.ToString("0.000")+","+TempRMSError.ToString("0.000");
        }

        /// <summary>
        /// Method takes in the raw files and pressure referance file and output a object[] in the following format: meanOfError, stDev, RMSerror, meanOfTempError, stDevOfTempError, TempRMSError
        /// All return objects are doubles.
        /// </summary>
        /// <param name="coefficients"></param>
        /// <param name="sondeRawDataFile"></param>
        /// <param name="pressureReferenceFile"></param>
        /// <returns></returns>
        public object[] analyzeCalibrationCoefficients2(double[] coefficients, string sondeRawDataFile, string pressureReferenceFile)
        {
            //Check coefficient array for proper length
            if (coefficients == null || coefficients.Length != 25)
            {
                return null;
            }

            //Get files in string[] format
            string[] sReferenceFile = System.IO.File.ReadAllLines(pressureReferenceFile);
            string[] sSondeFile = System.IO.File.ReadAllLines(sondeRawDataFile);

            //Declare number of data points for reference and sonde files(remove 5 lines for header info)
            int numberRefPoints = sReferenceFile.Length - 5;
            int numberSondePoints = sSondeFile.Length - 5;

            //Initialize reference data multi-dimensional array
            double[,] referenceData = new double[numberRefPoints, 3];

            //Read reference file information into arrays
            for (int i = 0; i < numberRefPoints; i++)
            {
                //Add 5 to start reading after header information
                string[] splitLine = sReferenceFile[i + 5].Split(',');

                double currentElapsedTime = double.Parse(splitLine[0]);
                double currentPressure = double.Parse(splitLine[1]);
                double currentTemperature = double.Parse(splitLine[2]);

                referenceData[i, 0] = currentElapsedTime;
                referenceData[i, 1] = currentPressure;
                referenceData[i, 2] = currentTemperature;
            }

            //Initialize sonde data arrays with length minus 5 lines for header information
            double[,] sondeRawData = new double[numberSondePoints, 5];

            //Parse each line of the sonde file to fill double arrays
            for (int i = 0; i < numberSondePoints; i++)
            {
                double sondeElapsedTime;
                double sondePressureCount;
                double sondePressureRefCount;
                double sondePressureTempCount;
                double sondePressureTempRefCount;

                try
                {
                    //Add 5 to start reading after header information
                    string[] splitLine = sSondeFile[i + 5].Split(',');

                    sondeElapsedTime = double.Parse(splitLine[0]);
                    sondePressureCount = double.Parse(splitLine[1]);
                    sondePressureRefCount = double.Parse(splitLine[2]);
                    sondePressureTempCount = double.Parse(splitLine[3]);
                    sondePressureTempRefCount = double.Parse(splitLine[4]);
                }
                catch
                {
                    sondeElapsedTime = 0.0;
                    sondePressureCount = 0.0;
                    sondePressureRefCount = 0.0;
                    sondePressureTempCount = 0.0;
                    sondePressureTempRefCount = 0.0;
                }

                sondeRawData[i, 0] = sondeElapsedTime;
                sondeRawData[i, 1] = sondePressureCount;
                sondeRawData[i, 2] = sondePressureRefCount;
                sondeRawData[i, 3] = sondePressureTempCount;
                sondeRawData[i, 4] = sondePressureTempRefCount;
            }

            //Filter sonde data points and record number of bad sonde packets
            int numberOfBadDataPoints = 0;
            for (int i = 0; i < numberSondePoints; i++)
            {
                //Criteria for good raw packet
                if ((sondeRawData[i, 1] != 0) && (sondeRawData[i, 1] < 800000)//Pressure Counts 
                    && (sondeRawData[i, 2] > 400000) && (sondeRawData[i, 2] < 800000)//Pressure Ref Counts
                    && (sondeRawData[i, 3] != 0) && (sondeRawData[i, 3] < 800000)//Temperature Counts
                    && (sondeRawData[i, 4] > 400000) && (sondeRawData[i, 4] < 800000))//Temperature Ref Counts
                {
                    //Intentionally left blank
                }
                else
                {
                    //Set Elapsed Time to 0.0 to identify as a bad data point
                    sondeRawData[i, 0] = 0.0;
                    numberOfBadDataPoints++;
                }
            }

            //Declare number of good data points
            int numberOfGoodSondePoints = numberSondePoints - numberOfBadDataPoints;

            //Build array of only good data packets and calculate pressure ratio and temperature
            double[,] filteredSondeData = new double[numberOfGoodSondePoints, 3];

            for (int i = 0, j = 0; i < numberSondePoints; i++)
            {
                if (sondeRawData[i, 0] != 0.0)
                {
                    filteredSondeData[j, 0] = sondeRawData[i, 0];
                    //Calculate Ratios
                    double pressureRatio = sondeRawData[i, 1] / sondeRawData[i, 2];
                    double temperatureRatio = sondeRawData[i, 3] / sondeRawData[i, 4];
                    //Add pressure ratio to array
                    filteredSondeData[j, 1] = pressureRatio;
                    //Calculate temperature
                    double Rtn = 6.04e3 * (1 - temperatureRatio) / temperatureRatio;
                    double calculatedTemperature = 1 / (1 / 3.375e3 * Math.Log(Rtn / 5e3) + 1 / 2.9815e2) - 273.15;
                    //Add temperature to array
                    filteredSondeData[j, 2] = calculatedTemperature;
                    //Increment only if good data packet
                    j++;
                }
            }

            double[,] timeSyncedData = new double[numberRefPoints, 6];
            int syncedDataPoints = 0;

            //Time sync sonde data to reference data
            for (int i = 0; i < numberRefPoints; i++)
            {
                timeSyncedData[i, 0] = referenceData[i, 0];//Elapsed Time
                timeSyncedData[i, 1] = referenceData[i, 1];//Reference Pressure
                timeSyncedData[i, 5] = referenceData[i, 2];//Reference Temperature

                timeSyncedData[i, 2] = 0.0;
                timeSyncedData[i, 3] = 0.0;
                timeSyncedData[i, 4] = 0.0;

                for (int j = i; j < numberOfGoodSondePoints; j++)
                {
                    if (Math.Abs(timeSyncedData[i, 0] - filteredSondeData[j, 0]) < 1)
                    {
                        timeSyncedData[i, 2] = filteredSondeData[j, 0];//Sonde packet time
                        timeSyncedData[i, 3] = filteredSondeData[j, 1];//Pressure Ratio
                        timeSyncedData[i, 4] = filteredSondeData[j, 2];//Calculated Temperature
                        syncedDataPoints++;
                        break;
                    }
                    else { }
                }
            }

            double[,] trimmedData = new double[syncedDataPoints, 6];

            for (int i = 0, j = 0; i < numberRefPoints; i++)
            {
                if ((timeSyncedData[i, 2] != 0.0) && (timeSyncedData[i, 3] != 0.0) && (timeSyncedData[i, 4] != 0.0))
                {
                    trimmedData[j, 0] = timeSyncedData[i, 0];
                    trimmedData[j, 1] = timeSyncedData[i, 1];
                    trimmedData[j, 2] = timeSyncedData[i, 2];
                    trimmedData[j, 3] = timeSyncedData[i, 3];
                    trimmedData[j, 4] = timeSyncedData[i, 4];
                    trimmedData[j, 5] = timeSyncedData[i, 5];
                    j++;
                }

            }

            timeSyncedData = trimmedData;

            //Check mean and standard deviation of calculated pressures
            double[] calculatedPressureError = new double[syncedDataPoints];
            double[] calculatedTemperatureError = new double[syncedDataPoints];

            for (int i = 0; i < syncedDataPoints; i++)
            {
                double referencePressure = timeSyncedData[i, 1];
                double referenceTemperature = timeSyncedData[i, 5];
                double pressureRatio = timeSyncedData[i, 3];
                double calculatedTemperature = timeSyncedData[i, 4];

                double calculatedPressure = coefficients[0] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[1] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[2] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[3] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 3) +
                    coefficients[5] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[6] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[7] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[10] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[11] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[15] * Math.Pow(pressureRatio, 3) * Math.Pow(calculatedTemperature, 0);

                calculatedPressureError[i] = calculatedPressure - referencePressure;
                calculatedTemperatureError[i] = calculatedTemperature - referenceTemperature;
            }

            //Debugging logging. Uncomment for data to analize calibraion.

            string debugFileName = sondeRawDataFile.Substring(0, sondeRawDataFile.Length - 6) + "_deb.csv";
            System.IO.File.WriteAllText(debugFileName, "RefP,RefT,PRatio,CalcTemp,PError,TError\n");

            for (int y = 0; y < syncedDataPoints; y++)
            {
                System.IO.File.AppendAllText(debugFileName, timeSyncedData[y, 1].ToString() + "," + timeSyncedData[y, 5].ToString() + "," + timeSyncedData[y, 3].ToString() + "," +
                                                                    timeSyncedData[y, 4].ToString() + "," + calculatedPressureError[y].ToString() + "," + calculatedTemperatureError[y].ToString() + "\n");
            }
            

            double meanOfError = calculateMean(calculatedPressureError);
            double stDev = calculateStDev(calculatedPressureError);
            double RMSerror = Math.Sqrt(Math.Pow(meanOfError, 2) + Math.Pow(stDev, 2));

            double meanOfTempError = calculateMean(calculatedTemperatureError);
            double stDevOfTempError = calculateStDev(calculatedTemperatureError);
            double TempRMSError = Math.Sqrt(Math.Pow(meanOfTempError, 2) + Math.Pow(stDevOfTempError, 2));

            object[] outputData = new object[6] { meanOfError, stDev, RMSerror, meanOfTempError, stDevOfTempError, TempRMSError };

            return outputData;
        }


        /// <summary>
        /// Method for additional processinf of radiosonde calibration.
        /// Returns RMS
        /// </summary>
        /// <param name="coefficients"></param>
        /// <param name="sondeRawDataFile"></param>
        /// <param name="pressureReferenceFile"></param>
        /// <returns></returns>
        public dataOutput analyzeCalibrationCoefficientsDataOutput(double[] coefficients, string sondeRawDataFile, string pressureReferenceFile)
        {
            //Check coefficient array for proper length
            if (coefficients == null || coefficients.Length != 25)
            {
                return null;
            }

            //Container for returning data.
            dataOutput compiledData = new dataOutput();



            //Get files in string[] format
            string[] sReferenceFile = System.IO.File.ReadAllLines(pressureReferenceFile);
            string[] sSondeFile = System.IO.File.ReadAllLines(sondeRawDataFile);

            //Declare number of data points for reference and sonde files(remove 5 lines for header info)
            int numberRefPoints = sReferenceFile.Length - 5;
            int numberSondePoints = sSondeFile.Length - 5;

            //Initialize reference data multi-dimensional array
            double[,] referenceData = new double[numberRefPoints, 3];

            //Read reference file information into arrays
            for (int i = 0; i < numberRefPoints; i++)
            {
                //Add 5 to start reading after header information
                string[] splitLine = sReferenceFile[i + 5].Split(',');

                double currentElapsedTime = double.Parse(splitLine[0]);
                double currentPressure = double.Parse(splitLine[1]);
                double currentTemperature = double.Parse(splitLine[2]);

                referenceData[i, 0] = currentElapsedTime;
                referenceData[i, 1] = currentPressure;
                referenceData[i, 2] = currentTemperature;
            }

            //Initialize sonde data arrays with length minus 5 lines for header information
            double[,] sondeRawData = new double[numberSondePoints, 5];

            //Parse each line of the sonde file to fill double arrays
            for (int i = 0; i < numberSondePoints; i++)
            {
                double sondeElapsedTime;
                double sondePressureCount;
                double sondePressureRefCount;
                double sondePressureTempCount;
                double sondePressureTempRefCount;

                try
                {
                    //Add 5 to start reading after header information
                    string[] splitLine = sSondeFile[i + 5].Split(',');

                    sondeElapsedTime = double.Parse(splitLine[0]);
                    sondePressureCount = double.Parse(splitLine[1]);
                    sondePressureRefCount = double.Parse(splitLine[2]);
                    sondePressureTempCount = double.Parse(splitLine[3]);
                    sondePressureTempRefCount = double.Parse(splitLine[4]);
                }
                catch
                {
                    sondeElapsedTime = 0.0;
                    sondePressureCount = 0.0;
                    sondePressureRefCount = 0.0;
                    sondePressureTempCount = 0.0;
                    sondePressureTempRefCount = 0.0;
                }

                sondeRawData[i, 0] = sondeElapsedTime;
                sondeRawData[i, 1] = sondePressureCount;
                sondeRawData[i, 2] = sondePressureRefCount;
                sondeRawData[i, 3] = sondePressureTempCount;
                sondeRawData[i, 4] = sondePressureTempRefCount;
            }

            //Filter sonde data points and record number of bad sonde packets
            int numberOfBadDataPoints = 0;
            for (int i = 0; i < numberSondePoints; i++)
            {
                //Criteria for good raw packet
                if ((sondeRawData[i, 1] != 0) && (sondeRawData[i, 1] < 800000)//Pressure Counts 
                    && (sondeRawData[i, 2] > 400000) && (sondeRawData[i, 2] < 800000)//Pressure Ref Counts
                    && (sondeRawData[i, 3] != 0) && (sondeRawData[i, 3] < 800000)//Temperature Counts
                    && (sondeRawData[i, 4] > 400000) && (sondeRawData[i, 4] < 800000))//Temperature Ref Counts
                {
                    //Intentionally left blank
                }
                else
                {
                    //Set Elapsed Time to 0.0 to identify as a bad data point
                    sondeRawData[i, 0] = 0.0;
                    numberOfBadDataPoints++;
                }
            }

            //Declare number of good data points
            int numberOfGoodSondePoints = numberSondePoints - numberOfBadDataPoints;

            //Build array of only good data packets and calculate pressure ratio and temperature
            double[,] filteredSondeData = new double[numberOfGoodSondePoints, 3];

            for (int i = 0, j = 0; i < numberSondePoints; i++)
            {
                if (sondeRawData[i, 0] != 0.0)
                {
                    filteredSondeData[j, 0] = sondeRawData[i, 0];
                    //Calculate Ratios
                    double pressureRatio = sondeRawData[i, 1] / sondeRawData[i, 2];
                    double temperatureRatio = sondeRawData[i, 3] / sondeRawData[i, 4];
                    //Add pressure ratio to array
                    filteredSondeData[j, 1] = pressureRatio;
                    //Calculate temperature
                    double Rtn = 6.04e3 * (1 - temperatureRatio) / temperatureRatio;
                    double calculatedTemperature = 1 / (1 / 3.375e3 * Math.Log(Rtn / 5e3) + 1 / 2.9815e2) - 273.15;
                    //Add temperature to array
                    filteredSondeData[j, 2] = calculatedTemperature;
                    //Increment only if good data packet
                    j++;
                }
            }

            double[,] timeSyncedData = new double[numberRefPoints, 6];
            int syncedDataPoints = 0;

            //Time sync sonde data to reference data
            for (int i = 0; i < numberRefPoints; i++)
            {
                timeSyncedData[i, 0] = referenceData[i, 0];//Elapsed Time
                timeSyncedData[i, 1] = referenceData[i, 1];//Reference Pressure
                timeSyncedData[i, 5] = referenceData[i, 2];//Reference Temperature

                timeSyncedData[i, 2] = 0.0;
                timeSyncedData[i, 3] = 0.0;
                timeSyncedData[i, 4] = 0.0;

                for (int j = i; j < numberOfGoodSondePoints; j++)
                {
                    if (Math.Abs(timeSyncedData[i, 0] - filteredSondeData[j, 0]) < 1)
                    {
                        timeSyncedData[i, 2] = filteredSondeData[j, 0];//Sonde packet time
                        timeSyncedData[i, 3] = filteredSondeData[j, 1];//Pressure Ratio
                        timeSyncedData[i, 4] = filteredSondeData[j, 2];//Calculated Temperature
                        syncedDataPoints++;
                        break;
                    }
                    else { }
                }
            }

            double[,] trimmedData = new double[syncedDataPoints, 6];

            for (int i = 0, j = 0; i < numberRefPoints; i++)
            {
                if ((timeSyncedData[i, 2] != 0.0) && (timeSyncedData[i, 3] != 0.0) && (timeSyncedData[i, 4] != 0.0))
                {
                    trimmedData[j, 0] = timeSyncedData[i, 0];
                    trimmedData[j, 1] = timeSyncedData[i, 1];
                    trimmedData[j, 2] = timeSyncedData[i, 2];
                    trimmedData[j, 3] = timeSyncedData[i, 3];
                    trimmedData[j, 4] = timeSyncedData[i, 4];
                    trimmedData[j, 5] = timeSyncedData[i, 5];
                    j++;
                }

            }

            timeSyncedData = trimmedData;

            //Check mean and standard deviation of calculated pressures
            double[] calculatedPressureError = new double[syncedDataPoints];
            double[] calculatedTemperatureError = new double[syncedDataPoints];


            for (int i = 0; i < syncedDataPoints; i++)
            {
                double referencePressure = timeSyncedData[i, 1];
                double referenceTemperature = timeSyncedData[i, 5];
                double pressureRatio = timeSyncedData[i, 3];
                double calculatedTemperature = timeSyncedData[i, 4];

                double calculatedPressure = coefficients[0] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[1] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[2] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[3] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 3) +
                    coefficients[5] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[6] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[7] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[10] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[11] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[15] * Math.Pow(pressureRatio, 3) * Math.Pow(calculatedTemperature, 0);

                calculatedPressureError[i] = calculatedPressure - referencePressure;
                calculatedTemperatureError[i] = calculatedTemperature - referenceTemperature;

                syncedData tempSData = new syncedData();
                tempSData.elapsedTime = timeSyncedData[i, 0];
                tempSData.refPressure = timeSyncedData[i, 1];
                tempSData.refTemp = timeSyncedData[i, 5];
                tempSData.sondePacketTime = timeSyncedData[i, 2];
                tempSData.sondePressureRatio = timeSyncedData[i, 3];
                tempSData.sondeCalculatedTemp = timeSyncedData[i, 4];
                tempSData.sondeCalculatedPressure = calculatedPressure;

                compiledData.timeSyncedData.Add(tempSData);

            }

            compiledData.meanOfError = calculateMean(calculatedPressureError);
            compiledData.stDev = calculateStDev(calculatedPressureError);
            compiledData.RMSerror = Math.Sqrt(Math.Pow(compiledData.meanOfError, 2) + Math.Pow(compiledData.stDev, 2));

            compiledData.meanOfTempError = calculateMean(calculatedTemperatureError);
            compiledData.stDevOfTempError = calculateStDev(calculatedTemperatureError);
            compiledData.TempRMSError = Math.Sqrt(Math.Pow(compiledData.meanOfTempError, 2) + Math.Pow(compiledData.stDevOfTempError, 2));

            return compiledData;
        }


        public void writePressureCalFile(string headerInformation, string fileNameAndDirectory, double[] coefficients) 
        {
            //Sanity Check
            if (coefficients==null ||coefficients.Length != 25)
            {
                throw new SystemException("Invalid coefficients.");
            }

            System.IO.TextWriter tw = new System.IO.StreamWriter(fileNameAndDirectory);

            tw.WriteLine(headerInformation);

            for (int i = 0; i < coefficients.Length; i++) 
            {
                tw.WriteLine(coefficients[i].ToString("0.00000000e+00"));
            }

            tw.WriteLine("+6.04000000e+03");
            tw.WriteLine("+5.00000000e+03");
            tw.WriteLine("+3.37500000e+03");
            tw.WriteLine("+2.98150000e+02");

            tw.Close();
        }

        public double[] getCoefficientsFromCalFile(string fileNameAndDirectory) 
        {
            double[] coefficients = new double[25];

            string[] allLines = System.IO.File.ReadAllLines(fileNameAndDirectory);
            if (allLines.Length != 30) 
            {
                throw new SystemException("Invalid calibration file format.");
            }

            try
            {
                for (int i = 0; i < coefficients.Length; i++)
                {
                    coefficients[i] = double.Parse(allLines[i + 1]);
                }
                return coefficients;
            }
            catch 
            {
                return null;
            }
            
        }

        private double calculateStDev(double[] numberList)
        {
            double arraySum = 0;
            foreach (double number in numberList)
            {
                arraySum += number;
            }
            double mean = arraySum / numberList.Length;
            double sumOfDevSquared = 0;
            foreach (double number in numberList)
            {
                sumOfDevSquared += Math.Pow((number - mean), 2);
            }
            return Math.Sqrt(sumOfDevSquared / (numberList.Length - 1));
        }

        private double calculateMean(double[] numberList)
        {
            double arraySum = 0;

            foreach (double number in numberList)
            {
                arraySum += number;
            }

            return arraySum / numberList.Length;
        }

        /// <summary>
        /// Method for calculating the radiosonde pressure air temp.
        /// </summary>
        /// <param name="sondePTCount"></param>
        /// <param name="sondePTRCount"></param>
        /// <returns></returns>
        public double calcPressureTemp(double sondePTCount, double sondePTRCount)
        {
            double temperatureRatio = sondePTCount / sondePTRCount;     //Temperature Ratio
            double Rtn = 6.04e3 * (1 - temperatureRatio) / temperatureRatio;    //Inverse Ratio
            return 1 / (1 / 3.375e3 * Math.Log(Rtn / 5e3) + 1 / 2.9815e2) - 273.15;     //Calcing Temp.
        }

        /// <summary>
        /// Calculates the radiosonde pressure output.
        /// </summary>
        /// <param name="pressureCount"></param>
        /// <param name="pressureRefCount"></param>
        /// <param name="calculatedTemperature"></param>
        /// <param name="coefficients"></param>
        /// <returns></returns>
        public double calcPressure(double pressureCount, double pressureRefCount, double calculatedTemperature, double[] coefficients)
        {
            double pressureRatio = pressureCount / pressureRefCount;

            double calculatedPressure = coefficients[0] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[1] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[2] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[3] * Math.Pow(pressureRatio, 0) * Math.Pow(calculatedTemperature, 3) +
                    coefficients[5] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[6] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[7] * Math.Pow(pressureRatio, 1) * Math.Pow(calculatedTemperature, 2) +
                    coefficients[10] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 0) +
                    coefficients[11] * Math.Pow(pressureRatio, 2) * Math.Pow(calculatedTemperature, 1) +
                    coefficients[15] * Math.Pow(pressureRatio, 3) * Math.Pow(calculatedTemperature, 0);

            return calculatedPressure;
        }
    }

    public class dataOutput
    {
        public double meanOfError;
        public double stDev;
        public double RMSerror;
        public double meanOfTempError;
        public double stDevOfTempError;
        public double TempRMSError;
        public List<syncedData> timeSyncedData;

        public dataOutput()
        {
            timeSyncedData = new List<syncedData>();
        }

    }

    public class syncedData
    {
        public double elapsedTime;
        public double refPressure;
        public double refTemp;
        public double sondePacketTime;
        public double sondePressureRatio;
        public double sondeCalculatedTemp;
        public double sondeCalculatedPressure;
    }

}
