﻿///Controller call for the NCD Relay board.
///Created by William Jones
///2013/07/26

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace equipmentNCD
{
    public class equipmentNCDRelay
    {
        //Public items
        public TcpSimpleClient.simpleTcpClient TCPClient;  //Communication client

        //Device Events
        public updateCreater.objectUpdate errorNCD = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate commandProcessed = new updateCreater.objectUpdate();

        //Private Items
        System.Timers.Timer sendDataCheck;              //Checks to see if messages need to be sent out.
        System.Threading.AutoResetEvent dataReceived = new System.Threading.AutoResetEvent(false);   //Responce from last command.
        List<command> outBoundCommands = new List<command>();  //Message to be sent out.
        List<command> commandLog = new List<command>();    //List of all the commands sent.
        System.ComponentModel.BackgroundWorker backgroundWorkerSender;  //Worker that sends out the commands.
        byte[] currentResponce;         //The current responce from the TCP port.

        List<relayBank> relayBoard = new List<relayBank>(); //Banks of relays that the current board has.

        public equipmentNCDRelay() { }

        public equipmentNCDRelay(string ipAddress, int port)
        {
            //Setting up the TCP client for communication
            TCPClient = new TcpSimpleClient.simpleTcpClient();      //Setting up connection
            TCPClient.simpleClientSetupTCP(ipAddress, port);        //Connecting to port.
            TCPClient.recivedMessage.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(recivedMessage_PropertyChange);

            //Setting up background worker.
            backgroundWorkerSender = new System.ComponentModel.BackgroundWorker();
            backgroundWorkerSender.WorkerSupportsCancellation = true;
            backgroundWorkerSender.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerSender_DoWork);

            sendDataCheck = new System.Timers.Timer(100);       //Setting up timer for command
            sendDataCheck.Elapsed += new System.Timers.ElapsedEventHandler(sendDataCheck_Elapsed);
            sendDataCheck.Enabled = true;

        }


        public void disconnectRelay() //Doesn't seem to work.... something worng with the tcp connection I think.
        {
            sendDataCheck.Enabled = false;      //Shutting down sender cheker
            backgroundWorkerSender.CancelAsync();   //Stopping the sender worker
            TCPClient.close();          //Closing connection.
        }

        #region Methods for adding and removing the banks of relays

        /// <summary>
        /// Method for adding a relay bank to the relay board object.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Description"></param>
        public void addRelayBank(int ID, string Description)
        {
            relayBank tempBank = new relayBank();   //The new bank
            tempBank.ID = ID;                       //The Bank ID
            tempBank.description = Description;     //The Desciprion. Not really needed, but an option.
            tempBank.startingRelayNumber = (ID - 1) * 8;

            relay[] newRelays = new relay[8];       //Temp container for the relays
            for(int x = 0; x < newRelays.Length; x++)   //Starting up the relays
            {
                newRelays[x] = new relay();
                newRelays[x].ID = x;        //Relays IDs
                newRelays[x].description = x.ToString();    //Relay Desc. Not needed.
                newRelays[x].state = false;     //Starting state is to be off... This will need to be updated.
            }

            tempBank.bankRelays = newRelays;    //Adding the newly created bank of relays to the bank object.

            relayBoard.Add(tempBank);   //Adding the bank to the relay board object.
        }

        /// <summary>
        /// Method for removing a relay bank from a relay board.
        /// </summary>
        /// <param name="ID"></param>
        public void removeRelayBank(int ID)
        {
            foreach (relayBank rb in relayBoard)    //Finding the relay bank to remove.
            {
                if (rb.ID == ID)    //If found removed it.
                {
                    relayBoard.Remove(rb);
                }
            }
        }


        #endregion

        #region Methods for commanding the relays.

        public void openAllRelays()
        {
            setRelayBank(0);
            addCommand(new byte[] { 0xFE, 0x1d });
        }

        /// <summary>
        /// This method controls the relay bank to be controlled.
        /// </summary>
        /// <param name="bank"></param>
        private void setRelayBank(int bank)
        {
            byte[] tempCommand = new byte[]{0xFE, (byte)49, (byte)bank};
            addCommand(tempCommand);
            
        }

        /// <summary>
        /// This method controls the 1-8 relay of the currently sellected bank.
        /// </summary>
        /// <param name="relay"></param>
        /// <param name="desireState"></param>
        private void setRelayState(int relay, bool desireState)
        {
            byte[] tempCommand; //Overall command.
            if (desireState) //If turning on.
            {
                relay += 8;
            }

            tempCommand = new byte[] { 0xFE, (byte)relay };
            addCommand(tempCommand);
        }

        /// <summary>
        /// Methods sends out the command to the board to change its state.
        /// </summary>
        /// <param name="relay"></param>
        /// <param name="state"></param>
        public void commandRelay(int relay, bool state)
        {
            for (int i = 0; i < relayBoard.Count; i++)
            {
                if (relayBoard[i].startingRelayNumber < relay && relay <= relayBoard[i].startingRelayNumber + 8)
                {
                    setRelayBank(i + 1);
                    relay = relay - (i * 8);
                    setRelayState(relay - 1, state);
                }
            }
        }


        public void startBankStatus(int bank)
        {
            System.ComponentModel.BackgroundWorker getBankStat = new System.ComponentModel.BackgroundWorker();
            getBankStat.DoWork += new System.ComponentModel.DoWorkEventHandler(getBankStatus);
            getBankStat.RunWorkerAsync(bank);
        }




        private void getBankStatus(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int bank = (int)e.Argument;

            for (int x = 0; x < relayBoard.Count; x++)
            {
                if (relayBoard[x].ID == bank)
                {
                    bool[] currentBankState = new bool[8];
                    for (int y = 0; y < currentBankState.Length; y++)
                    {
                        byte[] tempCommand = new byte[] { 0xFE, (byte)(116 + y), (byte)bank };
                        int commandRef = addCommand(tempCommand);

                        command totalPacket = findResponce(commandRef);
                        while (totalPacket == null)
                        {
                            System.Threading.Thread.Sleep(200);
                            totalPacket = findResponce(commandRef);
                        }
                        

                        if (totalPacket.responce[0] == 0x01)
                        {
                            currentBankState[y] = true;
                        }
                        else
                        {
                            currentBankState[y] = false;
                        }
                    }

                    //Sending out the relay information to the object.

                    for(int i = 0; i<currentBankState.Length; i++)
                    {
                        Console.WriteLine("Bank(" + bank.ToString() + ")-Relay(" + i.ToString() + ") State: " + currentBankState[i].ToString());
                    }


                }
            }
        }

        /// <summary>
        /// Add command to the Que
        /// </summary>
        /// <param name="command"></param>
        public int addCommand(byte[] command)
        {
            command tempCommand = new equipmentNCD.command();

            if (commandLog.Count > 0) { tempCommand.commandReferance = commandLog.Last().commandReferance++; }
            else { tempCommand.commandReferance = 0; }

            tempCommand.commandTime = DateTime.Now;
            tempCommand.outBoundCommand = command;

            outBoundCommands.Add(tempCommand);

            return tempCommand.commandReferance;
        }

        public command findResponce(int commandReferance)
        {
            foreach (command cmd in commandLog)
            {
                if (cmd.commandReferance == commandReferance)
                {
                    return cmd;
                }
            }

            return null;
        }


        #endregion

        #region Methods for communicating with the relay board

        //Timer for checking to see if there is data to be sent out.
        void sendDataCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (outBoundCommands.Count > 0 && !backgroundWorkerSender.IsBusy)  //If there are commands to be sent out.
            {
                backgroundWorkerSender.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Worker sends out the commands through the TCP communicator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void backgroundWorkerSender_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (outBoundCommands.Count > 0)      //Continue to run until all pending commands are sent.
            {
                string sendResponce = TCPClient.clientWriteByteTCP(outBoundCommands.First().outBoundCommand);

                if (sendResponce.Contains("ok"))
                {
                    dataReceived.WaitOne();     //Waiting for command responce.
                    outBoundCommands.First().responce = currentResponce;    //Bringing the responce into the outBound Packet.

                    commandLog.Add(outBoundCommands.First());   //Adding the current command to the command log.

                    if (currentResponce[0] == (byte)85)     //Checking to make sure the responce is correct.
                    {
                        commandProcessed.UpdatingObject = "Command Successful";
                    }
                    else
                    {
                        //Sending out an error event for bad command.
                        Exception commandError = new Exception("Bad Command (" + BitConverter.ToString(outBoundCommands.First().outBoundCommand) + ") - Responce=" + BitConverter.ToString(currentResponce));
                        errorNCD.UpdatingObject = commandError;

                    }

                    outBoundCommands.RemoveAt(0);   //Remove last run command.
                }

                
            }

        }

        //Data from TCP client
        void recivedMessage_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            currentResponce = (byte[])data.NewValue;

            dataReceived.Set();
        }

        #endregion


    }

    public class relayBank
    {
        public int ID;              //Basic ID
        public string description;  //Description if needed.
        public int startingRelayNumber;
        public relay[] bankRelays;  //Relays in the bank.

    }

    public class relay
    {
        public int ID;              //ID of relay
        public string description;  //Optional description
        public bool state;          //Current state of relay
    }

    public class command
    {
        public DateTime commandTime;
        public int commandReferance;
        public byte[] outBoundCommand;
        public byte[] responce;
    }

}
