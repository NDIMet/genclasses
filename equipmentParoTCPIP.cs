﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace equipmentTCPIP
{
    public class equipmentParoTCPIP
    {
        //Public Objects
        public equipmentTCPIP.tcpClient client;
        public DateTime commandTime;

        public double currentPressure = 0;

        //Events
        public updateCreater.objectUpdate updatedPressureValue = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate dataPacketRecived = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate sensorError = new updateCreater.objectUpdate();

        public List<dataPacket> dataLog = new List<dataPacket>(); //List of the data logged.


        //Private Objects
        bool runPressure = true;
        System.Threading.AutoResetEvent waitCommand = new System.Threading.AutoResetEvent(false);

        //Current pressure reading
        

        public equipmentParoTCPIP() { }

        public equipmentParoTCPIP(string ipaddress, int port)
        {
            client = new equipmentTCPIP.tcpClient(ipaddress, port);
            client.tcpData.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tcpData_PropertyChange);
            client.tcpError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tcpError_PropertyChange);
        }

        public void startCollect()
        {
            System.Threading.Thread getData = new System.Threading.Thread(getPressure);
            getData.IsBackground = true;
            runPressure = true;
            getData.Start();
        }

        public void stopCollect()
        {
            runPressure = false;
        }

        void getPressure()
        {
            while (runPressure)
            {
                client.sendData("READ?\r");
                commandTime = DateTime.Now;
                waitCommand.WaitOne();
            }
        }

        public double getCurrentPressure()
        {
            return currentPressure;
        }


        void tcpError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incomingData = (Exception)data.NewValue;

            System.Diagnostics.Debug.WriteLine(incomingData.Message);
            sensorError.UpdatingObject = (object)incomingData;
        }

        void tcpData_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            dataPacket incoming = (dataPacket)data.NewValue;
            dataPacketRecived.UpdatingObject = (dataPacket)incoming;

            TimeSpan timePassed = DateTime.Now - commandTime;
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.f") + " -pTimePassed: " + timePassed.TotalSeconds.ToString("0.0000"));

            waitCommand.Set();

            if (incoming.packetMessage.ToLower().Contains("error"))
            {
                //getError();
                //return;
            }

            if (!incoming.packetMessage.Contains("OK") && !incoming.packetMessage.Contains("Paro") && incoming.packetMessage != "\r\n" && incoming.packetMessage != "\n")
            {
                manageData();
                dataLog.Add(incoming);
                currentPressure = Convert.ToDouble(incoming.packetMessage.Trim());
                updatedPressureValue.UpdatingObject = incoming;
            }

        }

        void manageData()
        {
            if (dataLog.Count > 120)
            {
                //lock (dataLock)
                //{
                dataLog.RemoveAt(0);
                //}
            }
        }
         
    }

    public class tcpClient
    {
        //Public objects
        public System.Net.Sockets.TcpClient client;
        public System.Net.Sockets.NetworkStream stream;

        //Update objects
        public updateCreater.objectUpdate tcpStatus = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate tcpError = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate tcpData = new updateCreater.objectUpdate();

        //Thead to collect received data and had it out.
        System.Threading.Thread recData;

        //Private objects
        bool disconnect = false;
        System.Timers.Timer checkConnection;    //Check to make sure the connection is good.
        dataPacket latestPacket;

        string conIP;
        int conPort;

        public tcpClient(){}

        /// <summary>
        /// Method sets up the TCP Client
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public tcpClient(string address, int port)
        {
            conIP = address;
            conPort = port;
            setupConnection(address, port);
        }

        private void setupConnection(string ipaddress, int port)
        {
            client = new System.Net.Sockets.TcpClient();

            client.Connect(ipaddress, port);

            if (client.Connected)
            {
                stream = client.GetStream();
                stream.ReadTimeout = 2000;
                stream.WriteTimeout = 2000;

                recData = new System.Threading.Thread(recTCPData);
                recData.Name = "TCP Client:Paro";
                recData.IsBackground = true;
                recData.Start();

                //Setting up the timer to check the tcp  connection.
                checkConnection = new System.Timers.Timer(1000);
                checkConnection.Elapsed += new System.Timers.ElapsedEventHandler(checkConnection_Elapsed);
                //checkConnection.Enabled = true;

            }
            else
            {
                tcpError.UpdatingObject = new Exception("TCP Socket Error: Unable to connect to: " + ipaddress + ":" + port.ToString());
            }
        }


        void checkConnection_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (latestPacket != null)
            {
                TimeSpan timePassed = (DateTime.Now - latestPacket.packetDate);

                if (client != null)
                {
                    if (!client.Client.Poll(3000000, System.Net.Sockets.SelectMode.SelectRead))
                    {
                        tcpError.UpdatingObject = (object)new Exception("Error: Paro TCP,No new data for: " + timePassed.TotalSeconds.ToString());

                        if (client.Connected && timePassed.TotalSeconds > 60)
                        {
                            disconnect = true;
                            System.Threading.Thread.Sleep(250);

                            stream.Close();
                            client.Close();

                            stream = null;
                            client = null;
                            recData = null;

                            disconnect = false;
                        }

                    }
                }

                if (timePassed.TotalSeconds > 61 || client == null)
                {
                    disconnect = true;
                    System.Threading.Thread.Sleep(250);
                    disconnect = false;

                    try
                    {
                        setupConnection(conIP, conPort);
                        System.Diagnostics.Debug.WriteLine("Reconnect ready.");
                    }
                    catch (Exception conError)
                    {
                        System.Diagnostics.Debug.WriteLine("Reconnect Error: " + conError.Message);
                    }
                }
                

            }
        }

        /// <summary>
        /// Thread that collects all data from the TCP socket and raises the tcpData event when ready.
        /// </summary>
        void recTCPData()
        {
            String curData;
            while (!disconnect)
            {
                if (client.Connected)
                {
                    if (stream.DataAvailable)
                    {
                        System.Threading.Thread.Sleep(100);  //Sleep to allow all data to get into the buffer.
                        Byte[] received = new Byte[100];
                        int nBytesReceived = stream.Read(received, 0, received.Length);
                        Byte[] Output = new Byte[nBytesReceived];

                        for (int i = 0; i < Output.Length; i++)
                        {
                            Output[i] = received[i];
                        }

                        dataPacket tempIncoming = new dataPacket();
                        tempIncoming.packetDate = DateTime.Now;
                        tempIncoming.packetMessage = System.Text.Encoding.ASCII.GetString(Output);
                        latestPacket = tempIncoming;

                        tcpData.UpdatingObject = (object)tempIncoming;
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(50);  //Take a break and wait for data.
                    }
                }

                //Checking to make sure the connect is still good.
                if (!client.Connected)
                {
                    tcpError.UpdatingObject = new Exception("Error: Paro TCP,TCP clint disconnect.");
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// Method allows for sting to be sent out the TCP socket.
        /// </summary>
        /// <param name="message"></param>
        public void sendData(string message)
        {
            if (client.Connected)
            {
                if (stream.CanWrite)
                {
                    Byte[] inputToBeSent = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());
                    stream.Write(inputToBeSent, 0, inputToBeSent.Length);
                    stream.Flush();
                }
            }
            else
            {
                tcpError.UpdatingObject = new Exception("Error: Paro TCP,Client not connected.");
            }
        }

    }

    public class dataPacket
    {
        public DateTime packetDate;
        public string packetMessage;

    }
}
