﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;


namespace MFGPrinting
{
    public class printerSNLabel
    {
        public System.Drawing.Printing.PrintDocument labelSNPrinter;
        public String serialNumber;
        private List<printLine> lines = new List<printLine>();

        #region Methods relating to the printing the radiosonde SN
        public printerSNLabel()
        {
            labelSNPrinter = new PrintDocument();
            labelSNPrinter.PrintPage += new PrintPageEventHandler(labelSNPrinter_PrintPage);
            labelSNPrinter.DefaultPageSettings.Landscape = true;
        }

        public void setPrinter(string printer)
        {
            if (printer == null)
            {
                Exception printerNameError = new Exception("Printer name can not be null");
                throw printerNameError;
            }
            else
            {
                bool found = false;
                foreach (string pr in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {
                    if (printer == pr)
                    {
                        found = true;
                        labelSNPrinter.PrinterSettings.PrinterName = printer;
                    }

                }

                if(!found)
                {
                    Exception printerNameError = new Exception("Printer in not found.");
                    throw printerNameError;
                }
            }
        }

        public void setPageSize(int width, int height)
        {
            PaperSize snPaper = new PaperSize("dymo", width, height);
            labelSNPrinter.DefaultPageSettings.PaperSize = snPaper;
        }

        public void setLandscape(bool desiredLandscape)
        {
            labelSNPrinter.DefaultPageSettings.Landscape = desiredLandscape;
        }

        public void setSNMode()
        {
            setLandscape(true);
            setPageSize(100, 100);
        }

        public void addLabelLine(string line, Font desiredFont, int leftOffset, int topOffset)
        {
            printLine snLine = new printLine();
            snLine.lineToPrint = line;
            snLine.printFont = desiredFont;
            snLine.lineBrushes = System.Drawing.Brushes.Black;
            snLine.leftMargin = leftOffset;
            snLine.topMargin = topOffset;

            lines.Add(snLine);
        }

        public void printSNLabel(string incoming, string idInfo)
        {
            serialNumber = (String)incoming;

            idInfo = idInfo + " " + DateTime.Now.ToString("yyyyMMdd");

            printLine snLine = new printLine();
            snLine.lineToPrint = incoming;
            snLine.printFont = new System.Drawing.Font("Arial Bold", 14, System.Drawing.FontStyle.Bold);
            snLine.lineBrushes = System.Drawing.Brushes.Black;
            snLine.leftMargin = Convert.ToInt16((Convert.ToDouble(labelSNPrinter.DefaultPageSettings.PaperSize.Height) - (Convert.ToDouble(incoming.Length) * 15.4)) / 2);
            snLine.topMargin = 8;

            lines.Add(snLine);


            //Adding date line.
            printLine dateTimeLine = new printLine();
            dateTimeLine.lineToPrint = idInfo;
            dateTimeLine.printFont = new Font("Arial Bold", 5, System.Drawing.FontStyle.Bold);
            dateTimeLine.lineBrushes = System.Drawing.Brushes.Black;
            dateTimeLine.leftMargin = Convert.ToInt16((Convert.ToDouble(labelSNPrinter.DefaultPageSettings.PaperSize.Height) - (Convert.ToDouble(incoming.Length) * 13)) / 2);
            dateTimeLine.topMargin = 30;

            lines.Add(dateTimeLine);


            snLine = new printLine();
            snLine.lineToPrint = incoming;
            snLine.printFont = new System.Drawing.Font("Arial Bold", 14, System.Drawing.FontStyle.Bold);
            snLine.lineBrushes = System.Drawing.Brushes.Black;
            snLine.leftMargin = Convert.ToInt16((Convert.ToDouble(labelSNPrinter.DefaultPageSettings.PaperSize.Height) - (Convert.ToDouble(incoming.Length) * 15.4)) / 2);
            snLine.topMargin = 56;

            lines.Add(snLine);


            //Adding date line.
            dateTimeLine = new printLine();
            dateTimeLine.lineToPrint = idInfo;
            dateTimeLine.printFont = new Font("Arial Bold", 5, System.Drawing.FontStyle.Bold);
            dateTimeLine.lineBrushes = System.Drawing.Brushes.Black;
            dateTimeLine.leftMargin = Convert.ToInt16((Convert.ToDouble(labelSNPrinter.DefaultPageSettings.PaperSize.Height) - (Convert.ToDouble(incoming.Length) * 13)) / 2);
            dateTimeLine.topMargin = 78;

            lines.Add(dateTimeLine);




            labelSNPrinter.Print();
        }

        private void labelSNPrinter_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                e.Graphics.DrawString(lines[i].lineToPrint, lines[i].printFont, (System.Drawing.Brush)lines[i].lineBrushes, lines[i].leftMargin, lines[i].topMargin, new System.Drawing.StringFormat());
            }

            lines.Clear();  //Clearing all lines.
        }
        #endregion

    }

    public class printerBoxLabel
    {
        public System.Drawing.Printing.PrintDocument labelBoxPrinter;

        printLine title;
        printLine description;
        printLine count;
        printLine date;

        public printerBoxLabel()
        {
            labelBoxPrinter = new PrintDocument();
            labelBoxPrinter.PrintPage += new PrintPageEventHandler(labelBoxPrinter_PrintPage);
            setPageSize(225, 400);
            labelBoxPrinter.DefaultPageSettings.Landscape = true;

        }

        public void setPrinter(string printer)
        {
            if (printer == null)
            {
                Exception printerNameError = new Exception("Printer name can not be null");
                throw printerNameError;
            }
            else
            {
                bool found = false;
                foreach (string pr in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {
                    if (printer == pr)
                    {
                        found = true;
                        labelBoxPrinter.PrinterSettings.PrinterName = printer;
                    }

                }

                if (!found)
                {
                    Exception printerNameError = new Exception("Printer in not found.");
                    throw printerNameError;
                }
            }
        }

        public void setPageSize(int width, int height)
        {
            PaperSize snPaper = new PaperSize("dymo", width, height);
            labelBoxPrinter.DefaultPageSettings.PaperSize = snPaper;
        }

        public void printBoxLabel(string title, string desc, string count, string date)
        {
            System.Drawing.Font normalFont = new System.Drawing.Font("Arial Bold", 14, System.Drawing.FontStyle.Bold);

            this.title = new printLine();
            this.title.lineToPrint = title;
            this.title.printFont = normalFont;
            this.title.lineBrushes = System.Drawing.Brushes.Black;
            this.title.leftMargin = 0;
            this.title.topMargin = 0;

            this.description = new printLine();
            this.description.lineToPrint = desc;
            this.description.printFont = normalFont;
            this.description.lineBrushes = System.Drawing.Brushes.Black;
            this.description.leftMargin = 0;
            this.description.topMargin = 0;

            this.count = new printLine();
            this.count.lineToPrint = count;
            this.count.printFont = normalFont;
            this.count.lineBrushes = System.Drawing.Brushes.Black;
            this.count.leftMargin = 0;
            this.count.topMargin = 0;

            this.date = new printLine();
            this.date.lineToPrint = date;
            this.date.printFont = normalFont;
            this.date.lineBrushes = System.Drawing.Brushes.Black;
            this.date.leftMargin = 0;
            this.date.topMargin = 0;

            labelBoxPrinter.Print();
        }


        void labelBoxPrinter_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(title.lineToPrint, title.printFont, (System.Drawing.Brush)title.lineBrushes, 0, 0, new System.Drawing.StringFormat());
            e.Graphics.DrawString(description.lineToPrint, description.printFont, (System.Drawing.Brush)description.lineBrushes, 0, 15, new System.Drawing.StringFormat());
            e.Graphics.DrawString(count.lineToPrint, count.printFont, (System.Drawing.Brush)count.lineBrushes, 0, 180, new System.Drawing.StringFormat());
            e.Graphics.DrawString(date.lineToPrint, date.printFont, (System.Drawing.Brush)date.lineBrushes, 75, 180, new System.Drawing.StringFormat());
        }
    }

    public class printLine
    {
        public string lineToPrint;
        public Font printFont;
        public object lineBrushes;
        public int leftMargin;
        public int topMargin;
    }
}
