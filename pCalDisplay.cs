﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace displayCalibration
{
    public class pCalDisplay
    {
        //Public items.
        public updateCreater.objectUpdate addToMain = new updateCreater.objectUpdate(); 

        //Public Veriables
        public System.Windows.Forms.Panel mainDisplay;
        public System.Windows.Forms.Panel panelManifoldPanel;
        public System.Windows.Forms.Panel[] panelRack;
        public List<rackDisplay> rackDis = new List<rackDisplay>();
        public manifoldDisplay portManifold;

        public List<System.Drawing.Color> groupColors = new List<System.Drawing.Color>();

        //Private Veriables
        private System.Windows.Forms.BorderStyle myBoarder = System.Windows.Forms.BorderStyle.FixedSingle;
        private int manifoldDisplaySize = 20;
        System.Drawing.Color control = System.Drawing.Color.FromArgb(255, 240, 240, 240);

        private List<Calibration.runnerGroup> calGroups;


        public pCalDisplay() { }

        public pCalDisplay(System.Windows.Forms.Panel incomingPanel, List<rackCalibration.rackPressure>racks)
        {
            mainDisplay = incomingPanel;

            //Setting up and displaying racks.
            setupRacksDisplay(racks);
            //drawRacks();
        }

        public void reloadCalDisplay(System.Windows.Forms.Panel incomingPanel, List<rackCalibration.rackPressure> racks, RelayController.relayControllerSystem[] allThePressureManifolds)
        {
            mainDisplay = incomingPanel;

            //Setting up manifold display
            setupManifoldDisplay(allThePressureManifolds);

            //Setting up and displaying racks.
            setupRacksDisplay(racks);
            drawRacks();
        }

        private void setupManifoldDisplay(RelayController.relayControllerSystem[] allThePressureManifolds)
        {
            portManifold = new manifoldDisplay();
            portManifold.setupManifoldPanel(manifoldDisplaySize, mainDisplay.Size);

            //Sorting and building the list
            for (int pr = 0; pr < allThePressureManifolds.Length; pr++)
            {
                List<string> portNames = new List<string>(0);
                string[] relayFunction = allThePressureManifolds[pr].RelayFunction();

                for (int fu = 0; fu < relayFunction.Length; fu++)
                {
                    if (relayFunction != null)
                    {
                        portNames.Add(relayFunction[fu]);
                    }
                }

                portManifold.addManifoldBlock(portNames, allThePressureManifolds);

                //Setting up events for ports changing 
                foreach (RelayController.relayControllerSystem re in allThePressureManifolds)
                {
                    re.commandEvent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(commandEvent_PropertyChange);
                }

            }

            addToMain.UpdatingObject = (object)portManifold.manPanel;

        }


        void commandEvent_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            object[] incomingData = (object[])data.NewValue;
            
            //Going through the runners looking for matching ports on the runners.
            foreach (rackDisplay ra in rackDis)
            {
                foreach (runnerDisplay run in ra.runners)
                {
                    if (run.runnerData.manifoldPos == (string)incomingData[0])
                    {
                        
                        if ((bool)incomingData[1])
                        {
                            run.panelGroup.BackColor = System.Drawing.Color.Green;
                        }
                        else
                        {
                            if (calGroups != null)
                            {
                                setRunnerColor(run.runnerData);
                            }
                        }
                         
                    }
                }
            }
            

        }

        private void setupRacksDisplay(List<rackCalibration.rackPressure> incomingRacks)
        {
            panelRack = new System.Windows.Forms.Panel[incomingRacks.Count];

            int rackWidth = mainDisplay.Size.Width / 2;
            int rackHight = (mainDisplay.Size.Height - manifoldDisplaySize) / 2;


            int currentWidthPos = rackWidth;
            int currentHighPos = rackHight;

            //Building the rack display.
            for (int ra = 0; ra < panelRack.Length; ra++)
            {
                rackDisplay tempRackDis = new rackDisplay();
                tempRackDis.setupRackDisplay(incomingRacks[ra].rackId,rackWidth, rackHight);
                tempRackDis.rackPanel.Location = new System.Drawing.Point(currentWidthPos, currentHighPos);
                

                int colCount = 1;
                int rowCount = 1;

                int runnerHeightSpacing = (tempRackDis.rackPanel.Size.Height - (80 * 2)) /3;
                int runnerWidthSpacing = (tempRackDis.rackPanel.Size.Width - (70 * 3)) / 4;

                //Adding Runners.
                for(int ru = 0; ru<incomingRacks[ra].runners.Count; ru++)
                {
                    tempRackDis.addRunnerDisplay(incomingRacks[ra].runners[ru]);
                    //tempRackDis.runners[ru].runnerPanel.Location = new System.Drawing.Point((80 * (ru - secondCount)) + (10 * (ru - secondCount)) + 10, 10 + ((tempRackDis.runners[ru].runnerPanel.Height + 5) * rowOffset));
                    tempRackDis.runners[ru].runnerPanel.Location = new System.Drawing.Point(tempRackDis.rackPanel.Size.Width - ((runnerWidthSpacing * colCount) + tempRackDis.runners[0].runnerPanel.Size.Width * colCount),
                                                                                                tempRackDis.rackPanel.Size.Height - ((runnerHeightSpacing * rowCount) + tempRackDis.runners[0].runnerPanel.Size.Height * rowCount));

                    rowCount++;

                    //Resetting to the next row.
                    if (rowCount == 3)
                    {
                        rowCount = 1;
                        colCount++;
                    }

                }

                System.Windows.Forms.Label testLabel = new System.Windows.Forms.Label();
                testLabel.Name = incomingRacks[ra].rackId;
                testLabel.Text = incomingRacks[ra].rackId;
                testLabel.Location = new System.Drawing.Point(0, 0);
                tempRackDis.rackPanel.Controls.Add(testLabel);

                //Moving starting position
                currentHighPos -= rackHight;
                if (ra % 2 != 0)
                {
                    
                    currentWidthPos -= rackWidth;
                    if (currentHighPos <= 0)
                    {
                        currentHighPos = rackHight;
                    }

                }

                panelRack[ra] = tempRackDis.rackPanel;
                rackDis.Add(tempRackDis);

            }

        }

        private void drawRacks()
        {
            foreach (System.Windows.Forms.Panel rack in panelRack)
            {
                //mainDisplay.Controls.Add(rack);
                addToMain.UpdatingObject = (object)rack;
            }
        }

        public void portDisplayControl(string portName, bool state)
        {
            for (int p = 0; p < portManifold.blocks.Count; p++)
            {
                for (int j = 0; j < portManifold.blocks[p].ports.Count; j++)
                {
                    if (portManifold.blocks[p].ports[j].panelPort.Name == portName)
                    {
                        if (state)
                        {
                            portManifold.blocks[p].ports[j].panelPort.BackColor = System.Drawing.Color.Green;
                        }
                        else
                        {
                            portManifold.blocks[p].ports[j].panelPort.BackColor = control;
                        }
                    }
                }
            }
        }

        public void updateGroup(List<Calibration.runnerGroup> incomingGroups)
        {
            calGroups = incomingGroups;

            //Setting up colors
            groupColors.Add(System.Drawing.Color.Aquamarine);
            groupColors.Add(System.Drawing.Color.DarkSeaGreen);
            groupColors.Add(System.Drawing.Color.Tan);
            groupColors.Add(System.Drawing.Color.Orange);
            groupColors.Add(System.Drawing.Color.Plum);
            groupColors.Add(System.Drawing.Color.YellowGreen);
            groupColors.Add(System.Drawing.Color.PowderBlue);
            groupColors.Add(System.Drawing.Color.Silver);

            updateGroupColors();
        }

        public void updateGroupColors()
        {
            for(int group = 0; group< calGroups.Count; group++)
            {
                foreach (runnerPressureAssembly.runnerPressure run in calGroups[group].runners)
                {
                    foreach (rackDisplay ra in rackDis)
                    {
                        foreach(runnerDisplay rundis in ra.runners)
                        {
                            //When a runner ID in the group and the runner ID in the display match Change the color for the display to match the runner color.
                            //Also going to change the port to match the runner color.
                            if (run.runnerID == rundis.runnerPanel.Name)
                            {
                                //Changeing the runner color to match.
                                rundis.panelGroup.BackColor = groupColors[group];

                                //Go through all the manifold ports and look for matching runners to manifold port names.
                                foreach (manifoldBlock theBlock in portManifold.blocks)
                                {
                                    foreach (manifoldPort thePort in theBlock.ports)
                                    {
                                        if (thePort.panelPort.Name == run.manifoldPos)
                                        {
                                            thePort.panelPort.BackColor = groupColors[group];
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        private void setRunnerColor(runnerPressureAssembly.runnerPressure incomingRunner)
        {
            for (int group = 0; group < calGroups.Count; group++)
            {
                foreach (runnerPressureAssembly.runnerPressure run in calGroups[group].runners)
                {
                    if (run.runnerID == incomingRunner.runnerID)
                    {
                        foreach (rackDisplay ra in rackDis)
                        {
                            foreach (runnerDisplay rundis in ra.runners)
                            {
                                //When a runner ID in the group and the runner ID in the display match Change the color for the display to match the runner color.
                                //Also going to change the port to match the runner color.
                                if (rundis.runnerPanel.Name == incomingRunner.runnerID)
                                {
                                    //Changeing the runner color to match.
                                    rundis.panelGroup.BackColor = groupColors[group];

                                    //Go through all the manifold ports and look for matching runners to manifold port names.
                                    foreach (manifoldBlock theBlock in portManifold.blocks)
                                    {
                                        foreach (manifoldPort thePort in theBlock.ports)
                                        {
                                            if (thePort.panelPort.Name == run.manifoldPos)
                                            {
                                                thePort.panelPort.BackColor = groupColors[group];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class rackDisplay
    {
        public System.Windows.Forms.Panel rackPanel = new System.Windows.Forms.Panel();
        public List<runnerDisplay> runners = new List<runnerDisplay>();

        public rackDisplay() { }

        public System.Windows.Forms.Panel setupRackDisplay(string name, int width, int height)
        {
            rackPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            rackPanel.Name = name;
            rackPanel.Size = new System.Drawing.Size(width, height);


            rackPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(rackPanel_MouseClick);

            return rackPanel;
        }

        void rackPanel_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                System.Windows.Forms.ContextMenu popUp = new System.Windows.Forms.ContextMenu();

                System.Windows.Forms.MenuItem removeRunner = new System.Windows.Forms.MenuItem();
                removeRunner.Name = "Remove Runner";
                removeRunner.Text = "Remove Runner";

                foreach (runnerDisplay run in runners)
                {
                    System.Windows.Forms.MenuItem menuItem = new System.Windows.Forms.MenuItem();
                    menuItem.Name = run.runnerPanel.Name;
                    menuItem.Text = run.runnerPanel.Name;
                    menuItem.Click +=new EventHandler(menuItemRemove_Select);
                    removeRunner.MenuItems.Add(menuItem);
                }

                popUp.MenuItems.Add(removeRunner);
                System.Drawing.Point pos = new System.Drawing.Point(0, 0);
                popUp.Show(this.rackPanel, pos);

            }
        }

        void menuItemRemove_Select(object sender, System.EventArgs e)
        {
            System.Windows.Forms.MenuItem incomingMenuItem = sender as System.Windows.Forms.MenuItem;
            System.Windows.Forms.MessageBox.Show(incomingMenuItem.Name);
        }

        public void addRunnerDisplay(runnerPressureAssembly.runnerPressure incomingRunner)
        {
            runnerDisplay tempRunner = new runnerDisplay();
            tempRunner.setupRunnerDisplay(incomingRunner);
            runners.Add(tempRunner);
            rackPanel.Controls.Add(tempRunner.runnerPanel);

        }




       
    }

    public class runnerDisplay
    {

        public System.Windows.Forms.Panel runnerPanel = new System.Windows.Forms.Panel();
        public List<radiosondeDisplay> radiosondes = new List<radiosondeDisplay>();
        public System.Windows.Forms.Panel panelGroup = new System.Windows.Forms.Panel();
        public runnerPressureAssembly.runnerPressure runnerData;

        public runnerDisplay() {}

        

        public System.Windows.Forms.Panel setupRunnerDisplay(runnerPressureAssembly.runnerPressure incomingRunner)
        {
            runnerData = incomingRunner;
            runnerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            runnerPanel.Name = incomingRunner.runnerID;
            runnerPanel.Size = new System.Drawing.Size(70, 80);

            //Setting up the runner Group box.
            panelGroup.Size = new System.Drawing.Size(10, runnerPanel.Size.Height);
            panelGroup.Location = new System.Drawing.Point(runnerPanel.Size.Width - 10, 0);
            runnerPanel.Controls.Add(panelGroup);

            //Mouse control to control remove..... Not sure if this is the right way to go about this.
            panelGroup.MouseClick += new System.Windows.Forms.MouseEventHandler(panelGroup_MouseClick);

            //Setting up the radiosodnes on the runner.
            int nextRow = 0;
            int resetDown = 0;
            for (int radio = 0; radio < 8; radio++)
            {
                radiosondeDisplay tempSonde = new radiosondeDisplay();
                tempSonde.setupRadiosondeDisplay(radio, incomingRunner.runnerRadiosondePorts[radio]);
                tempSonde.radiosondePanel.Location = new System.Drawing.Point(0 + nextRow, 0 + ((20 * radio) - resetDown));
                runnerPanel.Controls.Add(tempSonde.radiosondePanel);
                radiosondes.Add(tempSonde);

                if (radio == 3)
                {
                    nextRow = nextRow + 30;
                    resetDown = runnerPanel.Height;
                }
            }

            return runnerPanel;
        }

        void panelGroup_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                System.Windows.Forms.ContextMenu popUp = new System.Windows.Forms.ContextMenu();

                System.Windows.Forms.MenuItem removeRunner = new System.Windows.Forms.MenuItem();
                removeRunner.Name = "removeRunner," + this.runnerPanel.Name;
                removeRunner.Text = "Remove Runner";
                removeRunner.Click += new EventHandler(testEvent);

                popUp.MenuItems.Add(removeRunner);

                System.Drawing.Point pos = new System.Drawing.Point(0, 0);
                popUp.Show(this.panelGroup, pos);
            }
        }

        void testEvent(object sender, System.EventArgs e)
        {
            System.Windows.Forms.MenuItem incoming = sender as System.Windows.Forms.MenuItem;
            
            System.Windows.Forms.MessageBox.Show(incoming.Name);
        }

    }

    public class radiosondeDisplay
    {
        public System.Windows.Forms.Panel radiosondePanel = new System.Windows.Forms.Panel();
        public ipRadiosonde.ipUniRadiosonde sondeData;
        public string radiosondeRunnerID = "";

        private System.Windows.Forms.Panel radiosondeMon = new System.Windows.Forms.Panel();

        public radiosondeDisplay(){}

        public System.Windows.Forms.Panel setupRadiosondeDisplay(int posCount, ipRadiosonde.ipUniRadiosonde sonde)
        {
            sondeData = sonde;
            radiosondeRunnerID = (1 + posCount).ToString();
            if (sonde != null)
            {
                sondeData.CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
                sondeData.bootingRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(bootingRadiosonde_PropertyChange);
                sondeData.readyRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
                sondeData.disconnectRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(disconnectRadiosonde_PropertyChange);
                radiosondePanel.Name = sonde.port.ToString();
            }

            radiosondePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            radiosondePanel.Size = new System.Drawing.Size(30, 20);

            //Adding id label
            System.Windows.Forms.Label idLabel = new System.Windows.Forms.Label();
            idLabel.Text = radiosondeRunnerID;
            idLabel.Location = new System.Drawing.Point(0, 0);
            idLabel.Size = new System.Drawing.Size(10, 30);

            //This is a test for the radiosonde.
            radiosondePanel.MouseClick += new System.Windows.Forms.MouseEventHandler(radiosondePanel_MouseClick);

            //Adding the radiosonde Indicator.
            radiosondeMon.Size = new System.Drawing.Size(10, 10);
            radiosondeMon.Location = new System.Drawing.Point(radiosondePanel.Size.Width-10, 0);
            radiosondeMon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            //Adding items to the radiosonde display.
            radiosondePanel.Controls.Add(idLabel);
            radiosondePanel.Controls.Add(radiosondeMon);

            

            return radiosondePanel;
        }

        void radiosondePanel_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show("test");
        }


        void disconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondeMon.BackColor = System.Drawing.Color.Red;
        }

        void readyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondeMon.BackColor = System.Drawing.Color.LightGreen;
        }

        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondeMon.BackColor = System.Drawing.Color.Green;
            System.Threading.Thread.Sleep(400);
            radiosondeMon.BackColor = System.Drawing.Color.DarkGray;
        }

        void bootingRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondeMon.BackColor = System.Drawing.Color.Yellow;
        }
    }

    public class manifoldDisplay
    {
        public System.Windows.Forms.Panel manPanel = new System.Windows.Forms.Panel();
        public List<manifoldBlock> blocks = new List<manifoldBlock>();
        public RelayController.relayControllerSystem[] relayManifold;

        System.Drawing.Color control = System.Drawing.Color.FromArgb(255, 240, 240, 240);

        public manifoldDisplay() { }

        public System.Windows.Forms.Panel setupManifoldPanel(int manifoldSize, System.Drawing.Size mainDisplay)
        {
            manPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            manPanel.Location = new System.Drawing.Point(0, mainDisplay.Height - manifoldSize);
            manPanel.Name = "panelManifold";
            manPanel.Size = new System.Drawing.Size(mainDisplay.Width, manifoldSize);

            return manPanel;
        }

        public void addManifoldBlock(List<string> portNames, RelayController.relayControllerSystem[] allThePressureManifolds)
        {
            //Setting and updating the relayManifolds
            relayManifold = allThePressureManifolds;
            
            //setting up event trigger.
            
            for (int all = 0; all < relayManifold.Length; all++)
            {
                relayManifold[all].commandEvent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(commandEvent_PropertyChange);
            }
            

            //Format any exsisting block to acomidate the incoming manifold.
            int heighToBe = manPanel.Size.Height;
            int widthToBe = manPanel.Size.Width / (blocks.Count + 1);
            System.Drawing.Size blockSizes = new System.Drawing.Size(widthToBe, heighToBe);

            //Adding the block
            manifoldBlock tempBlock = new manifoldBlock();
            tempBlock.setupManifoldBlock(blockSizes, blocks.Count.ToString(), portNames);
            manPanel.Controls.Add(tempBlock.panelBlock); // Have a problem in windows XP with this line.

            blocks.Add(tempBlock);

            //Setting up event code for manual control of the manifold port.
            foreach (manifoldBlock blk in blocks)
            {
                foreach (manifoldPort prt in blk.ports)
                {
                    prt.labelPort.MouseClick += new System.Windows.Forms.MouseEventHandler(labelPort_MouseClick);
                }
            }

            //resizing and reposition all current manifold blocks from left to right.
            if (blocks.Count > 0)
            {
                for (int x = 0; x < blocks.Count; x++)
                {
                    blocks[x].updateBlock(blockSizes, new System.Drawing.Point(blockSizes.Width * x, 0));
                }
            }
             
        }

        void labelPort_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            System.Windows.Forms.Label theSender = (System.Windows.Forms.Label)sender;

            //Checjing for right click to see if we want to open the port.
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                bool currentPortState = false;

                //Getting current state of the port.
                foreach (RelayController.relayControllerSystem re in relayManifold)
                {
                    string[] relayFunctions = re.RelayFunction();
                    foreach (string fun in relayFunctions)
                    {
                        if (theSender.Text == fun)
                        {
                            currentPortState = re.getRelayState(re.getItemsIndex(fun));

                            if (System.Windows.Forms.MessageBox.Show(theSender.Text + " is currently " + currentPortState.ToString() + ". Do you want to change that?", "Manual Control", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                            {
                                bool desiredState = false;
                                if(currentPortState)
                                {
                                    desiredState = false;
                                }
                                else
                                {
                                    desiredState = true;
                                }

                                re.commandRelayBoard(theSender.Text, desiredState);
                            }
                            else
                            {
                                return;
                            }

                        }
                    }
                }
                


            }
        }


        void commandEvent_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            object[] incomingData = (object[])data.NewValue;

            //Controling the pressure manifold display
            for (int j = 0; j < blocks.Count; j++)
            {
                for (int um = 0; um < blocks[j].ports.Count; um++)
                {
                    if (blocks[j].ports[um].panelPort.Name == (string)incomingData[0])
                    {
                        if ((bool)incomingData[1])
                        {
                            blocks[j].ports[um].panelPort.BackColor = System.Drawing.Color.Green;
                        }
                        else
                        {
                            blocks[j].ports[um].panelPort.BackColor = control;
                        }
                    }
                }
            }
        }
    }

    public class manifoldBlock
    {
        public List<manifoldPort> ports = new List<manifoldPort>();
        public System.Windows.Forms.Panel panelBlock = new System.Windows.Forms.Panel();

        private int portCount = 0;

        public manifoldBlock() { }

        public System.Windows.Forms.Panel setupManifoldBlock(System.Drawing.Size blockSize, string blockID, List<string> portNames)
        {
            panelBlock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelBlock.Size = blockSize;
            panelBlock.Name = blockID;

            this.portCount = 1;
            for (int lp = 0; lp < portNames.Count; lp++)
            {
                if (portNames[lp] != null)
                {
                    this.portCount++;
                }
            }

            //Determnining the port sizes.
            System.Drawing.Size portSize = new System.Drawing.Size(panelBlock.Size.Width / this.portCount, panelBlock.Size.Height);


            //Adding ports.
            for (int p = 0; p < this.portCount+1; p++)
            {
                if (portNames[p] != null)
                {

                    manifoldPort tempPort = new manifoldPort();
                    tempPort.setupPort(portSize, portNames[p], false);
                    tempPort.panelPort.Location = new System.Drawing.Point(portSize.Width * p, 0);
                    ports.Add(tempPort);
                    panelBlock.Controls.Add(tempPort.panelPort);
                }

            }


            return panelBlock;
        }




        public void updateBlock(System.Drawing.Size newSize, System.Drawing.Point newLocation)
        {
            //Adjust the panel
            panelBlock.Size = newSize;
            panelBlock.Location = newLocation;

            //Determnining the port sizes.
            System.Drawing.Size portSize = new System.Drawing.Size(panelBlock.Size.Width / this.portCount, panelBlock.Size.Height);

            //Adjust subPort items.
            for(int p = 0; p<ports.Count; p++)
            {
                ports[p].panelPort.Size = portSize;
                ports[p].panelPort.Location = new System.Drawing.Point(portSize.Width * p, 0);
            }
        }

    }

    public class manifoldPort
    {
        public bool status;

        public System.Windows.Forms.Panel panelPort;
        public System.Windows.Forms.Label labelPort;

        public manifoldPort() { }

        public System.Windows.Forms.Panel setupPort(System.Drawing.Size portSize, string incomingID, bool incomingStatus)
        {
            panelPort = new System.Windows.Forms.Panel();
            panelPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelPort.Size = portSize;
            panelPort.Name = incomingID;
            status = incomingStatus;

            //Some fun test code.
            labelPort = new System.Windows.Forms.Label();
            labelPort.Text = panelPort.Name;
            labelPort.Location = new System.Drawing.Point(0, 0);
            panelPort.Controls.Add(labelPort);

            return panelPort;
        }





        public void updatePortDisplay()
        {

        }
    }

}


