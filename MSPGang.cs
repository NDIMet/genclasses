﻿///Command class for MSP-GANG Programmer Hardware Rev 1.01
///Hardware from Elprotronic
///By: William "The crazy minded" Jones
///
///     The way this class is designed it to have an internal background worker to handle the internal data out to the programmer
///and then collect the processing back with enough of a delay to not miss the data.
///     Another background worker should be used to command and wait for the responce of the programmer in the pgmStatusUpdate
///and the pgmProcessComplete.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Programmer
{
    public class MSPGang
    {
        
        public MSPGang(){}


        System.IO.Ports.SerialPort ComPort;     //Com port for programmer
        public updateCreater.objectUpdate pgmError = new updateCreater.objectUpdate();    //Event thrown when an error has accured.
        public updateCreater.objectUpdate pgmProcessComplete = new updateCreater.objectUpdate();  //Command processed.
        public updateCreater.objectUpdate pgmMainComplete = new updateCreater.objectUpdate();       //Programing complete.
        public updateCreater.objectUpdate pgmStatusUpdate = new updateCreater.objectUpdate();     //Event thrown when new status is avaiable.

        System.ComponentModel.BackgroundWorker bwProcessor;     //Responceable to procssing commands out and receiveing the return message.
        System.Timers.Timer processCheck;       //Responceable for making sure commands are processed out if any are in que.

        List<command> cmd = new List<command>();    //List of commands to be processed.
        List<command> cmdLog = new List<command>();     //Log of commands processed.

        /// <summary>
        /// Sets up the serial port and connects to the MSP-430 Gang programmer.
        /// </summary>
        /// <param name="port"></param>
        public MSPGang(string port)
        {
            ComPort = new System.IO.Ports.SerialPort(port);     //Setting up com object
            ComPort.BaudRate = 115200;             //Setting speed
            ComPort.Parity = System.IO.Ports.Parity.Even;   //com parity
            ComPort.StopBits = System.IO.Ports.StopBits.One;    //com start and stop bit.
            ComPort.DataBits = 8;       //Data bit count
            ComPort.Handshake = System.IO.Ports.Handshake.RequestToSend;    //Hardware RTS/CTS is required.

            try
            {
                ComPort.Open();     //Opening com port.
            }
            catch (Exception e)
            {
                pgmError.UpdatingObject = e;
                return;
            }

            //Subscribing to the data processing for error checking.
            pgmProcessComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(pgmProcessComplete_PropertyChange);

            //Setting up the process command background worker.
            bwProcessor = new System.ComponentModel.BackgroundWorker();
            bwProcessor.WorkerSupportsCancellation = true;
            bwProcessor.DoWork += new System.ComponentModel.DoWorkEventHandler(bwProcessor_DoWork);

            //Starting the timer.
            processCheck = new System.Timers.Timer(100);    //Setting to 100ms to provent delays.
            processCheck.Elapsed += new System.Timers.ElapsedEventHandler(processCheck_Elapsed);
            processCheck.Enabled = true;
        }


        #region Methods relateing to processing data in and out of the system.

        void processCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!bwProcessor.IsBusy && cmd.Count > 0)
            {
                bwProcessor.RunWorkerAsync();   //Starting background worker.
            }
        }

        void bwProcessor_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (syncProgrammer()) //Syncing the programmer. Also checking for programmer.
            {
                while (cmd.Count > 0)
                {
                    if (cmd.Count > 0)
                    {
                        command tempOut = cmd.First();  //Pulling the first command in to be processed.
                        sendCommand(tempOut.data);      //Sending out command.
                        System.Threading.Thread.Sleep(100);    //Taking a small break to all the command to be process.
                        tempOut.responce = getComData();    //Getting a responce from the command.
                        cmd.Remove(cmd.First());    //Removing the processed command from the list

                        pgmProcessComplete.UpdatingObject = tempOut;

                        addToLog(tempOut);        //Adding the processed command to the log.
                    }

                    if (cmd.Count > 0)
                    {
                        System.Threading.Thread.Sleep(100); //If there is more then one command to process take a small break.
                    }
                }
            }
            else
            {
                Exception newError = new Exception("Unable to sync with programmer");
                pgmError.UpdatingObject = newError;
            }
        }

        /// <summary>
        /// Addeding commands to the log in a controlled way.
        /// </summary>
        /// <param name="newCmd"></param>
        private void addToLog(command newCmd)
        {
            cmdLog.Add(newCmd);     //Adding the current command to the log.

            //Editing the log to make sure there is no overrun
            if (cmdLog.Count > 600)
            {
                cmdLog.Remove(cmdLog.First());
            }
        }

        #region Methods for handeling coms
        /// <summary>
        /// Method for calcuating the checksum for communication with the programmer.
        /// </summary>
        /// <param name="incomingData"></param>
        /// <returns></returns>
        public byte[] calcCheckSum(byte[] incomingData)
        {
            var CKL = 0x00;
            var CKH = 0x00;

            //Calculating the Checksum Low
            for (int x = 0; x < incomingData.Length; x = x + 2)
            {
                CKL = CKL ^ incomingData[x];
            }

            //Calculating the Checksum High
            for (int y = 1; y < incomingData.Length; y = y + 2)
            {
                CKH = CKH ^ incomingData[y];
            }

            //Inverting the XOR
            var iCKL = 255 - CKL;
            var iCKH = 255 - CKH;

            //Building the returnable checksum.
            byte[] checksum = { (byte)iCKL, (byte)iCKH };
            return checksum;
        }

        public void sendCommand(byte[] data)
        {
            byte[] totalMessage = new byte[data.Length + 2];    //total message to be sent.

            //Getting the checkSum
            byte[] checkSum = calcCheckSum(data);

            //Adding data into the total message.
            for (int x = 0; x < data.Length; x++)
            {
                totalMessage[x] = data[x];
            }

            totalMessage[totalMessage.Length - 2] = checkSum[0];
            totalMessage[totalMessage.Length - 1] = checkSum[1];

            ComPort.Write(totalMessage, 0, totalMessage.Length);
        }

        private byte[] getComData()
        {
            byte[] data = new byte[ComPort.BytesToRead];

            for (int x = 0; x < data.Length; x++)
            {
                data[x] = (byte)ComPort.ReadByte();
            }

            return data;
        }

        private void addCommandToQue(byte[] message)
        {
            command curCommand = new command();     //Creating the temp command
            curCommand.Date = DateTime.Now;     //Logging the current time.
            curCommand.data = message;          //Adding the intended message to the command
            cmd.Add(curCommand);        //Putting the command into the que.
        }

        #endregion

        #endregion

        #region Methods for constructing the commands to send to the programmer.

        /// <summary>
        /// Sync's the programmer. Also works for detecting programmer.
        /// </summary>
        /// <returns></returns>
        public bool syncProgrammer()
        {
            ComPort.ReadExisting();
            byte[] data = {0x0D};
            ComPort.Write(data, 0, 1);
            int responce = ComPort.ReadByte();
            byte[] responceByte = { (byte)responce };
            //Adding some tracking information
            command tempCommand = new command();
            tempCommand.Date = DateTime.Now;
            tempCommand.data = data;
            tempCommand.responce = responceByte;

            addToLog(tempCommand);

            if (responce == 144)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void selectImage(int image)
        {
            byte[] message = { 0x3E, 0x50, 0x04, 0x04, (byte)image, 0x00, 0x00, 0x00 };     //Building my message
            addCommandToQue(message);
        }

        public void startMainProcess()
        {
            byte[] message = { 0x3E, 0x31, 0x04, 0x04, 0x00, 0x00, 0x00, 0x00 };
            addCommandToQue(message);
        }

        public void getStatus()
        {
            byte[] message = { 0xA5 };
            addCommandToQue(message);
        }

        #endregion

        #region Internal data processing

        void pgmProcessComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            command curData = (command)data.NewValue;

            if (curData.responce.Length == 50 && curData.responce[0] == (byte)0x80 && curData.responce[1] == (byte)0xA5) //Processing Status message. See SLAU358 MSP-GANG UG.PDF section 3.5.2.10 for details
            {
                pgmStatusUpdate.UpdatingObject = curData;   //Status update.

                if (curData.responce[24] != 0)  //Looking for programmer errors
                {
                    Exception newError = new Exception("Programmering Error:24");
                    pgmError.UpdatingObject = newError;
                }

                if (curData.responce[33] == 100)    //Looking for the programming process to conclude.
                {
                    pgmMainComplete.UpdatingObject = curData;    //Programming complete.
                }
            }

        }

        #endregion
    }

    /// <summary>
    /// Object that repersents the data flow to and from the divice.
    /// </summary>
    public class command
    {
        public DateTime Date;
        public byte[] data;
        public byte[] responce;
    }
}
