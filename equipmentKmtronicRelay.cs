﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KMtronicRelay
{
    class equipmentKmtronicRelay
    {
        //Comport to communicate with the hardware.
        System.IO.Ports.SerialPort relayComPort;

        //Relays in the system
        List<relay> curRelays = new List<relay>();

        //Events
        updateCreater.objectUpdate error = new updateCreater.objectUpdate();

        public equipmentKmtronicRelay(){}

        public equipmentKmtronicRelay(string comPort, int numRelays)
        {
            relayComPort = new System.IO.Ports.SerialPort(comPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            relayComPort.Handshake = System.IO.Ports.Handshake.None;

            for (int i = 0; i < numRelays; i++)
            {
                relay tempRelay = new relay();
                tempRelay.state = false;
                tempRelay.id = i + 1;
                curRelays.Add(tempRelay);
            }

            string[] allPorts = System.IO.Ports.SerialPort.GetPortNames();

            if (allPorts.Contains(comPort))
            {
                try
                {
                    relayComPort.Open();
                }
                catch (Exception comError)
                {
                    error.objectID = comPort;
                    error.UpdatingObject = comError;
                    return;
                }

                //Opening all relays.
                for (int c = 0; c < curRelays.Count; c++)
                {
                    setRelayState(curRelays[c].id, false);
                }


            }
            else
            {
                error.objectID = comPort;
                error.UpdatingObject = new Exception("Com port not found.");
            }

        }

        public void setRelayState(int relayID, bool state)
        {
            relayComPort.Write(new byte[] { 0xFF, (byte)relayID, Convert.ToByte(state) }, 0, 3);
        }

        
    }

    class relay
    {
        public bool state;
        public int id;
        public string name;
    }
}
