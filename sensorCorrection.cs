﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sensorCorrection
{
    #region HMP234Blk
    class sensorCorHMP234Blk
    {
        public sensorCorHMP234Blk()
        {
            double[] cerCoef = new double[3] { 1.7828, -0.0732, 0.0005 };   //Generic coef to get started with.
            correct = new sensorCorrection(cerCoef);
        }

        private sensorCorrection correct;

        public double[] getCoefficient()
        {
            return correct.coef;
        }

        public void setCoefficient(double[] desiredCoefficient)
        {
            correct.coef = desiredCoefficient;
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            return correct.getCorrectedHumidity(rawHumidity);
        }

    }
    #endregion

    #region HMP234SS
    class sensorCorHMP234SS
    {
        //This sensor was destroyed.... 
        public sensorCorHMP234SS()
        {
            coefficient[0] = 1.416;
            coefficient[1] = 1.0233;
            coefficient[2] = 0.0005;
        }

        //class veriables
        //Coefficient for the 2nd degree correction fix
        //y = 0.0005x2 + 1.0233x + 1.416
        private double[] coefficient = new double[3];

 

        public double[] getCoefficient()
        {
            return coefficient;
        }

        public void setCoefficient(double[] desiredCoefficient)
        {
            coefficient = desiredCoefficient;
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            //the calc output of the correction.
            double correctedHumidity = 0;

            correctedHumidity = coefficient[2] * Math.Pow(rawHumidity, 2) + coefficient[1] * rawHumidity + coefficient[0];

            return correctedHumidity;
        }
    }
    #endregion

    #region EE31
    class sensorCorEE31
    {
        public sensorCorEE31()
        {
            double[] cerCoef = new double[5] { 1.0115, -0.4008, 0.0154, -0.0002, 0.0000009 };   //Generic coef to get started with.
            correct = new sensorCorrection(cerCoef);
        }

        //class veriables

        private sensorCorrection correct;

        public double[] getCoefficient()
        {
            return correct.coef;
        }

        public void setCoefficient(double[] desiredCoefficient)
        {
            correct.coef = desiredCoefficient;
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            return correct.getCorrectedHumidity(rawHumidity);
        }
    }
    #endregion

    public class sensorCorrection
    {
        public double[] coef = new double[5]{0,0,0,0,0};   //Setting up the coef container.

        public sensorCorrection(double[] sensorCoef)
        {
            for (int i = 0; i < sensorCoef.Length; i++)
            {
                coef[i] = sensorCoef[i];
            }
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            //the calc output of the correction.
            double correctedHumidity = 0;

            for (int i = 0; i < coef.Length; i++)
            {
                correctedHumidity += (coef[i] * Math.Pow(rawHumidity, i));
            }

            return correctedHumidity;
        }

    }
}
