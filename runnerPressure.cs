﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace runnerPressureAssembly
{
    [Serializable]
    public class runnerPressure
    {
        //Public Veriables
        public ipRadiosonde.ipUniRadiosonde[] runnerRadiosondePorts = new ipRadiosonde.ipUniRadiosonde[8];

        public List<int> currentRadiosondes = new List<int>();

        //Event when the count of radiosondes change.
        public updateCreater.objectUpdate radiosondePresent = new updateCreater.objectUpdate();

        #region Public Veriables relateding to the Runner, ID, Current Pressure things like that.
        /// <summary>
        /// This is the current stored Pressure. This could change as the leak may change it.
        /// </summary>
        public double currentRunnerPressure = 9999.99; //May not not be useful.

        /// <summary>
        /// This is the current stored pressure average.
        /// </summary>
        public double currentPressureStabilityAvg = 9999.99;

        /// <summary>
        /// Does the runner have a leak or not.
        /// </summary>
        public bool leakStatus = false;


        /// <summary>
        /// Method get or sets the current manifold the runner is connected to.
        /// </summary>
        public string manifold = "XXXX";


        /// <summary>
        /// Method get/sets manifold position it is connected to. 
        /// </summary>
        public string manifoldPos = "XXXX";

        public string runnerID = "";


        #endregion

        //Private Veriables


        /// <summary>
        /// Starting up the runner.
        /// </summary>
        public runnerPressure()
        {
            runnerID = "XXXX";
        }

        /// <summary>
        /// Setup connection to the radiosonde.
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="ports"></param>e
        public void connectRunnerToSystem(string[] ipAddress, int[] ports)
        {
            //Starting up the radiosondes.
            for (int x = 0; x < ipAddress.Length; x++)
            {
                runnerRadiosondePorts[x] = new ipRadiosonde.ipUniRadiosonde();
                runnerRadiosondePorts[x].open(ipAddress[x], ports[x]); //Setting up the connection to the radiosonde
                runnerRadiosondePorts[x].rawDataRec.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(rawDataRec_PropertyChange);
                runnerRadiosondePorts[x].CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
                //runnerRadiosondePorts[x].bootingRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(bootingRadiosonde_PropertyChange);
                runnerRadiosondePorts[x].readyRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
                runnerRadiosondePorts[x].disconnectRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(disconnectRadiosonde_PropertyChange);
            }
        }

        void disconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.radiosondeDataPacket incomingData = (ipRadiosonde.radiosondeDataPacket)data.NewValue;
            //currentRadiosondes.Remove(incomingData.port);

            RetryRemove:

            for (int x = 0; x < currentRadiosondes.Count; x++)
            {
                //Tring to remove the radiosonde from the runner list.
                try
                {
                    if (currentRadiosondes[x] == incomingData.port)     //Finding the ports matching radiosonde.
                    {
                        currentRadiosondes.RemoveAt(x);     //Remove the radiosonde.
                    }
                }
                catch (ArgumentOutOfRangeException argError)    //Capturing index out of bounds error.
                {
                    System.Threading.Thread.Sleep(50);      //Pause before tring again.  
                    goto RetryRemove;       //Retrying removel.
                }
            }

            updateToConditions("Radiosonde Removed");
        }

        void readyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.radiosondeDataPacket incomingData = (ipRadiosonde.radiosondeDataPacket)data.NewValue;
            


            accountForSonde(incomingData.port);    

        }

        void bootingRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Might use this for some kind of error processing,,, not sure. Maybe wait a while then que some kind of error,,, like failed startup.
        }

        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.CalData incomingData = (ipRadiosonde.CalData)data.NewValue;

            //Checking to make sure the radiosonde connection is accounted for.
            foreach (ipRadiosonde.ipUniRadiosonde sonde in runnerRadiosondePorts)
            {
                if (sonde != null) //Protection during startup
                {
                    if (sonde.CalData.SerialNumber == incomingData.SerialNumber)
                    {
                        accountForSonde(sonde.port);
                    }
                }
            }

        }

        void rawDataRec_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.radiosondeDataPacket incomingData = (ipRadiosonde.radiosondeDataPacket)data.NewValue;

            //Checking for phantom radiosondes responces.
            if (incomingData.packetMessage == "sn\r\n" || incomingData.packetMessage == "vers\r\n" || incomingData.packetMessage == "txmode\r\n" ||
                incomingData.packetMessage == "rn\r\n" || incomingData.packetMessage == "pn\r\n" || incomingData.packetMessage.Contains("�"))
            {
                return;
            }
            else
            {
                string test = "tsdffs";
            }

            //Checking to make sure the radiosonde connection is accounted for.
            accountForSonde(incomingData.port);
        }

        private void accountForSonde(int checkingPort)
        {
            if (!currentRadiosondes.Contains(checkingPort))
            {
                currentRadiosondes.Add(checkingPort);
                updateToConditions("New Radiosonde Found");
            }
        }

        private void updateToConditions(string message)
        {
            dataPacketPressureRunner data = new dataPacketPressureRunner();
            data.runnerID = runnerID;
            data.message = message;
            radiosondePresent.UpdatingObject = data;
        }

    }

    [Serializable]
    public class dataPacketPressureRunner
    {
        public string runnerID;
        public string message;
    }

    

}
