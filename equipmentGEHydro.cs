﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace equipmentGE
{
    struct GEData
    {
        public dataPacket dewPointRaw;
        public double dewPoint;

        public dataPacket airTempRaw;
        public double airTemp;

        public dataPacket humidityRaw;
        public double humidity;
    }


    class GEHydro
    {
        //Public items
        public updateCreater.objectUpdate dataPacketRecived = new updateCreater.objectUpdate();    //Event that happens when any full line of data has been received.
        public updateCreater.objectUpdate sensorUpdate = new updateCreater.objectUpdate();         //Event that triggers when system updates. Temp/Humidity/Dew Point.
        public updateCreater.objectUpdate readyAirTemp = new updateCreater.objectUpdate();          //Air Temp ready
        public updateCreater.objectUpdate readyRH = new updateCreater.objectUpdate();               //Humidity Ready
        public updateCreater.objectUpdate readyDewPoint = new updateCreater.objectUpdate();         //Dew point ready


        //Private items
        TcpSimpleClient.simpleTcpClient tcpClient;  //TCP port connection.
        System.IO.Ports.SerialPort comPort;         //COM port connection.

        string tempDataBuffer;  //Buffer for dumping data into.
        List<dataPacket> recBuffer = new List<dataPacket>();    //The received data buffer.
        public System.Threading.AutoResetEvent waitForSocketData = new System.Threading.AutoResetEvent(false);  //Something to control the flow of the sensor data.
        System.Threading.Thread processRecData;

        public GEData currentStatus = new GEData();

        public GEHydro()
        {
        }

        /// <summary>
        /// Connect to sensor via TCP/IP telnet connection
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public GEHydro(string ipAddress, int port)
        {
            tcpClient = new TcpSimpleClient.simpleTcpClient();
            tcpClient.simpleClientSetupTCP(ipAddress,port);
            tcpClient.recivedMessage.PropertyChange +=new updateCreater.objectUpdate.PropertyChangeHandler(recivedMessage_PropertyChange);

            startInternalProcess(); //Starting to process incoming data.
        }

        public GEHydro(string comPort)
        {

        }

        public void close()
        {
            if (tcpClient != null)
            {
                tcpClient.recivedMessage.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(recivedMessage_PropertyChange);
                tcpClient.close();
                processRecData.Abort();
            }
        }

        private void startInternalProcess()
        {

            //Filling the stuct with starter data.
            dataPacket tempData = new dataPacket();
            tempData.packetDate = DateTime.Now;
            tempData.packetMessage = "0";

            currentStatus.airTemp = 0;
            currentStatus.airTempRaw = tempData;
            currentStatus.dewPoint = 0;
            currentStatus.dewPointRaw = tempData;
            currentStatus.humidity = 0;
            currentStatus.humidityRaw = tempData;

            if (processRecData == null)
            {
                processRecData = new System.Threading.Thread(processIncomingData);
                processRecData.Name = "GEProcessor";
                processRecData.IsBackground = true;
                processRecData.Start();
                
            }
            else
            {
                throw new Exception("Thread already started");
            }
        }

        /// <summary>
        /// Method for receiving data from the TCP/IP socket.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void recivedMessage_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            DateTime timeDataRec = new DateTime();
            timeDataRec = DateTime.Now;

            //End of message chars
            char[] EOM = new char[2] { '\r', '\n' };

            //Turing the byte data into a message
            byte[] incomingData = (byte[])data.NewValue;
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            //Removing addiional null data bytes.
            string str = enc.GetString(incomingData).Trim('\0');

            //Check for a complete data packet. If not a complete packet then place the data into the tempbuffer then wait for the next chunk.

            if (!str.Contains("\r\n"))
            {
                tempDataBuffer += str;
                return;
            }

            if (str.Contains("\r\n"))
            {
                str = tempDataBuffer + str;
                tempDataBuffer = "";

                //Checking to see if the last part of the message is the end of a line. If not add it back into the buffer for more processing.
                bool endOfLine = false;
                if (str.Substring(str.Length - 2, 2) == "\r\n")
                {
                    endOfLine = true;
                }

                //Checking to see if there is more data past the \r\n
                string[] breakingDown = str.Split(EOM);

                if (!endOfLine)
                {
                    tempDataBuffer = breakingDown[breakingDown.Length - 1];
                }


                for (int s = 0; s < breakingDown.Length; s++)
                {
                    if (breakingDown[s] != "" && s < breakingDown.Length - 2)
                    {
                        dataPacket currentData = new dataPacket();
                        currentData.packetDate = timeDataRec;
                        currentData.packetMessage = breakingDown[s] + "\r\n";
                        recBuffer.Add(currentData);     //Adding incoming packet to list to be processed.
                        System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " - geData: " + currentData.packetMessage.Trim());

                        dataPacketRecived.UpdatingObject = (object)currentData; //Sending out the update that a data packet has been received.

                    }
                }

                waitForSocketData.Set();    //letting processing happen.

            }
        }


        //Thread for Sorting incoming Data
        private void processIncomingData()
        {
            while (true)
            {
                if (recBuffer.Count > 0)    //If there is data to process goto it.
                {
                    //waitForSocketData.WaitOne();

                    dataPacket toProcess = recBuffer[0];
                    recBuffer.RemoveAt(0);

                    string[] breakDown = toProcess.packetMessage.Split('=');

                    if (toProcess.packetMessage.Contains("DP  C"))
                    {
                        currentStatus.dewPointRaw = toProcess;
                        currentStatus.dewPoint = Convert.ToDouble(breakDown[1]);
                        readyDewPoint.UpdatingObject = currentStatus;
                    }
                    if (toProcess.packetMessage.Contains("RH"))
                    {
                        currentStatus.humidityRaw = toProcess;
                        currentStatus.humidity = Convert.ToDouble(breakDown[1]);
                        readyRH.UpdatingObject = currentStatus;
                    }
                    if (toProcess.packetMessage.Contains("TMP C"))
                    {
                        currentStatus.airTempRaw = toProcess;
                        currentStatus.airTemp = Convert.ToDouble(breakDown[1]);
                        readyAirTemp.UpdatingObject = currentStatus;
                    }
                    sensorUpdate.UpdatingObject = (object)currentStatus;
                    
                }
                else
                {
                    System.Threading.Thread.Sleep(500); //If not sleep till there is.
                }

                
            }
        }

    }

    public class dataPacket
    {
        public DateTime packetDate;
        public string packetMessage;

    }
}
