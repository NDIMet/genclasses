﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iMet_1_Decoder
{
    class iMet1_Decoder
    {
        //Public Fields
        public System.IO.Ports.SerialPort COMPort;
        public updateCreater.objectUpdate decoderMessage = new updateCreater.objectUpdate();        //Decoder Error
        public updateCreater.objectUpdate packetReady = new updateCreater.objectUpdate();           //Packet ready.
        public updateCreater.objectUpdate packetBad = new updateCreater.objectUpdate();     //Bad Packet detected.
        public updateCreater.objectUpdate packetTimeout = new updateCreater.objectUpdate(); //Decoding timeout.
        public updateCreater.objectUpdate packetEmpty = new updateCreater.objectUpdate();   //Message from decoder that means there is no data.

        private DecoderData currentDecodeData = new DecoderData();

        private System.ComponentModel.BackgroundWorker bwCollectData;   //Background Worker that can collect the data.

        //Private Fields
        System.Timers.Timer timerTimeOut;    //Timer for missing data

        //Public Methods
        public iMet1_Decoder(string comPort)
        {
            this.COMPort = new System.IO.Ports.SerialPort(comPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            this.COMPort.ReadTimeout = 2000;
        }

        public bool connectToDecoder(string COMPortName)
        {
            this.COMPort = new System.IO.Ports.SerialPort(COMPortName, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            this.COMPort.ReadTimeout = 2000;

            try
            {
                this.COMPort.Open();
                this.COMPort.DiscardInBuffer();
                this.COMPort.Disposed += new EventHandler(COMPort_Disposed);


                
                if (getDecoderPacket() == null) 
                {
                    this.COMPort.Dispose();
                    return false;
                }


                timerTimeOut = new System.Timers.Timer(1200);
                timerTimeOut.Elapsed += new System.Timers.ElapsedEventHandler(timerTimeOut_Elapsed);
                //timerTimeOut.Enabled = true;

                return true;

            }
            catch(Exception ex)
            {
                this.COMPort.Dispose();
                return false;
            }
        }


        public bool connectToDecoderV2(string COMPortName)
        {
            this.COMPort = new System.IO.Ports.SerialPort(COMPortName, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            this.COMPort.ReadTimeout = 2000;

            try
            {
                this.COMPort.Open();
                this.COMPort.DiscardInBuffer();
                this.COMPort.Disposed += new EventHandler(COMPort_Disposed);


                timerTimeOut = new System.Timers.Timer(1200);
                timerTimeOut.Elapsed += new System.Timers.ElapsedEventHandler(timerTimeOut_Elapsed);

                return true;

            }
            catch (Exception ex)
            {
                this.COMPort.Dispose();
                return false;
            }
        }

        void COMPort_Disposed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }


        void timerTimeOut_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.IO.IOException timeout = new System.IO.IOException("Packet Time Out", 2000);
            packetTimeout.UpdatingObject = (object)timeout;
        }

        public DecoderData getRadiosondeData() 
        {
            byte[] bDecoderData = getDecoderPacket();
            DecoderData incoming = getDecoderData(bDecoderData); //Getting the decoded data and applying it to a local decodedData object.
            
            if(incoming.Sonde_ID == null)
            {
                return null;
            }
            else
            {
                currentDecodeData = incoming;   
                return currentDecodeData;
            }
            
            
        }

        /// <summary>
        /// Method for access the latest and greatest decoded data from decoder.
        /// </summary>
        /// <returns></returns>
        public DecoderData getCurrentDecoderData()
        {
            return currentDecodeData;
        }

        //public background worker to collect data.
        public void startCollectingData()
        {
            bwCollectData = new System.ComponentModel.BackgroundWorker();
            bwCollectData.WorkerSupportsCancellation = true;
            bwCollectData.DoWork += new System.ComponentModel.DoWorkEventHandler(bwCollectData_DoWork);
            bwCollectData.RunWorkerAsync();
        }

        public void stopCollectingData()
        {
            bwCollectData.CancelAsync();
        }

        void bwCollectData_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!bwCollectData.CancellationPending)
            {
                System.Threading.Thread.Sleep(1000);
                getRadiosondeData();

            }
        }


        private DecoderData getDecoderData(byte[] newData) 
        {
            DecoderData newDecoderData = new DecoderData();
            //Initialize Decoder Data
            newDecoderData.Sonde_ID = null;
            newDecoderData.pressure = -999.99;
            newDecoderData.temperature = -999.99;
            newDecoderData.humidity = -999.99;

            if (newData == null) 
            {
                return newDecoderData;
            }

            switch (newData.Length) 
            {
                case 5: //No Decoder Data
                    packetTimeout.UpdatingObject = new object();
                    return newDecoderData;
                    break;
                case 181:   //No GPS
                    string sondeID = "";
                    newDecoderData.packetTime = DateTime.Now;
                    for (int i = 13; i < 23; i++) 
                    {
                        if (newData[i] == 0x00) 
                        {
                            break;
                        }
                        sondeID += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.Sonde_ID = sondeID;

                    //PTU Data
                    byte[] bPressure = new byte[]{newData[60],newData[61],newData[62],0x00};
                    UInt32 pressure = BitConverter.ToUInt32(bPressure, 0);
                    newDecoderData.pressure = (double)pressure / 1000.0;
                    byte[] bTemperature = new byte[] { newData[78], newData[79], newData[80], 0x00 };
                    UInt32 temperature = BitConverter.ToUInt32(bTemperature, 0);
                    newDecoderData.temperature = (double)temperature / 1000.0 - 273.15;
                    byte[] bHumidity = new byte[] { newData[53], newData[54] };
                    UInt16 humidity = BitConverter.ToUInt16(bHumidity, 0);
                    newDecoderData.humidity = (double)humidity / 100.0;
                    

                    //GPS Data

                    Int32 latitude = BitConverter.ToInt32(newData, 128);
                    newDecoderData.latitude = latitude * 180.0 / Math.Pow(2, 31);
                    
                    Int32 longitude = BitConverter.ToInt32(newData, 132);
                    newDecoderData.longitude = longitude * 180.0 / Math.Pow(2, 31);

                    Int32 altitude = BitConverter.ToInt32(newData,136);
                    newDecoderData.altitude = altitude / 1000.0;
                    
                    break;
                case 190:   //IMS Sonde Format

                    byte[] bArray = null;   //Generic byte array used for converting
                    string sName = null;    //Generic string for getting IDs
                    newDecoderData.packetTime = DateTime.Now;
                    //Packet Count
                    newDecoderData.packetCount = BitConverter.ToUInt16(newData,3);

                    //Calibration Data
                    //newDecoderData.coefficient = BitConverter.ToDouble(newData, 5);
                    //Broken

                    //Sonde ID Number - 13 ASCII Characters
                    sName = "";
                    for (int i = 13; i < 26; i++) 
                    {
                        if (newData[i] == 0x00) //null character
                        {
                            break;
                        }
                        sName += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.Sonde_ID = sName;

                    //Probe Number - 13 ASCII Characters
                    sName = "";
                    for (int i = 26; i < 39; i++) 
                    {
                        if (newData[i] == 0x00) //null character
                        {
                            break;
                        }
                        sName += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.Probe_ID = sName;

                    //Sonde Serial Number - 13 ASCII Characters
                    sName = "";
                    for (int i = 39; i < 52; i++)
                    {
                        if (newData[i] == 0x00) //null character
                        {
                            break;
                        }
                        sName += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.Sonde_SN = sName;

                    //Firware Version
                    sName = "";
                    for (int i = 52; i < 60; i++)
                    {
                        if (newData[i] == 0x00) //null character
                        {
                            break;
                        }
                        sName += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.FirmwareVersion = sName;

                    //Skip bytes 60 and 61 for validity and number of ramps

                    //Humidity
                    newDecoderData.humidity = BitConverter.ToUInt16(newData, 62) / 100.0;

                    //GPS Data
                    newDecoderData.latitude = BitConverter.ToInt32(newData, 137) * 180.0 / Math.Pow(2, 31);
                    newDecoderData.longitude = BitConverter.ToInt32(newData, 141) * 180.0 / Math.Pow(2, 31);
                    newDecoderData.altitude = BitConverter.ToInt32(newData, 145) / 1000.0;

                    //Pressure
                    bPressure = new byte[]{newData[69],newData[70],newData[71],0x00};
                    pressure = BitConverter.ToUInt32(bPressure, 0);
                    newDecoderData.pressure = (double)pressure / 1000.0;

                    //Air Temp
                    bTemperature = new byte[] { newData[87], newData[88], newData[89], 0x00 };
                    temperature = BitConverter.ToUInt32(bTemperature, 0);
                    newDecoderData.temperature = (double)temperature / 1000.0 - 273.15;

                    //Battery Voltage 

                    byte[] bBattery = new byte[] { newData[105], newData[106], newData[107], 0x00 };
                    newDecoderData.battery_voltage = Convert.ToDouble(BitConverter.ToUInt32(bBattery, 0)) / 1000;
                    



                    break;
                default:
                    
                    break;
            }

            packetReady.UpdatingObject = (object)newDecoderData;

            return newDecoderData;
        }

        private byte[] getDecoderPacket()
        {
            this.COMPort.ReadTimeout = 1000;    //Read Timeout set to 1 second
            List<byte> bList = new List<byte>();

            while (true)
            {
                byte newByte;

                try { newByte = (byte)this.COMPort.ReadByte(); }
                catch { goto BadPacket; } //Catch read timeout

                if (newByte == 0x10)    //Start Byte
                {
                    try
                    {
                        newByte = (byte)this.COMPort.ReadByte();
                    }
                    catch { goto BadPacket; }

                    if (newByte == 0x00)    //No Packet report from decoder
                    {
                        bList.Add(0x10);
                        bList.Add(0x00);
                        goto NoRadiosondeData;
                    }
                    if (newByte == 0xB0) //No GPS Packet
                    {
                        bList.Add(0x10);
                        bList.Add(0xB0);
                        goto NoGPS_RadiosondePacket;
                    }
                    if (newByte == 0xB9) //GPS Packet
                    {
                        bList.Add(0x10);
                        bList.Add(0xB9);
                        goto GPS_RadiosondePacket;
                    }
                }
            }

        NoRadiosondeData:
            while (true)
            {
                byte newByte;
                try
                {
                    newByte = (byte)this.COMPort.ReadByte();
                }
                catch { goto BadPacket; }

                bList.Add(newByte);
                if (newByte == 0x04)
                {
                    goto BuildPacket;
                }
            }

        NoGPS_RadiosondePacket:
            while(bList.Count < 181)
            {
                byte newByte;
                try
                {
                    newByte = (byte)this.COMPort.ReadByte();
                }
                catch { goto BadPacket; }

                if (newByte == 0x10 && bList[bList.Count - 1] == 0x10)
                {
                    //Do not add byte
                }
                else
                {
                    bList.Add(newByte);
                }
            }

            goto BuildPacket;

        GPS_RadiosondePacket:
            while (bList.Count < 190)
            {
                byte newByte;
                try
                {
                    newByte = (byte)this.COMPort.ReadByte();
                }
                catch { goto BadPacket; }

                if (newByte == 0x10 && bList[bList.Count - 1] == 0x10)
                {
                    //Do not add byte
                }
                else
                {
                    bList.Add(newByte);
                }
            }

            goto BuildPacket;

        BuildPacket:
            byte[] sondePacket = new byte[bList.Count];
            for (int i = 0; i < bList.Count; i++)
            {
                sondePacket[i] = bList[i];
            }

            //Debug output
            /*
            string outputData = BitConverter.ToString(sondePacket);

            string[] breakDown = outputData.Split('-');
            string incomingData = "";

            for (int i = 0; i < breakDown.Length; i++)
            {
                incomingData += breakDown[i];
            }

            System.IO.File.AppendAllText("decoderRadioSonde.csv", DateTime.Now.ToString("HH:mm:ss.ff") + "," + sondePacket.Length.ToString() + "," + incomingData + "\n");
            */
            return sondePacket;

        BadPacket:
            System.IO.IOException badPacket = new System.IO.IOException("Bad iMet-1-TX data packet decoded.", 1001);
            packetBad.UpdatingObject = badPacket;
            return null;
        }//End method getDecoderPacket
    }//End class iMet1_Decoder

    public class DecoderData
    {
        //General Sonde Information
        public UInt16 packetCount;
        public double coefficient;
        public string Sonde_ID;
        public string Probe_ID;
        public string Sonde_SN;
        public string FirmwareVersion;
        public DateTime packetTime;
        
        //Calibration Information
        public UInt32 Pressure_Count;
        public UInt32 Pressure_RefCount;
        public UInt32 PTemp_Count;
        public UInt32 PTemp_RefCount;
        public UInt32 AirTemp_Count;
        public UInt32 AirTemp_RefCount;
        public UInt32 HumTemp_Count;
        public UInt32 HumTemp_RefCount;
        
        //Meteorological Data
        public double pressure;
        public double temperature;
        public double humidity;
        public double latitude;
        public double longitude;
        public double altitude;

        //Status Data
        public double internal_temp;
        public double battery_voltage;
    }
}