﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//User Classes
using elementStabilityEnviroment;

namespace EnviromentStability
{
    public class stabilityEnviroment
    {
        public stabilityEnviroment()
        {

        }

        //Class Veriables
        public System.Collections.ArrayList enviromentElements = new System.Collections.ArrayList();

        /// <summary>
        /// Method adds a element to watch to the list.
        /// </summary>
        /// <param name="desiredName">Can be whatever name you desire</param>
        /// <param name="desiredLimitLower">Lower range you wish to define</param>
        /// <param name="desiredLimitUpper">Upper Range you wish to define</param>
        /// <param name="desiredSetPoint">Set point that is trying to be achieved</param>
        public void addEnviromentElement(string desiredName, double desiredLimitLower, double desiredLimitUpper, double desiredSetPoint, int desiredNumberOfRefPoints)
        {
            //Generating the element to be tracked.
            elementStabilityEnviroment.elementStabilityEnviroment element = new elementStabilityEnviroment.elementStabilityEnviroment(desiredName, desiredLimitLower, desiredLimitUpper, desiredSetPoint, desiredNumberOfRefPoints);
            
            //Adding the element to the array.
            enviromentElements.Add(element);
        }

        /// <summary>
        /// Method removes desire named element.
        /// </summary>
        /// <param name="name"></param>
        public void removeEviromentElement(string name)
        {
            //Checking to make sure the element exsist.
            checkForExsisting(name);

            foreach (elementStabilityEnviroment.elementStabilityEnviroment elementToCheck in enviromentElements)
            {
                if (elementToCheck.getName() == name)
                {
                    enviromentElements.Remove(elementToCheck);
                    break;
                }
            }
        }

        public bool checkEnviromentElement(string name, double currentCondition)
        {
            //Checking to make sure the element exsist.
            checkForExsisting(name);

            foreach (elementStabilityEnviroment.elementStabilityEnviroment elementToCheck in enviromentElements)
            {
                if (elementToCheck.getName() == name)
                {
                    return elementToCheck.checkElement(currentCondition);
                }
            }

            return false;
        }

        public int checkEnviromentElementCount(string name)
        {
            int elementCount = 99999;

            //Checking to make sure the element exsist.
            checkForExsisting(name);

            foreach (elementStabilityEnviroment.elementStabilityEnviroment elementToCheck in enviromentElements)
            {
                if (elementToCheck.getName() == name)
                {
                    return elementToCheck.getReferancePointCount();
                }
            }

            return elementCount;
        }

        public System.Collections.ArrayList getElementsArrayList()
        {
            return enviromentElements;
        }

        #region Methods for accessing element data

        public double getElementSetPoint(string desireElementName)
        {
            checkForExsisting(desireElementName);

            foreach (elementStabilityEnviroment.elementStabilityEnviroment element in enviromentElements)
            {
                if (element.getName() == desireElementName)
                {
                    return element.getSetPoint();
                }
            }
            return 99999;
        }

        /// <summary>
        /// Gets the arry that is used to check point.
        /// </summary>
        /// <param name="desireElementName"></param>
        /// <returns></returns>
        public bool[] getDesiredNumberOfRefPoints(string desireElementName)
        {
            checkForExsisting(desireElementName);

            foreach (elementStabilityEnviroment.elementStabilityEnviroment element in enviromentElements)
            {
                if (element.getName() == desireElementName)
                {
                    return element.getReferancePoints();
                }
            }
            return null;
        }

        public int getElementReferancePointCount(string desireElementName)
        {
            checkForExsisting(desireElementName);

            foreach (elementStabilityEnviroment.elementStabilityEnviroment element in enviromentElements)
            {
                if (element.getName() == desireElementName)
                {
                    return element.getReferancePointCount();
                }
            }
            return 99999;
        }

        #endregion


        #region Internal methods for checking

        private void checkForExsisting(string name)
        {
            int counter = 0; //Number of total instances of the element

            foreach (elementStabilityEnviroment.elementStabilityEnviroment elementToCheck in enviromentElements)
            {
                if (elementToCheck.getName() == name)
                {
                    counter++;
                }
            }

            if (counter == 0)
            {
                //throw new System.ArgumentException("Enviroment Element not found.");
            }

            if (counter > 1)
            {
                //throw new System.ArgumentException("Duplicate Enviroment Element found.");
            }
        }

        #endregion

    }

}
