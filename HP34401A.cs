﻿// **************************************************************************
//
//      International Met Systems
//
//      Test Equipment Software
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   HP34401A.cs
//
//      CONTENTS    :   Routines for communication and control of the
//                      Hewlett Packard 34401A Multimeter
//
// **************************************************************************

// **************************************************************************
//
//        NAMESPACES
//
// **************************************************************************
using System;



// **************************************************************************
//
//        CLASSES
//
// **************************************************************************
public class HP34401A
{
    public System.IO.Ports.SerialPort SerialPort;
    public sConfiguration Configuration;
    public bool Online;
    public bool CommunicationErrorFlag;
    private float measurement;
    private bool flag_dataReady;

    public float Measurement 
    {
        get { return this.measurement; }
        set { this.measurement = value; }
    }

    public bool Flag_DataReady 
    {
        get { return this.flag_dataReady; }
        set { this.flag_dataReady = value; }
    }

    // **************************************************************************
    //
    //        CLASS INITIALIZATION
    //
    // **************************************************************************
	public HP34401A(string PortName)
	{
        // Initialize Serial Port
        this.SerialPort = new System.IO.Ports.SerialPort(PortName);
        this.SerialPort.BaudRate = 9600;
        this.SerialPort.DataBits = 8;
        this.SerialPort.Parity = System.IO.Ports.Parity.None;
        this.SerialPort.StopBits = System.IO.Ports.StopBits.One;
        this.SerialPort.NewLine = "\r\n";
        this.SerialPort.RtsEnable = true;
        this.SerialPort.DtrEnable = true;

        // Initialize Configuration
        this.Configuration.Type = MeasurementType.VoltageDC;
        this.Configuration.Resolution = Resolution.Default;
        this.Configuration.Range = Range.Default;

        // Initialize status
        this.Online = false;
        this.CommunicationErrorFlag = false;

        this.Measurement = 0;
	}

    // **************************************************************************
    //
    //        PRIVATE METHODS
    //
    // **************************************************************************

    // **************************************************************************
    //
    //  FUNCTION  : DelayMs
    //
    //  I/P       : None.
    //
    //  O/P       : None.
    //
    //  OPERATION : Homemade version of the System.Thread.Sleep() routine that
    //              should be more accurate when used in background threads.
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    private void DelayMs(UInt16 milliseconds)
    {
        System.DateTime CurrentTime = System.DateTime.UtcNow;
        System.DateTime Timer = CurrentTime.AddMilliseconds(milliseconds);

        while (CurrentTime <= Timer)
        {
            CurrentTime = System.DateTime.UtcNow;
        }
    } // DelayMs

    // **************************************************************************
    //
    //        PUBLIC METHODS
    //
    // **************************************************************************

    // **************************************************************************
    //
    //  FUNCTION  : HP34401A_Initialize
    //
    //  I/P       : string PortName = the name of the serial port (eg. "COM4")
    //
    //  O/P       : None.
    //
    //  OPERATION : Connects to the multimeter. After this routine is complete,
    //              the Online field will be set to "true" if initialization was
    //              successful, or else it will be set to "false".   
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public void HP34401A_Initialize()
    {
        string Response;

        try
        {
            this.SerialPort.Open();
        }
        catch 
        {
            this.Online = false;
            return;
        }

        // Send the reset command to get the meter into a known state
        this.SerialPort.WriteLine("*RST");

        // Wait for the reset
        DelayMs(750);

        // Check for errors
        this.SerialPort.WriteLine("*IDN?");
        // Setup read timeout
        this.SerialPort.ReadTimeout = 1000;
        
        try
        {
            // Get the equipment ID
            Response = this.SerialPort.ReadLine();
            DelayMs(200);
            if (Response.Contains("34401A")) 
            {
                this.Online = true;
            }
        }
        catch 
        {
            this.Online = false;
            this.SerialPort.Close();
        }
    } // HP34401A_Initialize

    public void Dispose()
    {
        if(SerialPort.IsOpen)
        {
            SerialPort.Close();
        }
    }

    // **************************************************************************
    //
    //  FUNCTION  : RemoteModeOn
    //
    //  I/P       : None.
    //
    //  O/P       : None.
    //
    //  OPERATION : Configures the multimeter for remote mode.  
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public void RemoteModeOn()
    {
        // Send the command for enabling remote mode
        this.SerialPort.WriteLine("SYST:REM");

        // Block so that we can ensure the command made it through
        DelayMs(200);
    } // RemoteModeOn

    // **************************************************************************
    //
    //  FUNCTION  : ReadErrors
    //
    //  I/P       : None.
    //
    //  O/P       : string = the message directly from the multimeter containing
    //                       the error message
    //
    //  OPERATION : Reads/clears the error message on the multimeter
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public string ReadErrors()
    {
        string ErrorMessage;

        // Send the command for enabling remote mode
        this.SerialPort.WriteLine("SYST:ERR?");

        // Get the error message
        ErrorMessage = this.SerialPort.ReadLine();

        // Block so that we can ensure the command made it through
        DelayMs(200);

        return ErrorMessage;
    } // RemoteModeOn

    // **************************************************************************
    //
    //  FUNCTION  : DisplayText
    //
    //  I/P       : string Text = The text to display on the meter
    //              UInt16 milliSeconds = The duration the text will flash onscreen
    //
    //  O/P       : None.
    //
    //  OPERATION : Displays a message on the multimeter for the duration specified.
    //              Remote mode must be turned on for this method to work properly.
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public void DisplayText(string Text, UInt16 milliSeconds) 
    {
        // Build the message
        string Message = "DISP:TEXT ";
        Message += '"';
        Message += Text;
        Message += '"';
        // Send the message
        this.SerialPort.WriteLine(Message);

        // Wait for the allotted time
        DelayMs(milliSeconds);

        // Clear the message
        this.SerialPort.WriteLine("DISP:TEXT:CLE");
    } // DisplayText

    // **************************************************************************
    //
    //  FUNCTION  : SetConfiguration
    //
    //  I/P       : MeasurementType Type = the type of measurment to be made
    //              Resolution Resolution = Min/Max/Default
    //              Range Range = Min/Max/Default
    //
    //  O/P       : None.
    //
    //  OPERATION : Configures the measurement of the multimeter
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public void SetConfiguration(MeasurementType Type, Resolution Resolution, Range Range)
    {
        string Command = "CONF:";

        switch (Type) 
        {
            case MeasurementType.VoltageDC:
                Command += "VOLT:DC";
                break;

            case MeasurementType.FourWireResistance:
                Command += "FRES";
                break;


            default:
                return;
        } // switch

        Command += " ";

        switch (Resolution) 
        {
            case Resolution.Min:
                Command += "MIN";
                break;
            case Resolution.Max:
                Command += "MAX";
                break;
            case Resolution.Default:
                Command += "DEF";
                break;
            default:
                return;
        } // switch

        Command += ",";

        switch (Range) 
        {
            case Range.Min:
                Command += "MIN";
                break;
            case Range.Max:
                Command += "MAX";
                break;
            case Range.Default:
                Command += "DEF";
                break;
            default:
                return;
        } // switch

        // Send the command
        this.SerialPort.WriteLine(Command);

        // Wait for command to be sent and processed
        DelayMs(200);
    } // SetConfiguration

    // **************************************************************************
    //
    //  FUNCTION  : InitMeasurement
    //
    //  I/P       : None.
    //
    //  O/P       : None.
    //
    //  OPERATION : Initializes the measurement. This routine should be called
    //              after the remote interface is turned on and the meter has
    //              been configured. This must be called before the "READ" or 
    //              "MEAS" commands are sent.
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public void InitMeasurement()
    {
        this.SerialPort.WriteLine("INIT");
        DelayMs(500);
    } // InitMeasurement

    // **************************************************************************
    //
    //  FUNCTION  : ReadMeasurement
    //
    //  I/P       : None
    //
    //  O/P       : float = Measurement Result
    //
    //  OPERATION : Measures according to the configuration. In order for this
    //              routine to work, InitMeasurement() must be run and the meter
    //              must be configured.
    //
    //  UPDATED   : 2017-05-17 JHM
    //
    // **************************************************************************
    public void ReadMeasurement()
    {
        string Response;

        this.SerialPort.WriteLine("READ?");

        // Set the read timeout
        this.SerialPort.ReadTimeout = 1000;

        try
        {
            Response = this.SerialPort.ReadLine();
            this.measurement = float.Parse(Response);
        }
        catch 
        {
            this.CommunicationErrorFlag = true;
            this.SerialPort.ReadTimeout = System.IO.Ports.SerialPort.InfiniteTimeout;
        }

        this.flag_dataReady = true;
    } // InitMeasurement

    public struct sConfiguration
    {
        public MeasurementType Type;
        public Resolution Resolution;
        public Range Range;
    }
}

public enum MeasurementType { VoltageDC, VoltageDCRatio, VoltageAC, Resistance, FourWireResistance, Frequency, Period, Continuity, Diode };
public enum Resolution { Min, Max, Default };
public enum Range { Min, Max, Default };