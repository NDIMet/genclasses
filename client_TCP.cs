﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client_TCP
{
    public class tcpClient
    {
        //Public objects
        public System.Net.Sockets.TcpClient client;
        public System.Net.Sockets.NetworkStream stream;

        //Update objects
        public updateCreater.objectUpdate tcpStatus = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate tcpError = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate tcpData = new updateCreater.objectUpdate();

        //Thead to collect received data and had it out.
        System.Threading.Thread recData;

        //Private objects
        bool disconnect = false;
        System.Timers.Timer checkConnection;    //Check to make sure the connection is good.
        dataPacket latestPacket;

        string conIP;
        int conPort;

        public tcpClient() { }

        /// <summary>
        /// Method sets up the TCP Client
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public tcpClient(string address, int port)
        {
            conIP = address;
            conPort = port;
            setupConnection(address, port);
        }

        private void setupConnection(string ipaddress, int port)
        {
            client = new System.Net.Sockets.TcpClient();

            client.Connect(ipaddress, port);

            if (client.Connected)
            {
                stream = client.GetStream();
                stream.ReadTimeout = 2000;
                stream.WriteTimeout = 2000;

                recData = new System.Threading.Thread(recTCPData);
                recData.Name = "TCP Client:Paro";
                recData.IsBackground = true;
                recData.Start();

                //Setting up the timer to check the tcp  connection.
                checkConnection = new System.Timers.Timer(1000);
                checkConnection.Elapsed += new System.Timers.ElapsedEventHandler(checkConnection_Elapsed);
                //checkConnection.Enabled = true;

            }
            else
            {
                tcpError.UpdatingObject = new Exception("TCP Socket Error: Unable to connect to: " + ipaddress + ":" + port.ToString());
            }
        }

        public  void disconnectConnection()
        {
            this.disconnect = true;
        }


        void checkConnection_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (latestPacket != null)
            {
                TimeSpan timePassed = (DateTime.Now - latestPacket.packetDate);

                if (client != null)
                {
                    if (!client.Client.Poll(3000000, System.Net.Sockets.SelectMode.SelectRead))
                    {
                        tcpError.UpdatingObject = (object)new Exception("Error: TCP,No new data for: " + timePassed.TotalSeconds.ToString());

                        if (client.Connected && timePassed.TotalSeconds > 60)
                        {
                            disconnect = true;
                            System.Threading.Thread.Sleep(250);

                            stream.Close();
                            client.Close();

                            stream = null;
                            client = null;
                            recData = null;

                            disconnect = false;
                        }

                    }
                }

                if (timePassed.TotalSeconds > 61 || client == null)
                {
                    disconnect = true;
                    System.Threading.Thread.Sleep(250);
                    disconnect = false;

                    try
                    {
                        setupConnection(conIP, conPort);
                        System.Diagnostics.Debug.WriteLine("Reconnect ready.");
                    }
                    catch (Exception conError)
                    {
                        System.Diagnostics.Debug.WriteLine("Reconnect Error: " + conError.Message);
                    }
                }


            }
        }

        /// <summary>
        /// Thread that collects all data from the TCP socket and raises the tcpData event when ready.
        /// </summary>
        void recTCPData()
        {
            String curData;
            while (!disconnect)
            {
                if (client.Connected)
                {
                    if (stream.DataAvailable)
                    {
                        System.Threading.Thread.Sleep(100);  //Sleep to allow all data to get into the buffer.
                        Byte[] received = new Byte[5000];
                        int nBytesReceived = stream.Read(received, 0, received.Length);
                        Byte[] Output = new Byte[nBytesReceived];

                        for (int i = 0; i < Output.Length; i++)
                        {
                            Output[i] = received[i];
                        }

                        string allData = System.Text.Encoding.ASCII.GetString(Output);

                        char[] splitter = new char[2] { '\r', '\n' };
                        string[] broke = allData.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

                        for (int x = 0; x < broke.Length; x++)
                        {
                            dataPacket tempIncoming = new dataPacket();
                            tempIncoming.packetDate = DateTime.Now;
                            tempIncoming.packetMessage = broke[x];
                            latestPacket = tempIncoming;

                            tcpData.UpdatingObject = (object)tempIncoming;
                        }
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(50);  //Take a break and wait for data.
                    }
                }

                //Checking to make sure the connect is still good.
                if (!client.Connected)
                {
                    tcpError.UpdatingObject = new Exception("Error: TCP,TCP clint disconnect.");
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// Method allows for sting to be sent out the TCP socket.
        /// </summary>
        /// <param name="message"></param>
        public void sendData(string message)
        {
            if (client.Connected)
            {
                if (stream.CanWrite)
                {
                    Byte[] inputToBeSent = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());

                    try
                    {
                        stream.Write(inputToBeSent, 0, inputToBeSent.Length);
                        stream.Flush();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                    }
                }
            }
            else
            {
                tcpError.UpdatingObject = new Exception("Error: TCP,Client not connected.");
            }
        }

    }

    public class dataPacket
    {
        public DateTime packetDate;
        public string packetMessage;

    }
}
