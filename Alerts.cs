﻿///William Jones
///2011/12/09
///This class is a collection of methods to allow the program to alert users via audio, text message, and email to get
///help or processing done.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alerts
{
    #region Methods for email alerts
    public class alertsEmail
    {
        public alertsEmail()
        { }

        /// <summary>
        /// Method sends email alerts out. If your sending out crash reports set the method Async to false.
        /// </summary>
        /// <param name="messageTo"></param>
        /// <param name="messageSubject"></param>
        /// <param name="messageBody"></param>
        /// <param name="messageFrom"></param>
        /// <param name="messageFromSMTP"></param>
        /// <param name="messageFromSMTPPort"></param>
        /// <param name="messageFromSMTPUsername"></param>
        /// <param name="messageFromSMTPPassword"></param>
        /// <param name="messageSSL"></param>
        /// <param name="Async"></param>
        /// <returns></returns>
        public bool alertsSendEmail(string messageTo, string messageSubject, string messageBody, string messageFrom, string messageFromSMTP, string messageFromSMTPPort, string messageFromSMTPUsername, string messageFromSMTPPassword, bool messageSSL, bool Async)
        {
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(messageFromSMTPUsername, messageFromSMTPPassword);
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress fromAddress = new System.Net.Mail.MailAddress(messageFrom);

            smtpClient.Host = messageFromSMTP;
            smtpClient.Port = Convert.ToInt16(messageFromSMTPPort);
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = messageSSL;
            smtpClient.Credentials = basicCredential;

            message.From = fromAddress;
            message.Subject = messageSubject;
            message.Body = messageBody;

            message.To.Add(messageTo);

            try
            {
                if (Async)
                {
                    smtpClient.SendAsync(message, 1);
                }
                else
                {
                    smtpClient.Send(message);
                }
                return true;
            }

            catch
            {
                return false;
            }           
        }
    }
    #endregion

    #region Methods for sending Text Messages
    public class alertsTextMessage
    {
        public alertsTextMessage()
        { }
        /// <summary>
        /// This method taks a properly formatted message and delivers it via the text message system.
        /// </summary>
        /// <param name="messageTo"></param>
        /// <param name="messageToCarrier"></param>
        /// <param name="messageSubject"></param>
        /// <param name="messageBody"></param>
        /// <param name="messageFrom"></param>
        /// <param name="messageFromSMTP"></param>
        /// <param name="messageFromSMTPPort"></param>
        /// <param name="messageFromSMTPUsername"></param>
        /// <param name="messageFromSMTPPassword"></param>
        /// <param name="messageSSL"></param>
        /// <returns></returns>
        public bool alertTextMessageSend(string messageTo, string messageToCarrier, string messageSubject, string messageBody,  string messageFrom, string messageFromSMTP, string messageFromSMTPPort, string messageFromSMTPUsername, string messageFromSMTPPassword, bool messageSSL)
        {
            //Setting up txt message objects.
            System.Net.Mail.SmtpClient alertSmtpClient = new System.Net.Mail.SmtpClient();
            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(messageFromSMTPUsername, messageFromSMTPPassword);
            System.Net.Mail.MailMessage messageText = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress fromAddress = new System.Net.Mail.MailAddress(messageFrom);

            alertSmtpClient.Host = messageFromSMTP;
            alertSmtpClient.Port = Convert.ToInt16(messageFromSMTPPort);
            alertSmtpClient.UseDefaultCredentials = false;
            alertSmtpClient.EnableSsl = messageSSL;
            alertSmtpClient.Credentials = basicCredential;

            messageText.From = fromAddress;
            messageText.Subject = messageSubject;
            messageText.Body = messageBody;
            messageText.To.Add(messageTo + messageToCarrier);

            try
            {
                //Sending out message to the server for delivery
                alertSmtpClient.SendAsync(messageText, 1);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
    #endregion

    #region Methods for sending audio alerts.
    public class alertsAudio
    {
        public alertsAudio()
        { }

        public void alertAudio(string soundPathandFilename, int delay)
        {
            alertDelay = delay;
            alertSound = new System.Media.SoundPlayer(soundPathandFilename);

        }

        //Public declares
        System.Media.SoundPlayer alertSound = null;
        System.ComponentModel.BackgroundWorker backgroundWorkerAlertAudio = null;
        public int alertDelay = 1000;

        /// <summary>
        /// Plays the selected audio file over and over until the stop command is sent.
        /// </summary>
        public void startAlertAudio()
        {
            if(backgroundWorkerAlertAudio == null)
            {
                backgroundWorkerAlertAudio = new System.ComponentModel.BackgroundWorker();
            }
            backgroundWorkerAlertAudio.WorkerSupportsCancellation = true;
            backgroundWorkerAlertAudio.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerAlertAudio_DoWork);
            backgroundWorkerAlertAudio.RunWorkerAsync();
        }

        public void stopAlertAudio()
        {
            backgroundWorkerAlertAudio.CancelAsync();
        }

        void backgroundWorkerAlertAudio_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!backgroundWorkerAlertAudio.CancellationPending)
            {
                alertSound.Play();
                System.Threading.Thread.Sleep(alertDelay);
            }
        }
    }
    #endregion
    

    /// <summary>
    /// Class for displaying that an error has happen
    /// </summary>
    public class errorDisplay
    {
        System.Windows.Forms.Form formError;
        System.Windows.Forms.Label errorMessage;

        public errorDisplay()
        {
            formError = new System.Windows.Forms.Form();

            //Making the form the size of the screen.
            var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            //formError.Size = new System.Drawing.Size(screen.Width, screen.Height);
            formError.Size = new System.Drawing.Size(640, 480);
            formError.ShowInTaskbar = false;
            formError.MinimizeBox = false;
            formError.MaximizeBox = false;
            formError.ShowIcon = false;
            formError.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            formError.Show();



            errorMessage = new System.Windows.Forms.Label();
            errorMessage.AutoSize = false;
            errorMessage.Size = new System.Drawing.Size(formError.Width, formError.Height);
            errorMessage.Location = new System.Drawing.Point(0, 0);
            errorMessage.BackColor = System.Drawing.Color.Transparent;
            errorMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            errorMessage.Font = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 20);
            


            errorMessage.Text = "Error!";
            
            //Adding elements to the form.
            formError.Controls.Add(errorMessage);

            System.Threading.Thread flash = new System.Threading.Thread(threadFlash);
            flash.IsBackground = true;
            flash.Name = "Flash Alert";
            flash.Start();

            formError.Resize += new EventHandler(formError_Resize);
            
        }

        void formError_Resize(object sender, EventArgs e)
        {
            //errorMessage.Location = new System.Drawing.Point(0, (formError.Height / 2) - (errorMessage.Height));
            errorMessage.Size = new System.Drawing.Size(formError.Width, formError.Height);
        }

        void threadFlash()
        {
            while (true)
            {
                formError.BackColor = System.Drawing.Color.Red;
                System.Threading.Thread.Sleep(500);
                formError.BackColor = System.Drawing.Color.Yellow;
                System.Threading.Thread.Sleep(500);

            }
        }

        public void updateText(string text)
        {
            errorMessage.Text = text;
        }
    }
}
