﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace FinalTest
{
    public class radiosondeTest
    {
        //Private items
        //Sensors
        TestEquipment.epluseEE31 currentEplusE;     //EE31 TU sensor
        TestEquipment.HMP234 currentHMP234;         //HMP233- HMP234 sensor
        TestEquipment.YoungBaro currentPressureSensor;  //Young Baro sensor.
        //Decoders
        iMet_1_Decoder.iMet1_Decoder currentDecoderIMS; //IMS Decoder data stream
        Radiosonde.iMet1 currentDecoderOldIMS;

        txDecoder.decoderBell202 currentDecoderBell202; //Bell202 Decoder data stream
        //Radiosondes
        Universal_Radiosonde.iMet1UV2 currentRadiosonde;   //Cable connection to the radiosondes.
        Universal_Radiosonde.CalData currentRadiosondeData; //The data from the cable above.

        //Interneal Timers.
        System.Timers.Timer testTimeout;    //Timer that triggers a fail test event.
        bool testRun = false;

        //Condition that radiosondes will be tested to.
        testParameters currentTestSettings;     //Current settings to the test being run.

        //Additional internal veriables.
        DateTime timeTestStarted;

        testResults[] testDataResults;      //This array contains the packets pass/fail requirments.
        testDataPoint latestTestData;      //Latest Test Data.

        int sondePassCount = 0;     //Number of counter needed to pass test.

        //Auto Reset Events
        System.Threading.AutoResetEvent radiosondePTUReady = new System.Threading.AutoResetEvent(false);    //Controls for radiosondes PTU ready.
        System.Threading.AutoResetEvent radiosondeGPSReady = new System.Threading.AutoResetEvent(false);    //Control for radioondes GPS ready.
        System.Threading.AutoResetEvent sensorsTempReady = new System.Threading.AutoResetEvent(false);      //Control for referance sensors ready.
        System.Threading.AutoResetEvent sensorsHumidityReady = new System.Threading.AutoResetEvent(false);      //Control for referance sensors ready.
        System.Threading.AutoResetEvent sensorsPressureReady = new System.Threading.AutoResetEvent(false);      //Control for referance sensors ready.
        System.Threading.AutoResetEvent cableDataReady = new System.Threading.AutoResetEvent(false);        //Control for aux cable ready.

        //Current Enviromental condidtions.
        public List<stabilityElement> testingEnviroment = new List<stabilityElement>();

        //Public items
        public updateCreater.objectUpdate testStatus = new updateCreater.objectUpdate();   //Way of updating current status of test.
        public updateCreater.objectUpdate testError = new updateCreater.objectUpdate();    //Test has error. Sending out the exception.
        public updateCreater.objectUpdate testComplete = new updateCreater.objectUpdate(); //Trigger when the test is complete.
        public updateCreater.objectUpdate updatePacket = new updateCreater.objectUpdate();  //Triggers when the a total packet has been collected.

        public radiosondeTest() { }

        /// <summary>
        /// Method sets up a test. Pass the decoder, cable connection type or null, referacnce Pressure sensor, and Referance TU sensor
        /// </summary>
        /// <param name="decoderData"></param>
        /// <param name="cableData"></param>
        /// <param name="sensorPressure"></param>
        /// <param name="sensorEplusE"></param>
        public radiosondeTest(object decoderData, Universal_Radiosonde.iMet1UV2 cableData, object sensorPressure, object sensorTU, testParameters desiredTest, List<stabilityElement> desiredES)
        {
            System.Diagnostics.Debug.WriteLine("Building the test internal.");

            //Applying test settings
            currentTestSettings = desiredTest;

            foreach (stabilityElement el in desiredES)
            {
                testingEnviroment.Add(el);
            }
             
            //Setting up the data pass/fail array
            testDataResults = new testResults[currentTestSettings.numberOfCorrect];

            //Setting up the counted needed to pass the test.
            getTotalPassCount();

            //Applying all equipment to internal objects
            System.Diagnostics.Debug.WriteLine("Internal Test Decoder Setting: " + sensorTU.GetType().Name);
            switch (sensorTU.GetType().Name)    //Pulling ref sensor type.
            {
                case "epluseEE31":
                    currentEplusE = (TestEquipment.epluseEE31)sensorTU;
                    currentEplusE.tempReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(refTempReady_PropertyChange);    //Subscribing to the temp ready event.
                    currentEplusE.humidityReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(refHumidityReady_PropertyChange);    //Subscribing to Humidity ready event.
                    break;

                case "HMP234":
                    currentHMP234 = (TestEquipment.HMP234)sensorTU;
                    currentHMP234.updateStatusReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(refTempReady_PropertyChange);    //Subscribing to the temp ready event.
                    currentHMP234.updateStatusReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(refHumidityReady_PropertyChange);    //Subscribing to Humidity ready event.
                    break;
            }

            if (sensorPressure != null)     //Checking to make sure there is a pressure sensor ready.
            {
                currentPressureSensor = (TestEquipment.YoungBaro)sensorPressure;    //Transfering the desired pressure sensor to local object.
            }

            //Setting up radiosonde tx data source.
            switch (decoderData.GetType().Name)
            {
                case "iMet1_Decoder":
                    
                    currentDecoderIMS = (iMet_1_Decoder.iMet1_Decoder)decoderData;
                    currentDecoderIMS.packetReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetReady_PropertyChange);
                     
                    break;

                case "decoderBell202":
                    currentDecoderBell202 = (txDecoder.decoderBell202)decoderData;
                    currentDecoderBell202.packetGPSXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSXReady_PropertyChange);
                    currentDecoderBell202.packetGPSReady.PropertyChange +=new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSXReady_PropertyChange);
                    currentDecoderBell202.packetPTUReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUXReady_PropertyChange);
                    currentDecoderBell202.packetPTUXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUXReady_PropertyChange);
                    break;
                    
                case "iMet1":
                    currentDecoderOldIMS = (Radiosonde.iMet1)decoderData;
                    currentDecoderOldIMS.decoderPacketDataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderPacketDataReady_PropertyChange);
                    
                    break;
            }

            if (cableData != null) //Cable data if we need it.
            {
                currentRadiosonde = cableData;
                currentRadiosonde.CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
                //currentRadiosonde.setManufacturingMode(true);
            }    

            //Setting up test timeout timer.
            testTimeout = new System.Timers.Timer(currentTestSettings.maxTimeToStablize * 60000);
            testTimeout.Elapsed += new System.Timers.ElapsedEventHandler(testTimeout_Elapsed);

        }

        public void intSondeStabilityElements()
        {
            //Setting up stability elements
            stabilityElement sondePressure = new stabilityElement();
            sondePressure.stabilityElementSetup("sondeP", currentTestSettings.numberOfCorrect);
            stabilityElement sondeAirTemp = new stabilityElement();
            sondeAirTemp.stabilityElementSetup("sondeT", currentTestSettings.numberOfCorrect);
            stabilityElement sondeHumidity = new stabilityElement();
            sondeHumidity.stabilityElementSetup("sondeU", currentTestSettings.numberOfCorrect);

            //Adding the elements to the list.
            testingEnviroment.Add(sondePressure);
            testingEnviroment.Add(sondeAirTemp);
            testingEnviroment.Add(sondeHumidity);
        }

        public void intSondeStabilityElements(stabilityElement sondeP, stabilityElement sondeT, stabilityElement sondeU)
        {
            //Setting up stability elements
            stabilityElement sondePressure = sondeP;
            sondePressure.stabilityElementSetup("sondeP", currentTestSettings.numberOfCorrect);
            stabilityElement sondeAirTemp = sondeT;
            sondeAirTemp.stabilityElementSetup("sondeT", currentTestSettings.numberOfCorrect);
            stabilityElement sondeHumidity = sondeU;
            sondeHumidity.stabilityElementSetup("sondeU", currentTestSettings.numberOfCorrect);

            //Adding the elements to the list.
            testingEnviroment.Add(sondePressure);
            testingEnviroment.Add(sondeAirTemp);
            testingEnviroment.Add(sondeHumidity);
        }

        public void startTest()
        {
            System.Diagnostics.Debug.WriteLine("Test Started");
            testTimeout.Enabled = true;     //Starting time out timer.
            timeTestStarted = DateTime.Now; //Noteing the time the test was started.

            System.Threading.Thread test = new System.Threading.Thread(this.backgroundWorkerRadiosondeTest_DoWork);
            test.IsBackground = true;
            testRun = true;
            test.Start();
        }

        public void stopTest()
        {
            testTimeout.Enabled = false;    //Stopping the timeout timer
            testRun = false;
        }

        void testTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            stopTest();
        }

        #region Methods that run the compare testing and stability work.

        /// <summary>
        /// Main processor. This is where most of the work gets done.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void backgroundWorkerRadiosondeTest_DoWork()//object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Test Background worker started.");

            List<testResults> currentTestResults = new List<testResults>(); //Contains the test results for the currrent tests.
            List<testDataPoint> currentTestData = new List<testDataPoint>();    //Contains the current packet of data from radiosondes and ref's

            if (testingEnviroment.Count == 0)   //Checking to see if the sonde stability elements are present.
            {
                intSondeStabilityElements();    //Setting up stability elements.
            }

            //pulling in stability elements.
            stabilityElement sondePressure = null;
            stabilityElement sondeAirTemp = null;
            stabilityElement sondeHumidity = null;
            foreach (stabilityElement se in testingEnviroment)
            {
                if (se.desciption == "sondePressure") { sondePressure = se; }
                if (se.desciption == "sondeAirTemp") { sondeAirTemp = se; }
                if (se.desciption == "sondeHumidity") { sondeHumidity = se; }

            }

            //Checking to make sure the stability elements are not null.
            if (sondePressure == null || sondeAirTemp == null || sondeHumidity == null)
            {
                Exception nullElementError = new Exception("Radiosonde test stability element error. Element was not started correctly.");
                testError.UpdatingObject = nullElementError;
            }

            int counterResults = 0;     //Current packet of test results.

            bool testResult = false;

            /*
            if (currentTestSettings.optionAuxCable)
            {
                System.Diagnostics.Debug.WriteLine("Cable needing testing. Starting mfg mode");

                if (currentRadiosonde.newRadiosondeDetected)
                {
                    currentRadiosonde.setManufacturingMode(true);
                }

                currentRadiosonde.readyRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
            }
            */

            while (testRun)     //Shutdown the background worker is commanded to.
            {
                FinalTest.testDataPoint tempPacket = new testDataPoint();   //Container for the current data.

                //Loading temp packet with radiosonde data.
                if (radiosondePTUReady.WaitOne(1500) && radiosondeGPSReady.WaitOne(1500)) //If radiosonde is ready collect data.
                {
                    //Getting decoded data.
                    if (currentDecoderBell202 != null)  //Processing if decoder setup.
                    {
                        txDecoder.radiosondePTUData tempPTU = currentDecoderBell202.getLatestPTU(); //Getting latest PTU data from the decoder.
                        //PTU
                        tempPacket.txSondePacketCount = tempPTU.packetNumber;
                        tempPacket.txSondePressure = tempPTU.pressure;
                        tempPacket.txSondeAirTemp = tempPTU.airTemp;
                        tempPacket.txSondeHumidity = tempPTU.humidity;
                        //GPS Data
                        txDecoder.radiosondeGPSData tempGPS = currentDecoderBell202.getLatestGPS(); //Getting the latest GPS data from decoder.
                        tempPacket.txSondeLat = tempGPS.latitude;
                        tempPacket.txSondeLong = tempGPS.longitude;
                        tempPacket.txSondeAlt = tempGPS.alt;
                    }
                    if ( currentDecoderIMS != null)
                    {
                        iMet_1_Decoder.DecoderData tempIMSData = currentDecoderIMS.getCurrentDecoderData();     //Getting the latest data from the IMS decoder.
                        //PTU
                        tempPacket.txSondePressure = tempIMSData.pressure;
                        tempPacket.txSondeAirTemp = tempIMSData.temperature;
                        tempPacket.txSondeHumidity = tempIMSData.humidity;
                        //GPS Data
                        tempPacket.txSondeLat = tempIMSData.latitude;
                        tempPacket.txSondeLong = tempIMSData.longitude;
                        tempPacket.txSondeAlt = tempIMSData.altitude;
                    }

                    if (currentDecoderOldIMS != null)
                    {
                        //PTU
                        tempPacket.txSondePressure = Convert.ToDouble(currentDecoderOldIMS.getPressure());
                        tempPacket.txSondeAirTemp = Convert.ToDouble(currentDecoderOldIMS.getAirTemp());
                        tempPacket.txSondeHumidity = Convert.ToDouble(currentDecoderOldIMS.getRH());
                        //GPS Data
                        tempPacket.txSondeLat = Convert.ToDouble(currentDecoderOldIMS.getGPSlat());
                        tempPacket.txSondeLong = Convert.ToDouble(currentDecoderOldIMS.getGPSLong());
                        tempPacket.txSondeAlt = Convert.ToDouble(currentDecoderOldIMS.getGPSAlt())/1000;
                    }

                    //Adding sonde data to the enviroment stability system.
                    updateElement("sondeP", tempPacket.txSondePressure);
                    updateElement("sondeT", tempPacket.txSondeAirTemp);
                    updateElement("sondeU", tempPacket.txSondeHumidity);

                    System.Diagnostics.Debug.WriteLine("TX Data Received.");
                }
                else
                {
                    System.TimeoutException noRadiosondeData = new TimeoutException("Radiosonde Data TimeOut");
                    testError.UpdatingObject = noRadiosondeData;
                }

                //Loading the referance data.
                if (sensorsTempReady.WaitOne(1500))    //If sensors are ready collect data.
                {

                    //Getting referance date
                    if (currentEplusE != null)
                    {
                        tempPacket.refAirTemp = currentEplusE.currentAirTemp;
                        tempPacket.refHumidity = currentEplusE.currentHumidity;
                    }
                    if (currentHMP234 != null)
                    {
                        tempPacket.refAirTemp = currentHMP234.CurrentTemp;
                        tempPacket.refHumidity = currentHMP234.CurrentHumidity;
                    }

                    if (currentPressureSensor != null)  //Collecting the data from the referance pressure sensor if not null.
                    {
                        tempPacket.refPressure = currentPressureSensor.CurrentPressure;     //Getting the current pressure reading.
                    }

                    updateElement("referancePressure", tempPacket.refPressure);
                    updateElement("referanceAirTemp", tempPacket.refAirTemp);
                    updateElement("referanceHumidity", tempPacket.refHumidity);

                }
                else
                {
                    System.TimeoutException noTURefData = new TimeoutException("TU Referance Data TimeOut");
                    testError.UpdatingObject = noTURefData;
                }
                currentTestData.Add(tempPacket);
                latestTestData = tempPacket;

                //Beginning the processing of comparing 
                testResults tempResults = new testResults();

                if (currentTestSettings.optionPressure)     //Checking to see if the radiosondes test includes an option to test pressure.
                {
                    if (Math.Abs(tempPacket.txSondePressure - tempPacket.refPressure) <= currentTestSettings.tolPressure)
                    {
                        tempResults.statusPressure = true;
                    }
                    else
                    {
                        tempResults.statusPressure = false;
                    }
                }
                else   //Testing non-pressure radiosonde to make sure there is no radiosonde pressure reading.
                {
                    if (Math.Abs(tempPacket.txSondePressure - 0) <= currentTestSettings.tolPressure)
                    {
                        tempResults.statusPressure = true;
                    }
                    else
                    {
                        tempResults.statusPressure = false;
                    }
                }

                if (currentTestSettings.optionAuxCable)   //If there radiosonde has a aux cable that needs testing.
                {
                    if (cableDataReady.WaitOne(100))   //Waiting for cable data is ready.
                    {
                        tempResults.statusCable = true;
                    }
                    else
                    {
                        Exception noCalData = new TimeoutException("No Cal Data from cable.");
                        testError.UpdatingObject = noCalData;
                        tempResults.statusCable = false;
                    }
                }

                if (Math.Abs(tempPacket.txSondeAirTemp - tempPacket.refAirTemp) <= currentTestSettings.tolAirTemp)
                {
                    tempResults.statusAirTemp = true;
                }
                else
                {
                    tempResults.statusAirTemp = false;
                }

                if (Math.Abs(tempPacket.txSondeHumidity - tempPacket.refHumidity) <= currentTestSettings.tolHumidity)
                {
                    tempResults.statusHumidity = true;
                }
                else
                {
                    tempResults.statusHumidity = false;
                }

                if (currentTestSettings.optionGPS)  //Checking to see if the radiosondes test includes an option to test GPS.
                {
                    if (Math.Abs(gpsHorzonalDiff(tempPacket)) <= currentTestSettings.tolHorizonal && Math.Abs(tempPacket.txSondeAlt - currentTestSettings.stationAlt) <= currentTestSettings.tolVertical)
                    {
                        tempResults.statusGPSPOS = true;
                    }
                    else
                    {
                        tempResults.statusGPSPOS = false;
                    }
                }

                //Checking to see if conditions form the radiosonde are stable.
                if (Math.Abs(sondePressure.getDiff(currentTestSettings.numberOfCorrect)) < currentTestSettings.MSLsondeP)    //Checking radiosondes pressure stability
                {
                    tempResults.sondePressureStability = true;
                }
                else
                {
                    tempResults.sondePressureStability = false;
                }

                if (Math.Abs(sondeAirTemp.getDiff(currentTestSettings.numberOfCorrect)) < currentTestSettings.MSLsondeT)     //Checking radiosondes air temp stability
                {
                    tempResults.sondeAirTempStability = true;
                }
                else
                {
                    tempResults.sondeAirTempStability = false;
                }

                if (Math.Abs(sondeHumidity.getDiff(currentTestSettings.numberOfCorrect)) < currentTestSettings.MSLsondeU)    //Checking radiosondes humidity stability
                {
                    tempResults.sondeHumidityStability = true;
                }
                else
                {
                    tempResults.sondeHumidityStability = false;
                    
                }

                /*
                #if DEBUG   //Checking to see the stability results.
                System.Diagnostics.Debug.WriteLine(sondePressure.getDiff(currentTestSettings.numberOfCorrect).ToString("0.000") + "=" + tempResults.sondePressureStability + "," +
                                    sondeAirTemp.getDiff(currentTestSettings.numberOfCorrect).ToString("0.000") + "=" + tempResults.sondeAirTempStability + "," +
                                    sondeHumidity.getDiff(currentTestSettings.numberOfCorrect).ToString("0.000") + "=" + tempResults.sondeHumidityStability);
                #endif
                */
                //Checking to see if conditons from the referances are stable.

                string testingCompile = "Referance Data: ";

                foreach (stabilityElement se in testingEnviroment)
                {
                    if (se.desciption == "referancePressure")
                    {
                        if (Math.Abs(se.getDiff(currentTestSettings.numberOfCorrect)) < currentTestSettings.MSLrefP)
                        {
                            tempResults.refPressureStability = true;
                        }
                        else
                        {
                            tempResults.refPressureStability = false;
                        }

                        testingCompile += se.getDiff(currentTestSettings.numberOfCorrect).ToString("0.000") + ",";
                    }

                    if (se.desciption == "referanceAirTemp")
                    {
                        if (Math.Abs(se.getDiff(currentTestSettings.numberOfCorrect)) < currentTestSettings.MSLrefT)
                        {
                            tempResults.refAirTempStability = true;
                        }
                        else
                        {
                            tempResults.refAirTempStability = false;
                        }

                        testingCompile += se.getDiff(currentTestSettings.numberOfCorrect).ToString("0.000") + ",";
                    }

                    if (se.desciption == "referanceHumidity")
                    {
                        if (Math.Abs(se.getDiff(currentTestSettings.numberOfCorrect)) < currentTestSettings.MSLrefU)
                        {
                            tempResults.refHumidityStability = true;
                        }
                        else
                        {
                            tempResults.refHumidityStability = false;
                        }

                        testingCompile += se.getDiff(currentTestSettings.numberOfCorrect).ToString("0.000") + ",";
                    }
                    
                }

                //System.Diagnostics.Debug.WriteLine(testingCompile);

                //Reporting status.
                updatePacket.UpdatingObject = (object)tempResults;

                //Adding the test results to the offical list.
                testDataResults[counterResults] = tempResults;      //Appling to the temp to offical.
                
                //Indexing offical data counter
                if (counterResults == currentTestSettings.numberOfCorrect - 1)
                {
                    counterResults = 0;
                }
                else
                {
                    counterResults++;
                }

                //Checking to see if the pass condition has been met in all offical packets
                int testCount = getCurrentPassCount();
                

                System.Diagnostics.Debug.WriteLine("Test Results: " + testCount + " : " + sondePassCount + " = " + getPercentageComplete());

                //If the test results matches the the needed amount of the count needed then pass test and break out.
                if (testCount == this.sondePassCount)
                {
                    testResult = true;
                    break;
                }

            }
            //e.Result = testResult; //Passing the results to the runWorkerCompleted.

            testComplete.UpdatingObject = (object)testResult; //Sending out the results of the test.

            //If the test has a cable that needs to be tested then this will disable the testing.
            if (currentRadiosonde != null)
            {
                currentRadiosonde.CalDataUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
            }

            //Cleaning up left over stuff from the test.
            testingEnviroment = null;
            System.Diagnostics.Debug.WriteLine("Test Background Worker Complete.");
        }

        void backgroundWorkerRadiosondeTest_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Background Worker post process started.");
            if (e.Error != null)    //Checking for background worker error.
            {
                testError.UpdatingObject = (object)e.Error;     //Passing error message out to lisenters.
                System.Diagnostics.Debug.WriteLine("Error, Background Worker Errored. " + e.Error.Message);
            }
        }

        //Calibration data received.
        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            System.Diagnostics.Debug.WriteLine("Cal Data Received.");
            //currentRadiosonde.getFirmwareVersion();
            cableDataReady.Set();
            currentRadiosondeData = (Universal_Radiosonde.CalData)data.NewValue;
        }

        void packetReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondePTUReady.Set();
            radiosondeGPSReady.Set();
        }

        void packetPTUXReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondePTUReady.Set();
        }

        void packetGPSXReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondeGPSReady.Set();
        }

        void refTempReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            sensorsTempReady.Set();
        }

        void refHumidityReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            sensorsHumidityReady.Set();
        }

        void decoderPacketDataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            radiosondePTUReady.Set();
            radiosondeGPSReady.Set();
        }

        void readyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            cableDataReady.Set();
        }

        #endregion  



        private double gpsHorzonalDiff(testDataPoint incomingData)
        {
            //GPS Horz differances in meters.
            double EarhtRadius = 6371;
            double DeltaLat = 0;
            double DeltaLong = 0;
            double a = 0;
            double c = 0;
            double differacneGPSHorzRadiosondeToStation = 0;


            //GPS Horz differacnce in meters
            DeltaLat = DegreeToRadian(incomingData.txSondeLat) - DegreeToRadian(currentTestSettings.stationLat);
            DeltaLong = DegreeToRadian(incomingData.txSondeLong) - DegreeToRadian(currentTestSettings.stationLong);

            a = Math.Sin(DeltaLat / 2) * Math.Sin(DeltaLat / 2) +
                    Math.Cos(DegreeToRadian(currentTestSettings.stationLong)) * Math.Cos(DegreeToRadian(incomingData.txSondeLat)) *
                    Math.Sin(DeltaLong / 2) * Math.Sin(DeltaLong / 2);
            c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            differacneGPSHorzRadiosondeToStation = (EarhtRadius * c) * 1000 ;
            return differacneGPSHorzRadiosondeToStation;
        }

        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        /// <summary>
        /// Adding a value to enviromently stability tracking
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="value"></param>
        private void updateElement(string itemName, double value)
        {
            foreach (FinalTest.stabilityElement item in testingEnviroment)
            {
                if (item.desciption == itemName)
                {
                    item.addDataPoint(value);
                }
            }
        }

        /// <summary>
        /// Method for getting the total resultes of the test.
        /// </summary>
        /// <returns></returns>
        public testResults[] getTestDataResults()
        {
            return testDataResults;
        }

        public int getTotalPassCount()
        {
            this.sondePassCount = 4;
            if (currentTestSettings.optionAuxCable) { this.sondePassCount += 1; }
            if (currentTestSettings.optionGPS) { this.sondePassCount += 1; }
            if (currentTestSettings.optionPressure) { this.sondePassCount += 2; }
            this.sondePassCount = this.sondePassCount * currentTestSettings.numberOfCorrect;
            return sondePassCount;
        }

        public int getCurrentPassCount()
        {
            int testCount = 0;
            foreach (testResults tr in testDataResults)  //Going through all the offical test results.
            {
                if (currentTestSettings.optionPressure)     //Checking for pressure test.
                {
                    //Compiling the pressure
                    if (tr.statusPressure) { testCount++; }
                    if (tr.sondePressureStability) { testCount++; }
                }

                //Compileing Air Temp
                if (tr.statusAirTemp) { testCount++; }
                if (tr.sondeAirTempStability) { testCount++; }

                //Compileing the humidity
                if (tr.statusHumidity) { testCount++; }
                if (tr.sondeHumidityStability) { testCount++; }

                //Compileing the GPS options
                if (currentTestSettings.optionGPS)
                {
                    if (tr.statusGPSPOS) { testCount++; }
                }

                //Compiling the AUX Cable
                if (currentTestSettings.optionAuxCable)
                {
                    if (tr.statusCable) { testCount++; }
                }

            }

            return testCount;
        }

        public double getPercentageComplete()
        {
            return Convert.ToDouble(getCurrentPassCount()) / Convert.ToDouble(getTotalPassCount()) * 100;
        }

        /// <summary>
        /// Method gets the current tests aux cable.
        /// </summary>
        /// <returns></returns>
        public Universal_Radiosonde.iMet1UV2 getAuxCable()
        {
            return currentRadiosonde;
        }

        /// <summary>
        /// Method returns the latest test data point.
        /// </summary>
        /// <returns></returns>
        public testDataPoint getLatestDataPoint()
        {
            return latestTestData;
        }

        public DateTime getTimeStarted()
        {
            return timeTestStarted;
        }

    }

    /// <summary>
    /// Setting for a test.
    /// </summary>
    public struct testParameters
    {
        //General Test Parameters
        public int maxTimeToStablize;   //Number of Min to stablize
        public int numberOfCorrect;     //Number of consecutive passing records.
        public bool optionPressure;     //Allowance for radiosondes without pressure
        public bool optionGPS;          //Allowance for radiosondes without GPS.
        public bool optionAuxCable;     //Allowance for radiosondes aux cable.

        //Sensor tolerances
        public double tolPressure;
        public double tolAirTemp;
        public double tolHumidity;
        
        //GPS Station LOC
        public double stationLat;
        public double stationLong;
        public double stationAlt;

        //GPS tolerances
        public double tolHorizonal;
        public double tolVertical;

        //Sensor Stability
        public double MSLrefP;      //Max Stability Referance Pressure
        public double MSLrefT;      //Max Stability Referance Air Temp
        public double MSLrefU;      //Max Stability Referance Humidity
        public double MSLsondeP;    //Max Stability Radiosonde Pressure
        public double MSLsondeT;    //Max Stability Radiosonde Air Temp
        public double MSLsondeU;    //Max Stability Radiosonde Humidity

        public double offsetPressure;   //Offset for referance pressure sensor
        public double offsetAirTemp;    //Offset for referance air temp
        public double offsetHumidity;   //Offset for Humidity

    }

    /// <summary>
    /// Result packet from the radiosonde test.
    /// </summary>
    public struct testResults
    {
        public bool statusPressure;
        public bool statusAirTemp;
        public bool statusHumidity;
        public bool statusGPSPOS;
        public bool statusCable;

        //Stability stuff.
        public bool sondePressureStability;
        public bool sondeAirTempStability;
        public bool sondeHumidityStability;
        public bool refPressureStability;
        public bool refAirTempStability;
        public bool refHumidityStability;
    }

    /// <summary>
    /// Test packet data.
    /// </summary>
    public struct testDataPoint
    {
        //Radiosonde Data
        public int txSondePacketCount;
        public double txSondePressure;
        public double txSondeAirTemp;
        public double txSondeHumidity;
        //Sonde GPS Data
        public double txSondeLat;
        public double txSondeLong;
        public double txSondeAlt;

        //Cable Data
        public double cableSondePressure;
        public double cableSondeAirTemp;
        public double cableSondeHumidity;

        //Referance data
        public double refPressure;
        public double refAirTemp;
        public double refHumidity;

    }

    public class stabilityElement
    {
        //Public items
        public string desciption = "";
        public int stabilityPacketCount;
        public double currentStubilityReading;
        
        //Private items.
        List<stabilityDataPacket> elementData = new List<stabilityDataPacket>();

        public stabilityElement()
        {

        }

        public void stabilityElementSetup(string name, int packetCount)
        {
            this.desciption = name;
            this.stabilityPacketCount = packetCount;
        }

        public void addDataPoint(double incomingData)
        {
            stabilityDataPacket tempPacket = new stabilityDataPacket();
            tempPacket.dataTime = DateTime.Now;
            tempPacket.currentReading = incomingData;


            if (elementData.Count >= stabilityPacketCount * 10)
            {
                elementData.Remove(elementData[0]);
            }

            elementData.Add(tempPacket);
        }

        public double getBoxcarAverage()
        {
            double avg = 0;

            if (elementData.Count >= stabilityPacketCount)
            {
                for (int i = elementData.Count - 1; i > elementData.Count - 11; i--)
                {
                    avg += elementData[i].currentReading;
                }

                return avg / 10;
            }
            else
            {
                return 9999.99;
            }
        }

        /// <summary>
        /// Method returns the differance from the desired time.
        /// </summary>
        /// <param name="sec"></param>
        public double getDiff(int sec)
        {
            if (elementData != null && elementData.Count > 2 + sec)   //Proventing the diff in not enough data.
            {
                double curDiff;
                //Find the closest to the desired packet.
                for (int x = 0; x < elementData.Count; x++)
                {
                    double last = elementData.Last().dataTime.TimeOfDay.TotalSeconds;
                    double current = elementData[x].dataTime.TimeOfDay.TotalSeconds;

                    curDiff = last - current;

                    if (curDiff > sec - 1 && curDiff < sec + 1)
                    {

                        return elementData.Last().currentReading - elementData[x].currentReading;
                    }
                }



            }
            else
            {
                return 9999.99;
            }

            return 9999.99;
        }
        

    }

    struct stabilityDataPacket
    {
        public DateTime dataTime;
        public double currentReading;
    }

}
