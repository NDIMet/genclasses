﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using updateCreater;

namespace thunderComm2
{
    public struct statusData
    {
        public DateTime timeDataReceived;
        public double currentFrostPoint;
        public double currentDewPoint;
        public double currentPPMv;
        public double currentPPMw;
        public double currentRH;
        public double currentSaturPSI;
        public double currentSaturC;
        public double currentTestPSI;
        public double currentTestC;
        public double currentFlowRate;
        public int currentStaus;
    }

    public struct setPointData
    {
        public DateTime timeDataReceived;
        public double setPointFrostPoint;
        public double setPointDewPoint;
        public double setPointPPMv;
        public double setPointPPMw;
        public double setPointRH;
        public double setPointSaturPSI;
        public double setPointSaturC;
        public double setPointTestPSI;
        public double setPointTestC;
        public double setPointFlowRate;
        public int setPointMode;
    }

    public struct errorData
    {
        public int errorNumber;
        public string errorOutput;
    }


    public class equipmentThunder
    {

        //Internetal properties
        //Connection device. Could be serial port of tcp/ip port.
        public object connectionDevice;

        //Background worker for handleing processing data
        System.ComponentModel.BackgroundWorker commandSender = new System.ComponentModel.BackgroundWorker();
        System.ComponentModel.BackgroundWorker internalUpdater = new System.ComponentModel.BackgroundWorker();

        //Auto reset event
        System.Threading.AutoResetEvent dataReceived = new System.Threading.AutoResetEvent(false);
        System.Threading.AutoResetEvent canSendCommand = new System.Threading.AutoResetEvent(false); //Controls flow of commands to device.
        System.Threading.AutoResetEvent commandInProgress = new System.Threading.AutoResetEvent(false);
        
        //Public Update Events
        public objectUpdate dataProcessingComplete = new objectUpdate();
        public objectUpdate errorAlert = new objectUpdate();
        public objectUpdate internetalUpdateComplete = new objectUpdate();

        public updateCreater.objectUpdate statusReady = new objectUpdate();
        public updateCreater.objectUpdate setPointReady = new objectUpdate();

        //Message enders. Might differ from command to command book is unclear about this.
        string outgoingEndOfLine = "\r";
        string incomingEndOfLine = "\r\n";

        //Current Data Packet.
        public string currentRawData = "";

        //Current Command send to thunder
        string currentCommand = "";

        //Data structs
        public statusData statsData = new statusData();
        public setPointData setPointData = new setPointData();
        public errorData errorData = new errorData();

        //Acceptable commands list
        List<string> allowedCommands;

        //Command buffer
        List<string> commandBuffer = new List<string>();

        public equipmentThunder(string comPortName)
        {
            buildErrorCheckList();
            //Composeing the serial port.
            System.IO.Ports.SerialPort comPort = new System.IO.Ports.SerialPort(comPortName, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            comPort.NewLine = incomingEndOfLine;
            //comPort.ReceivedBytesThreshold = 8;
            comPort.ReadTimeout = 30000;

            setupBackgroundWorker(); //Starting background worker to service device.
            //startBackgroundWorker();

            //Opening com port and clearing any left junk the the buffers.
            //This should be moved to own method. Here for testing.
            comPort.Open();

            //Apply the connection to the global device
            connectionDevice = comPort;

            //Setting auto reset so that the processing can start
            canSendCommand.Set();
            commandInProgress.Set();
        }

        /// <summary>
        /// Not supported as of current
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public equipmentThunder(string ipAddress, int port)
        {
            buildErrorCheckList();
            setupBackgroundWorker(); //Starting background worker to service device
        }

        private void buildErrorCheckList()
        {
            //Error checking to make sure that the command is something that the Thunder 3900 can respond to.
            allowedCommands = new List<string>();
            //Setpoint / Data Retrieval Commands
            allowedCommands.Add("?");
            allowedCommands.Add("?DP");
            allowedCommands.Add("?FL");
            allowedCommands.Add("?FP");
            allowedCommands.Add("?PG");
            allowedCommands.Add("?PS");
            allowedCommands.Add("?PT");
            allowedCommands.Add("?PV");
            allowedCommands.Add("?PW");
            allowedCommands.Add("?RH");
            allowedCommands.Add("?SP");
            allowedCommands.Add("?TS");
            allowedCommands.Add("?TT");

            //Action Commands
            allowedCommands.Add("GEN");
            allowedCommands.Add("PRI");
            allowedCommands.Add("PUR");
            allowedCommands.Add("SAV");
            allowedCommands.Add("STO");

            //Setpoint Commands
            allowedCommands.Add("DP=");
            allowedCommands.Add("FL=");
            allowedCommands.Add("FP=");
            allowedCommands.Add("PS=");
            allowedCommands.Add("PT=");
            allowedCommands.Add("PV=");
            allowedCommands.Add("PW=");
            allowedCommands.Add("RH=");
            allowedCommands.Add("TS=");
            allowedCommands.Add("TT=");

            //Utility/Status Commands
            allowedCommands.Add("?CL");
            allowedCommands.Add("CL=");
            allowedCommands.Add("?DA");
            allowedCommands.Add("?DF");
            allowedCommands.Add("?ER");
            allowedCommands.Add("?MW");
            allowedCommands.Add("MW=");
            allowedCommands.Add("?PI");
            allowedCommands.Add("PI=");
            allowedCommands.Add("?PR");
            allowedCommands.Add("PR=");
            allowedCommands.Add("?RU");
            allowedCommands.Add("?TF");
            allowedCommands.Add("?TI");
            allowedCommands.Add("?WM");
            allowedCommands.Add("WM=");
        }

        public void open()
        {
            switch (connectionDevice.GetType().ToString())
            {
                case "System.IO.Ports.SerialPort":
                    System.IO.Ports.SerialPort tempComObject = (System.IO.Ports.SerialPort)connectionDevice;

                    if (!tempComObject.IsOpen)
                    {
                        tempComObject.Open();
                    }

                    break;

            }
        }

        public void dispose()
        {
            stopInternalUpdate();

            switch (connectionDevice.GetType().ToString())
            {
                case "System.IO.Ports.SerialPort":
                    System.IO.Ports.SerialPort tempComObject = (System.IO.Ports.SerialPort)connectionDevice;
                    tempComObject.Close();
                    break;

            }
        }

        private void setOutgoingMessageEnd(string desiredEndOfLine)
        {
            this.outgoingEndOfLine = desiredEndOfLine;
        }

        private void setIncomingMessageEnd(string desiredEndOfLine)
        {
            this.incomingEndOfLine = desiredEndOfLine;
        }

        /// <summary>
        /// Method sends commands out to background work to be sent out when the system can.
        /// </summary>
        /// <param name="message"></param>
        public void sendCommand(string message)
        {
            //Checking to make sure the incoming command fit basic needes to be processed.
            if (message.Length < 3 && message != "?")
            {
                errorAlert.UpdatingObject = (object)("9999,Command not long enough.");
                return;
            }

            if (message != "?")
            {
                if (!allowedCommands.Contains(message.Substring(0, 3)))
                {
                    errorAlert.UpdatingObject = (object)("9999,Command not supported");
                    return;
                }
            }


            //Adding command to que
            commandBuffer.Add(message);

            //Starting command sender if not runnning.
            if (!commandSender.IsBusy)
            {
                commandSender.RunWorkerAsync();
            }
            
        }

        private void sendMessage(string message)
        {
            //storing command in memeory. This will be used in processing the data later.
            currentCommand = message;

            //Sending out message.
            switch (connectionDevice.GetType().ToString())
            {
                case "System.IO.Ports.SerialPort":
                    System.IO.Ports.SerialPort currentPort = (System.IO.Ports.SerialPort)connectionDevice;

                    if (currentPort.IsOpen) //Checking to make sure the comport is open.
                    {
                        string finalMessage = message + this.outgoingEndOfLine;
                        currentPort.Write(finalMessage);
                    }
                    else
                    {
                        throw new Exception("Thunder Com Port is not open.");
                    }
                    
                    break;
            }

            //Receiving the responce.
            string incomingData = ""; //Local data

            switch (connectionDevice.GetType().ToString())
            {
                case "System.IO.Ports.SerialPort":
                    System.IO.Ports.SerialPort tempComObject = (System.IO.Ports.SerialPort)connectionDevice;
                
                    //Trying to read the line from the serial port if unable purge in buffer reset the command send options and que error
                    try
                    {
                        //Console.WriteLine("Buffer has: " + tempComObject.BytesToRead + " Bytes in buffer");
                        incomingData = tempComObject.ReadLine();
                    }
                    catch (Exception comError)
                    {
                        errorAlert.UpdatingObject = (object)("9999," + currentCommand + "," + comError.Message);
                        System.Threading.Thread.Sleep(1000);
                        if (tempComObject.IsOpen)
                        {
                            incomingData = tempComObject.ReadExisting();
                        }
                        currentCommand = "error";
                        break;
                    }
                    currentRawData = incomingData; //Sending data to global for external processing.

                    break;
            }

            processData(incomingData); //Sending data out to be processed.

            //Compleing data and triggering event stating that new data is ready.
            object[] outputRaw = new object[2];
            outputRaw[0] = currentCommand;
            outputRaw[1] = incomingData;

            dataProcessingComplete.UpdatingObject = (object)outputRaw;
            canSendCommand.Set(); //Command has been processed and it is now ok to send the next command.
            commandInProgress.Set(); //Allowing the next command to be sent.
        }

        #region Background worker methods

        public void startInternalUpdate()
        {
            internalUpdater.RunWorkerAsync();
        }

        public void stopInternalUpdate()
        {
            internalUpdater.CancelAsync();
        }


        private void setupBackgroundWorker()
        {

            //Command sender
            commandSender.WorkerSupportsCancellation = true;
            commandSender.DoWork += new System.ComponentModel.DoWorkEventHandler(commandSender_DoWork);

            //Internal Data Updater
            internalUpdater.WorkerSupportsCancellation = true;
            internalUpdater.DoWork += new System.ComponentModel.DoWorkEventHandler(internalUpdater_DoWork);


        }

        private void startBackgroundWorker()
        {

        }

        private void stopBackgroundWork()
        {
            commandSender.CancelAsync();
        }

        private void processData(string incomingData)
        {
            //Processing out the data received.
            string[] incomingDataElements;

            switch (currentCommand)
            {
                case "?":
                    incomingDataElements = incomingData.Split(',');

                    if (incomingDataElements.Length == 11)
                    {
                        statsData.timeDataReceived = DateTime.Now;
                        statsData.currentFrostPoint = Convert.ToDouble(incomingDataElements[0]);
                        statsData.currentDewPoint = Convert.ToDouble(incomingDataElements[1]);
                        statsData.currentPPMv = Convert.ToDouble(incomingDataElements[2]);
                        statsData.currentPPMw = Convert.ToDouble(incomingDataElements[3]);
                        statsData.currentRH = Convert.ToDouble(incomingDataElements[4]);
                        statsData.currentSaturPSI = Convert.ToDouble(incomingDataElements[5]);
                        statsData.currentSaturC = Convert.ToDouble(incomingDataElements[6]);
                        statsData.currentTestPSI = Convert.ToDouble(incomingDataElements[7]);
                        statsData.currentTestC = Convert.ToDouble(incomingDataElements[8]);
                        statsData.currentFlowRate = Convert.ToDouble(incomingDataElements[9]);
                        statsData.currentStaus = Convert.ToInt16(incomingDataElements[10]);

                        statusReady.UpdatingObject = statsData;
                    }
                    else
                    {
                        errorAlert.UpdatingObject = "9999," + currentCommand + "," + incomingData + "Data Read Error. Aborting Command";
                    }
                    break;

                case "?SP":
                    incomingDataElements = incomingData.Split(',');

                    if(incomingDataElements.Length == 11)
                    {
                        setPointData.timeDataReceived = DateTime.Now;
                        setPointData.setPointFrostPoint = Convert.ToDouble(incomingDataElements[0]);
                        setPointData.setPointDewPoint = Convert.ToDouble(incomingDataElements[1]);
                        setPointData.setPointPPMv = Convert.ToDouble(incomingDataElements[2]);
                        setPointData.setPointPPMw = Convert.ToDouble(incomingDataElements[3]);
                        setPointData.setPointRH = Convert.ToDouble(incomingDataElements[4]);
                        setPointData.setPointSaturPSI = Convert.ToDouble(incomingDataElements[5]);
                        setPointData.setPointSaturC = Convert.ToDouble(incomingDataElements[6]);
                        setPointData.setPointTestPSI = Convert.ToDouble(incomingDataElements[7]);
                        setPointData.setPointTestC = Convert.ToDouble(incomingDataElements[8]);
                        setPointData.setPointFlowRate = Convert.ToDouble(incomingDataElements[9]);
                        setPointData.setPointMode = Convert.ToInt16(incomingDataElements[10]);

                        setPointReady.UpdatingObject = setPointData;
                    }
                    else
                    {
                        errorAlert.UpdatingObject = "9999," + currentCommand + "," + incomingData + "Data Read Error. Aborting Command";
                    }
                    break;

                case "?ER":

                    if (incomingData.Length == 3)
                    {
                        errorData.errorNumber = Convert.ToInt16(incomingData);

                        //Processing the string output of the error.
                        switch (errorData.errorNumber)
                        {
                            case 0:
                                errorData.errorOutput = "No Error.";
                                break;

                            case 1:
                                errorData.errorOutput = "Expansion Valve Not Closing.";
                                break;

                            case 2:
                                errorData.errorOutput = "Flow Valve Not Closing.";
                                break;

                            case 4:
                                errorData.errorOutput = "Low Supply Pressure.";
                                break;

                            case 8:
                                errorData.errorOutput = "Cabinet Temperature Overrange.";
                                break;

                            case 32:
                                errorData.errorOutput = "Referance Temperature Underrange.";
                                break;

                            case 48:
                                errorData.errorOutput = "Refreance Temperature Overrange.";
                                break;

                            case 64:
                                errorData.errorOutput = "Test Temperature Underrange.";
                                break;

                            case 80:
                                errorData.errorOutput = "Test Temperature Overrange.";
                                break;

                            case 128:
                                errorData.errorOutput = "Saturation Temperature Underrange.";
                                break;

                            case 144:
                                errorData.errorOutput = "Saturation Temperature Overrange.";
                                break;

                            case 512:
                                errorData.errorOutput = "Test Pressure Underrange.";
                                break;

                            case 768:
                                errorData.errorOutput = "Test Pressure Overrange.";
                                break;

                            case 1024:
                                errorData.errorOutput = "Low Range Saturation Pressure Underrange.";
                                break;

                            case 1280:
                                errorData.errorOutput = "Low Range Saturation Pressure Overrange.";
                                break;

                            case 2048:
                                errorData.errorOutput = "High Range Saturation Pressure Underrange.";
                                break;

                            case 2304:
                                errorData.errorOutput = "High Range Saturation Pressure Overrange.";
                                break;

                            case 9999:
                                errorData.errorOutput = "Data Error. String index error.";
                                break;
                        }

                        //Sending out the alert if an error has happen.
                        if (errorData.errorNumber > 0)
                        {
                            errorAlert.UpdatingObject = (object)(errorData.errorNumber.ToString() + "," + errorData.errorOutput);
                        }
                    }

                    else
                    {
                        errorAlert.UpdatingObject = "9999," + currentCommand + "," + incomingData + "Data Read Error. Aborting Command";
                    }

                    break;
            }
        }


        void comPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //System.Threading.Thread.Sleep(50);
            dataReceived.Set();
        }

        /// <summary>
        /// This background worker will send the commands out. This is done to create a buffer. Might not need this.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commandSender_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (commandBuffer.Count != 0 && !commandSender.CancellationPending)
            {
                string currentCommandProcessing = commandBuffer.First<string>();
                sendMessage(currentCommandProcessing);
                System.Threading.Thread.Sleep(250);
                commandBuffer.Remove(currentCommandProcessing);
            }
        }

        #endregion

        #region Interanl data update methods. These will collect data in the background so that the must up to data data is avaiable.

        void internalUpdater_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Setting the moitor to background. I in no way want this to run off if it has nothing to do.
            System.Threading.Thread.CurrentThread.IsBackground = true;

            //List of the normal commands that will run in the background when nothing is processing.
            List<string> monitorCommands = new List<string>();
            monitorCommands.Add("?");
            monitorCommands.Add("?SP");
            //monitorCommands.Add("?ER"); doesn't seem to update when error happend

            while (!internalUpdater.CancellationPending)
            {
                foreach (string command in monitorCommands)
                {
                    //canSendCommand.WaitOne();
                    //System.Threading.Thread.Sleep(100); //A little pause to see if that stops the error when changing modes.
                    sendCommand(command);
                }

                //Firing the event that internal update is complete
                internetalUpdateComplete.UpdatingObject = (object)DateTime.Now;

                //Pause the thread for a longer time if the buffer is greater then 10 count. This should keep the buffer from gettting full while waiting for a
                //mode change.
                if (commandBuffer.Count > 10)
                {
                    System.Threading.Thread.Sleep(10000);
                }
                else
                {
                    System.Threading.Thread.Sleep(2000);
                }
            }
        }

        #endregion


        #region Methods for getting and settings

        /*
        public void getSetPoints()
        {
            sendCommand("?SP");
        }

        public void getStatus()
        {
            sendCommand("?");
        }

        public void getError()
        {
            sendCommand("?ER");
        }
        */

        //setters for setting.
        public void setSaturationTemp(double desiredSatuationTemp)
        {
            sendCommand("TS=" + desiredSatuationTemp.ToString());
        }

        public void setSaturationPressure(double desiredSaturationPressure)
        {
            sendCommand("PS=" + desiredSaturationPressure.ToString());
        }

        public void setFlowRate(double desiredFlowRate)
        {
            sendCommand("FL=" + desiredFlowRate.ToString());
        }

        //Setters for modes.
        public void systemPurge()
        {
            sendCommand("PUR");
        }

        public void systemGenerate()
        {
            sendCommand("GEN");
        }

        public void systemStop()
        {
            sendCommand("STO");
        }

        #endregion



    }

    //Created by William Jones 01/02/2014 For connecting to the Thunder 3900 via a network IP address and port.
    //The network connection doen't seem to work.... Not sure why.
    //Seems that telnet connections CR add in a HEX 00 for no reason I can tell. Might need to just send bytes.
    public class equipmentThunderV2
    {
        //Data containers.
        string tempDataBuffer;  //Buffer for dumping data into.
        List<string> cmdBuffer = new List<string>();    //Buffer to hold cmds to be sent to equipment
        List<string> cmdOutBuffer = new List<string>(); //Buffer of commands sent out.
        List<dataPacket> recBuffer = new List<dataPacket>();    //Buffer to hold returned responces from equipemnt.
        public statusData currentStatus;        //Current status of the thunder.
        public setPointData currentSetPoint;    //Current set points of the thunder.
        List<string> allowedCommands = new List<string>();  //List of allowable commands.

        //Data connection elements
        TcpSimpleClient.simpleTcpClient tcpClient;


        //Update objects
        public updateCreater.objectUpdate dataPacketRecived = new objectUpdate();
        public updateCreater.objectUpdate readyStatus = new objectUpdate();
        public updateCreater.objectUpdate readySetPoint = new objectUpdate();
        public updateCreater.objectUpdate error = new objectUpdate();

        //Threading elements
        public System.Threading.AutoResetEvent waitForSocketData = new System.Threading.AutoResetEvent(false);  //Something to control the flow of the sensor data.
        System.Threading.Thread processRecData;
        System.Threading.Thread collectStatusSP;
        bool suspendComs = false;

        public equipmentThunderV2()
        {
        }

        public equipmentThunderV2(string ipAddress, int port)
        {
            tcpClient = new TcpSimpleClient.simpleTcpClient();
            tcpClient.simpleClientSetupTCP(ipAddress, port);
            tcpClient.recivedMessage.PropertyChange += new objectUpdate.PropertyChangeHandler(recivedMessage_PropertyChange);
            startInternalProcess();
        }

        public void startCollect()
        {
            collectStatusSP = new System.Threading.Thread(collectSTSP);
            collectStatusSP.IsBackground = true;
            collectStatusSP.Name = "collectThunderSPST";
            collectStatusSP.Start();
        }

        private void startInternalProcess()
        {

            //Filling the stuct with starter data.
            currentSetPoint = new setPointData();
            currentSetPoint.setPointDewPoint = 0;
            currentSetPoint.setPointFlowRate = 0;
            currentSetPoint.setPointFrostPoint = 0;
            currentSetPoint.setPointMode = 0;
            currentSetPoint.setPointPPMv = 0;
            currentSetPoint.setPointPPMw = 0;
            currentSetPoint.setPointRH = 0;
            currentSetPoint.setPointSaturC = 0;
            currentSetPoint.setPointSaturPSI = 0;
            currentSetPoint.setPointTestC = 0;
            currentSetPoint.setPointTestPSI = 0;
            currentSetPoint.timeDataReceived = DateTime.Now;

            currentStatus = new statusData();
            currentStatus.currentDewPoint = 0;
            currentStatus.currentFlowRate = 0;
            currentStatus.currentFrostPoint = 0;
            currentStatus.currentPPMv = 0;
            currentStatus.currentPPMw = 0;
            currentStatus.currentRH = 0;
            currentStatus.currentSaturC = 0;
            currentStatus.currentSaturPSI = 0;
            currentStatus.currentStaus = 0;
            currentStatus.currentTestC = 0;
            currentStatus.currentTestPSI = 0;
            currentStatus.timeDataReceived = DateTime.Now;



            if (processRecData == null)
            {
                processRecData = new System.Threading.Thread(processIncomingData);
                processRecData.Name = "ThunderProcessor";
                processRecData.IsBackground = true;
                processRecData.Start();

            }
            else
            {
                throw new Exception("Thread already started");
            }

            buildErrorCheckList();
        }

        private void buildErrorCheckList()
        {
            //Error checking to make sure that the command is something that the Thunder 3900 can respond to.
            allowedCommands = new List<string>();
            //Setpoint / Data Retrieval Commands
            allowedCommands.Add("?");
            allowedCommands.Add("?DP");
            allowedCommands.Add("?FL");
            allowedCommands.Add("?FP");
            allowedCommands.Add("?PG");
            allowedCommands.Add("?PS");
            allowedCommands.Add("?PT");
            allowedCommands.Add("?PV");
            allowedCommands.Add("?PW");
            allowedCommands.Add("?RH");
            allowedCommands.Add("?SP");
            allowedCommands.Add("?TS");
            allowedCommands.Add("?TT");

            //Action Commands
            allowedCommands.Add("GEN");
            allowedCommands.Add("PRI");
            allowedCommands.Add("PUR");
            allowedCommands.Add("SAV");
            allowedCommands.Add("STO");

            //Setpoint Commands
            allowedCommands.Add("DP=");
            allowedCommands.Add("FL=");
            allowedCommands.Add("FP=");
            allowedCommands.Add("PS=");
            allowedCommands.Add("PT=");
            allowedCommands.Add("PV=");
            allowedCommands.Add("PW=");
            allowedCommands.Add("RH=");
            allowedCommands.Add("TS=");
            allowedCommands.Add("TT=");

            //Utility/Status Commands
            allowedCommands.Add("?CL");
            allowedCommands.Add("CL=");
            allowedCommands.Add("?DA");
            allowedCommands.Add("?DF");
            allowedCommands.Add("?ER");
            allowedCommands.Add("?MW");
            allowedCommands.Add("MW=");
            allowedCommands.Add("?PI");
            allowedCommands.Add("PI=");
            allowedCommands.Add("?PR");
            allowedCommands.Add("PR=");
            allowedCommands.Add("?RU");
            allowedCommands.Add("?TF");
            allowedCommands.Add("?TI");
            allowedCommands.Add("?WM");
            allowedCommands.Add("WM=");
        }

        void recivedMessage_PropertyChange(object sender, PropertyChangeEventArgs data)
        {
            DateTime timeDataRec = new DateTime();
            timeDataRec = DateTime.Now;

            //End of message chars
            char[] EOM = new char[2] { '\r', '\n' };

            //Turing the byte data into a message
            byte[] incomingData = (byte[])data.NewValue;
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            //Removing addiional null data bytes.
            string str = enc.GetString(incomingData).Trim('\0');

            //Check for a complete data packet. If not a complete packet then place the data into the tempbuffer then wait for the next chunk.

            if (!str.Contains("\r\n"))
            {
                tempDataBuffer += str;
                return;
            }

            if (str.Contains("\r\n"))
            {
                str = tempDataBuffer + str;
                tempDataBuffer = "";

                //Checking to see if the last part of the message is the end of a line. If not add it back into the buffer for more processing.
                bool endOfLine = false;
                if (str.Substring(str.Length - 2, 2) == "\r\n")
                {
                    endOfLine = true;
                }

                //Checking to see if there is more data past the \r\n
                string[] breakingDown = str.Split(EOM);

                if (!endOfLine)
                {
                    tempDataBuffer = breakingDown[breakingDown.Length - 1];
                }


                for (int s = 0; s < breakingDown.Length; s++)
                {
                    if (breakingDown[s] != "" && s < breakingDown.Length - 2)
                    {
                        dataPacket currentData = new dataPacket();
                        currentData.packetDate = timeDataRec;
                        currentData.packetMessage = breakingDown[s] + "\r\n";
                        recBuffer.Add(currentData);     //Adding incoming packet to list to be processed.
                        System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " - thunderData: " + currentData.packetMessage.Trim());

                        dataPacketRecived.UpdatingObject = (object)currentData; //Sending out the update that a data packet has been received.

                    }
                }

                waitForSocketData.Set();    //letting processing happen.

            }
        }

        public void sendCommand(string command)
        {
            foreach (string cmd in allowedCommands) //Error checking to make sure bad commands are not passed to the system.
            {
                if(command.Contains(cmd))
                {
                    cmdBuffer.Add(command);
                    break;
                }
            }
            
        }

        private void collectSTSP()
        {
            while (true)
            {
                if (!suspendComs)
                {
                    if (!cmdBuffer.Contains("?"))
                    {
                        sendCommand("?");
                    }

                    System.Threading.Thread.Sleep(1500);

                    if (!cmdBuffer.Contains("?SP"))
                    {
                        sendCommand("?SP");
                    }
                    System.Threading.Thread.Sleep(1500);
                }
                else
                {
                    System.Threading.Thread.Sleep(60000);
                    recBuffer.Clear();
                    cmdBuffer.Clear();
                    cmdOutBuffer.Clear();
                    suspendComs = false;
                }
            }
        }

        //Thread for Sorting incoming Data
        private void processIncomingData()
        {
            string currentCommand = ""; //Something to hold the current command being processed.

            while (true)
            {
                string local = "";
                if (cmdBuffer.Count > 0)
                {
                    local = cmdBuffer[0];
                    cmdBuffer.RemoveAt(0);

                    if (local.Substring(local.Length - 1, 1) != "\r")
                    {
                        local += "\r";
                    }

                    tcpClient.clientWriteMessageTCP(local);
                    
                    
                    currentCommand = local;
                    if (local == "?\r" || local == "?SP\r")
                    {
                        cmdOutBuffer.Add(local);
                        waitForSocketData.WaitOne(10000);
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(3000);
                        cmdOutBuffer.Clear();
                        recBuffer.Clear();
                        cmdBuffer.Clear();
                    }
                   

                }

                if (recBuffer.Count > 0)    //If there is data to process goto it.
                {
                    /*
                    if (!waitForSocketData.WaitOne(3000) && currentCommand != "")  //Waiting for data to come in.
                    {
                        System.Diagnostics.Debug.WriteLine("10 Seconds pass and no new data trying again.");
                    }
                    else
                    {
                     */
                        dataPacket toProcess = recBuffer[0];    //Pulling data from stack.
                        recBuffer.RemoveAt(0);  //Removing data from stack.

                        currentCommand = cmdOutBuffer[0];
                        cmdOutBuffer.RemoveAt(0);

                        string[] incomingDataElements = toProcess.packetMessage.Split(',');

                        //Starting to process data.
                        switch (currentCommand)
                        {
                            case "?\r":

                                currentStatus.timeDataReceived = DateTime.Now;
                                currentStatus.currentFrostPoint = Convert.ToDouble(incomingDataElements[0]);
                                currentStatus.currentDewPoint = Convert.ToDouble(incomingDataElements[1]);
                                currentStatus.currentPPMv = Convert.ToDouble(incomingDataElements[2]);
                                currentStatus.currentPPMw = Convert.ToDouble(incomingDataElements[3]);
                                currentStatus.currentRH = Convert.ToDouble(incomingDataElements[4]);
                                currentStatus.currentSaturPSI = Convert.ToDouble(incomingDataElements[5]);
                                currentStatus.currentSaturC = Convert.ToDouble(incomingDataElements[6]);
                                currentStatus.currentTestPSI = Convert.ToDouble(incomingDataElements[7]);
                                currentStatus.currentTestC = Convert.ToDouble(incomingDataElements[8]);
                                currentStatus.currentFlowRate = Convert.ToDouble(incomingDataElements[9]);
                                currentStatus.currentStaus = Convert.ToInt16(incomingDataElements[10]);

                                readyStatus.UpdatingObject = currentStatus;

                                break;

                            case "?SP\r":
                                currentSetPoint.timeDataReceived = DateTime.Now;
                                currentSetPoint.setPointFrostPoint = Convert.ToDouble(incomingDataElements[0]);
                                currentSetPoint.setPointDewPoint = Convert.ToDouble(incomingDataElements[1]);
                                currentSetPoint.setPointPPMv = Convert.ToDouble(incomingDataElements[2]);
                                currentSetPoint.setPointPPMw = Convert.ToDouble(incomingDataElements[3]);
                                currentSetPoint.setPointRH = Convert.ToDouble(incomingDataElements[4]);
                                currentSetPoint.setPointSaturPSI = Convert.ToDouble(incomingDataElements[5]);
                                currentSetPoint.setPointSaturC = Convert.ToDouble(incomingDataElements[6]);
                                currentSetPoint.setPointTestPSI = Convert.ToDouble(incomingDataElements[7]);
                                currentSetPoint.setPointTestC = Convert.ToDouble(incomingDataElements[8]);
                                currentSetPoint.setPointFlowRate = Convert.ToDouble(incomingDataElements[9]);
                                currentSetPoint.setPointMode = Convert.ToInt16(incomingDataElements[10]);

                                readySetPoint.UpdatingObject = currentSetPoint;

                                break;

                            case "?ER\r":
                                int errorNumber = Convert.ToInt16(incomingDataElements[0]);
                                string errorOutput = "";
                                //Processing the string output of the error.
                                switch (errorNumber)
                                {
                                    case 0:
                                        errorOutput = "No Error.";
                                        break;

                                    case 1:
                                        errorOutput = "Expansion Valve Not Closing.";
                                        break;

                                    case 2:
                                        errorOutput = "Flow Valve Not Closing.";
                                        break;

                                    case 4:
                                        errorOutput = "Low Supply Pressure.";
                                        break;

                                    case 8:
                                        errorOutput = "Cabinet Temperature Overrange.";
                                        break;

                                    case 32:
                                        errorOutput = "Referance Temperature Underrange.";
                                        break;

                                    case 48:
                                        errorOutput = "Refreance Temperature Overrange.";
                                        break;

                                    case 64:
                                        errorOutput = "Test Temperature Underrange.";
                                        break;

                                    case 80:
                                        errorOutput = "Test Temperature Overrange.";
                                        break;

                                    case 128:
                                        errorOutput = "Saturation Temperature Underrange.";
                                        break;

                                    case 144:
                                        errorOutput = "Saturation Temperature Overrange.";
                                        break;

                                    case 512:
                                        errorOutput = "Test Pressure Underrange.";
                                        break;

                                    case 768:
                                        errorOutput = "Test Pressure Overrange.";
                                        break;

                                    case 1024:
                                        errorOutput = "Low Range Saturation Pressure Underrange.";
                                        break;

                                    case 1280:
                                        errorOutput = "Low Range Saturation Pressure Overrange.";
                                        break;

                                    case 2048:
                                        errorOutput = "High Range Saturation Pressure Underrange.";
                                        break;

                                    case 2304:
                                        errorOutput = "High Range Saturation Pressure Overrange.";
                                        break;

                                    case 9999:
                                        errorOutput = "Data Error. String index error.";
                                        break;
                                }

                                //Sending out the alert if an error has happen.
                                if (errorNumber > 0)
                                {
                                    error.UpdatingObject = (object)(errorNumber.ToString() + "," + errorOutput);
                                }

                                break;
                        }

                        currentCommand = "";    //Cleaning out left over data.
                    //}


                }
                else
                {
                    System.Threading.Thread.Sleep(500); //If not sleep till there is.
                }
            }
        }

        #region Methods for getting and settings

        //setters for setting.
        public void setSaturationTemp(double desiredSatuationTemp)
        {
            sendCommand("TS=" + desiredSatuationTemp.ToString());
        }

        public void setSaturationPressure(double desiredSaturationPressure)
        {
            sendCommand("PS=" + desiredSaturationPressure.ToString());
        }

        public void setFlowRate(double desiredFlowRate)
        {
            sendCommand("FL=" + desiredFlowRate.ToString());
        }

        public void setRH(double desiredRH)
        {
            sendCommand("RH=" + desiredRH.ToString());
        }

        //Setters for modes.
        public void systemPurge()
        {
            sendCommand("PUR");
            suspendComs = true;
        }

        public void systemGenerate()
        {
            sendCommand("GEN");
            suspendComs = true;
        }

        public void systemStop()
        {
            sendCommand("STO");
            suspendComs = true;
        }

        #endregion

    }

    public class dataPacket
    {
        public DateTime packetDate;
        public string packetMessage;

    }
}
