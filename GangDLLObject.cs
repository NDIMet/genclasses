﻿/// Written by William Jones 8/30/2011
/// Controll interface for the MSP-GANG430 using Dll included with gang programmer v1.54
///

#define F_ERASE_INFO

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;


namespace GangProgrammerDLL
{
    public class GangDLLWork
    {

        public enum loadParameters
        {
            F_ERASE_INFO = 0x0001,
            F_ERASE_MAIN = 0x0002,
            F_ERASE_PINFO = 0x0004,
            F_ERASE_MASS = 0x0007,

            F_PROGRAM_INFO = 0x0008,
            F_PROGRAM_MAIN = 0x0010,
            F_PROGRAM_MASS = 0x0018,
            F_VERIFY = 0x0020,
            F_SECURE_DEVICE = 0x0040,
            F_PROGRAM_RAM = 0x0080,

            F_ERASE_CHECK_INFO = 0x0100,
            F_ERASE_CHECK_MAIN = 0x0200,
            F_ERASE_CHECK_PINFO = 0x0400,
            F_ERASE_CHECK_MASS = 0x0700,

            F_USE_SBW = 0x0800,

            F_JTAG_SPEED1 = 0x0000,
            F_JTAG_SPEED2 = 0x1000,

            F_VERIFY_MARGINAL = 0x4000,
            F_ERASE_BSL = 0x010000,
            F_ERASE_CHECK_BSL = 0x020000,
            F_PROGRAM_BSL = 0x040000
        }

        public GangDLLWork() { }
        

        //Importing Dll functions
        [DllImport("GANG430.dll")]
        public static extern int InitCom(string lpszComPort, int lBaudRate);

        [DllImport("GANG430.dll")]
        public static extern int GangMainProcess(int lTimeout);

        [DllImport("GANG430.dll")]
        public static extern int ReleaseCom();

        [DllImport("GANG430.dll")]
        public static extern int GangSelectImage(int lImage);

        [DllImport("GANG430.dll")]
        public static extern int GangLoadImage(string lpszFileName, string lpszDeviceName);

        [DllImport("GANG430.dll")]
        public static extern int GangEraseImage();

        [DllImport("GANG430.dll")]
        public static extern int GangLoadParameters(int lFlags, int lSupply, int lVccSettleTime);

        [DllImport("GANG430.dll")]
        public static extern int GangGetResult(string desiredReturn);

        private string comPort = "COM5";
        private int baudRate = 115200;
        private int mainProcessTimeOut = 150;
        private int supplyVoltage = 30;
        private int settleTimeVcc = 200;

        
        #region Command for controlling the programmer

        /// <summary>
        /// Method setups the connection to the programmer
        /// </summary>
        /// <returns>returns 0 if successful</returns>
        public int InitCom()
        {
            return InitCom(comPort,baudRate);
        }

        /// <summary>
        /// Methods releases the connection to the gang programmer.
        /// </summary>
        /// <returns></returns>
        public int releaseCom()
        {
            return ReleaseCom();
        }

        /// <summary>
        /// Method starts the main programming process
        /// </summary>
        /// <returns></returns>
        public int commandStart()
        {
            return GangMainProcess(mainProcessTimeOut);
        }

        /// <summary>
        /// Method allows for the selection of either internal programmer flash memory
        /// </summary>
        /// <param name="desiredImage"></param>
        /// <returns></returns>
        public int commandSelectGangeImage(int desiredImage)
        {
            if (desiredImage < 0 || desiredImage > 1)
            {
                return 9;
            }
            return GangSelectImage(desiredImage);
        }

        /// <summary>
        /// Method loads a intel-hex, or intel-txt file. Requires complete file path and name, and MSP430 device.
        /// </summary>
        /// <param name="desiredFileName"></param>
        /// <param name="desiredDeviceName"></param>
        /// <returns></returns>
        public int commandGangLoadImage(string desiredFileName, string desiredDeviceName)
        {
            return GangLoadImage(desiredFileName, desiredDeviceName);
        }

        /// <summary>
        /// Method erases internal flash memory
        /// </summary>
        /// <returns></returns>
        public int commandGangEraseImage()
        {
            return GangEraseImage();
        }

        /// <summary>
        /// Method sets the Parameters of the programming
        /// </summary>
        /// <param name="desiredProgrammerSettings"></param>
        /// <returns></returns>
        public int commandGangLoadParameters(int desiredProgrammerSettings)
        {
            return GangLoadParameters(desiredProgrammerSettings, supplyVoltage, settleTimeVcc);
        }

        /// <summary>
        /// Method returns the results of the last programming cycle. Doesn't currently work.
        /// </summary>
        /// <returns></returns>
        public int commandGangGetResult()
        {
            return GangGetResult("D02");
        }

        /// <summary>
        /// Method takes the error code issed from the gang programmer and returns a string describing the command.
        /// </summary>
        /// <param name="responceToProcess"></param>
        /// <returns></returns>
        public string programmerReport(int responceToProcess)
        {
            switch (responceToProcess)
            {
                case 0:
                    return "Operation successful";

                case 1:
                    return "Communication − Frame has errors";

                case 2:
                    return "Unable to open COM port − already in use?";

                case 3:
                    return "Unable to close COM port";

                case 4:
                    return "Unable to modify COM port state";

                case 5:
                    return "Synchronization failed. Programmer connected?";

                case 6:
                    return "Timeout during operation − Correct COM port selected?";

                case 7:
                    return "Command did not complete correctly";

                case 8:
                    return "Command failed or not defined or Target not accessible";

                case 9:
                    return "Wrong baud rate specified";

                case 10:
                    return "Could not read GANG430.ini";

                case 11:
                    return "File contains invalid record";

                case 12:
                    return "Unexpected end of file";

                case 13:
                    return "Error during file I/O";

                case 14:
                    return "Selected file is of unrecognizable format";

                case 15:
                    return "Unable to open file";

                case 16:
                    return "Function argument(s) out of range";

                case 31:
                    return "Image Memory corrupted or erased";

                case 32:
                    return "Self test − No JTAG access to Image Buffer device";

                case 33:
                    return "Self test − Data connections to Image Buffer device invalid";

                case 34:
                    return "Self test − No access to one ore more Image Buffer devices";

                case 35:
                    return "Self test − No JTAG access to one or more Target channels";

                case 36:
                    return "Self test − Target Voltage Generator (VCCT) does not work properly";

                case 37:
                    return "Self test − System Voltage (VCC) not in range";

                case 38:
                    return "Self test − Blow Fuse Voltage (VPP) not in range";

                case 39:
                    return "Target not accessible";

                case 40:
                    return "Verification failed";

                case 41:
                    return "Main Process Parameters not yet set";

                case 42:
                    return "Could not erase Image Buffer";

                case 43:
                    return "Could not load Image Buffer";

                case 44:
                    return "Could not load Main Process Parameters";

                case 45:
                    return "Could not select Baud Rate";

                case 46:
                    return "Could not set target voltage (VCCT) − Short circuit or settling time too short";

                case 47:
                    return "Invalid firmware command";

                case 48:
                    return "Power supply voltage too low";

                case 49:
                    return "Sense voltage out of range − Check pin MSP_VCC_IN";

                case 50:
                    return "Wrong target device connected (target doesn’t match selected device)";

                case 51:
                    return "Image doesn’t fit memory model of selected device.";

                case 52:
                    return "No target device connected";

                case 53:
                    return "Code data saved to already used location (overwritten). Check code file_1 and file_2 if contains data in the same locations.";
            }
            return "error";
        }

        #endregion

        #region Setters and Getters

        public void setComPort(string desiredComPort)
        {
            comPort = desiredComPort;
        }

        public string getComPort()
        {
            return comPort;
        }

        public void setBaudRate(int desiredBaudRate)
        {
            baudRate = desiredBaudRate;
        }

        public int getBaudRate()
        {
            return baudRate;
        }

        public void setMainProcessTimeOut(int desiredTimeOut)
        {
            mainProcessTimeOut = desiredTimeOut;
        }

        public int getMainProcessTimeOut()
        {
            return mainProcessTimeOut;
        }

        public void setSupplyVoltage(int desiredSupplyVoltage)
        {
            supplyVoltage = desiredSupplyVoltage;
        }

        public int getSupplyVoltage()
        {
            return supplyVoltage;
        }

        public void setSettleTimeVcc(int desiredSettleTimeVcc)
        {
            settleTimeVcc = desiredSettleTimeVcc;
        }

        public int getSettleTimeVcc()
        {
            return settleTimeVcc;
        }

        #endregion

    }
}
