﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.ComponentModel;



namespace ServersTelnet
{
    #region Telnet Server

    public class serverTelnet
    {

        public void Server()
        { }

        //Internal objects

        //Array lists of the clients attached.
        //private System.Collections.ArrayList onlineClients = new System.Collections.ArrayList();
        private List<System.Net.Sockets.TcpClient> onlineClients = new List<TcpClient>();

        //List array of the clients threads.
        private List<System.Threading.Thread> handleClientThreads = new List<Thread>();

        private TcpListener tcpListener;
        private Thread listenThread;

        private int byteBufferSize = 4096;

        private bool binaryMode = false;

        //private TcpClient tcpClient;
        //private NetworkStream clientStream;

        //Fired when a server command has been received from the client.
        public Product incomingCommands = new Product();

        //Fired when a client connects to the socket server.
        public Product clientConnected = new Product();

        //Fired when binary data has been received from client
        public updateCreater.objectUpdate dataReceived = new updateCreater.objectUpdate();

        //Error Event.
        public updateCreater.objectUpdate serverError = new updateCreater.objectUpdate();

        //bool to control the server running.
        bool runServer = true;

        List<string> acceptableIP = new List<string>();

        public void Server(int port)
        {

            this.tcpListener = new TcpListener(IPAddress.Any, port);
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.IsBackground = true;
            this.listenThread.Start();

        }

        /// <summary>
        /// Method from changing the expecting incoming data from strings to binary.
        /// </summary>
        /// <param name="binary"></param>
        public void setBinayMode(bool binary)
        {
            binaryMode = binary;
        }

        /// <summary>
        /// Method for adding acceptable incoming ipaddress.
        /// </summary>
        /// <param name="address"></param>
        public void addAcceptableIP(string ipAddress)
        {
            string[] breakIP = ipAddress.Split('.');
            int[] fOct;
            int[] sOct;
            int[] thirdOct;
            int[] forthOct;

            List<string> address = new List<string>();

            //Building the entire list.
            if (breakIP[0] == "*")
            {
                fOct = new int[256];
                for (int fO = 0; fO < fOct.Length; fO++)
                {
                    fOct[fO] = fO;
                }
            }
            else
            {
                fOct = new int[1] { Convert.ToInt16(breakIP[0]) };
            }

            if (breakIP[1] == "*")
            {
                sOct = new int[256];
                for (int sO = 0; sO < sOct.Length; sO++)
                {
                    sOct[sO] = sO;
                }
            }
            else
            {
                sOct = new int[1] { Convert.ToInt16(breakIP[1]) };
            }

            if (breakIP[2] == "*")
            {
                thirdOct = new int[256];
                for (int tO = 0; tO < thirdOct.Length; tO++)
                {
                    thirdOct[tO] = tO;
                }
            }
            else
            {
                thirdOct = new int[1] { Convert.ToInt16(breakIP[2]) };
            }

            if (breakIP[3] == "*")
            {
                forthOct = new int[256];
                for (int fO = 0; fO < forthOct.Length; fO++)
                {
                    forthOct[fO] = fO;
                }
            }
            else
            {
                forthOct = new int[1] { Convert.ToInt16(breakIP[3]) };
            }


            //Compiling the list of all address to be added.
            for (int f = 0; f < fOct.Length; f++)
            {
                for (int s = 0; s < sOct.Length; s++)
                {
                    for (int t = 0; t < thirdOct.Length; t++)
                    {
                        for (int fth = 0; fth < forthOct.Length; fth++)
                        {
                            address.Add(fOct[f].ToString() + "." + sOct[s].ToString() + "." + thirdOct[t].ToString() + "." + forthOct[fth].ToString());
                        }
                    }
                }
            }

            acceptableIP.AddRange(address);

        }

        /// <summary>
        /// Method for removing approved ip address.
        /// </summary>
        /// <param name="ipAddress"></param>
        public void removeAcceptableIP(string ipAddress)
        {
            for (int i = 0; i < acceptableIP.Count; i++)
            {
                if (acceptableIP[i] == ipAddress)
                {
                    acceptableIP.RemoveAt(i);
                    break;
                }
            }
        }

        private void ListenForClients()
        {
            this.tcpListener.Start();

            while (runServer)
            {
            //Loop for errors.
            Restart:

                //blocks until a client has connected to the server

                TcpClient client = null;

                try
                {
                    client = this.tcpListener.AcceptTcpClient();
                }
                catch (Exception e)
                {
                    serverError.UpdatingObject = e;
                    goto Restart;
                }

                if (acceptableIP.Count > 0)
                {

                    //Checking to see if the connection is from an acceptable ipaddress
                    int ipFound = 0;

                    //Looking through the acceptable list of address.
                    for(int i =0; i< acceptableIP.Count; i++)
                    {
                        if (client.Client.RemoteEndPoint.ToString().Contains(acceptableIP[i]))
                        {
                            ipFound++;
                        }
                    }

                    //If not on list kick connected from the server.
                    if (ipFound == 0)
                    {
                        client.Client.Close();
                        serverError.UpdatingObject = new Exception("Unknown ip connected to the server");
                        goto Restart;
                    }
                }

                //Adding the client to the list of online clients.
                onlineClients.Add(client);

                clientConnected.CommandTelnet = "client,connected," + client.Client.RemoteEndPoint.ToString();

                TcpClient tcpClient;
                NetworkStream clientStream;

                //Collecting the client and stream information.
                tcpClient = (TcpClient)client;
                clientStream = tcpClient.GetStream();

                //Informing the user that they have connected.
                //sendMessage("Connected");

                //create a thread to handle communication
                //with connected client
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                clientThread.Name = client.Client.RemoteEndPoint.ToString();
                clientThread.IsBackground = true;
                handleClientThreads.Add(clientThread);
                clientThread.Start(client);

                if (handleClientThreads.Count > 1000)
                {
                    System.Diagnostics.Debug.WriteLine("1000");
                }
                
            }
        }

        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();

            bool userEchoSetting = false;

            byte[] message = new byte[byteBufferSize];
            int bytesRead;

            string overallMessage = "";

            while (true)
            {
                bytesRead = 0;

                try
                {
                    //blocks until a client sends a message
                    message = new byte[byteBufferSize];
                    bytesRead = clientStream.Read(message, 0, byteBufferSize);


                }
                catch
                {
                    //a socket error has occured
                    break;
                }

                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }


                if (!binaryMode)
                {
                    //message has successfully been received
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    string incomingStringMessage = encoder.GetString(message, 0, bytesRead);

                    overallMessage += incomingStringMessage;

                    //Looking for the carrir return or line feed or both or a odd one I found.
                    if (incomingStringMessage.EndsWith("\r") || incomingStringMessage.EndsWith("\n") ||
                        incomingStringMessage.EndsWith("\r\n") || incomingStringMessage.EndsWith("\r\0"))
                    {
                        incomingCommands.CommandTelnet = tcpClient.Client.RemoteEndPoint.ToString() + "," + overallMessage;
                        overallMessage = "";

                    }
                }
                else
                {
                    dataReceived.objectID = tcpClient.Client.RemoteEndPoint.ToString();

                    byte[] trimMessage = new byte[bytesRead];

                    //Cutting out all the extra buffer data.
                    for (int i = 0; i < trimMessage.Length; i++)
                    {
                        trimMessage[i] = message[i];
                    }

                    //Firing off event that data is in.
                    dataReceived.UpdatingObject = trimMessage;
                }

            }

            try
            {
                clientConnected.CommandTelnet = "client,disconnected," + tcpClient.Client.RemoteEndPoint.ToString();

                tcpClient.Close();

                //Removing the client from the online array.
                onlineClients.Remove(tcpClient);
                handleClientThreads.Remove(System.Threading.Thread.CurrentThread);
            }
            catch
            {
            }
        }

        public void sendMessage(string message, object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            if (tcpClient.Connected)
            {
                NetworkStream clientStream = null;
                clientStream = tcpClient.GetStream();

                if (tcpClient.Connected)
                {
                    clientStream = tcpClient.GetStream();
                }

                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] buffer = encoder.GetBytes(message + "\r\n");

                try
                {
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                }
                catch (System.IO.IOException ioError)
                {
                    serverError.objectID = this.tcpListener.Server.LocalEndPoint.ToString();
                    serverError.UpdatingObject = ioError;
                }
            }
        }

        public void fakeData(string theMessage)
        {
            incomingCommands.CommandTelnet = theMessage + "\r\0";
        }


        public void sendData(byte[] data, object client)
        {
            try
            {
                TcpClient tcpClient = (TcpClient)client;
                NetworkStream clientStream = tcpClient.GetStream();
                clientStream = tcpClient.GetStream();
                byte[] buffer = data;

                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch
            { }
        }

        public void serverDispose()
        {
            //Stoping the listener.
            runServer = false;

            //Disconnecting from connected clients
            foreach (System.Net.Sockets.TcpClient client in onlineClients)
            {
                client.Close();
            }

            //Fully stopping server
            this.tcpListener.Server.Close();
            this.tcpListener = null;
        }

        #region Methods working with the online clients

        public List<System.Net.Sockets.TcpClient> getOnlineClients()
        {
            return onlineClients;
        }

        public void disconnectClient(string clientID)
        {
            foreach (System.Net.Sockets.TcpClient cl in onlineClients)
            {
                if (cl.Client.LocalEndPoint.ToString() == clientID)
                {
                    
                    NetworkStream clientStream = cl.GetStream();
                    clientStream.Close();
                    cl.Close();
                    onlineClients.Remove(cl);
                }
            }
        }

        #endregion

        #region Setters and Getters

        /// <summary>
        /// Method set the current byte buffer size desired for the server.
        /// </summary>
        /// <param name="desiredByteBufferSize"></param>
        public void setByteBufferSize(int desiredByteBufferSize) { byteBufferSize = desiredByteBufferSize; }

        /// <summary>
        /// Method returns the current server byte buffer size.
        /// </summary>
        /// <returns>int of the nyte buffer.</returns>
        public int getByteBufferSize() { return byteBufferSize; }


        #endregion

    }


    #region This section retaining to the properties of the command input
    public class Product
    {
        private string commandTelnet;
        public string CommandTelnet
        {
            get
            {
                return this.commandTelnet;
            }
            set
            {
                Object old = this.commandTelnet;
                this.commandTelnet = value;
                OnPropertyChange(this, new PropertyChangeEventArgs("Command", old, value));
            }
        }

        // Delegate
        public delegate void PropertyChangeHandler(object sender, PropertyChangeEventArgs data);

        // The event
        public event PropertyChangeHandler PropertyChange;

        // The method which fires the Event
        public void OnPropertyChange(object sender, PropertyChangeEventArgs data)
        {
            // Check if there are any Subscribers
            if (PropertyChange != null)
            {
                // Call the Event
                PropertyChange(this, data);
            }
        }
    }



    public class PropertyChangeEventArgs : EventArgs
    {
        public string PropertyName { get; internal set; }
        public object OldValue { get; internal set; }
        public object NewValue { get; internal set; }

        public PropertyChangeEventArgs(string propertyName, object oldValue, object newValue)
        {
            this.PropertyName = propertyName;
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }

    #endregion


    #endregion

}
