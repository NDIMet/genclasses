﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace updateCreater
{
    #region This section retaining to the properties of the command input
    [Serializable]
    public class objectUpdate 
    {
        private object updatingObject;

        public string objectID;

        public object UpdatingObject 
        {
            get 
            {
                return this.updatingObject;
            }
            set 
            {
                Object old = this.updatingObject;
                this.updatingObject = value;

                OnPropertyChange(this, new PropertyChangeEventArgs("Updating", old, value));
            }
        }
        // Delegate
        public delegate void PropertyChangeHandler(object sender, PropertyChangeEventArgs data);

        // The event
        public event PropertyChangeHandler PropertyChange;

        // The method which fires the Event
        public void OnPropertyChange(object sender, PropertyChangeEventArgs data)
        {
            // Check if there are any Subscribers
            if (PropertyChange != null)
            {
                // Call the Event
                PropertyChange(this, data);
            }
        }

    }

    [Serializable]
    public class stringUpdate
    {
        private string commandTelnet;
        public string CommandTelnet
        {
            get
            {
                return this.commandTelnet;
            }
            set
            {
                Object old = this.commandTelnet;
                this.commandTelnet = value;
                OnPropertyChange(this, new PropertyChangeEventArgs("Command", old, value));
            }
        }

        // Delegate
        public delegate void PropertyChangeHandler(object sender, PropertyChangeEventArgs data);
      
        // The event
        public event PropertyChangeHandler PropertyChange;

        // The method which fires the Event
        public void OnPropertyChange(object sender, PropertyChangeEventArgs data)
        {
            // Check if there are any Subscribers
            if (PropertyChange != null)
            {
                // Call the Event
                PropertyChange(this, data);
            }
        }
    }

    [Serializable]
    public class doubleUpdate
    {
        private double item;
        public double Item
        {
            get
            {
                return this.item;
            }
            set
            {
                Object old = this.item;
                this.item = value;
                OnPropertyChange(this, new PropertyChangeEventArgs("Command", old, value));
            }
        }

        // Delegate
        public delegate void PropertyChangeHandler(object sender, PropertyChangeEventArgs data);

        // The event
        public event PropertyChangeHandler PropertyChange;

        // The method which fires the Event
        public void OnPropertyChange(object sender, PropertyChangeEventArgs data)
        {
            // Check if there are any Subscribers
            if (PropertyChange != null)
            {
                // Call the Event
                PropertyChange(this, data);
            }
        }
    }


    [Serializable]
    public class PropertyChangeEventArgs : EventArgs
    {
        public string PropertyName { get; internal set; }
        public object OldValue { get; internal set; }
        public object NewValue { get; internal set; }

        public PropertyChangeEventArgs(string propertyName, object oldValue, object newValue)
        {
            this.PropertyName = propertyName;
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }

    #endregion

}
