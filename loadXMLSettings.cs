﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsLoader
{
    class loadXMLSettings
    {
        #region Methods for loading/saving iMet-X sensor settings.

        public List<sensor> loadSessionXML(string fileName)
        {
            List<sensor> instance = default(List<sensor>);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<sensor>));
            using (System.IO.TextReader r = new System.IO.StreamReader(fileName, Encoding.UTF8))
            {
                instance = (List<sensor>)serializer.Deserialize(r);
            }

            return instance;
        }

        public void saveSessionXML(string fileName, List<sensor> saveData)
        {
            sensor[] testConvert = (sensor[])saveData.ToArray();

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(sensor[]));
            System.IO.TextWriter textWriter = new System.IO.StreamWriter(fileName, false, Encoding.UTF8);
            serializer.Serialize(textWriter, testConvert);
            textWriter.Close();
        }

        #endregion

        #region Methods for loading calibration settings.

        public List<equipment> loadXMLEquipmentSettings(string fileName)
        {
            List<equipment> instance = default(List<equipment>);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<equipment>));
            using (System.IO.TextReader r = new System.IO.StreamReader(fileName, Encoding.UTF8))
            {
                instance = (List<equipment>)serializer.Deserialize(r);
            }

            return instance;
        }

        public void saveSessionXML(string fileName, List<equipment> saveData)
        {
            equipment[] testConvert = (equipment[])saveData.ToArray();

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(equipment[]));
            System.IO.TextWriter textWriter = new System.IO.StreamWriter(fileName, false, Encoding.UTF8);
            serializer.Serialize(textWriter, testConvert);
            textWriter.Close();
        }

        #endregion

        #region Methods for loading and saving the ADC sensor options.

        public SettingsADC loadXMLADCSettings(string fileName)
        {
            SettingsADC instance = default(SettingsADC);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(SettingsADC));
            using (System.IO.TextReader r = new System.IO.StreamReader(fileName, Encoding.UTF8))
            {
                instance = (SettingsADC)serializer.Deserialize(r);
            }

            return instance;
        }

        public void saveADCXML(string fileName, SettingsADC saveData)
        {
            SettingsADC testConvert = (SettingsADC)saveData;

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(SettingsADC));
            System.IO.TextWriter textWriter = new System.IO.StreamWriter(fileName, false, Encoding.UTF8);
            serializer.Serialize(textWriter, testConvert);
            textWriter.Close();
        }

        #endregion

    }

#region Classes related to sensor settings items.

    public class sensor
    {
        public string id;
        public List<sensorElement> element;
    }

    public class sensorElement
    {
        public string name;
        public double format;
        public int pos;
    }

#endregion

#region Classes related to loading calibration settings.

    public class equipment
    {
        public string name;
        public string equipmentType;
        public string equipmentSubType;
        public string connectionMode;
        public string connectionInfo;
        public List<equipmentElement> element;

    }

    public class equipmentElement
    {
        public string elementID;
        public string elementSetting;

    }

#endregion

#region Classes related to the loading of the ADC settings

    public class SettingsADC   //Board configurations
    {
        public List<BoardItem> boardConfigurations = new List<BoardItem>();

        public List<SensorItem> channelConfigurations = new List<SensorItem>();
    }

    public class BoardItem
    {
        /// <summary>
        /// Name of the board.
        /// </summary>
        public string boardName;

        /// <summary>
        /// Array of ADC Sensors mode is will work with this board.
        /// </summary>
        public string[] sensorList;

        /// <summary>
        /// Board ADC channel configurations.
        /// </summary>
        public List<BoardADCChannel> channels = new List<BoardADCChannel>();
    }

    public class BoardADCChannel    //Settings/details of each channel
    {
        /// <summary>
        /// Name of the ADC channel.
        /// </summary>
        public string name;
   
        /// <summary>
        /// Address of the ADC setting.
        /// </summary>
        public string address;

        /// <summary>
        /// Item on the ADC board used to implement this configuration.
        /// </summary>
        public string boardConfig;

        /// <summary>
        /// File for the example image.
        /// </summary>
        public string boardImageFile;
    }

    public class SensorItem
    {
        /// <summary>
        /// ID of the channel
        /// </summary>
        public string id;

        /// <summary>
        /// List of ADCs and the options.
        /// </summary>
        public List<SensorChannel> sensorOptions = new List<SensorChannel>();

    }

    public class SensorChannel
    {
        /// <summary>
        /// Channel ADC ID
        /// </summary>
        public string channelID;

        /// <summary>
        /// Sensor Option
        /// </summary>
        public string channelSensorOption;
    }

#endregion

}
