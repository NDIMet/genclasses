﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace managerChamber
{
    public class chamberManager
    {
        //Starting up the manager system.
        public chamberManager(TestEquipment.Thermotron incomingChamber)
        {
            desiredEnviroment = new EnviromentStability.stabilityEnviroment();

            chamber = incomingChamber;
            
            watchDog = new System.Timers.Timer(5000);
            watchDog.Elapsed += new System.Timers.ElapsedEventHandler(watchDog_Elapsed);
            watchDog.Enabled = true;

            //Setting up the way to get chamber data into the manager
            chamber.statusPacket.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPacket_PropertyChange);
        }


        //Public veriables
        public updateCreater.objectUpdate chamberConditionReady = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate chamberStabilityReady = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate chamberTHReady = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate managerError = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate statusChamberCondition = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate statusChamberStability = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate statusChamberTH = new updateCreater.objectUpdate();

        //Box car min differance AVG.
        public Bill.boxCarAverage thBoxCar = new Bill.boxCarAverage(60, 30);

        //Private veriables
        private TestEquipment.Thermotron chamber;

        private EnviromentStability.stabilityEnviroment desiredEnviroment;

        //Watchdog timer to watch for system hold up.
        private System.Timers.Timer watchDog;

        //List of the current readings taken
        private List<TestEquipment.dataPacket> currentReadings = new List<TestEquipment.dataPacket>();

        //Previous rate reading.
        double prevReading = 999.99;


        //Starting the methods
        #region Methods related to setting up and getting the enviroment settings.
        public void setDesiredEnviroment(double lowRangePoint, double highRangePoint, double desiredSetPoint, int aboutOfRecords)
        {
            try
            {
                desiredEnviroment.removeEviromentElement("chamberCondition");
            }
            catch (Exception removeError)
            {
                managerError.UpdatingObject = (object)removeError;
            }


            desiredEnviroment.addEnviromentElement("chamberCondition", lowRangePoint, highRangePoint, desiredSetPoint, aboutOfRecords);
        }

        public double getDesiredEnviromentSP()
        {
            return desiredEnviroment.getElementSetPoint("chamberCondition");
        }

        public void setDesiredEnviromentStability(double lowRangePoint, double highRangePoint, double desiredSetPoint, int aboutOfRecords)
        {
            try
            {
                desiredEnviroment.removeEviromentElement("chamberStability");
            }
            catch (Exception removeError)
            {
                managerError.UpdatingObject = (object)removeError;
            }

            desiredEnviroment.addEnviromentElement("chamberStability", lowRangePoint, highRangePoint, desiredSetPoint, aboutOfRecords);
        }

        public double getDesiredStabilitySP()
        {
            return desiredEnviroment.getElementSetPoint("chamberStability");
        }

        public void setDesiredEnviromentTh(double lowRangePoint, double highRangePoint, double desiredSetPoint, int aboutOfRecords)
        {
            try
            {
                desiredEnviroment.removeEviromentElement("chamberTH");
            }
            catch (Exception removeError)
            {
                managerError.UpdatingObject = (object)removeError;
            }

            desiredEnviroment.addEnviromentElement("chamberTH", lowRangePoint, highRangePoint, desiredSetPoint, aboutOfRecords);
        }

        public double getDesiredEnviromentTh()
        {
            return desiredEnviroment.getElementSetPoint("chamberTH");
        }

        #endregion

        #region Process Methods

        /// <summary>
        /// Method returns the 7sec box car min differacnce or 9999.99 if not enough data.
        /// </summary>
        /// <returns></returns>
        public double getCurrentStabilityAvg()
        {
            double currentMinAvg = prevReading; //If there is not enough data to complete request this will be sent.

            int desiredBoxCount = 30;

            DateTime justChecking = DateTime.Now;
            DateTime searchTime = DateTime.Now.AddSeconds(-60); //Time looking for.

            //Looking for a data item that meets my timeing needs.
            if (currentReadings.Count > 60 + 10 + desiredBoxCount) //Limiting it so we don't needlessly look for data.
            {
                for (int x = 0; x < currentReadings.Count; x++)
                {
                    if (currentReadings[x].packetDate <= searchTime.AddSeconds(0.7) && currentReadings[x].packetDate >= searchTime.AddSeconds(-0.7))
                    {
                        if (x - desiredBoxCount > 0) //Making sure we are still in data to compare.
                        {
                            double pastAverage = 0; //Past Avg
                            double currentAverage = 0; //Current Avg.
                            //Compile past average.
                            for (int pA = desiredBoxCount; pA > 0; pA--)
                            {
                                pastAverage += Convert.ToDouble(currentReadings[x - pA].packetMessage); //Adding up the past data.
                            }
                            pastAverage = pastAverage / desiredBoxCount; //Getting avg.

                            //Compileing current avg.
                            for (int cA = desiredBoxCount; cA > 0; cA--)
                            {
                                currentAverage += Convert.ToDouble(currentReadings[currentReadings.Count - cA].packetMessage); //Adding up recent data.
                            }
                            currentAverage = currentAverage / desiredBoxCount; //Getting avg.

                            currentMinAvg = currentAverage - pastAverage; //Outputing the boxcar avg.
                            break; //Breaking out of loops.
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return currentMinAvg;
        }

        private void logChamberStability()
        {
            string dataToLog = "";
            dataToLog = DateTime.Now.ToString("yyyy/MM/dd") + "," + DateTime.Now.ToString("HH:mm:ss.fff") + "," + chamber.CurrentChamberC.ToString() + "," + chamber.CurrentChamberTempTHTL.ToString() +
                            "," + thBoxCar.getCurrentBoxCar().ToString("0.0000") + "\r\n";

            System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + DateTime.Now.ToString("yyyyMMdd") + "-ChamberData.csv", dataToLog);
        }

        #endregion

        #region Event Methods.

        void statusPacket_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            watchDog.Enabled = false;

            //Get the differance per/min
            //Compaing the interal data packet.
            TestEquipment.dataPacket tempDataPacket = new TestEquipment.dataPacket();
            tempDataPacket.packetDate = DateTime.Now;

            //Using product Temp
            //tempDataPacket.packetMessage = chamber.CurrentChamberC.ToString();
            tempDataPacket.packetMessage = chamber.CurrentChamberPC.ToString();



            currentReadings.Add(tempDataPacket);

            //Checking current conditons for enviroment and stability.
            if (desiredEnviroment.enviromentElements.Count > 0)
            {
                checkConditions();
            }
            else
            {
                managerError.UpdatingObject = (object)"No enviromental settings set";
            }

            //Starting watchDog
            watchDog.Enabled = true;

        }

        /// <summary>
        /// Method checks the current reading agenst the desired enviromental conditons and stability desired and report ready if met.
        /// </summary>
        private void checkConditions()
        {
            double currentMinAvg = getCurrentStabilityAvg();

            //Checking the current condition
            //bool currentChamberConditionState = desiredEnviroment.checkEnviromentElement("chamberCondition", chamber.CurrentChamberC);
            bool currentChamberConditionState = desiredEnviroment.checkEnviromentElement("chamberCondition", chamber.CurrentChamberPC);
            int currentChamberConditionStateCount = desiredEnviroment.checkEnviromentElementCount("chamberCondition");
            object[] statusCondition = new object[2] { currentChamberConditionState, currentChamberConditionStateCount };
            statusChamberCondition.UpdatingObject = (object)statusCondition;

            //Checking the current stability
            bool currentChamberStabilityState = desiredEnviroment.checkEnviromentElement("chamberStability", currentMinAvg);
            int currentChamberStabilityStateCount = desiredEnviroment.checkEnviromentElementCount("chamberStability");
            object[] statusStability = new object[2] { currentChamberStabilityState, currentChamberStabilityStateCount };
            statusChamberStability.UpdatingObject = (object)statusStability;

            //Checking the current TH
            //bool currentChamberTHState = desiredEnviroment.checkEnviromentElement("chamberTH", thBoxCar.addData(chamber.CurrentChamberTempTHTL));
            bool currentChamberTHState = desiredEnviroment.checkEnviromentElement("chamberTH", thBoxCar.addData(chamber.CurrentChamberPTHTL));
            int currentChamberTHStateCount = desiredEnviroment.checkEnviromentElementCount("chamberTH");
            object[] statusTH = new object[2] { currentChamberTHState, currentChamberTHStateCount };
            statusChamberTH.UpdatingObject = (object)statusTH;

            //Logging chamber stability
            logChamberStability();

            Console.WriteLine("AirTemp:" + currentChamberConditionState.ToString() + "/" + currentChamberConditionStateCount.ToString() + " - Stab:" + currentChamberStabilityState.ToString() + "/" +
                                currentChamberStabilityStateCount.ToString() + " - TH:" + currentChamberTHState.ToString() + "/" + currentChamberTHStateCount.ToString());

            //Checking to see if the pressure Conditions are ready and if so trigger that event.
            if (desiredEnviroment.getElementReferancePointCount("chamberCondition") == desiredEnviroment.getDesiredNumberOfRefPoints("chamberCondition").Length)
            {
                chamberConditionReady.UpdatingObject = (object)"Ready";
            }

            //Checking to see if the pressure conditon has stabilized and if so trigger that event.
            if (desiredEnviroment.getElementReferancePointCount("chamberStability") == desiredEnviroment.getDesiredNumberOfRefPoints("chamberStability").Length)
            {
                chamberStabilityReady.UpdatingObject = (object)"Ready";
            }

            //Checking to see if the chamber th is readt to trigger that event.
            if (desiredEnviroment.getElementReferancePointCount("chamberTH") == desiredEnviroment.getDesiredNumberOfRefPoints("chamberTH").Length)
            {
                chamberTHReady.UpdatingObject = (object)"Ready";
            }
        }

        void watchDog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimeoutException managerTimeOutError = new TimeoutException("Chamber Manager Time Out");
            managerError.UpdatingObject = (object)managerTimeOutError;
        }

        #endregion


    }
}
