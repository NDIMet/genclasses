﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elementStabilityEnviroment
{
    public class elementStabilityEnviroment
    {
        public elementStabilityEnviroment(string desiredName, double desiredLimitLower, double desiredLimitUpper, double desiredSetPoint, int desiredNumberOfRefPoints)
        {
            //appling startup settings.
            setName(desiredName);
            setLimitLower(desiredLimitLower);
            setLimitUpper(desiredLimitUpper);
            setSetPoint(desiredSetPoint);
            setReferancePointsAmount(desiredNumberOfRefPoints);

            //initulizing the array
            for (int i = 0; i < referancePoints.Length; i++)
            {
                referancePoints[i] = false;
            }

        }

        //Class Variables
        private string name = "";
        private double limitUpper = 0;
        private double limitLower = 0;
        private double setPoint = 0;
        private int referanceCounter = 0;
        private bool[] referancePoints;

        #region Setters and Getters

        public void setName(string desiredName) { name = desiredName; }

        public string getName() { return name; }


        public void setLimitUpper(double desiredLimitUpper) { limitUpper = desiredLimitUpper; }

        public double getLimitUpper() { return limitUpper; }


        public void setLimitLower(double desiredLimitLower) { limitLower = desiredLimitLower; }

        public double getLimitLower() { return limitLower; }


        public void setSetPoint(double desiredSetPoint) { setPoint = desiredSetPoint; }

        public double getSetPoint() { return setPoint; }


        public bool[] getReferancePoints()
        {
            return referancePoints;
        }

        public void setReferancePoint(int desirePoint, bool desiredCondition)
        {
            if (desirePoint >= 0 && desirePoint < referancePoints.Length)
            {
                referancePoints[desirePoint] = desiredCondition;
            }
            else
            {
                //throw new System.ArgumentException("Referance point desired outside of array."); 
            }
        }

        public void setReferancePointsAmount(int desiredAmountOfPoints)
        {
            if (referancePoints != null)
            {
                referancePoints = null;
            }
            setCounterPOS(0);
            referancePoints = new bool[desiredAmountOfPoints];
        }

        public int getReferancePointCount()
        {
            int count = 0;
            for (int i = 0; i < referancePoints.Length; i++)
            {
                if (referancePoints[i])
                {
                    count++;
                }
            }

            return count;
        }

        public void setCounterPOS(int desirePos)
        {
            referanceCounter = desirePos;
        }

        public int getCounterPOS()
        {
            return referanceCounter;
        }

        private void indexReferanceCounter()
        {
            if (referanceCounter < referancePoints.Length-1)
            {
                referanceCounter++;
            }
            else
            {
                referanceCounter = 0;
            }
        }

        public bool checkElement(double currentConditionCompare)
        {
            bool elementStatus = false;
            if (currentConditionCompare <= getLimitUpper() && currentConditionCompare >= getLimitLower())
            {
                elementStatus = true;
            }

            setReferancePoint(getCounterPOS(), elementStatus);
            indexReferanceCounter();

            return elementStatus;
        }

        #endregion

    }
}
