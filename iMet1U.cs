﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using updateCreater;

namespace Universal_Radiosonde
{
    public struct CalData
    {
        public string ComPortName;
        public string FirmwareVersion;
        public int PacketNumber;
        public string SerialNumber;
        public string ProbeID;
        public string SondeID;
        public double Pressure;
        public int PressureCounts;
        public int PressureRefCounts;
        public double Temperature;
        public int TemperatureCounts;
        public int TemperatureRefCounts;
        public double Humidity;
        public double HumidityFrequency;
        public double PressureTemperature;
        public int PressureTempCounts;
        public int PressureTempRefCounts;
        public double InternalTemp;
    }

    public struct PTUData 
    {
        public double Pressure;
        public double AirTemperature;
        public double RelativeHumidity;
        public double HumidityTemperature;
    }

    public struct StatData 
    {
        public double BatteryVoltage;
        public double InternalTemperature;
        public string Firmware;
        public string TXMode;
    }

    public struct GPSData 
    {
        public double Latitude;
        public double Longitude;
        public double Altitude;
        public int NumberOfSatellites;
        public string GPSTime;
    }

    public struct GPSXData 
    {
        public double Latitude;
        public double Longitude;
        public double Altitude;
        public int NumberOfSatellites;
        public string GPSTime;
        public double EastVelocity;
        public double NorthVelocity;
        public double AscentVelocity;
    }

    /// <summary>
    /// Version 1 of the universal radiosonde class.
    /// </summary>
    public class iMet1U
    {
        public iMet1U() { }

        public iMet1U(string IPAddress, int port) 
        {
            System.Net.IPAddress address = System.Net.IPAddress.Parse(IPAddress);
            this.SondeIPEndPoint = new System.Net.IPEndPoint(address, port);
            this.communicationProtocol = 1;
        }
        public iMet1U(string ComPortName) 
        {
            this.COMPort = new System.IO.Ports.SerialPort(ComPortName, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            this.communicationProtocol = 2;
        }
    
        //Private
        string sondeResponse;
        //COM Port Construction
        private System.IO.Ports.SerialPort COMPort;

        //IP Construction
        private System.Net.Sockets.TcpClient SondeTCPClient;
        private System.Net.Sockets.NetworkStream SondeNetworkStream;
        private System.Net.IPEndPoint SondeIPEndPoint;

        //Threading
        private System.Threading.Thread sondeMonitorThread;
        private bool cancelSondeMonitor = false;
        private System.Threading.AutoResetEvent sondeResponseEvent = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent sondeInitialized;

        public objectUpdate CalDataUpdate = new objectUpdate();
        public objectUpdate PTUDataUpdate = new objectUpdate();
        public objectUpdate StatDataUpdate = new objectUpdate();
        public objectUpdate GPSDataUpdate = new objectUpdate();
        public objectUpdate GPSXDataUpdate = new objectUpdate();
        public objectUpdate InitializedUpdate = new objectUpdate();

        //Thread Safety
        static readonly object threadLock = new object();
        static readonly object sondeDataLock = new object();

        //Public
        public int communicationProtocol = 0;
        public CalData CalData = new CalData();
        public PTUData PTUData = new PTUData();
        public StatData StatData = new StatData();
        public GPSData GPSData = new GPSData();
        public GPSXData GPSXData = new GPSXData();
        public bool newRadiosondeDetected = false;
        public string ComPortName;
        public string FirmwareVersion;
        
  
        //Public Methods
        /// Opens the port for the radiosonde.  Must be performed before the monitor thread can begin.
        /// 
        /// </summary>
        /// Boolean indicating whether or not the port was successfully opened.
        public void OpenPort() 
        {
            int protocol = this.communicationProtocol;
       
            switch (protocol) 
            { 
                case 1://IP protocol
                    openPortIP();
                    break;
                case 2://COM protocol
                    openPortCOM();
                    break;      
                default:
                    throw new SystemException("Undefined communication protocol.");                               
            }

            this.disablePortTimeOut();

            startSondeMonitorThread();
        }
        /// Closes the communication port for the radiosonde
        /// 
        /// </summary>
        public void closePort() 
        {
            int protocol = this.communicationProtocol;
            switch (protocol) 
            {
                case 1://IP protocol
                    this.SondeTCPClient.Close();
                    break;
                case 2://COM protocol
                    this.COMPort.DiscardInBuffer();
                    this.COMPort.Close();
                    break;
                default:
                    throw new SystemException("Undefined communication protocol.");
            }
        }

        /// <summary>
        /// Method returns the port that the sonde is connected to. May be a ip address and port(192.168.0.1:1000) or a com port (COM32).
        /// </summary>
        /// <returns></returns>
        public string getPortName()
        {
            string result = "";
            switch (communicationProtocol)
            {
                case 1:
                    result = this.SondeIPEndPoint.Address.ToString() + ":" + this.SondeIPEndPoint.Port.ToString();
                    break;
                case 2:
                    result = this.COMPort.PortName;
                    break;
            }
            return result;
        }
        
        public bool checkForRadiosonde() 
        {
            if (this.sondeMonitorThread != null && this.sondeMonitorThread.IsAlive) 
            {
                throw new SystemException("Cannot search for radiosonde while the monitoring thread is already open.");
            }

            int protocol = this.communicationProtocol;

            switch (protocol)
            {
                case 1://IP protocol
                    try
                    {
                        openPortIP();
                    }
                    catch { return false; }
                    break;
                case 2://COM protocol
                    try
                    {
                        openPortCOM();
                    }
                    catch { return false; }
                    break;
                default:
                    throw new SystemException("Undefined communication protocol.");
            }

            setPortTimeOut(900);
    
            try
            {
                sendSondeCommand("sn\r\n");
                string response = readLine();
                while(!response.Contains("sn="))
                {
                    response = readLine();
                }
                
                closePort();
                return true;
            }
            catch 
            {
                closePort();
                return false;             
            }
         }

        public bool checkActivePortForRadiosonde()
        {


            int protocol = this.communicationProtocol;

            setPortTimeOut(700);

            try
            {
                sendSondeCommand("sn\r\n");
                string response = readLine();
                while (!response.Contains("sn="))
                {
                    response = readLine();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void setManufacturingMode(bool mode_ON) 
        {

            if (mode_ON)
            {
                this.ComPortName = this.COMPort.PortName;
                this.FirmwareVersion = getFirmwareVersion();
                updateTXModeData(getTXMode());

                sendSondeCommand("calmode=on\r\n");
                string response = this.getSondeResponse();
                if (!(response == "calmode=on"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into manufacturing mode.");
                }
            }
            else 
            {
                sendSondeCommand("calmode=off\r\n");
                string response = this.getSondeResponse();
                if (!(response == "calmode=off")) 
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of manufacturing mode.");
                }
            }
        }

        public void setAllDataMode(bool mode_ON) 
        {
            if (mode_ON)
            {
                sendSondeCommand("data=on\r\n");
                string response = this.getSondeResponse();      
            }
            else
            {
                sendSondeCommand("data=off\r\n");
                string response = this.getSondeResponse();
                if (!(response == "ptu=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of PTU mode.");
                }
                response = this.getSondeResponse();
                if (!(response == "stat=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into PTU mode.");
                }
                response = this.getSondeResponse();
                if (!(response == "gps=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into PTU mode.");
                }
            }

        }

        public void setPollResponseMode() 
        {
            System.Threading.Thread.Sleep(100);

            sendSondeCommand("ptu=off\r\n");
            string response = getSondeResponse();
            if(response!="ptu=off")
            {
                response = getSondeResponse();
            }

            System.Threading.Thread.Sleep(100);

            sendSondeCommand("stat=off\r\n");
            response = getSondeResponse();

            System.Threading.Thread.Sleep(100);

            sendSondeCommand("gps=off\r\n");
            response = getSondeResponse();

            System.Threading.Thread.Sleep(100);

            sendSondeCommand("calmode=off\r\n");
            response = getSondeResponse();
            return;
        }

        public void setPTUMode(bool mode_ON) 
        {
            if (mode_ON)
            {
                sendSondeCommand("ptu=on\r\n");
                string response = this.getSondeResponse();
                if (!(response == "ptu=on"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into PTU mode.");
                }
            }
            else
            {
                sendSondeCommand("ptu=off\r\n");
                string response = this.getSondeResponse();
                if (!(response == "ptu=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of PTU mode.");
                }
            }
        }

        public void setStatusMode(bool mode_ON) 
        {
            if (mode_ON)
            {
                sendSondeCommand("stat=on\r\n");
                string response = this.getSondeResponse();
                if (!(response == "stat=on"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into status mode.");
                }
            }
            else
            {
                sendSondeCommand("stat=off\r\n");
                string response = this.getSondeResponse();
                if (!(response == "stat=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of status mode.");
                }
            }
        }

        public void setGPSMode(bool mode_ON)
        {
            if (mode_ON)
            {
                sendSondeCommand("gps=on\r\n");
                string response = this.getSondeResponse();
                if (!(response == "gps=on"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into GPS mode.");
                }
            }
            else
            {
                sendSondeCommand("gps=off\r\n");
                string response = this.getSondeResponse();
                if (!(response == "gps=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of GPS mode.");
                }
            }
        }

        public void saveConfiguration() 
        {
            //clearPortBuffer();
            sendSondeCommand("save\r\n");
            string response = getSondeResponse();
            if (response.Contains("OK")) { }
            else throw new SystemException("Unexpected sonde response.");
        }

        public string getSondeID() 
        {
            sendSondeCommand("sid\r\n");
            string response = this.getSondeResponse();
            response = response.Replace("sid=", null);
            return response;
        }

        public void setSondeID(string newSondeID) 
        {
            if (newSondeID.Length > 13) 
            {
                throw new SystemException("The sonde ID must be less than 13 characters.");
            }
            string command = "sid=" + newSondeID + "\r\n";
            sendSondeCommand(command);
            command = command.Trim().ToLower();
            string response = this.getSondeResponse();
            if (response != command) 
            {
                throw new SystemException("Invalid sonde response: " + response);
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse();
            if (response != "OK") 
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
        }

        public void setProbeID(string newProbeID)
        {
            if (newProbeID.Length > 13)
            {
                throw new SystemException("The probe ID must be less than 13 characters.");
            }
            string command = "pid=" + newProbeID + "\r\n";
            sendSondeCommand(command);
            command = command.Trim().ToLower();
            System.Threading.Thread.Sleep(1000);

            string response = getSondeResponse();

            if (response != command)
            {
                throw new SystemException("Invalid sonde response: " + response);
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse();
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
        }

        public string getProbeID()
        {
            sendSondeCommand("pid\r\n");
            string response = this.getSondeResponse();
            response = response.Replace("pid=", null);
            return response;
        }

       

        public void setSerialNumber(string newSN)
        {
            if (newSN.Length > 13)
            {
                throw new SystemException("The serial number must be less than 13 characters.");
            }
            string command = "sn=" + newSN + "\r\n";
            sendSondeCommand(command);
            command = command.Trim().ToLower();
            string response = this.getSondeResponse();
            if (response != command)
            {
                throw new SystemException("Invalid sonde response: " + response);
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse();
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
        }

        public string getSerialNumber()
        {
            sendSondeCommand("sn\r\n");
            string response = this.getSondeResponse();
            response = response.Replace("sn=", null);

            //Added storing the to the local structure to help older sondes.
            CalData.SerialNumber = response;

            return response;
        }

        public void setTransmitterRF(bool ON)
        {
            if (ON)
            {
                sendSondeCommand("txrf=on\r\n");
                string response = this.getSondeResponse();
                if (!(response == "txrf=on"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into GPS mode.");
                }
            }
            else
            {
                sendSondeCommand("txrf=off\r\n");
                string response = this.getSondeResponse();
                if (!(response == "txrf=off"))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of GPS mode.");
                }
            }
        }

        public bool getTransmitterRF() 
        {
            sendSondeCommand("txrf\r\n");
            string response = this.getSondeResponse();
            response = response.Replace("txrf=", null);
            if (response == "on") 
            {
                return true;
            }
            if (response == "off") 
            {
                return false;
            }
            throw new SystemException("Invalid sonde response: " + response);
        }

        /// <summary>
        /// Method for setting the radiosondes to event trigger at a GPS time.
        /// </summary>
        /// <param name="incomingSetting"></param>
        public void setCREvent(eventTrigger incomingSetting)
        {
            if (incomingSetting.triggerHR == 99)
            {
                string toBeSent = "cr" + incomingSetting.CRLoc.ToString() + "=off\r\n";
                sendSondeCommand(toBeSent);
                string back = this.getSondeResponse();
            }
            else
            {
                string toBeSent = "cr" + incomingSetting.CRLoc.ToString() + "=on" + incomingSetting.triggerHR.ToString("00") + incomingSetting.triggerMIN.ToString("00") + incomingSetting.triggerTime.ToString() + "\r\n";
                sendSondeCommand(toBeSent);
                string test = this.getSondeResponse();
                int byteCounter = COMPort.BytesToRead;
            }

        }

        public string getCREvent(int eventNumber)
        {
            sendSondeCommand("cr" + eventNumber.ToString() + "\r\n");
            return (this.getSondeResponse());
        }

        public void crEventTest(bool onOff)
        {
            if (onOff)
            {
                sendSondeCommand("CR1=test\r\n");
                this.getSondeResponse();
            }
            else
            {
                sendSondeCommand("CR1=off\r\n");
                this.getSondeResponse();
            }
        }

        public void cleanUp()
        {
            string test = this.getSondeResponse();
        }

        public void sendSondeCommand(string command)
        {
            int sondeType = this.communicationProtocol;

            System.Threading.Thread.Sleep(100);

            switch (sondeType)
            {
                case 1:
                    byte[] bCommand = Encoding.ASCII.GetBytes(command);
                    this.SondeNetworkStream.Write(bCommand, 0, bCommand.Length);
                    break;
                case 2:
                    System.ComponentModel.BackgroundWorker backgroundWorkerInternalSender = new System.ComponentModel.BackgroundWorker();
                    backgroundWorkerInternalSender.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerInternalSender_DoWork);
                    backgroundWorkerInternalSender.RunWorkerAsync(command);
                    //this.COMPort.Write(command);
                    break;
                default:
                    throw new System.Exception("Radiosonde communications protocol has not been initialized.");
            }
        }

        void backgroundWorkerInternalSender_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (this.COMPort.IsOpen)
            {
                string incomingCommand = (string)e.Argument;
                this.COMPort.Write(incomingCommand);
            }
        }
        

        public void setCoefficient(int coefficientNumber, double coefficientValue) 
        {
            if (coefficientNumber > 43 || coefficientNumber < 0) 
            {
                throw new SystemException("Invalid coefficient number. Must be between 0 and 43");
            }
            string command = "cal[" + coefficientNumber.ToString() + "]=" + coefficientValue.ToString("0.00000e+00") + "\r\n";
            sendSondeCommand(command);
            command = command.Trim();
            string response = getSondeResponse();
            if (!(command == response)) 
            {
                throw new SystemException("Could not verify successful coefficient load.");
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse();
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }                        
        }

        public bool setTXMode(string desiredMode)
        {
            bool modeSet = false;

            if (desiredMode.Contains("IMS") || desiredMode.Contains("bel202"))
            {
                string command = "txmode=" + desiredMode + "\r\n";
                sendSondeCommand(command);
                command = command.Trim();

                this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);

                string response = getSondeResponse();
                if (!(command.ToLower() == response.ToLower()))
                {
                    throw new SystemException("Could not verify successful TX Mode");
                }
                sendSondeCommand("save\r\n");
                response = this.getSondeResponse();
                if (response != "OK")
                {
                    throw new SystemException("Invalid sonde response to save command: " + response);
                }

                modeSet = true;
            }
            else
            {
            }

            //Updating current tx object.
            updateTXModeData(getTXMode());

            return modeSet;

        }

        /// <summary>
        /// Method for setting radiosonde to the wide(w) or narrow(n) tx mode.
        /// </summary>
        /// <returns></returns>
        public void setTXBand(string mode)
        {
            if (mode != "w" && mode != "n") //Checking to make sure the right command is right.
            {
                return;
            }

            string command = "txb=" + mode + "\r\n";

            sendSondeCommand(command);
            command = command.Trim();

            this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);

            string response = getSondeResponse();
            if (!(command.ToLower() == response.ToLower()))
            {
                throw new SystemException("Could not verify setting txb settings.");
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse();
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }

        }

        /// <summary>
        /// Method sets the Transmittion Time Limit. Length in min.
        /// </summary>
        public void setTTL(int min)
        {
            string command = "txl=";
            command += min.ToString() + "\r\n";

            sendSondeCommand(command);
            command = command.Trim();

            this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);

            string response = getSondeResponse();
            if (!(command.ToLower() == response.ToLower()))
            {
                throw new SystemException("Could not verify setting txl settings.");
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse();
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }

        }

        public bool setTXFreq(double[] desiredFreq)
        {
            //Results from programming the radiosonde.
            bool programmingResults = false;

            //composing the txFreq command.
            string command = "txfreq=" + desiredFreq[0].ToString("0.00") + "," + desiredFreq[1].ToString("0.00") + "," +
                desiredFreq[2].ToString("0.00") + "," + desiredFreq[3].ToString("0.00") + "\r\n";
            sendSondeCommand(command);

            string[] response = new string[4]; //Responce for sonde 5.09 and older
            string responseNew = ""; //Responce for 5.10 and newer.

            //Getting sonde revision number

            double revNumber = 0;

            try
            {
                revNumber = Convert.ToDouble(StatData.Firmware.Substring(3, StatData.Firmware.Length - 3));
            }
            catch
            {
                revNumber = Convert.ToDouble(StatData.Firmware.Substring(3, StatData.Firmware.Length - 4));
            }


            if (revNumber <= 5.09)
            {
                for (int i = 0; i < response.Length; i++)
                {
                    this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
                    response[i] = this.getSondeResponse();

                    //Checking responces
                    /*
                    for (int x = 0; x < desiredFreq.Length; x++)
                    {
                        if (response[x].Contains(desiredFreq[x].ToString()))
                        {
                            programmingResults = true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    */
                }
            }
            else
            {
                this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
                responseNew = getSondeResponse();

                //Checking responce
                if (responseNew.Contains(desiredFreq[0].ToString()) && responseNew.Contains(desiredFreq[1].ToString()) &&
                    responseNew.Contains(desiredFreq[2].ToString()) && responseNew.Contains(desiredFreq[3].ToString()))
                {
                    programmingResults = true;
                }

            }

            //Saving command to the system.
            sendSondeCommand("save\r\n");
            string responseSave = this.getSondeResponse();
            if (responseSave != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }

            return programmingResults;
        }

        public double getCoefficient(int coefficientNumber) 
        {
            if (coefficientNumber > 43 || coefficientNumber < 0)
            {
                throw new SystemException("Invalid coefficient number. Must be between 0 and 43");
            }
            string command = "cal[" + coefficientNumber.ToString() + "]\r\n";
            sendSondeCommand(command);
            string response = this.getSondeResponse();
            response = response.Replace("cal[" + coefficientNumber.ToString() + "]=", null);
            return double.Parse(response);
        }

        public double[] getAllCoefficients() 
        {
            double[] coefficients = new double[44];
            string command = "cals\r\n";
            string response;
            sendSondeCommand(command);
            for (int i = 0; i < coefficients.Length; i++) 
            {
                response = this.getSondeResponse();
                response = response.Replace("cal[" + i.ToString() + "]=", null).Trim();
                coefficients[i] = double.Parse(response);
            }
            return coefficients;
        }

        public string getFirmwareVersion() 
        {
            this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
            string command = "vers\r\n";
            sendSondeCommand(command);
            string response = this.getSondeResponse();
            updateFirmwareData(response);
            return response;
        }

        public string getTXMode()
        {
            this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
            string command = "txmode\r\n";
            sendSondeCommand(command);
            string responce = this.getSondeResponse();
            updateTXModeData(responce);
            return responce;
        }

        public void Reset() 
        {
            int protocol = this.communicationProtocol;
            switch (protocol) 
            {
                case 1:
                    openPortIP();
                    break;
                case 2:
                    openPortCOM();
                    break;
                default:
                    throw new SystemException("Invalid protocol");
            }
            string command = "reset\r\n";
            sendSondeCommand(command);
            closePort();
        }

        public void resetSonde()
        {
            string command = "reset\r\n";
            sendSondeCommand(command);
        }

        public void waitForInitialization() 
        {
            this.sondeInitialized = new System.Threading.AutoResetEvent(false);
            this.sondeInitialized.WaitOne();
        }

        //Private Methods
        private void openPortIP() 
        {
            try
            {
                this.SondeTCPClient = new System.Net.Sockets.TcpClient();
                this.SondeTCPClient.Connect(this.SondeIPEndPoint);
                this.SondeNetworkStream = this.SondeTCPClient.GetStream();
                this.SondeNetworkStream.Flush();
            }
            catch (Exception ex)
            {
                if (this.SondeTCPClient != null) 
                {
                    this.SondeTCPClient.Close();
                }
                if (this.SondeNetworkStream != null) 
                {
                    this.SondeNetworkStream.Close();
                }
                throw ex;
            }
        }

        private void openPortCOM() 
        {
            try
            {
                this.COMPort.Open();
            }
            catch (Exception ex) 
            {
                this.COMPort.Dispose();
                throw ex;
            }
        }

        private void clearPortBuffer() 
        {
            int protocol = this.communicationProtocol;
            switch (protocol) 
            {
                case 1:
                    this.SondeNetworkStream.Flush();
                    break;
                case 2:
                    this.COMPort.DiscardInBuffer();
                    break;
                default:
                    throw new SystemException("No protocol selected.");
            }
        }

        public void setPortTimeOut(int milliseconds) 
        {
            if (this.communicationProtocol == 1) 
            {
                this.SondeNetworkStream.ReadTimeout = milliseconds;
                return;
            }
            if (this.communicationProtocol == 2) 
            {
                this.COMPort.ReadTimeout = milliseconds;
                return;
            }
            throw new SystemException("Communications protocol is not valid.");
        }

        public void disablePortTimeOut() 
        {
            if (this.communicationProtocol == 1)
            {
                this.SondeNetworkStream.ReadTimeout = System.Threading.Timeout.Infinite;
                return;
            }
            if (this.communicationProtocol == 2)
            {
                this.COMPort.ReadTimeout = System.IO.Ports.SerialPort.InfiniteTimeout;
                return;
            }
            throw new SystemException("Communications protocol is not valid.");
        }

        public string readLine() 
        {
            int protocol = this.communicationProtocol;
            string line = null;
            switch(protocol)
            {
                case 1://IP protocol
                    line=readLineIP();
                    break;
                case 2://COM protocol
                    line=readLineCom();
                    break;
                default:
                    throw new SystemException("Radiosonde communication protocol has not been initialized.");
            }

            return line;
            
        }

        private bool isResponse(string line)
        {
            if (line.Contains("sn="))
            {
                return true;
            }
            else { return false; }
        }

        private void startSondeMonitorThread()
        {
            if (this.sondeMonitorThread!=null && this.sondeMonitorThread.IsAlive) 
            {
                throw new SystemException("Sonde thread is already active.");
            }
            lock (threadLock)
            {
                this.cancelSondeMonitor = false;
            }
            this.sondeMonitorThread = new System.Threading.Thread(sondeMonitor);
            this.sondeMonitorThread.Start();
        }

        private void sondeMonitor() 
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;  //Added to provent this from continuning to run after program closes.
            bool cancelThread = false;
            lock (threadLock)
            {
                cancelThread = this.cancelSondeMonitor;
            }
            while (!cancelThread) 
            {

                try
                {
                    string line = this.readLine();
                    this.sondeDataDecoder(line);
                        
                }
                catch (Exception e)
                {
                    return;
                }
                  
                lock (threadLock) 
                {
                    cancelThread = this.cancelSondeMonitor;
                }
            }
        }

        private void sondeDataDecoder(string line) 
        {

            //PTU Line
            if (line.Contains("PTUX:")) 
            {
                updatePTUData(line);
                return;
            }
            //STAT Line
            if (line.Contains("STAT:")) 
            {
                updateStatusData(line);
                return;
            }
            //GPS Line
            if (line.Contains("GPS:")) 
            {
                updateGPSData(line);
                return;
            }
            //GPSX Line
            if (line.Contains("GPSX:")) 
            {
                updateGPSXData(line);
                return;
            }
            //Calmode Line
            if (line.Split(',').Length == 16) 
            {
                updateCalibrationData(line);
                return;
            }
            if (line.Contains("booting") || line.Contains("init gps"))
            {
                return;
            }
            if(line.Contains("ready"))
            {
                this.newRadiosondeDetected = true;
                InitializedUpdate.UpdatingObject = this.newRadiosondeDetected;
                this.newRadiosondeDetected = false;
                return;
            }

            if (line.Contains('=')||line.Contains("OK")||line.Contains("RSv"))
            {
                lock (sondeDataLock)
                {
                    this.sondeResponse = line;
                }
                if (this.sondeResponseEvent != null)
                {
                    this.sondeResponseEvent.Set();
                }
                return;
            }

        }

        private string getSondeResponse() 
        {
            //this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
            bool waitingForResponce = this.sondeResponseEvent.WaitOne(5000,false);

            return this.sondeResponse;


        }

        private void updateCalibrationData(string line) 
        {
            string[] splitLine = line.Split(',');
            lock (sondeDataLock) 
            {
                this.CalData.ComPortName = this.ComPortName;
                this.CalData.FirmwareVersion = this.FirmwareVersion;
                this.CalData.PacketNumber=int.Parse(splitLine[0]);
                this.CalData.SerialNumber = splitLine[1];
                this.CalData.ProbeID = splitLine[2];
                this.CalData.SondeID = splitLine[3];
                this.CalData.Pressure = double.Parse(splitLine[4]);
                this.CalData.PressureCounts = int.Parse(splitLine[5]);
                this.CalData.PressureRefCounts = int.Parse(splitLine[6]);
                this.CalData.Temperature = double.Parse(splitLine[7]);
                this.CalData.TemperatureCounts = int.Parse(splitLine[8]);
                this.CalData.TemperatureRefCounts = int.Parse(splitLine[9]);
                this.CalData.Humidity = double.Parse(splitLine[10]);
                this.CalData.HumidityFrequency = double.Parse(splitLine[11]);
                this.CalData.PressureTemperature = double.Parse(splitLine[12]);
                this.CalData.PressureTempCounts = int.Parse(splitLine[13]);
                this.CalData.PressureTempRefCounts = int.Parse(splitLine[14]);
                this.CalData.InternalTemp = double.Parse(splitLine[15]);
            }

            CalDataUpdate.UpdatingObject = this.CalData;
        }

        private void updatePTUData(string line) 
        {
            line = line.Replace("PTUX:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock) 
            {
                this.PTUData.Pressure = double.Parse(splitLine[0]);
                this.PTUData.AirTemperature = double.Parse(splitLine[1]);
                this.PTUData.RelativeHumidity = double.Parse(splitLine[2]);
                this.PTUData.HumidityTemperature = double.Parse(splitLine[3]);
            }

            PTUDataUpdate.UpdatingObject = this.PTUData;
        }

        private void updateStatusData(string line) 
        {
            line = line.Replace("STAT:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.StatData.BatteryVoltage = double.Parse(splitLine[0]);
                this.StatData.InternalTemperature = double.Parse(splitLine[1]);
            }
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateFirmwareData(string line)
        {
            this.StatData.Firmware = line;
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateTXModeData(string line)
        {
            this.StatData.TXMode = line;
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateGPSData(string line) 
        {
            line = line.Replace("GPS:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.GPSData.Latitude = double.Parse(splitLine[0]);
                this.GPSData.Longitude = double.Parse(splitLine[1]);
                this.GPSData.Altitude = double.Parse(splitLine[2]);
                this.GPSData.NumberOfSatellites = int.Parse(splitLine[3]);
                this.GPSData.GPSTime = splitLine[4];
            }
           
            GPSDataUpdate.UpdatingObject = this.GPSData;
            
        }

        private void updateGPSXData(string line)
        {
            line = line.Replace("GPSX:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.GPSXData.Latitude = double.Parse(splitLine[0]);
                this.GPSXData.Longitude = double.Parse(splitLine[1]);
                this.GPSXData.Altitude = double.Parse(splitLine[2]);
                this.GPSXData.NumberOfSatellites = int.Parse(splitLine[3]);
                this.GPSXData.GPSTime = splitLine[4];
                this.GPSXData.EastVelocity = double.Parse(splitLine[5]);
                this.GPSXData.NorthVelocity = double.Parse(splitLine[6]);
                this.GPSXData.AscentVelocity = double.Parse(splitLine[7]);
            }
            GPSXDataUpdate.UpdatingObject = this.GPSXData;
        }
        /// Reads one line of data using the IP communications protocol. The read timeout must be set by the user
        /// outside of this method
        /// 
        /// string containing the line of data transmitted by the sonde.
        private string readLineIP() 
        {
            byte[] buffer = new byte[1096];
            int bytesRead;
            
            ASCIIEncoding encoder = new ASCIIEncoding();
            while (true)
            {
                bytesRead = 0;
                bytesRead = this.SondeNetworkStream.Read(buffer, 0, buffer.Length);
                string message = encoder.GetString(buffer, 0, bytesRead);
                if(message.EndsWith("\r\n"))
                {
                    return message.Trim();
                }
            }
        }

        private string readLineCom() 
        {
            string line = null;

            line = this.COMPort.ReadLine();
            return line.Trim();
           
        }
    }

    /// <summary>
    /// Version 2 of the serial base iMet1 Uni class.
    /// This class is not designed for large number of radiosondes at the same time.
    /// </summary>
    public class iMet1UV2
    {
        //Public items.

        public CalData CalData = new CalData();
        public PTUData PTUData = new PTUData();
        public StatData StatData = new StatData();
        public GPSData GPSData = new GPSData();
        public GPSXData GPSXData = new GPSXData();
        public bool newRadiosondeDetected = false;
        public string ComPortName;
        public string FirmwareVersion;

        //Thread Safety
        static readonly object threadLock = new object();
        static readonly object sondeDataLock = new object();

        //Events
        //Update objects for the radiosonde data elements.
        public updateCreater.objectUpdate CalDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate PTUDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate StatDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate GPSDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate GPSXDataUpdate = new updateCreater.objectUpdate();

        //Updates relateding to radiosonde connected massages
        public updateCreater.objectUpdate bootingRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate initGPSRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate readyRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate disconnectRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate error = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate errorConnect = new objectUpdate();
        public updateCreater.objectUpdate otherDataCollected = new updateCreater.objectUpdate();

        //Internal events of command processing.
        public updateCreater.objectUpdate cmdProcessComplete = new updateCreater.objectUpdate();  //Command processed.
        public updateCreater.objectUpdate statusReceived = new objectUpdate();      //New status message recived and needs processing.

        //Private items.
        System.IO.Ports.SerialPort comPort;
        DateTime lastPacketRecTime = DateTime.Now;

        #region Items related to communicating in and out of the system.
        System.ComponentModel.BackgroundWorker bwProcessor;     //Responceable to procssing commands out and receiveing the return message.
        System.Timers.Timer processCheck;       //Responceable for making sure commands are processed out if any are in que.

        System.ComponentModel.BackgroundWorker bwIncomingStream; //Incoming data scream.
        System.Threading.AutoResetEvent dataRequested = new System.Threading.AutoResetEvent(false);     //Something to cause a pause state until the requested data is retreaved.
        System.Threading.AutoResetEvent dataProcessed = new System.Threading.AutoResetEvent(false);
        string currentResponce = "";

        List<command> cmd = new List<command>();    //List of commands to be processed.
        List<command> cmdLog = new List<command>();     //Log of commands processed.
        #endregion

        #region Items related to detecting radiosonde

        //System.Timers.Timer portCheck;      //Timer for sending out search requests
        System.Threading.Thread portCheck;    //Thread for sending out search requests.

        public bool radiosondeConnected = false;   //Is a radiosonde Connected or not.
        public bool modeMFG = false;    //Is radiosonde currently in mfgMode.
        public bool modeInitGPS = false;    //Is the radiosonde currently init the gps?

        #endregion

        public iMet1UV2()
        {

        }
        
        public iMet1UV2(string port)
        {
            //Moved starting the whole thing to another function so that error events could work.
            portStart(port);
        }

        public void portStart(string port)
        {
            comPort = new System.IO.Ports.SerialPort(port, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            try
            {
                comPort.Open();
            }
            catch (Exception e)
            {
                errorConnect.UpdatingObject = e;
                return;
            }

            //Connecting to the comport data event.
            comPort.ReceivedBytesThreshold = 4;
            comPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(comPort_DataReceived);


            //Setting up internal command process event
            cmdProcessComplete.PropertyChange += new objectUpdate.PropertyChangeHandler(cmdProcessComplete_PropertyChange);
            statusReceived.PropertyChange += new objectUpdate.PropertyChangeHandler(statusReceived_PropertyChange);

            //Setting up the process command background worker.
            bwProcessor = new System.ComponentModel.BackgroundWorker();
            bwProcessor.WorkerSupportsCancellation = true;
            bwProcessor.DoWork += new System.ComponentModel.DoWorkEventHandler(bwProcessor_DoWork);

            //Setting up th incoming data stream.
            bwIncomingStream = new System.ComponentModel.BackgroundWorker();
            bwIncomingStream.WorkerSupportsCancellation = true;
            bwIncomingStream.DoWork += new System.ComponentModel.DoWorkEventHandler(bwIncomingStream_DoWork);
            bwIncomingStream.RunWorkerAsync();

            //Starting the timer.
            processCheck = new System.Timers.Timer(100);    //Setting to 100ms to provent delays.
            processCheck.Elapsed += new System.Timers.ElapsedEventHandler(processCheck_Elapsed);
            processCheck.Enabled = true;

            //Timer for checking for a radiosonde.
            /*
            portCheck = new System.Timers.Timer(1200);
            portCheck.Elapsed += new System.Timers.ElapsedEventHandler(portCheck_Elapsed);
            portCheck.Enabled = true;
            */

            portCheck = new System.Threading.Thread(portSearch);
            portCheck.Name = "radiosondeSearchProcess";
            portCheck.IsBackground = true;
            portCheck.Start();
        }

        /// <summary>
        /// Method searchs the port looking/waiting for radiosondes to connect.
        /// </summary>
        void portSearch()
        {
            while (true)
            {
                //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " -Sonde Count: " + cmdLog.Count.ToString());

                string snData = getSerialNumber();  //Sending out a request for a serial number. If I get null then nothing is connected.

                if (!radiosondeConnected)
                {

                    if (snData == null)
                    {
                        cmdLog.Clear();     //Clearing past radiosondes command log
                        cmd.Clear();        //Clearing pending radiosonde commands.
                    }
                }
                else
                {
                    DateTime currentTime = DateTime.Now;
                    TimeSpan timePassed = currentTime - lastPacketRecTime;

                    int disconnectWaitTime = 3;     //Time the system will wait before disconnecting the system.
                    if (modeInitGPS)
                    {
                        disconnectWaitTime = 6;     //Radiosonde is startup still and initing the GPS so a longer wait time is needed to provent false disconnect.
                    }

                    if (timePassed.TotalSeconds > disconnectWaitTime)    //Enough time has passed that I belive no radiosondes is still connected.
                    {
                        if (radiosondeConnected)    //If a radiosonde was connected doing the following.
                        {
                            disconnectRadiosonde.UpdatingObject = "Radiosonde Disconnected";    //Sending out event indicating that the past radiosonde has been removed.
                            System.Diagnostics.Debug.WriteLine("Radiosonde Disconnected");
                            radiosondeConnected = false;        //Setting the radiosonde connected state to false.
                            modeMFG = false;
                        }
                    }

                    //Starting mfg mode when fulling running.
                    if (radiosondeConnected && cmdLog.Count > 3 && !modeMFG)
                    {
                        setManufacturingMode(true);
                    }

                }
                
                System.Threading.Thread.Sleep(1200);    //Pause between checking.

            }   
        }

        void comPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Telling the system that a new radiosonde has connected.
            if (!radiosondeConnected)
            {
                radiosondeConnected = true;
                readyRadiosonde.UpdatingObject = "Radiosonde Connected";
                System.Diagnostics.Debug.WriteLine("Radiosonde Connected");

                //setManufacturingMode(true);
            }
        }

      
        void portCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " -Sonde Count: " + cmdLog.Count.ToString());

            if (cmdLog.Count > 0) //Dectecting disconnected radiosonde.
            {
                DateTime currentTime = DateTime.Now;
                TimeSpan timePassed = currentTime - lastPacketRecTime;

                //System.Diagnostics.Debug.WriteLine(timePassed.TotalSeconds);

                if (timePassed.TotalSeconds > 3)    //Enough time has passed that I belive no radiosondes is still connected.
                {
                    
                    cmdLog.Clear();     //Clearing past radiosondes command log
                    cmd.Clear();        //Clearing pending radiosonde commands.

                    if (radiosondeConnected)    //If a radiosonde was connected doing the following.
                    {
                        disconnectRadiosonde.UpdatingObject = "Radiosonde Disconnected";    //Sending out event indicating that the past radiosonde has been removed.
                        System.Diagnostics.Debug.WriteLine("Radiosonde Disconnected");
                        radiosondeConnected = false;        //Setting the radiosonde connected state to false.
                    }
                }
            }
            
            if(cmdLog.Count == 0)
            {
                //If no data in cmdLog then checking port for radiosondes.
                getSerialNumber();
                if (cmdLog.Count > 0)
                {
                    command responce = cmdLog.Last();
                    if (responce.strResponce.ToLower().Contains("sn"))
                    {
                        if (!radiosondeConnected)
                        {
                            cmd.Clear(); //clearing any additional command that have found there way into the que.
                            radiosondeConnected = true;
                            readyRadiosonde.UpdatingObject = "Radiosonde Connected";
                            System.Diagnostics.Debug.WriteLine("Radiosonde Connected");
                            setManufacturingMode(true);
                        }
                    }
                }
            }


        }
        

        #region Methods relateing to processing data in and out of the system.

        void processCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)  //Kick starts the data collector if no data has been received in a while.
        {
            if (!bwProcessor.IsBusy && cmd.Count > 0)
            {
                bwProcessor.RunWorkerAsync();   //Starting background worker.
            }
        }

        void bwProcessor_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

                while (cmd.Count > 0)
                {
                    if (cmd.Count > 0)
                    {
                        command tempOut = cmd.First();  //Pulling the first command in to be processed.

                        dataRequested = new System.Threading.AutoResetEvent(false); //Resetting to provent reading old data.

                        sendCommand(tempOut.strData);      //Sending out command.
                        //System.Threading.Thread.Sleep(100);    //Taking a small break to all the command to be process.
                        if (!dataRequested.WaitOne(5000))
                        {
                            Exception noResponce = new Exception("No responce");
                            error.UpdatingObject = noResponce;

                            tempOut.strResponce = noResponce.Message;

                            //Removing the command from the list. Protecting if it has already been cleared.
                            if (cmd.Count > 0)
                            {
                                cmd.Remove(cmd.First());
                            }

                            addToLog(tempOut);
                        }
                        else
                        {
                            tempOut.strResponce = currentResponce;    //Getting a responce from the command.

                            //Removing the command from the list. Protecting if it has already been cleared.
                            if (cmd.Count > 0)
                            {
                                cmd.Remove(cmd.First());    //Removing the processed command from the list
                            }

                            addToLog(tempOut);        //Adding the processed command to the log.

                            cmdProcessComplete.UpdatingObject = tempOut;
                        }
                    }

                    if (cmd.Count > 0)
                    {
                        System.Threading.Thread.Sleep(150); //If there is more then one command to process take a small break.
                    }
                }

        }

        void bwIncomingStream_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!bwIncomingStream.CancellationPending)
            {
                if (comPort.BytesToRead > 0)
                {
                    string incomingData = comPort.ReadLine();   //Reading in total readiosonde line.
                    lastPacketRecTime = DateTime.Now;   //Updating the lastest time data was last received.

                    //Sorting responce into poll responces or status data.
                    if (incomingData.Contains('=') || incomingData.Contains("error") || incomingData.ToLower().Contains("ok") || incomingData.ToLower().Contains("rsv"))  //This is bull shit where is the consistance on this sonde!!!!!
                    {
                        if (incomingData.Contains("error"))
                        {
                            Exception errorMessage = new Exception(incomingData);
                            error.UpdatingObject = errorMessage;
                        }

                        //Requested Data.
                        currentResponce = incomingData;
                        dataRequested.Set();
                    }
                    else
                    {
                        //Status Data.
                        statusReceived.UpdatingObject = incomingData;   //Triggering event to indicated that a new status message has been received and is in need of processing.
                    }

                }
                else
                {
                    System.Threading.Thread.Sleep(150);
                }
            }
        }

        /// <summary>
        /// Addeding commands to the log in a controlled way.
        /// </summary>
        /// <param name="newCmd"></param>
        private void addToLog(command newCmd)
        {
            cmdLog.Add(newCmd);     //Adding the current command to the log.

            //Editing the log to make sure there is no overrun
            if (cmdLog.Count > 300)
            {
                cmdLog.Remove(cmdLog.First());
            }
        }

        public command getLastCommand()
        {
            return cmdLog.Last();
        }

        #region Methods for handeling coms

        private void sendCommand(string data)
        {
            comPort.WriteLine(data);
        }

        private string getComData()
        {
            return comPort.ReadLine();
        }

        /// <summary>
        /// Not needed but left over code.
        /// </summary>
        /// <param name="message"></param>
        private void addCommandToQue(byte[] message)
        {
            command curCommand = new command();     //Creating the temp command
            curCommand.Date = DateTime.Now;     //Logging the current time.
            curCommand.data = message;          //Adding the intended message to the command
            cmd.Add(curCommand);        //Putting the command into the que.
        }

        public void addCommandToQue(string message) //Should be private
        {
            //Checking command for CR NL
            if (message.Substring(message.Length - 1, 1) != "\r")
            {
                message = message + "\r";
            }

            command curCommand = new command();     //Creating the temp command
            curCommand.Date = DateTime.Now;     //Logging the current time.
            curCommand.strData = message;          //Adding the intended message to the command
            cmd.Add(curCommand);        //Putting the command into the que.
        }

        public void addCommandToQue(string message, bool needResponce)
        {
            //Checking command for CR NL
            if (!message.Contains("\r"))
            {
                message = message + "\r";
            }

            if (needResponce)
            {
                dataProcessed = new System.Threading.AutoResetEvent(false);
            }

            command curCommand = new command();     //Creating the temp command
            curCommand.Date = DateTime.Now;     //Logging the current time.
            curCommand.strData = message;          //Adding the intended message to the command
            cmd.Add(curCommand);        //Putting the command into the que.
        }

        /// <summary>
        /// Method processes the repsonces form the commands issued.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void cmdProcessComplete_PropertyChange(object sender, PropertyChangeEventArgs data)
        {
            command curData = (command)data.NewValue;
            dataProcessed.Set();
        }

        /// <summary>
        /// New status message received and is in need of processing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void statusReceived_PropertyChange(object sender, PropertyChangeEventArgs data)
        {
            string line = (string)data.NewValue;

            if(line.Contains("booting"))    //Tiggering event to inform a radiosondes is connected and booting.
            {
                bootingRadiosonde.UpdatingObject = line;
            }

            if (line.Contains("init gps"))  //Triiger to indicated starting GPS
            {
                modeInitGPS = true;     //Telling the system that the radiosondes is starting up and connecting to the GPS.
                initGPSRadiosonde.UpdatingObject = line;
            }

            if (line.Contains("ready"))     //Triggering to inform that radiosondes is ready.
            {
                radiosondeConnected = true;
                modeInitGPS = false;    //Radiosonde finished starting up.
                readyRadiosonde.UpdatingObject = line;
            }

            //PTU Line
            if (line.Contains("PTUX:"))
            {
                updatePTUData(line);
                return;
            }
            //STAT Line
            if (line.Contains("STAT:"))
            {
                updateStatusData(line);
                return;
            }
            //GPS Line
            if (line.Contains("GPS:"))
            {
                updateGPSData(line);
                return;
            }
            //GPSX Line
            if (line.Contains("GPSX:"))
            {
                updateGPSXData(line);
                return;
            }
            //Calmode Line
            if (line.Split(',').Length == 16)
            {
                updateCalibrationData(line);
                return;
            }
        }

        #endregion

        

        #endregion

        #region Methods involved with updating the current status of the radiosonde

        private void updateCalibrationData(string line)
        {
            if (line.Contains("?")) //Filtering out any error packet.
            {
                return;
            }

            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.CalData.ComPortName = this.ComPortName;
                this.CalData.FirmwareVersion = this.FirmwareVersion;
                this.CalData.PacketNumber = int.Parse(splitLine[0]);
                this.CalData.SerialNumber = splitLine[1];
                this.CalData.ProbeID = splitLine[2];
                this.CalData.SondeID = splitLine[3];
                this.CalData.Pressure = double.Parse(splitLine[4]);
                this.CalData.PressureCounts = int.Parse(splitLine[5]);
                this.CalData.PressureRefCounts = int.Parse(splitLine[6]);
                this.CalData.Temperature = double.Parse(splitLine[7]);
                this.CalData.TemperatureCounts = int.Parse(splitLine[8]);
                this.CalData.TemperatureRefCounts = int.Parse(splitLine[9]);
                this.CalData.Humidity = double.Parse(splitLine[10]);
                this.CalData.HumidityFrequency = double.Parse(splitLine[11]);
                this.CalData.PressureTemperature = double.Parse(splitLine[12]);
                this.CalData.PressureTempCounts = int.Parse(splitLine[13]);
                this.CalData.PressureTempRefCounts = int.Parse(splitLine[14]);
                this.CalData.InternalTemp = double.Parse(splitLine[15]);
            }

            CalDataUpdate.UpdatingObject = this.CalData;

            //System.Diagnostics.Debug.WriteLine(this.StatData.Firmware);
        }

        private void updatePTUData(string line)
        {
            line = line.Replace("PTUX:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.PTUData.Pressure = double.Parse(splitLine[0]);
                this.PTUData.AirTemperature = double.Parse(splitLine[1]);
                this.PTUData.RelativeHumidity = double.Parse(splitLine[2]);
                this.PTUData.HumidityTemperature = double.Parse(splitLine[3]);
            }

            PTUDataUpdate.UpdatingObject = this.PTUData;
        }

        private void updateStatusData(string line)
        {
            line = line.Replace("STAT:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.StatData.BatteryVoltage = double.Parse(splitLine[0]);
                this.StatData.InternalTemperature = double.Parse(splitLine[1]);
            }
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateFirmwareData(string line)
        {
            this.StatData.Firmware = line.Trim();
            this.CalData.FirmwareVersion = line.Trim();
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateTXModeData(string line)
        {
            this.StatData.TXMode = line.Trim();
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateGPSData(string line)
        {
            line = line.Replace("GPS:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.GPSData.Latitude = double.Parse(splitLine[0]);
                this.GPSData.Longitude = double.Parse(splitLine[1]);
                this.GPSData.Altitude = double.Parse(splitLine[2]);
                this.GPSData.NumberOfSatellites = int.Parse(splitLine[3]);
                this.GPSData.GPSTime = splitLine[4];
            }

            GPSDataUpdate.UpdatingObject = this.GPSData;

        }

        private void updateGPSXData(string line)
        {
            line = line.Replace("GPSX:", null);
            string[] splitLine = line.Split(',');
            lock (sondeDataLock)
            {
                this.GPSXData.Latitude = double.Parse(splitLine[0]);
                this.GPSXData.Longitude = double.Parse(splitLine[1]);
                this.GPSXData.Altitude = double.Parse(splitLine[2]);
                this.GPSXData.NumberOfSatellites = int.Parse(splitLine[3]);
                this.GPSXData.GPSTime = splitLine[4];
                this.GPSXData.EastVelocity = double.Parse(splitLine[5]);
                this.GPSXData.NorthVelocity = double.Parse(splitLine[6]);
                this.GPSXData.AscentVelocity = double.Parse(splitLine[7]);
            }
            GPSXDataUpdate.UpdatingObject = this.GPSXData;
        }
        #endregion

        #region Methods for issuing commands to the radiosonde.

        public void setManufacturingMode(bool mode_ON)
        {

            if (mode_ON)
            {
                addCommandToQue("calmode=on\r", true);
                if (dataProcessed.WaitOne(350))
                {
                    modeMFG = true;
                    getFirmwareVersion();
                }
            }
            else
            {
                addCommandToQue("calmode=off\r", true);
                if (dataProcessed.WaitOne(350))
                {
                    modeMFG = false;
                }
            }
        }

        public void setAllDataMode(bool mode_ON)
        {
            if (mode_ON)
            {
                addCommandToQue("data=on\r\n", true);
                dataProcessed.WaitOne();
            }
            else
            {
                addCommandToQue("data=off\r\n", true);
                dataProcessed.WaitOne();

            }

        }

        public void setGPSMode(bool mode_ON)
        {
            if (mode_ON)
            {
                addCommandToQue("gps=on\r", true);
                dataProcessed.WaitOne();
            }
            else
            {
                addCommandToQue("gps=off\r", true);
                dataProcessed.WaitOne();
            }
        }

        public string getSondeID()
        {
            addCommandToQue("sid\r", true);
            dataProcessed.WaitOne();
            command temp = getLastCommand();
            return temp.strResponce.Replace("sid=", null).Trim();
        }

        public void setSondeID(string newSondeID)
        {
            if (newSondeID.Length > 13)
            {
                Exception errorMessage = new Exception("The sonde ID must be less than 13 characters.");
                error.UpdatingObject = errorMessage;
            }

            string command = "sid=" + newSondeID + "\r";
            addCommandToQue(command, true);
            dataProcessed.WaitOne();

            saveConfiguration();
        }

        public void setProbeID(string newProbeID)
        {
            if (newProbeID.Length > 13)
            {
                Exception errorMessage = new Exception("The probe ID must be less than 13 characters.");
                error.UpdatingObject = errorMessage;
            }
            string command = "pid=" + newProbeID + "\r";
            addCommandToQue(command, true);
            dataProcessed.WaitOne();
            saveConfiguration();
        }

        public string getProbeID()
        {
            addCommandToQue("pid\r", true);
            dataProcessed.WaitOne();
            command temp = getLastCommand();
            return temp.strResponce.Replace("pid=", null).Trim();
        }

        public void setSerialNumber(string newSN)
        {
            if (newSN.Length > 13)
            {
                Exception errorMessage = new Exception("The serial number must be less than 13 characters.");
                error.UpdatingObject = errorMessage;
            }
            string command = "sn=" + newSN + "\r";
            addCommandToQue(command, true);
            dataProcessed.WaitOne();
            saveConfiguration();
        }

        public string getSerialNumber()
        {
            addCommandToQue("sn\r", true);

            if (dataProcessed.WaitOne(250))
            {
                command temp = getLastCommand();
                string response = temp.strResponce;
                response = response.Replace("sn=", null).Trim();

                //Added storing the to the local structure to help older sondes.
                CalData.SerialNumber = response;
                return response;
            }

            return null;
            
        }

        public bool setTXMode(string desiredMode)
        {
            bool modeSet = false;

            if (desiredMode.Contains("IMS") || desiredMode.Contains("bel202"))
            {
                string command = "txmode=" + desiredMode + "\r";
                addCommandToQue(command, true);
                dataProcessed.WaitOne();
                command temp = getLastCommand();

                if (temp.strResponce.ToLower() == command.ToLower())
                {
                    modeSet = true;
                    updateTXModeData(temp.strResponce);
                    saveConfiguration();
                }
                else
                {
                    modeSet = false;
                }
            }

            saveConfiguration();

            return modeSet;

        }

        public string getFirmwareVersion()
        {
            string command = "vers\r";
            addCommandToQue(command, true);
            dataProcessed.WaitOne();
            command temp = getLastCommand();
            string response = temp.strResponce;
            updateFirmwareData(response);
            return response;
        }

        public bool setTXFreq(double[] desiredFreq)
        {
            //composing the txFreq command.
            string command = "txfreq=" + desiredFreq[0].ToString("0.00") + "," + desiredFreq[1].ToString("0.00") + "," +
                desiredFreq[2].ToString("0.00") + "," + desiredFreq[3].ToString("0.00") + "\r";
            addCommandToQue(command, true);
            dataProcessed.WaitOne();
            saveConfiguration();

            return true;
        }

        /// <summary>
        /// Method formats the double into the right format and check it.
        /// Unlike the old method it doesn't save after each write.
        /// </summary>
        /// <param name="coefficientNumber"></param>
        /// <param name="coefficientValue"></param>
        public void setCoefficient(int coefficientNumber, double coefficientValue)
        {
            if (coefficientNumber > 43 || coefficientNumber < 0)
            {
                Exception errorMessage = new Exception("Invalid coefficient number. Must be between 0 and 43");
                error.UpdatingObject = errorMessage;
            }
            string command = "cal[" + coefficientNumber.ToString() + "]=" + coefficientValue.ToString("0.00000e+00") + "\r";    //Formatting the command

            addCommandToQue(command, true); //Adding command to the que with a check needed.
            dataProcessed.WaitOne();        //Waiting for command to be processed.

            Universal_Radiosonde.command check = getLastCommand();  //Pulling last command from log.

            string[] breakDown = check.strResponce.Split('=');
            if (coefficientValue.ToString("0.00000e+00") != breakDown[1].Trim())
            {
                Exception errorMessage = new Exception("Coefficent doesn't match");
                error.UpdatingObject = errorMessage;
            }
        }

        public void saveConfiguration()
        {
            addCommandToQue("save\r", true);
            dataProcessed.WaitOne();        //Waiting for command to be processed.

            command temp = getLastCommand();
            if (!temp.strResponce.ToLower().Contains("ok"))
            {
                Exception errorMessage = new Exception("Config not saved.");
                error.UpdatingObject = errorMessage;
            }
        }

        #endregion
    }


    /// <summary>
    /// Class for event triggering the radiosonde based on GPS time. Also the time on.
    /// </summary>
    [Serializable]
    public class eventTrigger
    {
        public int CRLoc;
        public int triggerHR;
        public int triggerMIN;
        public int triggerTime;
    }

    [Serializable]
    public class command
    {
        public DateTime Date;
        public byte[] data;
        public byte[] responce;

        public string strData;
        public string strResponce;
    }
}
