﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComRadiosondePortMFG
{
    class equipmentRadiosondePortMFG
    {
        //Communication items
        TcpSimpleClient.simpleTcpClient clientTCP;  //TCP client for connecting via a network port
        System.IO.Ports.SerialPort comPort;     //Serial port for connection via a RS-232 device.

        //Radiosonde related data items.
        statusRadiosondePort curSondeStatus;  //The overall status of the radiosonde currently connected.


        //Command/Responce list
        List<string> commandsToSend;        //List of commands to be processd out to the device.
        List<string> responceToCommand;     //Responce from the device.
        DateTime lastTimeDataReceived;      //The last time some data was recieved.

        //Threads
        System.Threading.Thread threadProcessCommand;   //Thread for processing commands added to the command list.
        System.Threading.Thread threadCollectPortData;   //Thread for collecting PTU data from the device.
        bool controlInternalThread = false; //Used to kill internal process.
        System.Timers.Timer checkPort;  //Timer that will check for some kind of data received.

        //Events
        public updateCreater.objectUpdate eventErrors = new updateCreater.objectUpdate(); //Error event. Tiggering that something has gone wrong.
        public updateCreater.objectUpdate eventRawDataLineReceived = new updateCreater.objectUpdate(); //Event for triggering when raw data coms in.
        public updateCreater.objectUpdate eventRawDataLineSent = new updateCreater.objectUpdate();  //Raw data sent out port.
        public updateCreater.objectUpdate eventNoNewData = new updateCreater.objectUpdate();    //No data received for a while

        public updateCreater.objectUpdate eventNewRadiosondeConnect = new updateCreater.objectUpdate(); //Event triggered when a new radiosonde is connect.
        public updateCreater.objectUpdate eventFlashLoaded = new updateCreater.objectUpdate();  //Flash load message received.
        public updateCreater.objectUpdate eventPressureSensorOnline = new updateCreater.objectUpdate(); //Pressure sensor online!
        public updateCreater.objectUpdate eventGPSOnline = new updateCreater.objectUpdate();    //GPS is online.
        public updateCreater.objectUpdate eventPOSLLHActive = new updateCreater.objectUpdate(); //GPS POSLLH message active.
        public updateCreater.objectUpdate eventSOLActive = new updateCreater.objectUpdate();    //GPS SOL message is active.
        public updateCreater.objectUpdate eventTransmitterOnline = new updateCreater.objectUpdate();    //RF transmitter is online.
        public updateCreater.objectUpdate eventRadiosondeReady = new updateCreater.objectUpdate();      //Event triggered when all startup messages have beed received.

        //Met Data items.
        public updateCreater.objectUpdate eventPressureDataReady = new updateCreater.objectUpdate();  //Pressure Data Ready
        public updateCreater.objectUpdate eventTemperatureDataReady = new updateCreater.objectUpdate();    //Air Temperature Data Ready
        public updateCreater.objectUpdate eventHumidityDataReady = new updateCreater.objectUpdate();   //Humidity Data Ready
        public updateCreater.objectUpdate eventRTDDataReady = new updateCreater.objectUpdate();    //RTD Data Ready

        //SOL/GPS Data information
        public updateCreater.objectUpdate eventSOLDataReady = new updateCreater.objectUpdate();    //GPS Data Ready
        public updateCreater.objectUpdate eventLLHDataReady = new updateCreater.objectUpdate();    //latitude/longitude/height Data Ready

        public updateCreater.objectUpdate eventBatteryDataReady = new updateCreater.objectUpdate();    //Battery Data Ready
        public updateCreater.objectUpdate eventSavedToFlash = new updateCreater.objectUpdate(); //Event to indicate that the settings have been saved.
        public updateCreater.objectUpdate eventTXPowerDataReady = new updateCreater.objectUpdate();  //Transmitter data ready.
        public updateCreater.objectUpdate eventHeaterDataReady = new updateCreater.objectUpdate();  //Heater update ready.
        public updateCreater.objectUpdate eventPressureBiasReady = new updateCreater.objectUpdate();    //Pressure bias data ready.

        #region Methods for setting up the connection to the radiosonde.

        /// <summary>
        /// Generic container for a Radiosonde connection object.
        /// </summary>
        public equipmentRadiosondePortMFG()
        {
            if (commandsToSend == null) { commandsToSend = new List<string>(); }    //Init the command list.

            curSondeStatus = new statusRadiosondePort();  //Setting up a new statis container.
            lastTimeDataReceived = DateTime.Now;    //Start time.
        }

        /// <summary>
        /// Method for connecting to a MFG port via a TCP/IP port
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public equipmentRadiosondePortMFG(string ipAddress, int port)
        {
            if (commandsToSend == null) { commandsToSend = new List<string>(); }    //Init the command list.

            curSondeStatus = new statusRadiosondePort();  //Setting up a new statis container.

            checkPort = new System.Timers.Timer(2000);
            checkPort.Elapsed += new System.Timers.ElapsedEventHandler(checkPort_Elapsed);
            checkPort.Enabled = true;

            clientTCP = new TcpSimpleClient.simpleTcpClient();  //Setting up the connection.
            clientTCP.simpleClientSetupTCP(ipAddress, port);    //Passing the connection information and connecting.
            clientTCP.recivedMessage.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(recivedMessage_PropertyChange); //Subscribing to the recieved message events.
            clientTCP.error.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(error_PropertyChange);   //Subscribing to the TCP/IP error events.

            controlInternalThread = true;

            //Starting the command processor.
            threadProcessCommand = new System.Threading.Thread(threadProcessCommandList);
            threadProcessCommand.Name = "Process Command List Thread";
            threadProcessCommand.IsBackground = true;
            threadProcessCommand.Start();
        }

        /// <summary>
        /// Method for connecting to a MFG via a rs-232 serial port.
        /// </summary>
        /// <param name="comPort"></param>
        public equipmentRadiosondePortMFG(string comPortDesired)
        {
            if (commandsToSend == null) { commandsToSend = new List<string>(); }    //Init the command list.

            curSondeStatus = new statusRadiosondePort();  //Setting up a new statis container.

            checkPort = new System.Timers.Timer(2000);
            checkPort.Elapsed += new System.Timers.ElapsedEventHandler(checkPort_Elapsed);
            checkPort.Enabled = true;

            comPort = new System.IO.Ports.SerialPort(comPortDesired, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);  //Setting up the serial port connection.

            eventRawDataLineReceived.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(eventRawDataLineReceived_PropertyChange);

            controlInternalThread = true;

            //Starting a thread to collect data from the serial port.
            threadCollectPortData = new System.Threading.Thread(threadCollectSerialData);
            threadCollectPortData.Name = "Serial Port monitor";
            threadCollectPortData.IsBackground = true;
            threadCollectPortData.Start();

            //Starting the command processor.
            threadProcessCommand = new System.Threading.Thread(threadProcessCommandList);
            threadProcessCommand.Name = "Process Command List Thread";
            threadProcessCommand.IsBackground = true;
            threadProcessCommand.Start();
        }

        public void connect()
        {
            if (clientTCP != null)
            {
              
            }

            if (comPort != null)
            {
                try
                {
                    comPort.Open(); //Opening the com port
                }
                catch (Exception comError)
                {
                    eventErrors.UpdatingObject = comError;  //Unable to connect/open the port.
                }
            }
        }

        public void disconnect()
        {
            if (clientTCP != null)
            {
            }

            if (comPort != null)
            {
                try
                {
                    comPort.Close();    //Closing the com port.
                    controlInternalThread = false;
                    checkPort.Enabled = false;
                }
                catch (Exception closeError)
                {
                    eventErrors.UpdatingObject = closeError;    //Something went wrong.
                }
            }
        }

        void checkPort_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimeSpan timePassed = DateTime.Now - lastTimeDataReceived;

            if (timePassed.TotalSeconds > 10)
            {
                eventNoNewData.UpdatingObject = "No data recieved.";
            }
        }


        /// <summary>
        /// Resets the internal data to get ready for a new radiosonde.
        /// </summary>
        void resetConnection()
        {
            eventNewRadiosondeConnect.UpdatingObject = "New Radiosonde";    //Triggering the new radiosonde event.
            curSondeStatus = new statusRadiosondePort();    //Resetting the contatiners for current data.
            commandsToSend.Clear();     //Clearing any commands left in the buffer.
        }

        public statusRadiosondePort getCurStatus()
        {
            //Compiling data.

            statusRadiosondePort tempData = new statusRadiosondePort(); //Creating a container for the data.

            tempData.firmwareType = curSondeStatus.firmwareType;
            tempData.firmwareVersion = curSondeStatus.firmwareVersion;
            tempData.flashLoaded = curSondeStatus.flashLoaded;
            tempData.initPressureSensor = curSondeStatus.initPressureSensor;
            tempData.initGPS = curSondeStatus.initGPS;
            tempData.initPOSLLH = curSondeStatus.initPOSLLH;
            tempData.initSOL = curSondeStatus.initSOL;
            tempData.initTransmitter = curSondeStatus.initTransmitter;

            tempData.serialNumber = curSondeStatus.serialNumber;
            tempData.heaterMode = curSondeStatus.heaterMode;
            tempData.heaterOn = curSondeStatus.heaterOn;
            tempData.txPower = curSondeStatus.txPower;

            tempData.pressure = curSondeStatus.pressure;
            tempData.pressureTemp = curSondeStatus.pressureTemp;
            tempData.countPressure = curSondeStatus.countPressure;
            tempData.countPressureTemp = curSondeStatus.countPressureTemp;

            tempData.airTemp = curSondeStatus.airTemp;
            tempData.airTempResistance = curSondeStatus.airTempResistance;
            tempData.airTempResistanceCorrected = curSondeStatus.airTempResistanceCorrected;

            tempData.humidity = curSondeStatus.humidity;
            tempData.humidityTemp = curSondeStatus.humidityTemp;
            tempData.humidityCount = curSondeStatus.humidityCount;
            tempData.humidityTempCount = curSondeStatus.humidityTempCount;

            tempData.airTempRTD = curSondeStatus.airTempRTD;
            tempData.airTempRTDMode = curSondeStatus.airTempRTDMode;
            tempData.airTempRTDResistance = curSondeStatus.airTempRTDResistance;

            tempData.battery = curSondeStatus.battery;

            tempData.ecefX = curSondeStatus.ecefX;
            tempData.ecefY = curSondeStatus.ecefY;
            tempData.ecefZ = curSondeStatus.ecefZ;
            tempData.ecefSat = curSondeStatus.ecefSat;
            tempData.ecefTime = curSondeStatus.ecefTime;

            tempData.gpsLat = curSondeStatus.gpsLat;
            tempData.gpsLong = curSondeStatus.gpsLong;
            tempData.gpsElipsoid = curSondeStatus.gpsElipsoid;
            tempData.gpsAlt = curSondeStatus.gpsAlt;
            tempData.gpsSat = curSondeStatus.gpsSat;

            return tempData;
        }

        #endregion


        #region Methods related to commanding the device.

        //Thread for sending out the command to the unit.
        void threadProcessCommandList()
        {
            if (commandsToSend == null) { commandsToSend = new List<string>(); }    //Init the command list.

            while (controlInternalThread)   //Allowing for controlling the thread.
            {
                if (commandsToSend.Count > 0)   //If there is a command in the list send it out.
                {
                    sendData(commandsToSend[0]);    //Sending the qued message out to the device.

                    if (commandsToSend.Count > 0)   //Protecting from trying to removed data that doesn't exsist.
                    {
                        commandsToSend.RemoveAt(0);     //Removing the command from the que.
                    }

                    System.Threading.Thread.Sleep(50);
                }
                else  //If nothing is to be processed wait a while.
                {
                    System.Threading.Thread.Sleep(50);
                }
            }
        }

        #region Threads related to communication with the serial port.

        /// <summary>
        /// Thread for collecting data from the serial port.
        /// </summary>
        void threadCollectSerialData()
        {
            string incomingData = "";   //Data line container.

            while (controlInternalThread)   //Run till the shutdown is issued.
            {

                if (comPort.IsOpen) //Checking to see if the serial port is open.
                {

                        if (comPort.BytesToRead > 0)    //If there is data in the buffer process it.
                        {
                            
                            byte[] byteData = new byte[comPort.BytesToRead]; //Setting up a raw container.
                            comPort.Read(byteData, 0, byteData.Length); //Reading in all data in buffer.

                            string curData = System.Text.Encoding.UTF8.GetString(byteData);  //Converting the bytes to a string.

                            for (int i = 0; i < curData.Length; i++)    //Moving through the incoming data looking for the \r\n then sending that out.
                            {
                                incomingData += curData[i]; //Compiling the data.

                                if (incomingData.Contains("\r\n"))   //\r\n Found Trigger the raw message found.
                                {
                                    incomingData = incomingData.Replace("\r\n", string.Empty);
                                    eventRawDataLineReceived.UpdatingObject = incomingData;     //Triggering that a new line has been found.
                                    incomingData = "";
                                }
                            }

                        }
                        else
                        {
                            System.Threading.Thread.Sleep(100);     //If no data fround taking a 100ms break.
                        }
                
                }
                else
                {
                    eventErrors.UpdatingObject = new Exception("Serial Port " + comPort.PortName + " is not open.");    //Error event.
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Method for sending out data to the device.
        /// </summary>
        /// <param name="message"></param>
        public void sendData(string message)
        {
            if (comPort != null)    //Checking to see if the serial port is configured for use.
            {
                try
                {
                    comPort.WriteLine(message); //Sending out the serial port.
                    eventRawDataLineSent.UpdatingObject = message;  //Triggering that the message has been sent.
                }
                catch (Exception sendError)
                {
                    eventErrors.UpdatingObject = sendError;

                    if (!comPort.IsOpen)
                    {
                        controlInternalThread = false;
                    }
                }
            }

            if (clientTCP != null)  //Checking to see if the TCP/IP port is configure for use.
            {
                clientTCP.clientWriteMessageTCP(message);   //Sening out a ASCII formatted message out the TCP/IP port.
            }
        }

        #endregion

        #region TCP/IP event threads

        void error_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception currentError = null;  //Init the exception

            if (data.NewValue.GetType().ToString() == "string")  //If the incoming data is a string reformatting it to a except.
            {
                currentError = new Exception((string)data.NewValue);
            }
            else
            {
                currentError = (Exception)data.NewValue;    //Passing the incoming exception out.
            }

            eventErrors.UpdatingObject = currentError;  //Sending out the error exception.
        }

        #endregion

        #region Methods related to responding to incoming data.

        void eventRawDataLineReceived_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            lastTimeDataReceived = DateTime.Now;    //Getting the time the last line was received.
            processData((string)data.NewValue); //Sending the data out to be processed.
        }

        void recivedMessage_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            processData((string)data.NewValue); //Sending the data out to be processed.     
        }

        /// <summary>
        /// Method for processing incoming messages.
        /// </summary>
        /// <param name="data"></param>
        void processData(string data)
        {

            #region startup messages from the radiosonde.

            if (data.Contains("Radiosonde Firmware"))    //First line of communications from the iMet-4/iQ-3
            {
                resetConnection();  //Resetting container to init state.
                string[] breakData = data.Split(' ');   //breaking down the data line.
                curSondeStatus.firmwareType = breakData[0];   //The firmware type
                curSondeStatus.firmwareVersion = breakData[breakData.Length - 1]; //Firmware version.
                return;
            }

            if (data.Contains("Default flash configuration used."))
            {
                curSondeStatus.flashLoaded = true; //Flash loaded baby!
                eventFlashLoaded.UpdatingObject = data;
                return;
            }
            
            if (data.Contains("Flash configuration loaded successfully."))
            {
                curSondeStatus.flashLoaded = true; //Flash loaded baby!
                eventFlashLoaded.UpdatingObject = data;
                return;
            }
            
            if (data.Contains("Initializing pressure sensor...Online.")) 
            {
                curSondeStatus.initPressureSensor = true;   //Pressure sensor online.
                eventPressureSensorOnline.UpdatingObject = data;
                return;
            } 

            if (data.Contains("Ublox CAM-M8Q configured successfully."))
            {
                curSondeStatus.initGPS = true;  //UBlox's GPS is online..
                eventGPSOnline.UpdatingObject = data;
                return;
            }   

            if (data.Contains("POSLLH message activated."))
            {
                curSondeStatus.initPOSLLH = true;   //POSLLH connected.
                eventPOSLLHActive.UpdatingObject = data;
                return;
            } 

            if (data.Contains("SOL message activated."))
            {
                curSondeStatus.initSOL = true;  //SOL connected.
                eventSOLActive.UpdatingObject = data;
                return;
            }   

            if (data.Contains("CC115L Transmitter online and enabled."))    //Transmitter connected to.
            {
                curSondeStatus.initTransmitter = true;    //Transmitter data patch is good and online.
                eventTransmitterOnline.UpdatingObject = data;
                eventRadiosondeReady.UpdatingObject = "Ready";  //Triggering that the radiosonde is ready.
                commandSRN();   //Getting the serial number.
                return;
            }

            #endregion

            #region Responces to requests for data.

            string responce = ""; //The command responce form the radiosonde.

            if (data.Length > 2) //Error handeling around the ok command.
            {
                responce = data.Substring(0, 3);
            }
            else
            {
                responce = data;
            }

            switch (responce)
            {
                case "SPP":
                    string[] pBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.pressure = Convert.ToDouble(pBreak[1]) / 100;
                    curSondeStatus.pressureTemp = Convert.ToDouble(pBreak[2]) / 100;
                    curSondeStatus.countPressure = Convert.ToInt32(pBreak[3]);
                    curSondeStatus.countPressureTemp = Convert.ToInt32(pBreak[4]);

                    eventPressureDataReady.UpdatingObject = data;   //Sending out the pressure data line as a que.
                    break;

                case "SPT":
                    string[] tBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.airTemp = Convert.ToDouble(tBreak[1]) / 100;
                    curSondeStatus.airTempResistance = Convert.ToDouble(tBreak[2]);
                    curSondeStatus.airTempResistanceCorrected = Convert.ToDouble(tBreak[3]);

                    eventTemperatureDataReady.UpdatingObject = data;
                    break;

                case "SPH":
                    string[] hBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.humidity = Convert.ToDouble(hBreak[1]) / 10;
                    curSondeStatus.humidityTemp = Convert.ToDouble(hBreak[2]) / 100;

                    if (hBreak.Length > 3)  //Expanded humidity.
                    {
                        curSondeStatus.humidityCount = Convert.ToInt32(hBreak[3]);
                        curSondeStatus.humidityTempCount = Convert.ToInt32(hBreak[4]);
                    }

                    eventHumidityDataReady.UpdatingObject = data;
                    break;

                case "SOL":
                    string[] ecefBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.ecefX = Convert.ToInt32(ecefBreak[1]);
                    curSondeStatus.ecefY = Convert.ToInt32(ecefBreak[2]);
                    curSondeStatus.ecefZ = Convert.ToInt32(ecefBreak[3]);
                    curSondeStatus.ecefSat = Convert.ToInt16(ecefBreak[4]);
                    curSondeStatus.gpsSat = Convert.ToInt16(ecefBreak[4]);
                    curSondeStatus.ecefTime = Convert.ToInt32(ecefBreak[5]);

                    eventSOLDataReady.UpdatingObject = data;
                    break;

                case "LLH":
                    string[] llhBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.gpsLat = Convert.ToDouble(llhBreak[1]) / 10000000;
                    curSondeStatus.gpsLong = Convert.ToDouble(llhBreak[2]) / 10000000;
                    curSondeStatus.gpsElipsoid = Convert.ToDouble(llhBreak[3]) / 10000; //Not sure about this one.
                    curSondeStatus.gpsAlt = Convert.ToDouble(llhBreak[4]) / 1000;

                    eventLLHDataReady.UpdatingObject = data;
                    break;

                case "BAT":
                    string[] batBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.battery = Convert.ToDouble(batBreak[1]) / 1000;

                    eventBatteryDataReady.UpdatingObject = data;
                    break;

                //case "RTD":
                //    string[] rtdBreak = data.Split(new char[] { '=', ',' });
                //    curSondeStatus.airTempRTD = Convert.ToDouble(rtdBreak[1]) / 1000;
                //    curSondeStatus.airTempRTDResistance = Convert.ToDouble(rtdBreak[2]);
                //    curSondeStatus.airTempRTDMode = Convert.ToInt16(rtdBreak[3]);

                //    eventRTDDataReady.UpdatingObject = data;
                //    break;

                case "SRN":
                    string[] srnBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.serialNumber = srnBreak[1];
                    break;

                case "TXM":
                    string[] txmBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.txMode = Convert.ToInt16(txmBreak[1]);
                    break;

                case "TXD":
                    string[] txdBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.txDeviation = Convert.ToInt16(txdBreak[1]);
                    break;

                case "HTR":
                    string[] htrBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.heaterMode = Convert.ToInt16(htrBreak[1]);
                    curSondeStatus.heaterOn = Convert.ToBoolean(Convert.ToInt16(htrBreak[2]));
                    eventHeaterDataReady.UpdatingObject = data;
                    break;

                case "TXP":
                    string[] txpBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.txPower = Convert.ToInt16(txpBreak[1]);
                    eventTXPowerDataReady.UpdatingObject = Convert.ToInt16(txpBreak[1]);
                    break;

                case "CPB":
                    string[] cpbBreak = data.Split(new char[] { '=', ',' });
                    curSondeStatus.curPressureBias = Convert.ToInt16(cpbBreak[1]);
                    eventPressureBiasReady.UpdatingObject = Convert.ToInt16(cpbBreak[1]);
                    break;


                case "OK":
                    eventSavedToFlash.UpdatingObject = data;
                    break;
            }



            #endregion
        }

        #endregion

        #endregion

        #region Commands that can be sent.

        public void commandReset()  //Resets firmware.
        {
            commandsToSend.Add("/RST\r");
        }

        public void commandSave()   //Saves radiosonde config.
        {
            commandsToSend.Add("/SAV\r");
        }

        public void commandSPP()    //Request Pressure.
        {
            commandsToSend.Add("/SPP\r");
        }

        public void commandSPT()    //Request Air Temperature
        {
            commandsToSend.Add("/SPT\r");
        }

        public void commandSPH()    //Request Humidity
        {
            commandsToSend.Add("/SPH\r");
        }

        public void commandGPS()    //Request GPS ECEF
        {
            //commandsToSend.Add("/GPS");
        }

        public void commandLLH()    //Request Lat,Long,Alt
        {
            commandsToSend.Add("/LLH\r");
        }

        public void commandBAT()    //Request Battery Voltage
        {
            commandsToSend.Add("/BAT\r");
        }

        //public void commandRTD()    //Request Humidity Temperature
        //{
        //    commandsToSend.Add("/RTD");
        //}

        public void commandSRN()    //Request serial number
        {
            commandsToSend.Add("/SRN?\r");
        }

        public void commandHTR()
        {
            commandsToSend.Add("/HTR?\r");
        }

        public void commandSetHTR(string state)
        {
            commandsToSend.Add("/HTR=" + state + "\r\n");
        }

        public void setSRN(string desiredSN)    //Setting the serial number
        {
            commandsToSend.Add("/SRN=" + desiredSN + "\r\n");    //Sending the desired SN to the radiosonde.
            commandSave();      //Saving the new sn to flash.
        }

        public void getPressureBias()
        {
            commandsToSend.Add("/CPB?\r");
        }

        public void setPressureBias(double pressure)
        {

            commandsToSend.Add("/CPB=" + (pressure * 100).ToString("0") + "\r\n");
            commandSave();
        }


        /// <summary>
        /// Methods for loading air temperature coefs.
        /// </summary>
        /// <param name="coef"></param>
        public void commandLoadATCoef(double[] coef)
        {
            for (int i = 0; i < coef.Length; i++)
            {
                commandsToSend.Add("/ST" + i.ToString() + "=" + coef[i].ToString("E8"));    //Building the coef output.
            }

            commandSave();  //Saving the changes.
        }

        /// <summary>
        /// Method for setting the power level of the transmitter.
        /// </summary>
        /// <param name="power"></param>
        public void commandSetTXPower(int power)
        {
            
            if (power >= 0 && power < 6) //Checking to make sure this is acceptable settings.
            {
                commandsToSend.Add("/TXP=" + power.ToString() + "\r\n");     //Building the tx power command.
            }

        }

        /// <summary>
        /// Gets the current tx power settings.
        /// </summary>
        public void commandGetTXPower()
        {
            commandsToSend.Add("/TXP?\r");
        }

        public void commandGetTXMode()
        {
            commandsToSend.Add("/TXM?\r");
        }

        public void commandSetTXMode(int mode)
        {
            commandsToSend.Add("/TXM=" + mode.ToString() + "\r\n");
        }

        public void commandGetTXDeviation()
        {
            commandsToSend.Add("/TXD?\r");
        }

        public void commandSetTXDeviation(int deviation)
        {
            commandsToSend.Add("/TXD=" + deviation.ToString() + "\r\n");
        }

        #endregion

    }

    public class statusRadiosondePort
    {
        public DateTime dataTime;

        public string firmwareType = "";
        public string firmwareVersion = "";
        public bool flashLoaded = false;
        public bool initPressureSensor = false;
        public bool initGPS = false;
        public bool initPOSLLH = false;
        public bool initSOL = false;
        public bool initTransmitter = false;

        //Radiosonde Equipment Items
        public string serialNumber = "";
        public int heaterMode = 0;
        public bool heaterOn = false;
        public int txPower = 0;
        public int txMode = 9;      //Transmittion mode.
        public int txDeviation = 9999;      //Transmittion Deviation
        public int curPressureBias = 99999;    //Pressure bias setting.

        //Radiosonde Sensor Data.
        //Pressure
        public double pressure;     //mBar  
        public double pressureTemp; //C
        public Int32 countPressure;    //Count of Pressure Sensor.
        public Int32 countPressureTemp;    //Count of pressure temp sensor. Also used as the internal temp.

        //Air Temp
        public double airTemp;  // C
        public double airTempResistance;    //Resistance in OHMs
        public double airTempResistanceCorrected;   //Corrected resistance

        //Humidity
        public double humidity;     //Humidity
        public double humidityTemp; //Humidity Temp
        public Int32 humidityCount; //IST humidity Count
        public Int32 humidityTempCount;   //IST humidity temp Count


        //RTD PT100
        public double airTempRTD;   //C
        public double airTempRTDResistance; //Resistance
        public int airTempRTDMode;  //Mode. Read/Heat

        //Battery
        public double battery;  //Battery Voltage

        //Radiosonde ECEF GPS Data
        public Int32 ecefX;
        public Int32 ecefY;
        public Int32 ecefZ;
        public int ecefSat;
        public Int32 ecefTime;

        //Radiosonde LLH GPS Data
        public double gpsLat;
        public double gpsLong;
        public double gpsElipsoid;
        public double gpsAlt;
        public double gpsSat;

        //Way to init all the data items.
        public statusRadiosondePort() 
        {
            dataTime = DateTime.Now;

            pressure = 0;
            pressureTemp = 0;
            countPressure = 0;
            countPressureTemp = 0;

            airTemp = 0;
            airTempResistance = 0;
            airTempResistanceCorrected = 0;

            humidity = 0;
            humidityTemp = 0;

            airTempRTD = 0;
            airTempRTDResistance = 0;
            airTempRTDMode = 3;

            battery = 0;

            ecefX = 0;
            ecefY = 0;
            ecefZ = 0;
            ecefSat = 0;
            ecefTime = 0;

            gpsLat = 0;
            gpsLong = 0;
            gpsAlt = 0;
            gpsSat = 0;
        }
    }
}
