﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TcpSimpleClient;

namespace ipRadiosonde
{
    [Serializable]
    public struct CalData
    {
        public string ComPortName;
        public string FirmwareVersion;
        public int PacketNumber;
        public string SerialNumber;
        public string ProbeID;
        public string SondeID;
        public double Pressure;
        public int PressureCounts;
        public int PressureRefCounts;
        public double Temperature;
        public int TemperatureCounts;
        public int TemperatureRefCounts;
        public double Humidity;
        public double HumidityFrequency;
        public double PressureTemperature;
        public int PressureTempCounts;
        public int PressureTempRefCounts;
        public double InternalTemp;
    }

    [Serializable]
    public struct PTUData
    {
        public double Pressure;
        public double AirTemperature;
        public double RelativeHumidity;
        public double HumidityTemperature;
    }

    [Serializable]
    public struct StatData
    {
        public double BatteryVoltage;
        public double InternalTemperature;
        public string Firmware;
        public string TXMode;
    }

    [Serializable]
    public struct GPSData
    {
        public double Latitude;
        public double Longitude;
        public double Altitude;
        public int NumberOfSatellites;
        public string GPSTime;
    }

    [Serializable]
    public struct GPSXData
    {
        public double Latitude;
        public double Longitude;
        public double Altitude;
        public int NumberOfSatellites;
        public string GPSTime;
        public double EastVelocity;
        public double NorthVelocity;
        public double AscentVelocity;
    }

    [Serializable]
    public class ipUniRadiosonde
    {
        //Public veriables
        public string ipAddress = "";
        public int port = 0;

        //Update object for the raw incoming data.
        public updateCreater.objectUpdate rawDataRec = new updateCreater.objectUpdate();

        //Update objects for the radiosonde data elements.
        public updateCreater.objectUpdate CalDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate PTUDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate StatDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate GPSDataUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate GPSXDataUpdate = new updateCreater.objectUpdate();

        //Updates relateding to radiosonde connected massages
        public updateCreater.objectUpdate bootingRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate initGPSRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate readyRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate disconnectRadiosonde = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate error = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate otherDataCollected = new updateCreater.objectUpdate();

        //Connecting to the data containers
        public CalData CalData = new CalData();
        public PTUData PTUData = new PTUData();
        public StatData StatData = new StatData();
        public GPSData GPSData = new GPSData();
        public GPSXData GPSXData = new GPSXData();

        //Private veriables
        string tempIncomingBuffer = "";

        string mode = "";

        //List of packet recieved by this radiosonde port.
        List<radiosondeDataPacket> recBuffer = new List<radiosondeDataPacket>();
        
        //Internal contructed communication socket.
        private simpleTcpClient clientRadiosonde;

        //Background worker for detecting and reseting connetion when radiosondes are removed.
        System.ComponentModel.BackgroundWorker radiosondeConnectionTest = new System.ComponentModel.BackgroundWorker();

        //Timer for checking if things are ok
        System.Timers.Timer connectionTest = new System.Timers.Timer(5000);

        //Timer for collecting the data not found in other modes like ptu,gps,stat, and calmode.
        System.Timers.Timer otherDataCollector = new System.Timers.Timer(2500);

        //Long term testing.
        System.Timers.Timer checkPort = new System.Timers.Timer(20000);

        bool activeRadiosonde = false;

        public ipUniRadiosonde(){}

        /// <summary>
        /// Use this method only if you have already configured the port.
        /// </summary>
        public void open()
        {
            open(ipAddress, this.port);
        }

        public void open(string address, int port)
        {
            //Sending out to public
            ipAddress = address;
            this.port = port;

            //Setting up tcp client
            clientRadiosonde = new simpleTcpClient();
            clientRadiosonde.simpleClientSetupTCP(address, this.port);

            //Setting up a way to collect data from the tcp client
            clientRadiosonde.recivedMessage.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(processingRawResponce);

            //New packet received. Que the process to see what it is and sort it out.
            this.rawDataRec.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(processLatestDataPacket);

            //Setting up and starting the radiosonde watchdog background worker.
            /*
            radiosondeConnectionTest.WorkerSupportsCancellation = true;
            radiosondeConnectionTest.DoWork += new System.ComponentModel.DoWorkEventHandler(radiosondeConnectionTest_DoWork);
            radiosondeConnectionTest.RunWorkerAsync();
            */
            connectionTest.Elapsed += new System.Timers.ElapsedEventHandler(connectionTest_Elapsed);
            connectionTest.Enabled = true;

            //Long Term Port checker
            checkPort.Elapsed += new System.Timers.ElapsedEventHandler(checkPort_Elapsed);

            //Other data timer
            otherDataCollector.Elapsed += new System.Timers.ElapsedEventHandler(otherDataCollector_Elapsed);
            otherDataCollector.Enabled = true;


            //Starting check on port to see if a radiosonde is connected.
            activeRadiosonde = detectSonde();


            //setting the id to the propertiest
            CalDataUpdate.objectID = address + ":" + port.ToString();
            GPSDataUpdate.objectID = address + ":" + port.ToString();
            PTUDataUpdate.objectID = address + ":" + port.ToString();
            disconnectRadiosonde.objectID = address + ":" + port.ToString();
            bootingRadiosonde.objectID = address + ":" + port.ToString();
            initGPSRadiosonde.objectID = address + ":" + port.ToString();
            readyRadiosonde.objectID = address + ":" + port.ToString();
            error.objectID = address + ":" + port.ToString();
            otherDataCollected.objectID = address + ":" + port.ToString();

        }

        public void close()
        {
            activeRadiosonde = false;

            //Stopping timers.
            connectionTest.Elapsed -= new System.Timers.ElapsedEventHandler(connectionTest_Elapsed);
            connectionTest.Enabled = false;

            //Long Term Port checker
            checkPort.Elapsed -= new System.Timers.ElapsedEventHandler(checkPort_Elapsed);
            checkPort.Enabled = false;

            //Other data timer
            otherDataCollector.Elapsed -= new System.Timers.ElapsedEventHandler(otherDataCollector_Elapsed);
            otherDataCollector.Enabled = false;

            //Closing the tcp socket
            try
            {
                clientRadiosonde.close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

            //shutting down object.
            clientRadiosonde = null;

            recBuffer.Clear();
        }

        public void autoStartCalMode(bool state)
        {
            if (state)
            {
                readyRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
                mode = "autoCal";
            }
            else
            {
                readyRadiosonde.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
                mode = "";
            }
        }

        void readyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            setManufacturingMode(true);
        }


        public bool detectSonde()
        {
            string sondeSN = getSerialNumber();

            if (sondeSN != "" && !sondeSN.Contains("sn"))
            {
                if (sondeSN.Contains('p'))
                {
                    if (mode == "autoCal")
                    {
                        setManufacturingMode(true);
                    }
                    return true;
                }
                else
                {
                    if (Convert.ToInt32(sondeSN) % 2 == 0 || Convert.ToInt32(sondeSN) % 1 == 0)
                    {
                        if (mode == "autoCal")
                        {
                            setManufacturingMode(true);
                        }
                        return true;
                    }

                    else
                    {
                        checkPort.Enabled = true; //Starting long term checks for radiosondes.
                        return false;
                    }
                }
            }

            return false;
        }

        void connectionTest_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (recBuffer.Count > 0) //Checking to make sure there is data in the log.
            {

                radiosondeDataPacket checking = null;
                try
                {
                    checking = recBuffer.Last();
                }
                catch (Exception curError)
                {
                    System.Diagnostics.Debug.WriteLine(curError.Message);
                    return;
                }


                TimeSpan timePassed = (DateTime.Now - checking.packetDate);

                if (timePassed.TotalSeconds > 6 && activeRadiosonde)
                {
                    double timePass = secSinceLastPacket();

                    //Performing double check
                    if (!detectSonde())
                    {

                        if (timePass > 6)
                        {
                            activeRadiosonde = false;
                            radiosondeDataPacket disconnect = new radiosondeDataPacket();
                            disconnect.ipAddress = ipAddress;
                            disconnect.port = port;
                            disconnect.packetDate = DateTime.Now;
                            disconnect.packetMessage = "Disconnected";

                            //Clearing data from the old radiosonde.
                            clearData();

                            disconnectRadiosonde.UpdatingObject = disconnect;
                            checkPort.Enabled = true; //Starting long term checks for radiosondes.
                        }
                    }
                }
            }
            else
            {
                checkPort.Enabled = true;
            }
        }

        void otherDataCollector_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (activeRadiosonde) //If there is a radiosonde then do this.
            {
                /*
                getAllCoefficients();
                getTransmitterRF();

                getFirmwareVersion();
                getTXMode();
                */
            }
        }

        /// <summary>
        /// Method clears out old data from a disconnected radiosonde.
        /// </summary>
        private void clearData()
        {
            //Clearing cal data.
            CalData.ComPortName = "";
            CalData.FirmwareVersion = "";
            CalData.PacketNumber = 0;
            CalData.SerialNumber = "";
            CalData.ProbeID = "";
            CalData.SondeID = "";
            CalData.Pressure = 0;
            CalData.PressureCounts = 0;
            CalData.PressureRefCounts = 0;
            CalData.Temperature = 0;
            CalData.TemperatureCounts = 0;
            CalData.TemperatureRefCounts = 0;
            CalData.Humidity = 0;
            CalData.HumidityFrequency = 0;
            CalData.PressureTemperature = 0;
            CalData.PressureTempCounts = 0;
            CalData.PressureTempRefCounts = 0;
            CalData.InternalTemp = 0;

            //clearing PTU data
            PTUData.Pressure = 0;
            PTUData.AirTemperature = 0;
            PTUData.RelativeHumidity = 0;
            PTUData.HumidityTemperature = 0;

            //Clearing Status data
            StatData.BatteryVoltage = 0;
            StatData.InternalTemperature = 0;
            StatData.Firmware = "";
            StatData.TXMode = "";

            //Clearing GPS data
            GPSData.Latitude = 0;
            GPSData.Longitude = 0;
            GPSData.Altitude = 0;
            GPSData.NumberOfSatellites = 0;
            GPSData.GPSTime = "";

            //Clearing GPSX data
            GPSXData.Latitude = 0;
            GPSXData.Longitude = 0;
            GPSXData.Altitude = 0;
            GPSXData.NumberOfSatellites = 0;
            GPSXData.GPSTime = "";
            GPSXData.EastVelocity = 0;
            GPSXData.NorthVelocity = 0;
            GPSXData.AscentVelocity = 0;

        }

        /// <summary>
        /// This checks the port every 20 sec looking for radiosonde.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void checkPort_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!activeRadiosonde)
            {
                string sondeSNCheck = getSerialNumber(); //might replace with detect sonde.
                double timePass = secSinceLastPacket();
                if (timePass < 19)
                {
                    activeRadiosonde = true;
                    checkPort.Enabled = false;
                }

            }
        }

        public void sendSondeCommand(string message)
        {
            if (clientRadiosonde != null)
            {
                clientRadiosonde.clientWriteMessageTCP(message);
            }
            else
            {
                SystemException errorNull = new SystemException("Object Null, or port closed");
                error.UpdatingObject = errorNull;
            }
        }

        private string getSondeResponse(string lookingFor)
        {
            System.Threading.Thread.Sleep(300);

            //Pervents null data connection.
            if (recBuffer.Count == 0)
            {
                return "";
            }

            radiosondeDataPacket currentDataPacket = recBuffer.Last();

            if (currentDataPacket.packetMessage.Contains(lookingFor))
            {
                return currentDataPacket.packetMessage.Substring(0, currentDataPacket.packetMessage.Length - 2);
            }
            else //a more indepth look is started.
            {
                /*
                int packetCounter = 2;
                while (packetCounter != 0)
                {
                    currentDataPacket = recBuffer[recBuffer.Count - 1];

                    if (currentDataPacket.packetMessage.Contains(lookingFor))
                    {
                        return currentDataPacket.packetMessage.Substring(0, currentDataPacket.packetMessage.Length - 2);
                    }

                    packetCounter--;
                }
                */

                System.Threading.Thread.Sleep(700);

                int counter = recBuffer.Count - 1; //The last packet.

                //Preventing -1 error from finding packets.
                if (counter == -1)
                {
                    return "";
                }

                while ((DateTime.Now - recBuffer[counter].packetDate).TotalMilliseconds < 3000 && counter >= 0)
                {
                    double test = (DateTime.Now - recBuffer[counter].packetDate).TotalMilliseconds;

                    currentDataPacket = recBuffer[counter];

                    if (currentDataPacket.packetMessage.Contains(lookingFor))
                    {
                        return currentDataPacket.packetMessage.Substring(0, currentDataPacket.packetMessage.Length - 2);
                    }

                    if (counter != 0)
                    {
                        counter--;
                    }
                    else
                    {
                        break;
                    }
                }


            }
            return "";
        }

        /// <summary>
        /// Reports the time passed from last packet to now.
        /// </summary>
        /// <returns></returns>
        public double secSinceLastPacket()
        {
            if (recBuffer.Count != 0)
            {
                radiosondeDataPacket lastPacket = recBuffer.Last();
                TimeSpan timePassed = DateTime.Now - lastPacket.packetDate;
                return timePassed.TotalSeconds;
            }
            else
            {
                return 9999.99;
            }
        }

        /// <summary>
        /// Method return the state if a radiosonde is connected.
        /// </summary>
        /// <returns></returns>
        public bool getRadiosondeConnectionStatus()
        {
            return activeRadiosonde;
        }

        #region Methods for preocessing the data requests in independent background workers. This was done to provent system sleep hang-ups.

        private void processRequest(string outGoingCommand, string desiredResponce)
        {
            //Compiling the incoming data into a string [] that will be passed to the background worker for full processing.
            string[] dataToWorker = new string[2] {outGoingCommand,desiredResponce};

            //Starting the guy that will do the work and provent the lockup and errors.
            System.ComponentModel.BackgroundWorker backgroundWorkerProcessingRequest = new System.ComponentModel.BackgroundWorker();
            //backgroundWorkerProcessingRequest.DoWork += (o, s) => { throw new Exception( (string)s.Result); };
            backgroundWorkerProcessingRequest.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerProcessingRequest_DoWork);
            backgroundWorkerProcessingRequest.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerProcessingRequest_RunWorkerCompleted);
            backgroundWorkerProcessingRequest.RunWorkerAsync(dataToWorker);


        }

        void backgroundWorkerProcessingRequest_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Getting the command out of the event.
            string[] command = (string[])e.Argument;
            
            //Checking to make sure the command had the proper ending
            if (!command[0].Contains("\r\n"))
            {
                command[0] = command[0] + "\r\n";
            }
                
            //Sending out command
            sendSondeCommand(command[0]);

            //Taking a small break to allow the radiosonde to responde.
            System.Threading.Thread.Sleep(50);

            //Checking for the respnce.
            string response = this.getSondeResponse(command[1]);

            //If the responce is not found, or incorrect throwing a SystemException
            if (!(response.Contains(command[1])))
            {
                //This seems to not be catchable to I am making an event that can be subscribed to.
                //throw new SystemException("Radiosonde responded incorrectly and could not be put into manufacturing mode.");

                SystemException comError = new SystemException("Radiosonde responded incorrectly and could not be put into manufacturing mode.");
                error.UpdatingObject = (object)comError;
                
            }
        }

        void backgroundWorkerProcessingRequest_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine(e.Error);
        }

        #endregion
        
        #region Methods of setting modes and setting settings in the radiosonde

        public void setManufacturingMode(bool mode_ON)
        {
            string command = "";

            if (mode_ON)
            {
                //this.CalData.FirmwareVersion = getFirmwareVersion(); //Needs to be moved somewhere?
                //updateTXModeData(getTXMode()); //Needs to be moved somewhere?

                command = "calmode=on"; 
            }
            else
            {
                //Setting comand to "calmode=off" to  stop outputting the calmode data.
                command = "calmode=off";
            }

            //Sending out the command to be processed.
            processRequest(command, command);
        }

        public void setAllDataMode(bool mode_ON)
        {
            string command = "";
            string response = "";

            if (mode_ON)
            {
                command = "data=on";
                response = "ptu=on";
                //sendSondeCommand(command + "\r\n");
                //string response = this.getSondeResponse(command);
            }
            else
            {
                command = "data=off";
                response = "ptu=off";
            }

            //Sending out the command to be processed.
            processRequest(command, response);

        }

        /*
        public void setPollResponseMode()
        {
            System.Threading.Thread.Sleep(100);

            sendSondeCommand("ptu=off\r\n");
            string response = getSondeResponse();
            if (response != "ptu=off")
            {
                response = getSondeResponse();
            }

            System.Threading.Thread.Sleep(100);

            sendSondeCommand("stat=off\r\n");
            response = getSondeResponse();

            System.Threading.Thread.Sleep(100);

            sendSondeCommand("gps=off\r\n");
            response = getSondeResponse();

            System.Threading.Thread.Sleep(100);

            sendSondeCommand("calmode=off\r\n");
            response = getSondeResponse();
            return;
        }
        */

        public void setPTUMode(bool mode_ON)
        {
            string command = "";
            string response = "";

            if (mode_ON)
            {
                command = "ptu=on";
                response = "ptu=on";

            }
            else
            {
                command = "ptu=off";
                response = "ptu=off";
            }

            //Sending out the command to be processed.
            processRequest(command, response);
        }

        public void setStatusMode(bool mode_ON)
        {
            string command = "";
            string response = "";

            if (mode_ON)
            {
                command = "stat=on";
                response = "stat=on";
            }
            else
            {
                command = "stat=off";
                response = "stat=off";
            }

            //Sending out the command to be processed.
            processRequest(command, response);
        }

        public void setGPSMode(bool mode_ON)
        {
            string command = "";
            string response = "";

            if (mode_ON)
            {
                command = "gps=on";
                response = "gps=on";
            }
            else
            {
                command = "gps=off";
                response = "gps=off";
            }

            //Sending out the command to be processed.
            processRequest(command, response);
        }

        public void saveConfiguration()
        {
            string command = "save";
            string response = "OK";

            //Sending out the command to be processed.
            processRequest(command, response);

        }

        public string getSondeID()
        {
            string command = "sid";
            sendSondeCommand(command + "\r\n");
            string response = this.getSondeResponse(command);
            response = response.Replace("sid=", null);
            return response;
        }

        public void setSondeID(string newSondeID)
        {
            if (newSondeID.Length > 13)
            {
                throw new SystemException("The sonde ID must be less than 13 characters.");
            }
            string command = "sid=" + newSondeID;
            sendSondeCommand(command + "\r\n");
            command = command.Trim().ToLower();
            string response = this.getSondeResponse(command);
            if (response != command)
            {
                throw new SystemException("Invalid sonde response: " + response);
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse("OK");
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
        }

        public void setProbeID(string newProbeID)
        {
            if (newProbeID.Length > 13)
            {
                throw new SystemException("The probe ID must be less than 13 characters.");
            }
            string command = "pid=" + newProbeID;
            sendSondeCommand(command + "\r\n");
            command = command.Trim().ToLower();
            System.Threading.Thread.Sleep(1000);

            string response = getSondeResponse(command);

            if (response != command)
            {
                throw new SystemException("Invalid sonde response: " + response);
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse("OK");
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
        }

        public string getProbeID()
        {
            sendSondeCommand("pid\r\n");
            string response = this.getSondeResponse("pid");
            response = response.Replace("pid=", null);
            return response;
        }

        public void setSerialNumber(string newSN)
        {
            if (newSN.Length > 13)
            {
                throw new SystemException("The serial number must be less than 13 characters.");
            }
            string command = "sn=" + newSN;
            sendSondeCommand(command + "\r\n");
            command = command.Trim().ToLower();
            string response = this.getSondeResponse(command);
            if (response != command)
            {
                throw new SystemException("Invalid sonde response: " + response);
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse("OK");
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
        }

        public string getSerialNumber()
        {
            sendSondeCommand("sn\r\n");
            string response = this.getSondeResponse("sn");
            response = response.Replace("sn=", null);
            CalData.SerialNumber = response;
            return response;
        }

        public void setTransmitterRF(bool ON)
        {
            if (ON)
            {
                string command = "txrf=on";
                sendSondeCommand(command + "\r\n");
                string response = this.getSondeResponse(command);
                if (!(response == command))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be put into GPS mode.");
                }
            }
            else
            {
                string command = "txrf=off";
                sendSondeCommand(command + "\r\n");
                string response = this.getSondeResponse(command);
                if (!(response == command))
                {
                    throw new SystemException("Radiosonde responded incorrectly and could not be taken out of GPS mode.");
                }
            }
        }

        public bool getTransmitterRF()
        {

            sendSondeCommand("txrf\r\n");
            string response = this.getSondeResponse("txrf");
            response = response.Replace("txrf=", null);
            if (response == "on")
            {
                return true;
            }
            if (response == "off")
            {
                return false;
            }
            throw new SystemException("Invalid sonde response: " + response);
        }

        /// <summary>
        /// Sets one coef to the radiosonde. Make sure to turn off add data modes before using.
        /// </summary>
        /// <param name="coefficientNumber"></param>
        /// <param name="coefficientValue"></param>
        public void setCoefficient(int coefficientNumber, double coefficientValue)
        {
            if (coefficientNumber > 43 || coefficientNumber < 0)
            {
                throw new SystemException("Invalid coefficient number. Must be between 0 and 43");
            }

            string command = "cal[" + coefficientNumber.ToString() + "]=" + coefficientValue.ToString("0.00000e+00");
            sendSondeCommand(command + "\r\n");
            System.Threading.Thread.Sleep(100);
            sendSondeCommand(command + "\r\n");
            System.Threading.Thread.Sleep(100);

            sendSondeCommand("save\r\n");
            /*
            string command = "cal[" + coefficientNumber.ToString() + "]=" + coefficientValue.ToString("0.00000e+00");
            sendSondeCommand(command + "\r\n");
            System.Threading.Thread.Sleep(150);
            command = command.Trim();
            string response = getSondeResponse(command);
            if (!(command == response))
            {
                throw new SystemException("Could not verify successful coefficient load.");
            }
            sendSondeCommand("save\r\n");
            response = this.getSondeResponse("OK");
            if (response != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }
             * 
             *
             */ 
        }

        public void setTXMode(string desiredMode)
        {
            if (desiredMode.Contains("IMS") || desiredMode.Contains("Bel202"))
            {
                string command = "txmode=" + desiredMode + "\r\n";
                sendSondeCommand(command);
                command = command.Trim();

                //this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);

                string response = getSondeResponse(command);
                if (!(command.ToLower() == response.ToLower()))
                {
                    throw new SystemException("Could not verify successful TX Mode");
                }
                sendSondeCommand("save\r\n");
                response = this.getSondeResponse("OK");
                if (response != "OK")
                {
                    throw new SystemException("Invalid sonde response to save command: " + response);
                }
            }
            else
            {

            }

            //Updating current tx object.
            updateTXModeData(getTXMode());

        }

        public bool setTXFreq(double[] desiredFreq)
        {
            //Results from programming the radiosonde.
            bool programmingResults = false;

            //composing the txFreq command.
            string command = "txfreq=" + desiredFreq[0].ToString("0.00") + "," + desiredFreq[1].ToString("0.00") + "," +
                desiredFreq[2].ToString("0.00") + "," + desiredFreq[3].ToString("0.00" + "\r\n");
            sendSondeCommand(command);

            string[] response = new string[4]; //Responce for sonde 5.09 and older
            string responseNew = ""; //Responce for 5.10 and newer.

            //Getting sonde revision number
            double revNumber = Convert.ToDouble(StatData.Firmware.Substring(3, StatData.Firmware.Length - 3));

            if (revNumber <= 5.09)
            {
                for (int i = 0; i < response.Length; i++)
                {
                    //this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
                    //response[i] = this.getSondeResponse();

                    //Checking responces
                    /*
                    for (int x = 0; x < desiredFreq.Length; x++)
                    {
                        if (response[x].Contains(desiredFreq[x].ToString()))
                        {
                            programmingResults = true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    */
                }
            }
            else
            {
                //this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
                responseNew = getSondeResponse("txfreq");

                //Checking responce
                if (responseNew.Contains(desiredFreq[0].ToString()) && responseNew.Contains(desiredFreq[1].ToString()) &&
                    responseNew.Contains(desiredFreq[2].ToString()) && responseNew.Contains(desiredFreq[3].ToString()))
                {
                    programmingResults = true;
                }

            }

            //Saving command to the system.
            sendSondeCommand("save\r\n");
            string responseSave = this.getSondeResponse("OK");
            if (responseSave != "OK")
            {
                throw new SystemException("Invalid sonde response to save command: " + response);
            }

            return programmingResults;
        }

        public double getCoefficient(int coefficientNumber)
        {
            if (coefficientNumber > 43 || coefficientNumber < 0)
            {
                throw new SystemException("Invalid coefficient number. Must be between 0 and 43");
            }
            string command = "cal[" + coefficientNumber.ToString() + "]";
            sendSondeCommand(command + "\r\n");
            string response = this.getSondeResponse("cal");
            response = response.Replace("cal[" + coefficientNumber.ToString() + "]=", null);
            return double.Parse(response);
        }

        public double[] getAllCoefficients()
        {
            double[] coefficients = new double[44];
            string command = "cals\r\n";
            string response;
            sendSondeCommand(command);
            for (int i = 0; i < coefficients.Length; i++)
            {
                response = this.getSondeResponse("cal");
                response = response.Replace("cal[" + i.ToString() + "]=", null).Trim();
                coefficients[i] = double.Parse(response);
            }
            return coefficients;
        }

        public string getFirmwareVersion()
        {
            //this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
            string command = "vers\r\n";
            sendSondeCommand(command);
            string response = this.getSondeResponse("RSv");
            updateFirmwareData(response);
            return response;
        }

        public string getTXMode()
        {
            //this.sondeResponseEvent = new System.Threading.AutoResetEvent(false);
            string command = "txmode\r\n";
            sendSondeCommand(command);
            string responce = this.getSondeResponse("txmode");
            updateTXModeData(responce);
            return responce;
        }

        public void Reset()
        {

            string command = "reset\r\n";

            //Sending out the command to be processed.
            processRequest(command, "");
        }
        

        #endregion

        #region Methods for processing data form the radiosonde.

        /// <summary>
        /// First meathod useding in all communications. All data coming from the system passed throw this method.
        /// It looks for complete messages and assemblies messages that have been sent in more then one packet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        private void processingRawResponce(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            DateTime timeDataRec = new DateTime();
            timeDataRec = DateTime.Now;

            //End of message chars
            char[] EOM = new char[2] {'\r','\n'};

            //Turing the byte data into a message
            byte[] incomingData = (byte[])data.NewValue;
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            //Removing addiional null data bytes.
            string str = enc.GetString(incomingData).Trim('\0');

            //Check for a complete data packet. If not a complete packet then place the data into the tempbuffer then wait for the next chunk.
                        
            if (!str.Contains("\r\n"))
            {
                tempIncomingBuffer += str;
                return;
            }

            if (str.Contains('�')) //Data error clearing buffers and starting over.
            {
                tempIncomingBuffer = "";
                return;
            }


            if (str.Contains("\r\n"))
            {
                str = tempIncomingBuffer + str;
                tempIncomingBuffer = "";

                //Checking to see if there is more data past the \r\n
                string[] breakingDown = str.Split(EOM);

                for(int s = 0; s<breakingDown.Length; s++)
                {
                    if (breakingDown[s] != "" && s < breakingDown.Length - 2)
                    {
                        radiosondeDataPacket currentData = new radiosondeDataPacket();
                        currentData.ipAddress = ipAddress;
                        currentData.port = port;
                        currentData.packetDate = timeDataRec;
                        currentData.packetMessage = breakingDown[s] + "\r\n";

                        //Managing the data in the buffer.
                        manageRecBuffer();

                        recBuffer.Add(currentData);
                    }
                    else
                    {
                        tempIncomingBuffer = breakingDown[breakingDown.Length - 1];
                    }
                }

            }

            //activeRadiosonde = true;


            rawDataRec.UpdatingObject = (object)recBuffer.Last();

        }

        /// <summary>
        /// Method remove the oldest radiosonde datapacket in the rec buffer.
        /// </summary>
        private void manageRecBuffer()
        {
            if (recBuffer.Count >= 360)
            {
                recBuffer.RemoveAt(0);
            }
        }

        private void processLatestDataPacket(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //opening up the data package
            radiosondeDataPacket currentData = (radiosondeDataPacket)data.NewValue;

            string line = currentData.packetMessage;

            //PTU Line
            if (line.Contains("PTUX:"))
            {
                updatePTUData(line);
                activeRadiosonde = true;
                return;
            }
            //STAT Line
            if (line.Contains("STAT:"))
            {
                updateStatusData(line);
                activeRadiosonde = true;
                return;
            }
            //GPS Line
            if (line.Contains("GPS:"))
            {
                updateGPSData(line);
                activeRadiosonde = true;
                return;
            }
            //GPSX Line
            if (line.Contains("GPSX:"))
            {
                updateGPSXData(line);
                activeRadiosonde = true;
                return;
            }
            //Calmode Line
            if (line.Split(',').Length == 16)
            {
                updateCalibrationData(line);
                activeRadiosonde = true;
                return;
            }
            if (line.Contains("booting"))
            {
                currentData.ipAddress = ipAddress;
                currentData.port = port;
                bootingRadiosonde.UpdatingObject = currentData;
            }

            if (line.Contains("init gps"))
            {
                currentData.ipAddress = ipAddress;
                currentData.port = port;
                initGPSRadiosonde.UpdatingObject = currentData;
            }

            if (line.Contains("ready"))
            {
                currentData.ipAddress = ipAddress;
                currentData.port = port;
                activeRadiosonde = true;
                readyRadiosonde.UpdatingObject = currentData;
            }
            if (line.Contains('=') || line.Contains("OK") || line.Contains("RSv"))
            {
                /*
                lock (sondeDataLock)
                {
                    this.sondeResponse = line;
                }
                if (this.sondeResponseEvent != null)
                {
                    this.sondeResponseEvent.Set();
                }
                return;
                */
            }
        }

        private void updateCalibrationData(string line)
        {
            string[] splitLine = line.Split(',');

            //Data checks to prevent errors.
            if (splitLine[0] == "")
            {
                return;
            }

            //lock (sondeDataLock)
            {
                //this.CalData.ComPortName = this.ComPortName;
                this.CalData.FirmwareVersion = getFirmwareVersion();
                this.CalData.PacketNumber = int.Parse(splitLine[0]);
                this.CalData.SerialNumber = splitLine[1];
                this.CalData.ProbeID = splitLine[2];
                this.CalData.SondeID = splitLine[3];
                this.CalData.Pressure = double.Parse(splitLine[4]);
                this.CalData.PressureCounts = int.Parse(splitLine[5]);
                this.CalData.PressureRefCounts = int.Parse(splitLine[6]);
                this.CalData.Temperature = double.Parse(splitLine[7]);
                this.CalData.TemperatureCounts = int.Parse(splitLine[8]);
                this.CalData.TemperatureRefCounts = int.Parse(splitLine[9]);
                this.CalData.Humidity = double.Parse(splitLine[10]);
                this.CalData.HumidityFrequency = double.Parse(splitLine[11]);
                this.CalData.PressureTemperature = double.Parse(splitLine[12]);
                this.CalData.PressureTempCounts = int.Parse(splitLine[13]);
                this.CalData.PressureTempRefCounts = int.Parse(splitLine[14]);
                this.CalData.InternalTemp = double.Parse(splitLine[15]);
            }

            CalDataUpdate.UpdatingObject = this.CalData;
        }

        private void updatePTUData(string line)
        {
            line = line.Replace("PTUX:", null);
            string[] splitLine = line.Split(',');
            //lock (sondeDataLock)
            {
                this.PTUData.Pressure = double.Parse(splitLine[0]);
                this.PTUData.AirTemperature = double.Parse(splitLine[1]);
                this.PTUData.RelativeHumidity = double.Parse(splitLine[2]);
                this.PTUData.HumidityTemperature = double.Parse(splitLine[3]);
            }

            PTUDataUpdate.UpdatingObject = this.PTUData;
        }

        private void updateStatusData(string line)
        {
            line = line.Replace("STAT:", null);
            string[] splitLine = line.Split(',');
            //lock (sondeDataLock)
            {
                this.StatData.BatteryVoltage = double.Parse(splitLine[0]);
                this.StatData.InternalTemperature = double.Parse(splitLine[1]);
            }
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateFirmwareData(string line)
        {
            this.StatData.Firmware = line;
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateTXModeData(string line)
        {
            this.StatData.TXMode = line;
            StatDataUpdate.UpdatingObject = this.StatData;
        }

        private void updateGPSData(string line)
        {
            line = line.Replace("GPS:", null);
            string[] splitLine = line.Split(',');
            //lock (sondeDataLock)
            {
                this.GPSData.Latitude = double.Parse(splitLine[0]);
                this.GPSData.Longitude = double.Parse(splitLine[1]);
                this.GPSData.Altitude = double.Parse(splitLine[2]);
                this.GPSData.NumberOfSatellites = int.Parse(splitLine[3]);
                this.GPSData.GPSTime = splitLine[4];
            }

            GPSDataUpdate.UpdatingObject = this.GPSData;

        }

        private void updateGPSXData(string line)
        {
            line = line.Replace("GPSX:", null);
            string[] splitLine = line.Split(',');
            //lock (sondeDataLock)
            {
                this.GPSXData.Latitude = double.Parse(splitLine[0]);
                this.GPSXData.Longitude = double.Parse(splitLine[1]);
                this.GPSXData.Altitude = double.Parse(splitLine[2]);
                this.GPSXData.NumberOfSatellites = int.Parse(splitLine[3]);
                this.GPSXData.GPSTime = splitLine[4];
                this.GPSXData.EastVelocity = double.Parse(splitLine[5]);
                this.GPSXData.NorthVelocity = double.Parse(splitLine[6]);
                this.GPSXData.AscentVelocity = double.Parse(splitLine[7]);
            }
            GPSXDataUpdate.UpdatingObject = this.GPSXData;
        }

        #endregion

        #region Methods dealing the access to the Radiosonde Data Packets

        /// <summary>
        /// Clears all the data in the buffer.
        /// </summary>
        public void clearRadiosondePacket()
        {
            recBuffer.Clear();
        }

        /// <summary>
        /// Returns all the data packets in the buffer.
        /// </summary>
        /// <returns></returns>
        public List<radiosondeDataPacket> getRadiosondeDataPackets()
        {
            return recBuffer;
        }

        #endregion

        #region Methods for testing the ipRadiosonde function.

        public void triggerDisconnectEvent()
        {
            //Creating the data packet message and seending it out.
            disconnectRadiosonde.UpdatingObject = new radiosondeDataPacket
            {
                ipAddress = ipAddress,
                port = port,
                packetDate = DateTime.Now,
                packetMessage = "Disconnected"
            };
        }

        #endregion
    }

    /// <summary>
    /// Radioasonde data packets.
    /// </summary>
    [System.Xml.Serialization.XmlInclude(typeof(radiosondeDataPacket))]
    [Serializable]
    public class radiosondeDataPacket
    {
        public DateTime packetDate;
        public string packetMessage;
        public string ipAddress;
        public int port;
    }

    [System.Xml.Serialization.XmlInclude(typeof(radiosondeCalData))]
    [Serializable]
    public class radiosondeCalData
    {
        public string sondeID;
        public string sondeRawFile;
        public string sondeRefFile;
        public double[] coefData;
        public string status;
        public double meanPressureError;
        public double stDevPressure;
        public double RMSPressure;
        public double meanTempError;
        public double stDevTemp;
        public double RMSTemp;
    }
}
