﻿//Created by William Jones for use with the B&B relay controller.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestEquipment;

namespace RelayController
{
    public class relayControllerSystem
    {
        //Public Veriables
        public string[] RelayFunction()
        {
            return relayFunction;
        }

        //Event that is triggered when a command has happen.
        public updateCreater.objectUpdate commandEvent = new updateCreater.objectUpdate();


        //Local Veriables
        private BBModule relay;     //relay controller for a maximum of 16 relay.
        private equipmentNCD.equipmentNCDRelay NCDRelay;    //TCP/IP based 32 relay controller board.
        private string mode = "B&B";    //Relay controller mode. The default is set to B&B for legacy support.

        /// <summary>
        /// This array repersents the 16 possiable relays on a B&B relay module.
        /// </summary>
        private bool[] relayStatus; //= new bool[16];

        /// <summary>
        /// These strings will hold one - two word desctriptings of the relay functinos.
        /// </summary>
        private string[] relayFunction; //= new string[16];

        /// <summary>
        /// These arrays allow for a dependances to be declared. preventing relays from being thrown if one of the
        /// dependances is already open.
        /// </summary>
        private string[] relayDependency; //= new string[16];

        public relayControllerSystem()
        {
        }

        #region These methods are for setting up the objects and defining what each relay does.

        /// <summary>
        /// Method for setting the type of relay system being used.
        /// Default mode is B&B controller.
        /// Use B&B or NCD
        /// </summary>
        /// <param name="type"></param>
        public void setRelayType(string type)
        {
            if (type != "B&B" || type != "NCD")
            {
                mode = type;
            }
        }

        /// <summary>
        /// Starts up the relay board and preps the relay deffinitions.
        /// </summary>
        /// <param name="relayComPort"></param>
        /// <returns></returns>
        public string relayController(string relayComPort)
        {
            setupRelayBoard();  //Configureing the relay board as needed in the current mode.

            switch(mode)
            {
                case "B&B":
                    try
                    {
                        relay = new BBModule(relayComPort);
                        
                        //Opening com port
                        if (!relay.ComPort.IsOpen)
                        {
                            relay.ComPort.Open();
                        }

                        //Detecting the relay board. This will set all the relays to open.
                        relay.detectModule();

                        //Setting all the array to false;
                        for (int i = 0; i < relayStatus.Length; i++)
                        {
                            relayStatus[i] = false;
                        }

                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }
            }
            return null;

        }

        public string relayController(string ipaddress, int port)
        {
            setupRelayBoard();  //Configureing the relay board as needed in the current mode.

            switch (mode)
            {
                case "B&B":
                    try
                    {
                        relay = new BBModule(ipaddress, port);

                        //Detecting the relay board. This will set all the relays to open.
                        relay.detectModule();

                        relay.resetRelays();

                        //Setting all the array to false;
                        for (int i = 0; i < relayStatus.Length; i++)
                        {
                            relayStatus[i] = false;
                        }

                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }

                case "NCD":
                    try
                    {
                        NCDRelay = new equipmentNCD.equipmentNCDRelay(ipaddress, port);
                        NCDRelay.openAllRelays();

                        //Setting up the relay bank
                        int bankCount = this.relayFunction.Length / 8;
                        for (int x = 0; x < bankCount; x++)
                        {
                            NCDRelay.addRelayBank(x + 1, "Bank " + x + 1);
                        }

                        //Setting all the array to false;
                        for (int i = 0; i < relayStatus.Length; i++)
                        {
                            relayStatus[i] = false;
                        }
                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }

            }

            return null;

        }

        public void shutdownRelaySystem()
        {
            switch (mode)
            {
                case "NCD":
                    NCDRelay.openAllRelays();
                    //NCDRelay.disconnectRelay();    
                    break;
            }
        }

        /// <summary>
        /// Method for setting up the relay board. Default is B&B controller.
        /// </summary>
        private void setupRelayBoard()
        {
            int relayCount = 0;
            switch (mode)
            {
                case "B&B":
                    relayCount = 16;
                    break;

                case "NCD":
                    relayCount = 32;
                    break;
            }

            setRelayCount(relayCount); //Setting up the relay status, dependencies, counts.
        }

        public int getRelayCount()
        {
            return relayFunction.Length;
        }

        public void setRelayCount(int relayCount)
        {
            this.relayStatus = new bool[relayCount];
            this.relayFunction = new string[relayCount];
            this.relayDependency = new string[relayCount];
        }

        /// <summary>
        /// Input the relay number 0-15, the function to call it by, and the dependency.
        /// </summary>
        /// <param name="relayNumber"></param>
        /// <param name="relayFunctionDesc"></param>
        /// <param name="relayDependencyDesc"></param>
        /// <returns></returns>
        public string setRelayFunction(int relayNumber, string relayFunctionDesc, string relayDependencyDesc)
        {
            if (relayNumber >= relayFunction.Length)
            {
                return "Error, Relay Out Of Bounds.";
            }

            if (getRelayFunction(relayNumber) == null)
            {
                relayFunction[relayNumber] = relayFunctionDesc;

                if (relayDependencyDesc != "")
                {
                    relayDependency[relayNumber] = relayDependencyDesc;
                }
                return "Success";
            }
            else
            {
                return "Error, Function already declared. Clear before preceeding.";
            }
        }

        /// <summary>
        /// Returns in string format what the relay function is.
        /// </summary>
        /// <param name="relayNumber"></param>
        /// <returns></returns>
        public string getRelayFunction(int relayNumber)
        {
            if (relayNumber < relayFunction.Length)
            {
                return relayFunction[relayNumber];
            }
            else
            {
                throw new System.ArgumentException("Relay request out of bounds.");
            }
        }

        /// <summary>
        /// Resets relay to stating conditions, clears dependancies and function name.
        /// </summary>
        /// <param name="relayNumber"></param>
        /// <returns></returns>
        public string clearRelayFunction(int relayNumber)
        {
            if (relayNumber < relayFunction.Length)
            {
                relayDependency[relayNumber] = null;
                setRelayState(relayNumber, false);
                relayFunction[relayNumber] = null;
                return "Success";
            }

            throw new System.ArgumentException("Relay clear request out of bounds.");
        }

        /// <summary>
        /// Returns the dependendencies of the requested relay.
        /// </summary>
        /// <param name="relayNumber"></param>
        /// <returns></returns>
        public string getRelaydependency(int relayNumber)
        {
            if (relayNumber < relayDependency.Length)
            {
                return relayDependency[relayNumber];
            }
            else
            {
                throw new System.ArgumentException("Relay request out of bounds.");
            }
            
        }

        #endregion

        #region Basic methods for interacting with the relay


        /// <summary>
        /// Sets the relay state to the desired state.
        /// </summary>
        /// <param name="relayPos"></param>
        /// <param name="desiredState"></param>
        /// <returns></returns>
        private string setRelayState(int relayPos, bool desiredState)
        {
            switch (mode)
            {
                case "B&B":
                    try
                    {
                        relay.setOutput(relayPos, desiredState);
                        relayStatus[relayPos] = desiredState;
                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }

                case "NCD":
                    try
                    {
                        NCDRelay.commandRelay(relayPos + 1, desiredState);
                        relayStatus[relayPos] = desiredState;
                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }
            }

            return null;
        }

        /// <summary>
        /// Returns the requested relay state.
        /// </summary>
        /// <param name="relayPos"></param>
        /// <returns></returns>
        public bool getRelayState(int relayPos)
        {
            return relayStatus[relayPos];
        }

        public string commandRelayBoard(string desiredFunction, bool desiredState)
        {
            object[] eventData = new object[2]{ desiredFunction, desiredState };

            commandEvent.UpdatingObject = (object)eventData;

            //Check to make sure that any depent relay is not already set true.
            int relayIndex = getItemsIndex(desiredFunction);

            if (relayDependency[relayIndex] != null)
            {
                if (relayDependency[relayIndex] != "")
                {
                    string[] testRelayState = relayDependency[relayIndex].Split(',');

                    for (int i = 0; i < testRelayState.Length; i++)
                    {
                        if (getRelayState(Convert.ToInt16(testRelayState[i])))
                        {
                            return "Error, Relay " + getRelayFunction(Convert.ToInt16(testRelayState[i])) + " currently open.";
                        }
                    }
                }
            }

            for (int i = 0; i < relayFunction.Length; i++)
            {
                if (relayFunction[i] == desiredFunction)
                {
                    return setRelayState(getItemsIndex(desiredFunction), desiredState);
                }
            }

            return "Error, Unknown relay function.";

        }

        #endregion
        

        /// <summary>
        /// Method for finding the index of a description.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int getItemsIndex(string item)
        {
            for (int i = 0; i < relayFunction.Length; i++)
            {
                if (relayFunction[i] == item)
                {
                    return i;
                }
            }
            return 9999;
        }

        /// <summary>
        /// Checks to make sure the requested function exsist.
        /// </summary>
        /// <param name="itemToCheck"></param>
        /// <returns></returns>
        public bool checkValidRequest(string itemToCheck)
        {
            return relayFunction.Contains(itemToCheck);
        }

    }

    /// <summary>
    /// Class for storing temp settings. This helps to during setup of main programs to collect and build an object containing all the information needed from a config or setting file.
    /// </summary>
    public class relaySettings
    {
        public string relayID;
        public string relaySystemEquipmentIP;
        public int relaySystemEquipmentPort;
        public List<string> relaySystemEquipmentAddresses;
        public string relayType;
    }



}
