﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bill
{

    public class Interfaces
    {
        public static updateCreater.objectUpdate inputResult = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate inputResultNonStatic;

        //List of interfaces currently active.
        public List<System.Windows.Forms.Form> currentDisplay = new List<System.Windows.Forms.Form>();

        //Basic settup.
        public Interfaces() { }

        public static System.Windows.Forms.DialogResult InputBox(string title, string promptText, ref string value)
        {
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            System.Windows.Forms.Label label = new System.Windows.Forms.Label();
            System.Windows.Forms.TextBox textBox = new System.Windows.Forms.TextBox();
            System.Windows.Forms.Button buttonOk = new System.Windows.Forms.Button();
            System.Windows.Forms.Button buttonCancel = new System.Windows.Forms.Button();

            form.ShowInTaskbar = false;
            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | System.Windows.Forms.AnchorStyles.Right;
            buttonOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            System.Windows.Forms.DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static void InputBoxBW(string title, string promptText, string value)
        {
            string[] boxData = new string[]{ title, promptText, value };
            //System.ComponentModel.BackgroundWorker inputBW = new System.ComponentModel.BackgroundWorker();
            //inputBW.DoWork += new System.ComponentModel.DoWorkEventHandler(inputBW_DoWork);
            //inputBW.RunWorkerAsync(boxData);

            System.Threading.Thread inputBox = new System.Threading.Thread(inputBW_DoWork);
            inputBox.Start((object)boxData);
        }

        static void inputBW_DoWork(object data)//object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string[] incomingData = (string[])data;
            string title = incomingData[0];
            string promptText = incomingData[1];
            string value = incomingData[2];

            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            System.Windows.Forms.Label label = new System.Windows.Forms.Label();
            System.Windows.Forms.TextBox textBox = new System.Windows.Forms.TextBox();
            System.Windows.Forms.Button buttonOk = new System.Windows.Forms.Button();
            System.Windows.Forms.Button buttonCancel = new System.Windows.Forms.Button();

            form.ShowInTaskbar = false;
            form.Text = title;
            form.Name = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | System.Windows.Forms.AnchorStyles.Right;
            buttonOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            System.Windows.Forms.DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;


            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                inputResult.UpdatingObject = value;
            }
        }

        //Non static interface
        public void InputBoxBWNonStatic(string title, string promptText, string value)
        {
            string[] boxData = new string[]{ title, promptText, value };
            //System.ComponentModel.BackgroundWorker inputBW = new System.ComponentModel.BackgroundWorker();
            //inputBW.DoWork += new System.ComponentModel.DoWorkEventHandler(inputBW_DoWork);
            //inputBW.RunWorkerAsync(boxData);

            inputResultNonStatic = new updateCreater.objectUpdate();

            System.Threading.Thread inputBox = new System.Threading.Thread(inputBWnonStatic_DoWork);
            inputBox.Start((object)boxData);
        }

        void inputBWnonStatic_DoWork(object data)//object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string[] incomingData = (string[])data;
            string title = incomingData[0];
            string promptText = incomingData[1];
            string value = incomingData[2];

            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            System.Windows.Forms.Label label = new System.Windows.Forms.Label();
            System.Windows.Forms.TextBox textBox = new System.Windows.Forms.TextBox();
            System.Windows.Forms.Button buttonOk = new System.Windows.Forms.Button();
            System.Windows.Forms.Button buttonCancel = new System.Windows.Forms.Button();

            form.ShowInTaskbar = false;
            form.Text = title;
            form.Name = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | System.Windows.Forms.AnchorStyles.Right;
            buttonOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            //Adding the forum to the list of active forums.
            currentDisplay.Add(form);

            System.Windows.Forms.DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;

            //inputResult.UpdatingObject = value;
            inputResultNonStatic.objectID = dialogResult.ToString();    //Sending out the state of the selection.
            inputResultNonStatic.UpdatingObject = value;

            System.Threading.Thread.Sleep(1000);

            currentDisplay.Remove(form);
        }

    }


    public class statusPanel
    {
        public statusPanel()
        { }

        public statusPanel(string title, string message, int locX, int locY)
        {
            System.Windows.Forms.Panel panelStatus = new System.Windows.Forms.Panel();
            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            System.Windows.Forms.Label labelMessage = new System.Windows.Forms.Label();

            //Panel settings
            panelStatus.Location = new System.Drawing.Point(locX, locY);
            panelStatus.Width = 200;
            panelStatus.Height = 100;
            //panelStatus.Size = System.Drawing.Size(200, 100);
            panelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            panelStatus.Visible = true;
            panelStatus.Name = "Status Panel";

            //Title label settings
            labelTitle.SetBounds(5, 5, 372, 13);
            labelTitle.AutoSize = false;
            labelTitle.Text = title;
            labelTitle.Visible = true;

            //Message label settings
            labelMessage.SetBounds(5, 30, 200, 13);
            labelMessage.AutoSize = false;
            labelMessage.Text = message;
            labelMessage.Visible = true;

            panelStatus.Show();

        }
    }

    public class informationDisplay
    {
        public informationDisplay()
        { }

        public informationDisplay(string title, string message)
        {
            //Setting up visable display of status
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            System.Windows.Forms.Label labelCurrentProcess = new System.Windows.Forms.Label();
            System.Windows.Forms.Label labelCurrentAction = new System.Windows.Forms.Label();
            //System.Windows.Forms.Button buttonCancel = new System.Windows.Forms.Button();
            //System.Windows.Forms.ProgressBar progressBarCoeffients = new System.Windows.Forms.ProgressBar();

            form.ShowInTaskbar = false;
            form.Text = title;
            labelCurrentProcess.Text = title + ": ";
            labelCurrentAction.Text = message;
            //progressBarCoeffients.Minimum = 0;
            //if (coefficentNumber.Length > 20) { progressBarCoeffients.Maximum = coefficentNumber.Length + 6; }
            //else { progressBarCoeffients.Maximum = coefficentNumber.Length + 1; }
            //progressBarCoeffients.Step = 1;


            labelCurrentProcess.Font = new System.Drawing.Font(labelCurrentProcess.Font, System.Drawing.FontStyle.Bold);
            labelCurrentAction.Font = new System.Drawing.Font(labelCurrentAction.Font, System.Drawing.FontStyle.Bold);

            //buttonCancel.Text = "Cancel";
            //buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            labelCurrentProcess.SetBounds(2, 5, 372, 13);
            labelCurrentAction.SetBounds(9, 25, 760, 13);
            //buttonCancel.SetBounds(309, 72, 75, 23);
            //progressBarCoeffients.SetBounds(0, 94, 394, 13);

            labelCurrentProcess.AutoSize = true;
            labelCurrentAction.AutoSize = true;
            //buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(780, 50);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { labelCurrentProcess, labelCurrentAction });
            form.ClientSize = new System.Drawing.Size(Math.Max(780, labelCurrentProcess.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            form.BackColor = System.Drawing.Color.Red;
            //form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;

            //form.CancelButton = buttonCancel;
            form.Show();
        }
    }

    public class programErrorReport
    {
        public programErrorReport() { }

        //Class variables
        //Log File
        private string logFileLoc = "";
        
        //email settings
        private string fromAddress = "";
        private string serverAddress = "";
        private string serverPort = "";
        private string userName = "";
        private string password = "";
        private bool ssl = true;
        private string toAddress = "";

        //Setters
        public void setFromAddress(string from) { fromAddress = from; }
        public void setServerAddress(string server) { serverAddress = server; }
        public void setServerPort(string port) { serverPort = port; }
        public void setUserName(string user) { userName = user; }
        public void setPassword(string pass) { password = pass; }
        public void setSSL(bool _ssl) { ssl = _ssl; }
        public void setToAddress(string to) { toAddress = to; }
        public void setLogFileLoc(string file) { logFileLoc = file; }


        public void writeErrorLog(Exception incomingError)
        {
            System.IO.TextWriter loggingError = System.IO.File.AppendText(logFileLoc);
            DateTime errorTime = DateTime.Now;

            loggingError.WriteLine(errorTime.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            loggingError.WriteLine(incomingError.Source);
            loggingError.WriteLine(incomingError.Message);
            loggingError.WriteLine(incomingError.StackTrace);
            //If another layer if needed.
            if (incomingError.InnerException != null)
            {
                loggingError.WriteLine(incomingError.InnerException.Source);
                loggingError.WriteLine(incomingError.InnerException.Message);
                loggingError.WriteLine(incomingError.InnerException.StackTrace);
            }
            loggingError.WriteLine("");

            loggingError.Close();
        }

        public void sendErrorEmail(string errorDesc, Exception incomingError, DateTime errorTime)
        {
            //Email error to Admin
            Alerts.alertsEmail emailAdmin = new Alerts.alertsEmail();

            //Loading Admin and server config.
            string[] fileData = System.IO.File.ReadAllLines("Admin.cfg");

            emailAdmin.alertsSendEmail(toAddress, "Error in " + errorDesc + errorTime.ToString("yyyy-MM-dd HH:mm:ss"),
                "Error at: " + errorTime.ToString("yyyy-MM-dd HH:mm:ss") + " in " + incomingError.Source + "\n" + incomingError.Message + "\nSee log file at: " + logFileLoc +
                " for more detail.", fromAddress, serverAddress, serverPort, userName, password, ssl, false);

        }

        


    }

    public class boxCarAverage
    {
        //Public items.

        
        //Private items.
        List<dataPoint> boxCarData = new List<dataPoint>(); //The Data
        int secDifferance = 60; //Time Analized from one point to the next.
        int endointAvgCount = 10; //Amount of record averaged before differance.

        public boxCarAverage() { }

        public boxCarAverage(int desiredSecDifferance, int desiredEndPointAvgCount)
        {
            secDifferance = desiredSecDifferance;
            endointAvgCount = desiredEndPointAvgCount;
        }

        /// <summary>
        /// Method adds sent data to the boxCar list and return the process box car. If 9999.99 then box car is not ready.
        /// </summary>
        /// <param name="incomingData"></param>
        /// <returns></returns>
        public double addData(double incomingData)
        {
            //Formatting the data.
            dataPoint newData = new dataPoint();
            newData.dataDate = DateTime.Now;
            newData.dataDouble = incomingData;
            newData.dataMessage = "";

            manageData(); //Cleaning up the data.

            boxCarData.Add(newData); //Adding the data

            return processBoxCarAvg(); //Getting the desire box car average.
        }

        public double getCurrentBoxCar()
        {
            return processBoxCarAvg();
        }

        /// <summary>
        /// Method looks for current time data and past time data to create a box car average.
        /// </summary>
        /// <returns></returns>
        private double processBoxCarAvg()
        {
            double boxCar = 9999.99;
            DateTime searchTime = DateTime.Now;

            if (boxCarData.Count > secDifferance + endointAvgCount) //Waiting for enough data is in the system.
            {
                //Our lovly data.
                double pastPointAvg = 0;
                double currentPointAvg = 0;

                for (int x = 0; x < boxCarData.Count; x++)
                {
                    //Getting the current data
                    if (boxCarData[x].dataDate <= DateTime.Now.AddSeconds(0.7) && boxCarData[x].dataDate >= DateTime.Now.AddSeconds(-0.7))
                    {
                        for (int curCount = endointAvgCount; curCount > -1; curCount--)
                        {
                            currentPointAvg += boxCarData[x - curCount].dataDouble;
                        }

                        currentPointAvg = currentPointAvg / endointAvgCount;
                    }

                    

                    //Getting the past data

                    DateTime checking = searchTime.AddSeconds((Convert.ToDouble(secDifferance) + 0.7) * -1);
                    DateTime checking2 = searchTime.AddSeconds((Convert.ToDouble(secDifferance) - 0.7) * -1);

                    if (boxCarData[x].dataDate >= searchTime.AddSeconds((Convert.ToDouble(secDifferance) + 0.7) * -1) && boxCarData[x].dataDate <= searchTime.AddSeconds((Convert.ToDouble(secDifferance) - 0.7)*-1))
                    {
                        for (int pastCount = endointAvgCount; pastCount > -1; pastCount--)
                        {
                            pastPointAvg += boxCarData[x + pastCount].dataDouble;
                        }
                        pastPointAvg = pastPointAvg / endointAvgCount;
                    }


                }

                boxCar = currentPointAvg - pastPointAvg;
            }

            return boxCar;
        }

        /// <summary>
        /// Method managed the data so it doesn't grow out of control. It removed the first data in the list after more then enough data is stored.
        /// </summary>
        private void manageData()
        {
            if (boxCarData.Count > secDifferance + endointAvgCount + 20)
            {
                boxCarData.Remove(boxCarData.First());
            }
        }

    }

    internal class dataPoint
    {
        public DateTime dataDate;
        public double dataDouble;
        public string dataMessage;
    }

    /// <summary>
    /// Calibration point information object.
    /// </summary>
    public class calibraitonPoint
    {
        //Calibration point.
        public int index;

        //Air Temp
        public double airTempSetPoint;
        public double airTempLower;
        public double airTempUpper;
        public int packetCountAT;

        //Air Temp Theratal... 
        public double airTHSetPoint;
        public double airTHLower;
        public double airTHUpper;
        public int packetCountTH;

        //Pressure Point
        public double pressureSetPoint;
        public double pressureLower;
        public double pressureUpper;
        public int packetCountPressure;

        public double pressureStabilitySetPoint;
        public double pressureStabilityLower;
        public double pressureStabilityUpper;
        public int packetCountPressureStability;

        //Humidity Point
        public double humidtySetPoint;
        public double humidityLower;
        public double humidityUpper;
        public int packetCountHumidity;

        //Humidity Stability
        public double humidityStabilitySetPoint;
        public double humidityStabilityLower;
        public double humidityStabilityUpper;
        public int packetCountHumidityStability;

    }

    public class progressBar
    {
        System.Windows.Forms.Form loading;
        System.Windows.Forms.Label labelLoading;
        System.Windows.Forms.ProgressBar loadBar;
        int counter = 0;

        public progressBar()
        {
            //Creating a form to show loading.
            loading = new System.Windows.Forms.Form();
            loading.Size = new System.Drawing.Size(400, 100);
            loading.MinimizeBox = false;
            loading.MaximizeBox = false;
            loading.ControlBox = false;
            loading.ShowIcon = false;
            loading.ShowInTaskbar = false;
            //loading.StartPosition = FormStartPosition.CenterParent;

            //Loading Label
            labelLoading = new System.Windows.Forms.Label();
            labelLoading.Size = new System.Drawing.Size(100, 25);
            labelLoading.Location = new System.Drawing.Point(10, 5);
            labelLoading.AutoSize = true;
            labelLoading.ForeColor = System.Drawing.Color.Black;
            labelLoading.Text = "Loading";


            //Setting up a progress bar.
            loadBar = new System.Windows.Forms.ProgressBar();
            loadBar.Size = new System.Drawing.Size(loading.Size.Width - 30, loading.Size.Height - 50);
            loadBar.Location = new System.Drawing.Point(10, 25);
            loadBar.Minimum = 0;
            loadBar.Maximum = 100;
            loadBar.Step = 1;

            //Adding the controls.
            loading.Controls.Add(loadBar);
            loading.Controls.Add(labelLoading);

            
        }

        public void setup(int max)
        {
            loadBar.Maximum = max;
            loadBar.Step = max / 100;

            //Showing the new display.
            loading.Show();
            refresh();
        }

        public void refresh()
        {
            loading.Refresh();
        }

        public void addValue(int amount)
        {
            loadBar.Value += amount;
        }

        /// <summary>
        /// Updates the label on the display.
        /// </summary>
        /// <param name="labelText"></param>
        public void updateLabelText(string labelText)
        {
            labelLoading.Text = labelText;
        }

        public void showBar()
        {
            loadBar.Show();

            loadBar.Refresh();
        }

        public void close()
        {
            loading.Close();
        }
    }
}
