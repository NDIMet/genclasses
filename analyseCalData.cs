﻿/// William Jones, for InterMet Systems
/// 10/21/2011
/// This class analyses the collected data during the calibration and determins if the data is useable.
///

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace analyseCalData
{
    class analyseEPlusECalData
    {
        public analyseEPlusECalData() { }

        public object[] checkData(string rawCalData, string referanceData, double airTempTolerance, double humidityMeanCount, double humidityCountTolerance)
        {

            //Needs work to provent index out of bounds error.
            //Need to check to make sure that the data meets some minimum requirments before processing, such as data no longer then 1500 packets.

            //Loading desired data to compare.
            string[] referanceDataLines = System.IO.File.ReadAllLines(referanceData);
            string[] rawCalDataLines = System.IO.File.ReadAllLines(rawCalData);

            //Test Results
            bool[,] testResults = new bool[rawCalDataLines.Length,3];
            object[] totalResults = new object[2]; //Data that will be returned.

            /*
            if (referanceDataLines.Length < 600)
            {
                totalResults[0] = 0;
                totalResults[1] = testResults;
                return totalResults;
            }
             * 
            */

            //Creating contatiner for referance data to be compared to.
            double[,] referanceDataElements = new double[referanceDataLines.Length, 3];

           
            //Filling the referance data in.
            for (int x = 5; x < referanceDataLines.Length; x++)
            {
                string[] temp = referanceDataLines[x].Split(',');

                for (int fillRef = 0; fillRef < 3; fillRef++)
                {
                    referanceDataElements[x, fillRef] = Convert.ToDouble(temp[fillRef]);
                }

            }

            //Comparing cal data to ref and tolerance and reporting the results to the testResults array
            for (int a = 5; a < rawCalDataLines.Length; a++)
            {
                string[] rawCalDataElements = rawCalDataLines[a].Split(',');

                for (int compare = 5; compare < rawCalDataLines.Length; compare++)
                {
                    //Locating the cloest referance packet.
                    if (Convert.ToDouble(rawCalDataElements[0]) < referanceDataElements[compare, 0] + 0.300 &&
                        Convert.ToDouble(rawCalDataElements[0]) > referanceDataElements[compare, 0] - 0.300)
                    {
                        //Performing the test requied to pass as a good point.
                        //Starting with air temp compare.
                        if (Convert.ToDouble(rawCalDataElements[2]) < (referanceDataElements[compare, 1] + airTempTolerance) &&
                            Convert.ToDouble(rawCalDataElements[2]) > (referanceDataElements[compare, 1] - airTempTolerance))
                        {
                            testResults[compare, 0] = true;
                        }
                        else
                        {
                            testResults[compare, 0] = false;
                        }

                        //Validating humidity count
                        if (Convert.ToDouble(rawCalDataElements[1]) < (humidityMeanCount + humidityCountTolerance) &&
                            Convert.ToDouble(rawCalDataElements[1]) > (humidityMeanCount - humidityCountTolerance))
                        {
                            testResults[compare, 1] = true;
                        }
                        else
                        {
                            testResults[compare, 1] = false;
                        }

                        //Checking internal temp to insure that it is reporting something close to what I expect to see
                        if (Convert.ToDouble(rawCalDataElements[3]) < (referanceDataElements[compare, 1] + 6) &&
                            Convert.ToDouble(rawCalDataElements[3]) > (referanceDataElements[compare, 1] - 6))
                        {
                            testResults[compare,2] = true;
                        }
                        else
                        {
                            testResults[compare,2] = false;
                        }
                    }
                }
            }

            int test = testResults.Length;

            //Computing pass/fail results.
            int resultsCount = 0;
            for (int check = 5; check < rawCalDataLines.Length; check++)
            {
                if (testResults[check, 0] && testResults[check, 1] && testResults[check, 2])
                {
                    resultsCount++;
                }
            }

            //Returning the tested results.
            //return (Convert.ToDouble(resultsCount) / Convert.ToDouble(rawCalDataLines.Length));
            
            totalResults[0] = (Convert.ToDouble(resultsCount) / Convert.ToDouble(rawCalDataLines.Length));
            totalResults[1] = testResults;

            return totalResults;
        }

        public object[] checkData2(string rawCalData, string referanceData, double airTempTolerance, double humidityMeanCount, double humidityCountTolerance)
        {
            //Loading file into memory
            string[] referanceDataLines = System.IO.File.ReadAllLines(referanceData);
            string[] rawCalDataLines = System.IO.File.ReadAllLines(rawCalData);

            //Processing data into structs
            List<referanceData> refData = new List<referanceData>();
            List<rawData> sondeData = new List<rawData>();
            List<reportData> sondeReport = new List<reportData>();

            //loading the referance list
            for(int r = 5; r<referanceDataLines.Length; r++)
            {
                string[] breakDownR = referanceDataLines[r].Split(',');
                referanceData tempData = new referanceData();
                tempData.packetTime =  DateTime.MinValue.AddSeconds(Convert.ToDouble(breakDownR[0]));
                tempData.airTemp = Convert.ToDouble(breakDownR[1]);
                tempData.humidity = Convert.ToDouble(breakDownR[2]);
                refData.Add(tempData);
            }

            //Loading sonde data list.
            for(int s = 5; s <rawCalDataLines.Length; s++)
            {
                string[] breakDownS = rawCalDataLines[s].Split(',');
                rawData tempRaw = new rawData();
                tempRaw.packetTime = DateTime.MinValue.AddSeconds(Convert.ToDouble(breakDownS[0]));
                tempRaw.humidityCount = Convert.ToDouble(breakDownS[1]);
                tempRaw.airTemp = Convert.ToDouble(breakDownS[2]);
                tempRaw.internalTemp = Convert.ToDouble(breakDownS[3]);

                sondeData.Add(tempRaw);
            }
            
            //Searching and comparing data.
            for (int c = 0; c < sondeData.Count; c++)
            {
                reportData tempReport = new reportData(); //Temp Report of the system.
                tempReport.airTemp = false;
                tempReport.humidityFreq = false;
                tempReport.internalTemp = false;

                for (int s = 0; s < refData.Count; s++)
                {
                    TimeSpan diffTime = sondeData[c].packetTime - refData[s].packetTime;
                    if (diffTime.Seconds < 0.7 || diffTime.Seconds > 0.7)
                    {
                        tempReport.sondeRawTime = sondeData[c].packetTime;
                        tempReport.refTime = refData[s].packetTime;

                        //Checking air temp.
                        if (Math.Abs(sondeData[c].airTemp - refData[s].airTemp) < airTempTolerance)
                        {
                            tempReport.airTemp = true;
                        }

                        if (sondeData[c].humidityCount <= (humidityMeanCount + humidityCountTolerance) &&
                            sondeData[c].humidityCount >= (humidityMeanCount - humidityCountTolerance))
                        {
                            tempReport.humidityFreq = true;
                        }

                        //Per Job Barns request internal temp checking has been suspendded.
                        /*
                        if (sondeData[c].internalTemp < (refData[s].airTemp + 6) &&
                            sondeData[c].internalTemp > (refData[s].airTemp - 6))
                        {
                            tempReport.internalTemp = true;
                        }
                        */

                        tempReport.internalTemp = true;
                    }
                }

                sondeReport.Add(tempReport);
            }

            int resultCount = 0;
            foreach(reportData rd in sondeReport)
            {
                if(rd.airTemp && rd.humidityFreq && rd.internalTemp)
                {
                    resultCount++;
                }
            }

            object[] totalResults = new object[2];
            totalResults[0] = (Convert.ToDouble(resultCount)/Convert.ToDouble(sondeData.Count));
            totalResults[1] = sondeReport;

            return totalResults;

        }

    }

    struct referanceData
    {
        public DateTime packetTime;
        public double airTemp;
        public double humidity;
    }

    struct rawData
    {
        public DateTime packetTime;
        public double airTemp;
        public double humidityCount;
        public double internalTemp;
    }

    public struct reportData
    {
        public DateTime sondeRawTime;
        public DateTime refTime;
        public bool airTemp;
        public bool humidityFreq;
        public bool internalTemp;
    }
}
