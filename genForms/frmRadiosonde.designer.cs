﻿namespace iMetTest
{
    partial class frmRadiosonde
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelRadiosondeHumidityCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelRadiosondeAirTemp = new System.Windows.Forms.Label();
            this.labelRadiosondeAirTempCount = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelReferenceHumidity = new System.Windows.Forms.Label();
            this.labelDifferanceHumidity = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelRadiosondePressureCount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelReferencePressure = new System.Windows.Forms.Label();
            this.labelDifferancePressure = new System.Windows.Forms.Label();
            this.labelDifferanceAirTemp = new System.Windows.Forms.Label();
            this.labelReferenceAirTemp = new System.Windows.Forms.Label();
            this.labelRadiosondeHumidity = new System.Windows.Forms.Label();
            this.labelRadiosondePressure = new System.Windows.Forms.Label();
            this.groupBoxRadiosondePTU = new System.Windows.Forms.GroupBox();
            this.labelRadiosondePressureTempRefCount = new System.Windows.Forms.Label();
            this.labelRadiosondeATRefCount = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelRadiosondePressureRefferanceCount = new System.Windows.Forms.Label();
            this.labelRadiosondeFirmware = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelRadiosondeBatteryCount = new System.Windows.Forms.Label();
            this.labelRadiosondeIntTemp = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelRadiosondeIntTempCount = new System.Windows.Forms.Label();
            this.labelRadiosondeBattery = new System.Windows.Forms.Label();
            this.labelRadiosondePressureTemp = new System.Windows.Forms.Label();
            this.labelRadiosondePressureTempCount = new System.Windows.Forms.Label();
            this.groupBoxGPS = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.labelGPSAlt = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelGPSLongitude = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelGPSLatitude = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelGPSNumberOfSV = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelGPSTime = new System.Windows.Forms.Label();
            this.menuStripRadiosonde = new System.Windows.Forms.MenuStrip();
            this.loggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.startLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainBoardTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.probeLoaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tXModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label16 = new System.Windows.Forms.Label();
            this.labelComPort = new System.Windows.Forms.Label();
            this.labelRadiosondeSerialNumber = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelRadiosondeTrackingNumber = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelRadiosondeProbeNumber = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.statusStripRadiosonde = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelRadiosonde = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxCommands = new System.Windows.Forms.GroupBox();
            this.buttonLoadAll = new System.Windows.Forms.Button();
            this.buttonLoadHumidity = new System.Windows.Forms.Button();
            this.buttonLoadAT = new System.Windows.Forms.Button();
            this.buttonLoadPressure = new System.Windows.Forms.Button();
            this.comboBoxTU = new System.Windows.Forms.ComboBox();
            this.groupBoxReferance = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBoxPressure = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBoxRadiosondePTU.SuspendLayout();
            this.groupBoxGPS.SuspendLayout();
            this.menuStripRadiosonde.SuspendLayout();
            this.statusStripRadiosonde.SuspendLayout();
            this.groupBoxCommands.SuspendLayout();
            this.groupBoxReferance.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Radiosonde";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Pressure";
            // 
            // labelRadiosondeHumidityCount
            // 
            this.labelRadiosondeHumidityCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeHumidityCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeHumidityCount.Location = new System.Drawing.Point(220, 66);
            this.labelRadiosondeHumidityCount.Name = "labelRadiosondeHumidityCount";
            this.labelRadiosondeHumidityCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeHumidityCount.TabIndex = 27;
            this.labelRadiosondeHumidityCount.Text = "XXXXX.XXX";
            this.labelRadiosondeHumidityCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Count";
            // 
            // labelRadiosondeAirTemp
            // 
            this.labelRadiosondeAirTemp.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeAirTemp.Location = new System.Drawing.Point(149, 41);
            this.labelRadiosondeAirTemp.Name = "labelRadiosondeAirTemp";
            this.labelRadiosondeAirTemp.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeAirTemp.TabIndex = 22;
            this.labelRadiosondeAirTemp.Text = "XXXXX.XXX";
            this.labelRadiosondeAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeAirTempCount
            // 
            this.labelRadiosondeAirTempCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeAirTempCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeAirTempCount.Location = new System.Drawing.Point(149, 66);
            this.labelRadiosondeAirTempCount.Name = "labelRadiosondeAirTempCount";
            this.labelRadiosondeAirTempCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeAirTempCount.TabIndex = 26;
            this.labelRadiosondeAirTempCount.Text = "XXXXX.XXX";
            this.labelRadiosondeAirTempCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Differance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Reference";
            // 
            // labelReferenceHumidity
            // 
            this.labelReferenceHumidity.BackColor = System.Drawing.Color.White;
            this.labelReferenceHumidity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelReferenceHumidity.Location = new System.Drawing.Point(220, 113);
            this.labelReferenceHumidity.Name = "labelReferenceHumidity";
            this.labelReferenceHumidity.Size = new System.Drawing.Size(68, 20);
            this.labelReferenceHumidity.TabIndex = 31;
            this.labelReferenceHumidity.Text = "0";
            this.labelReferenceHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelDifferanceHumidity
            // 
            this.labelDifferanceHumidity.BackColor = System.Drawing.Color.White;
            this.labelDifferanceHumidity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDifferanceHumidity.Location = new System.Drawing.Point(220, 137);
            this.labelDifferanceHumidity.Name = "labelDifferanceHumidity";
            this.labelDifferanceHumidity.Size = new System.Drawing.Size(68, 20);
            this.labelDifferanceHumidity.TabIndex = 35;
            this.labelDifferanceHumidity.Text = "XXXXX.XXX";
            this.labelDifferanceHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(158, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Air Temp";
            // 
            // labelRadiosondePressureCount
            // 
            this.labelRadiosondePressureCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondePressureCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondePressureCount.Location = new System.Drawing.Point(77, 66);
            this.labelRadiosondePressureCount.Name = "labelRadiosondePressureCount";
            this.labelRadiosondePressureCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondePressureCount.TabIndex = 25;
            this.labelRadiosondePressureCount.Text = "XXXXX.XXX";
            this.labelRadiosondePressureCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(229, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Humidity";
            // 
            // labelReferencePressure
            // 
            this.labelReferencePressure.BackColor = System.Drawing.Color.White;
            this.labelReferencePressure.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelReferencePressure.Location = new System.Drawing.Point(77, 113);
            this.labelReferencePressure.Name = "labelReferencePressure";
            this.labelReferencePressure.Size = new System.Drawing.Size(68, 20);
            this.labelReferencePressure.TabIndex = 29;
            this.labelReferencePressure.Text = "0";
            this.labelReferencePressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelDifferancePressure
            // 
            this.labelDifferancePressure.BackColor = System.Drawing.Color.White;
            this.labelDifferancePressure.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDifferancePressure.Location = new System.Drawing.Point(77, 137);
            this.labelDifferancePressure.Name = "labelDifferancePressure";
            this.labelDifferancePressure.Size = new System.Drawing.Size(68, 20);
            this.labelDifferancePressure.TabIndex = 33;
            this.labelDifferancePressure.Text = "XXXXX.XXX";
            this.labelDifferancePressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelDifferanceAirTemp
            // 
            this.labelDifferanceAirTemp.BackColor = System.Drawing.Color.White;
            this.labelDifferanceAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDifferanceAirTemp.Location = new System.Drawing.Point(149, 137);
            this.labelDifferanceAirTemp.Name = "labelDifferanceAirTemp";
            this.labelDifferanceAirTemp.Size = new System.Drawing.Size(68, 20);
            this.labelDifferanceAirTemp.TabIndex = 34;
            this.labelDifferanceAirTemp.Text = "XXXXX.XXX";
            this.labelDifferanceAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelReferenceAirTemp
            // 
            this.labelReferenceAirTemp.BackColor = System.Drawing.Color.White;
            this.labelReferenceAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelReferenceAirTemp.Location = new System.Drawing.Point(149, 113);
            this.labelReferenceAirTemp.Name = "labelReferenceAirTemp";
            this.labelReferenceAirTemp.Size = new System.Drawing.Size(68, 20);
            this.labelReferenceAirTemp.TabIndex = 30;
            this.labelReferenceAirTemp.Text = "0";
            this.labelReferenceAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeHumidity
            // 
            this.labelRadiosondeHumidity.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeHumidity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeHumidity.Location = new System.Drawing.Point(220, 41);
            this.labelRadiosondeHumidity.Name = "labelRadiosondeHumidity";
            this.labelRadiosondeHumidity.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeHumidity.TabIndex = 24;
            this.labelRadiosondeHumidity.Text = "XXXXX.XXX";
            this.labelRadiosondeHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondePressure
            // 
            this.labelRadiosondePressure.BackColor = System.Drawing.Color.White;
            this.labelRadiosondePressure.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondePressure.Location = new System.Drawing.Point(77, 41);
            this.labelRadiosondePressure.Name = "labelRadiosondePressure";
            this.labelRadiosondePressure.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondePressure.TabIndex = 20;
            this.labelRadiosondePressure.Text = "XXXXX.XXX";
            this.labelRadiosondePressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxRadiosondePTU
            // 
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondePressureTempRefCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeATRefCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.label23);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondePressureRefferanceCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeFirmware);
            this.groupBoxRadiosondePTU.Controls.Add(this.label22);
            this.groupBoxRadiosondePTU.Controls.Add(this.label7);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeBatteryCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeIntTemp);
            this.groupBoxRadiosondePTU.Controls.Add(this.label12);
            this.groupBoxRadiosondePTU.Controls.Add(this.label13);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeIntTempCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeBattery);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondePressureTemp);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondePressureTempCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.label1);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeHumidityCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeAirTemp);
            this.groupBoxRadiosondePTU.Controls.Add(this.label6);
            this.groupBoxRadiosondePTU.Controls.Add(this.label2);
            this.groupBoxRadiosondePTU.Controls.Add(this.label3);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelReferencePressure);
            this.groupBoxRadiosondePTU.Controls.Add(this.label4);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelDifferanceHumidity);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelDifferanceAirTemp);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeAirTempCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondeHumidity);
            this.groupBoxRadiosondePTU.Controls.Add(this.label5);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelReferenceAirTemp);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondePressure);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelDifferancePressure);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelReferenceHumidity);
            this.groupBoxRadiosondePTU.Controls.Add(this.labelRadiosondePressureCount);
            this.groupBoxRadiosondePTU.Controls.Add(this.label8);
            this.groupBoxRadiosondePTU.Location = new System.Drawing.Point(12, 35);
            this.groupBoxRadiosondePTU.Name = "groupBoxRadiosondePTU";
            this.groupBoxRadiosondePTU.Size = new System.Drawing.Size(506, 174);
            this.groupBoxRadiosondePTU.TabIndex = 38;
            this.groupBoxRadiosondePTU.TabStop = false;
            this.groupBoxRadiosondePTU.Text = "PTU and Health Data";
            // 
            // labelRadiosondePressureTempRefCount
            // 
            this.labelRadiosondePressureTempRefCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondePressureTempRefCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondePressureTempRefCount.Location = new System.Drawing.Point(291, 90);
            this.labelRadiosondePressureTempRefCount.Name = "labelRadiosondePressureTempRefCount";
            this.labelRadiosondePressureTempRefCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondePressureTempRefCount.TabIndex = 52;
            this.labelRadiosondePressureTempRefCount.Text = "XXXXX.XXX";
            this.labelRadiosondePressureTempRefCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeATRefCount
            // 
            this.labelRadiosondeATRefCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeATRefCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeATRefCount.Location = new System.Drawing.Point(149, 90);
            this.labelRadiosondeATRefCount.Name = "labelRadiosondeATRefCount";
            this.labelRadiosondeATRefCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeATRefCount.TabIndex = 51;
            this.labelRadiosondeATRefCount.Text = "XXXXX.XXX";
            this.labelRadiosondeATRefCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(16, 94);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 13);
            this.label23.TabIndex = 50;
            this.label23.Text = "Ref Count";
            // 
            // labelRadiosondePressureRefferanceCount
            // 
            this.labelRadiosondePressureRefferanceCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondePressureRefferanceCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondePressureRefferanceCount.Location = new System.Drawing.Point(77, 90);
            this.labelRadiosondePressureRefferanceCount.Name = "labelRadiosondePressureRefferanceCount";
            this.labelRadiosondePressureRefferanceCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondePressureRefferanceCount.TabIndex = 49;
            this.labelRadiosondePressureRefferanceCount.Text = "XXXXX.XXX";
            this.labelRadiosondePressureRefferanceCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeFirmware
            // 
            this.labelRadiosondeFirmware.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeFirmware.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeFirmware.Location = new System.Drawing.Point(395, 126);
            this.labelRadiosondeFirmware.Name = "labelRadiosondeFirmware";
            this.labelRadiosondeFirmware.Size = new System.Drawing.Size(78, 20);
            this.labelRadiosondeFirmware.TabIndex = 48;
            this.labelRadiosondeFirmware.Text = "XXXXX.XXX";
            this.labelRadiosondeFirmware.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(337, 130);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 47;
            this.label22.Text = "Firmware:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(287, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Pressure Temp";
            // 
            // labelRadiosondeBatteryCount
            // 
            this.labelRadiosondeBatteryCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeBatteryCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeBatteryCount.Location = new System.Drawing.Point(434, 66);
            this.labelRadiosondeBatteryCount.Name = "labelRadiosondeBatteryCount";
            this.labelRadiosondeBatteryCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeBatteryCount.TabIndex = 46;
            this.labelRadiosondeBatteryCount.Text = "XXXXX.XXX";
            this.labelRadiosondeBatteryCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeIntTemp
            // 
            this.labelRadiosondeIntTemp.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeIntTemp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeIntTemp.Location = new System.Drawing.Point(363, 41);
            this.labelRadiosondeIntTemp.Name = "labelRadiosondeIntTemp";
            this.labelRadiosondeIntTemp.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeIntTemp.TabIndex = 41;
            this.labelRadiosondeIntTemp.Text = "XXXXX.XXX";
            this.labelRadiosondeIntTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(372, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "Int Temp";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(443, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "Battery";
            // 
            // labelRadiosondeIntTempCount
            // 
            this.labelRadiosondeIntTempCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeIntTempCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeIntTempCount.Location = new System.Drawing.Point(363, 66);
            this.labelRadiosondeIntTempCount.Name = "labelRadiosondeIntTempCount";
            this.labelRadiosondeIntTempCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeIntTempCount.TabIndex = 45;
            this.labelRadiosondeIntTempCount.Text = "XXXXX.XXX";
            this.labelRadiosondeIntTempCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeBattery
            // 
            this.labelRadiosondeBattery.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeBattery.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeBattery.Location = new System.Drawing.Point(434, 41);
            this.labelRadiosondeBattery.Name = "labelRadiosondeBattery";
            this.labelRadiosondeBattery.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondeBattery.TabIndex = 43;
            this.labelRadiosondeBattery.Text = "XXXXX.XXX";
            this.labelRadiosondeBattery.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondePressureTemp
            // 
            this.labelRadiosondePressureTemp.BackColor = System.Drawing.Color.White;
            this.labelRadiosondePressureTemp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondePressureTemp.Location = new System.Drawing.Point(291, 41);
            this.labelRadiosondePressureTemp.Name = "labelRadiosondePressureTemp";
            this.labelRadiosondePressureTemp.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondePressureTemp.TabIndex = 39;
            this.labelRadiosondePressureTemp.Text = "XXXXX.XXX";
            this.labelRadiosondePressureTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondePressureTempCount
            // 
            this.labelRadiosondePressureTempCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondePressureTempCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondePressureTempCount.Location = new System.Drawing.Point(291, 66);
            this.labelRadiosondePressureTempCount.Name = "labelRadiosondePressureTempCount";
            this.labelRadiosondePressureTempCount.Size = new System.Drawing.Size(68, 20);
            this.labelRadiosondePressureTempCount.TabIndex = 44;
            this.labelRadiosondePressureTempCount.Text = "XXXXX.XXX";
            this.labelRadiosondePressureTempCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxGPS
            // 
            this.groupBoxGPS.Controls.Add(this.label15);
            this.groupBoxGPS.Controls.Add(this.labelGPSAlt);
            this.groupBoxGPS.Controls.Add(this.label11);
            this.groupBoxGPS.Controls.Add(this.labelGPSLongitude);
            this.groupBoxGPS.Controls.Add(this.label14);
            this.groupBoxGPS.Controls.Add(this.labelGPSLatitude);
            this.groupBoxGPS.Controls.Add(this.label10);
            this.groupBoxGPS.Controls.Add(this.labelGPSNumberOfSV);
            this.groupBoxGPS.Controls.Add(this.label9);
            this.groupBoxGPS.Controls.Add(this.labelGPSTime);
            this.groupBoxGPS.Location = new System.Drawing.Point(12, 209);
            this.groupBoxGPS.Name = "groupBoxGPS";
            this.groupBoxGPS.Size = new System.Drawing.Size(506, 64);
            this.groupBoxGPS.TabIndex = 39;
            this.groupBoxGPS.TabStop = false;
            this.groupBoxGPS.Text = "GPS Data";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(453, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 55;
            this.label15.Text = "Alt";
            // 
            // labelGPSAlt
            // 
            this.labelGPSAlt.BackColor = System.Drawing.Color.White;
            this.labelGPSAlt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelGPSAlt.Location = new System.Drawing.Point(424, 37);
            this.labelGPSAlt.Name = "labelGPSAlt";
            this.labelGPSAlt.Size = new System.Drawing.Size(71, 20);
            this.labelGPSAlt.TabIndex = 56;
            this.labelGPSAlt.Text = "XXXXX.XXX";
            this.labelGPSAlt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(337, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 53;
            this.label11.Text = "Longitude";
            // 
            // labelGPSLongitude
            // 
            this.labelGPSLongitude.BackColor = System.Drawing.Color.White;
            this.labelGPSLongitude.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelGPSLongitude.Location = new System.Drawing.Point(308, 37);
            this.labelGPSLongitude.Name = "labelGPSLongitude";
            this.labelGPSLongitude.Size = new System.Drawing.Size(110, 20);
            this.labelGPSLongitude.TabIndex = 54;
            this.labelGPSLongitude.Text = "XXXXX.XXX";
            this.labelGPSLongitude.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(224, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "Latitude";
            // 
            // labelGPSLatitude
            // 
            this.labelGPSLatitude.BackColor = System.Drawing.Color.White;
            this.labelGPSLatitude.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelGPSLatitude.Location = new System.Drawing.Point(192, 37);
            this.labelGPSLatitude.Name = "labelGPSLatitude";
            this.labelGPSLatitude.Size = new System.Drawing.Size(110, 20);
            this.labelGPSLatitude.TabIndex = 52;
            this.labelGPSLatitude.Text = "XXXXX.XXX";
            this.labelGPSLatitude.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(115, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 49;
            this.label10.Text = "Number of SV";
            // 
            // labelGPSNumberOfSV
            // 
            this.labelGPSNumberOfSV.BackColor = System.Drawing.Color.White;
            this.labelGPSNumberOfSV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelGPSNumberOfSV.Location = new System.Drawing.Point(113, 37);
            this.labelGPSNumberOfSV.Name = "labelGPSNumberOfSV";
            this.labelGPSNumberOfSV.Size = new System.Drawing.Size(75, 20);
            this.labelGPSNumberOfSV.TabIndex = 50;
            this.labelGPSNumberOfSV.Text = "XXXXX.XXX";
            this.labelGPSNumberOfSV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Time";
            // 
            // labelGPSTime
            // 
            this.labelGPSTime.BackColor = System.Drawing.Color.White;
            this.labelGPSTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelGPSTime.Location = new System.Drawing.Point(11, 37);
            this.labelGPSTime.Name = "labelGPSTime";
            this.labelGPSTime.Size = new System.Drawing.Size(96, 20);
            this.labelGPSTime.TabIndex = 48;
            this.labelGPSTime.Text = "XXXXX.XXX";
            this.labelGPSTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStripRadiosonde
            // 
            this.menuStripRadiosonde.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loggingToolStripMenuItem,
            this.modeToolStripMenuItem});
            this.menuStripRadiosonde.Location = new System.Drawing.Point(0, 0);
            this.menuStripRadiosonde.Name = "menuStripRadiosonde";
            this.menuStripRadiosonde.Size = new System.Drawing.Size(624, 24);
            this.menuStripRadiosonde.TabIndex = 40;
            this.menuStripRadiosonde.Text = "menuStripRadiosonde";
            this.menuStripRadiosonde.Visible = false;
            // 
            // loggingToolStripMenuItem
            // 
            this.loggingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newLogToolStripMenuItem,
            this.openLogToolStripMenuItem,
            this.logIntervalToolStripMenuItem,
            this.toolStripSeparator1,
            this.startLoggingToolStripMenuItem,
            this.pauseLoggingToolStripMenuItem,
            this.stopLoggingToolStripMenuItem});
            this.loggingToolStripMenuItem.Name = "loggingToolStripMenuItem";
            this.loggingToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.loggingToolStripMenuItem.Text = "Logging";
            // 
            // newLogToolStripMenuItem
            // 
            this.newLogToolStripMenuItem.Name = "newLogToolStripMenuItem";
            this.newLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newLogToolStripMenuItem.Text = "New Log";
            // 
            // openLogToolStripMenuItem
            // 
            this.openLogToolStripMenuItem.Name = "openLogToolStripMenuItem";
            this.openLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openLogToolStripMenuItem.Text = "Open Log";
            // 
            // logIntervalToolStripMenuItem
            // 
            this.logIntervalToolStripMenuItem.Name = "logIntervalToolStripMenuItem";
            this.logIntervalToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.logIntervalToolStripMenuItem.Text = "Log Interval";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // startLoggingToolStripMenuItem
            // 
            this.startLoggingToolStripMenuItem.Name = "startLoggingToolStripMenuItem";
            this.startLoggingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.startLoggingToolStripMenuItem.Text = "Start Logging";
            // 
            // pauseLoggingToolStripMenuItem
            // 
            this.pauseLoggingToolStripMenuItem.Name = "pauseLoggingToolStripMenuItem";
            this.pauseLoggingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pauseLoggingToolStripMenuItem.Text = "Pause Logging";
            // 
            // stopLoggingToolStripMenuItem
            // 
            this.stopLoggingToolStripMenuItem.Name = "stopLoggingToolStripMenuItem";
            this.stopLoggingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stopLoggingToolStripMenuItem.Text = "Stop Logging";
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainBoardTestToolStripMenuItem,
            this.probeLoaderToolStripMenuItem,
            this.tXModeToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // mainBoardTestToolStripMenuItem
            // 
            this.mainBoardTestToolStripMenuItem.Name = "mainBoardTestToolStripMenuItem";
            this.mainBoardTestToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.mainBoardTestToolStripMenuItem.Text = "Main Board Test";
            // 
            // probeLoaderToolStripMenuItem
            // 
            this.probeLoaderToolStripMenuItem.Name = "probeLoaderToolStripMenuItem";
            this.probeLoaderToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.probeLoaderToolStripMenuItem.Text = "Probe Loader";
            // 
            // tXModeToolStripMenuItem
            // 
            this.tXModeToolStripMenuItem.Name = "tXModeToolStripMenuItem";
            this.tXModeToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.tXModeToolStripMenuItem.Text = "TX Mode";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(20, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 41;
            this.label16.Text = "Com Port:";
            // 
            // labelComPort
            // 
            this.labelComPort.BackColor = System.Drawing.Color.White;
            this.labelComPort.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelComPort.Location = new System.Drawing.Point(79, 9);
            this.labelComPort.Name = "labelComPort";
            this.labelComPort.Size = new System.Drawing.Size(64, 20);
            this.labelComPort.TabIndex = 47;
            this.labelComPort.Text = "COMXXXX";
            this.labelComPort.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRadiosondeSerialNumber
            // 
            this.labelRadiosondeSerialNumber.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeSerialNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeSerialNumber.Location = new System.Drawing.Point(187, 9);
            this.labelRadiosondeSerialNumber.Name = "labelRadiosondeSerialNumber";
            this.labelRadiosondeSerialNumber.Size = new System.Drawing.Size(82, 20);
            this.labelRadiosondeSerialNumber.TabIndex = 49;
            this.labelRadiosondeSerialNumber.Text = "SXXXXXXXXX";
            this.labelRadiosondeSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(159, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 13);
            this.label18.TabIndex = 48;
            this.label18.Text = "SN:";
            // 
            // labelRadiosondeTrackingNumber
            // 
            this.labelRadiosondeTrackingNumber.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeTrackingNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeTrackingNumber.Location = new System.Drawing.Point(311, 9);
            this.labelRadiosondeTrackingNumber.Name = "labelRadiosondeTrackingNumber";
            this.labelRadiosondeTrackingNumber.Size = new System.Drawing.Size(82, 20);
            this.labelRadiosondeTrackingNumber.TabIndex = 51;
            this.labelRadiosondeTrackingNumber.Text = "SXXXXXXXXX";
            this.labelRadiosondeTrackingNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(283, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 50;
            this.label19.Text = "SID:";
            // 
            // labelRadiosondeProbeNumber
            // 
            this.labelRadiosondeProbeNumber.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeProbeNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRadiosondeProbeNumber.Location = new System.Drawing.Point(432, 9);
            this.labelRadiosondeProbeNumber.Name = "labelRadiosondeProbeNumber";
            this.labelRadiosondeProbeNumber.Size = new System.Drawing.Size(82, 20);
            this.labelRadiosondeProbeNumber.TabIndex = 53;
            this.labelRadiosondeProbeNumber.Text = "SXXXXXXXXX";
            this.labelRadiosondeProbeNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(404, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(25, 13);
            this.label21.TabIndex = 52;
            this.label21.Text = "PN:";
            // 
            // statusStripRadiosonde
            // 
            this.statusStripRadiosonde.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelRadiosonde});
            this.statusStripRadiosonde.Location = new System.Drawing.Point(0, 279);
            this.statusStripRadiosonde.Name = "statusStripRadiosonde";
            this.statusStripRadiosonde.Size = new System.Drawing.Size(624, 22);
            this.statusStripRadiosonde.TabIndex = 57;
            this.statusStripRadiosonde.Text = "statusStrip1";
            // 
            // toolStripStatusLabelRadiosonde
            // 
            this.toolStripStatusLabelRadiosonde.AutoSize = false;
            this.toolStripStatusLabelRadiosonde.Name = "toolStripStatusLabelRadiosonde";
            this.toolStripStatusLabelRadiosonde.Size = new System.Drawing.Size(400, 17);
            this.toolStripStatusLabelRadiosonde.Text = "Waiting.....";
            this.toolStripStatusLabelRadiosonde.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBoxCommands
            // 
            this.groupBoxCommands.Controls.Add(this.buttonLoadAll);
            this.groupBoxCommands.Controls.Add(this.buttonLoadHumidity);
            this.groupBoxCommands.Controls.Add(this.buttonLoadAT);
            this.groupBoxCommands.Controls.Add(this.buttonLoadPressure);
            this.groupBoxCommands.Location = new System.Drawing.Point(520, 9);
            this.groupBoxCommands.Name = "groupBoxCommands";
            this.groupBoxCommands.Size = new System.Drawing.Size(99, 131);
            this.groupBoxCommands.TabIndex = 58;
            this.groupBoxCommands.TabStop = false;
            this.groupBoxCommands.Text = "Commands";
            // 
            // buttonLoadAll
            // 
            this.buttonLoadAll.Location = new System.Drawing.Point(6, 94);
            this.buttonLoadAll.Name = "buttonLoadAll";
            this.buttonLoadAll.Size = new System.Drawing.Size(87, 23);
            this.buttonLoadAll.TabIndex = 6;
            this.buttonLoadAll.Text = "Load All";
            this.buttonLoadAll.UseVisualStyleBackColor = true;
            // 
            // buttonLoadHumidity
            // 
            this.buttonLoadHumidity.Location = new System.Drawing.Point(6, 69);
            this.buttonLoadHumidity.Name = "buttonLoadHumidity";
            this.buttonLoadHumidity.Size = new System.Drawing.Size(87, 23);
            this.buttonLoadHumidity.TabIndex = 5;
            this.buttonLoadHumidity.Text = "Load Humidity";
            this.buttonLoadHumidity.UseVisualStyleBackColor = true;
            // 
            // buttonLoadAT
            // 
            this.buttonLoadAT.Location = new System.Drawing.Point(6, 44);
            this.buttonLoadAT.Name = "buttonLoadAT";
            this.buttonLoadAT.Size = new System.Drawing.Size(87, 23);
            this.buttonLoadAT.TabIndex = 4;
            this.buttonLoadAT.Text = "Load AT";
            this.buttonLoadAT.UseVisualStyleBackColor = true;
            // 
            // buttonLoadPressure
            // 
            this.buttonLoadPressure.Location = new System.Drawing.Point(6, 19);
            this.buttonLoadPressure.Name = "buttonLoadPressure";
            this.buttonLoadPressure.Size = new System.Drawing.Size(87, 23);
            this.buttonLoadPressure.TabIndex = 3;
            this.buttonLoadPressure.Text = "Load Pressure";
            this.buttonLoadPressure.UseVisualStyleBackColor = true;
            // 
            // comboBoxTU
            // 
            this.comboBoxTU.FormattingEnabled = true;
            this.comboBoxTU.Location = new System.Drawing.Point(4, 34);
            this.comboBoxTU.Name = "comboBoxTU";
            this.comboBoxTU.Size = new System.Drawing.Size(92, 21);
            this.comboBoxTU.TabIndex = 47;
            // 
            // groupBoxReferance
            // 
            this.groupBoxReferance.Controls.Add(this.label20);
            this.groupBoxReferance.Controls.Add(this.comboBoxPressure);
            this.groupBoxReferance.Controls.Add(this.label17);
            this.groupBoxReferance.Controls.Add(this.comboBoxTU);
            this.groupBoxReferance.Enabled = false;
            this.groupBoxReferance.Location = new System.Drawing.Point(520, 147);
            this.groupBoxReferance.Name = "groupBoxReferance";
            this.groupBoxReferance.Size = new System.Drawing.Size(99, 101);
            this.groupBoxReferance.TabIndex = 59;
            this.groupBoxReferance.TabStop = false;
            this.groupBoxReferance.Text = "Referances";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 13);
            this.label20.TabIndex = 50;
            this.label20.Text = "Pressure";
            // 
            // comboBoxPressure
            // 
            this.comboBoxPressure.FormattingEnabled = true;
            this.comboBoxPressure.Location = new System.Drawing.Point(3, 78);
            this.comboBoxPressure.Name = "comboBoxPressure";
            this.comboBoxPressure.Size = new System.Drawing.Size(92, 21);
            this.comboBoxPressure.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "T/U";
            // 
            // frmRadiosonde
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 301);
            this.Controls.Add(this.groupBoxReferance);
            this.Controls.Add(this.groupBoxCommands);
            this.Controls.Add(this.statusStripRadiosonde);
            this.Controls.Add(this.labelRadiosondeProbeNumber);
            this.Controls.Add(this.labelRadiosondeTrackingNumber);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.labelRadiosondeSerialNumber);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.labelComPort);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupBoxGPS);
            this.Controls.Add(this.groupBoxRadiosondePTU);
            this.Controls.Add(this.menuStripRadiosonde);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmRadiosonde";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Radiosonde Data";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRadiosonde_FormClosing);
            this.groupBoxRadiosondePTU.ResumeLayout(false);
            this.groupBoxRadiosondePTU.PerformLayout();
            this.groupBoxGPS.ResumeLayout(false);
            this.groupBoxGPS.PerformLayout();
            this.menuStripRadiosonde.ResumeLayout(false);
            this.menuStripRadiosonde.PerformLayout();
            this.statusStripRadiosonde.ResumeLayout(false);
            this.statusStripRadiosonde.PerformLayout();
            this.groupBoxCommands.ResumeLayout(false);
            this.groupBoxReferance.ResumeLayout(false);
            this.groupBoxReferance.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelRadiosondeHumidityCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelRadiosondeAirTemp;
        private System.Windows.Forms.Label labelRadiosondeAirTempCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelReferenceHumidity;
        private System.Windows.Forms.Label labelDifferanceHumidity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelRadiosondePressureCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelReferencePressure;
        private System.Windows.Forms.Label labelDifferancePressure;
        private System.Windows.Forms.Label labelDifferanceAirTemp;
        private System.Windows.Forms.Label labelReferenceAirTemp;
        private System.Windows.Forms.Label labelRadiosondeHumidity;
        private System.Windows.Forms.Label labelRadiosondePressure;
        private System.Windows.Forms.GroupBox groupBoxRadiosondePTU;
        private System.Windows.Forms.GroupBox groupBoxGPS;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelRadiosondeIntTemp;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelRadiosondeIntTempCount;
        private System.Windows.Forms.Label labelRadiosondePressureTemp;
        private System.Windows.Forms.Label labelRadiosondePressureTempCount;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelGPSAlt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelGPSLongitude;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelGPSLatitude;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelGPSNumberOfSV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelGPSTime;
        private System.Windows.Forms.MenuStrip menuStripRadiosonde;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelComPort;
        private System.Windows.Forms.Label labelRadiosondeSerialNumber;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelRadiosondeTrackingNumber;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelRadiosondeProbeNumber;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelRadiosondeBatteryCount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelRadiosondeBattery;
        private System.Windows.Forms.StatusStrip statusStripRadiosonde;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRadiosonde;
        private System.Windows.Forms.GroupBox groupBoxCommands;
        private System.Windows.Forms.Button buttonLoadHumidity;
        private System.Windows.Forms.Button buttonLoadAT;
        private System.Windows.Forms.Button buttonLoadPressure;
        private System.Windows.Forms.Button buttonLoadAll;
        private System.Windows.Forms.ComboBox comboBoxTU;
        private System.Windows.Forms.GroupBox groupBoxReferance;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBoxPressure;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelRadiosondeFirmware;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelRadiosondePressureRefferanceCount;
        private System.Windows.Forms.ToolStripMenuItem loggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem startLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopLoggingToolStripMenuItem;
        private System.Windows.Forms.Label labelRadiosondePressureTempRefCount;
        private System.Windows.Forms.Label labelRadiosondeATRefCount;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainBoardTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem probeLoaderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tXModeToolStripMenuItem;
    }
}