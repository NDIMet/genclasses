﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iMetTest
{

    delegate void updateSondeData(ipRadiosonde.CalData data);

    public partial class frmRadiosonde : Form
    {
        //Public objects.
        ipRadiosonde.ipUniRadiosonde radiosonde;

        public frmRadiosonde()
        {
            InitializeComponent();

        }

        public void setRadiosonde(ipRadiosonde.ipUniRadiosonde sonde)
        {
            radiosonde = sonde;
            radiosonde.CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
        }

        private void frmRadiosonde_FormClosing(object sender, FormClosingEventArgs e)
        {
            radiosonde.CalDataUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
        }

        #region Events

        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Getting latest radiosonde data.
            ipRadiosonde.CalData curData = (ipRadiosonde.CalData)data.NewValue;

            try
            {
                BeginInvoke(new updateSondeData(updateRadiosondeDataDisplay), new object[] { curData });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            
        }

        #endregion

        #region Methods for updating the display.

        private void updateRadiosondeDataDisplay(ipRadiosonde.CalData data)
        {
            string numberFormat = "0.000";

            //Updating display
            labelComPort.Text = data.ComPortName;

            //ID Data.
            this.Text = "Radiosonde Data: " + data.SerialNumber;
            labelRadiosondeSerialNumber.Text = data.SerialNumber;
            labelRadiosondeProbeNumber.Text = data.ProbeID;
            labelRadiosondeTrackingNumber.Text = data.SondeID;

            //Sensor Data.
            labelRadiosondePressure.Text = data.Pressure.ToString(numberFormat);
            labelRadiosondeAirTemp.Text = data.Temperature.ToString(numberFormat);
            labelRadiosondeHumidity.Text = data.Humidity.ToString(numberFormat);
            labelRadiosondePressureTemp.Text = data.PressureTemperature.ToString(numberFormat);
            labelRadiosondeIntTemp.Text = data.InternalTemp.ToString(numberFormat);

            //Counts
            labelRadiosondePressureCount.Text = data.PressureCounts.ToString("0");
            labelRadiosondeAirTempCount.Text = data.TemperatureCounts.ToString("0");
            labelRadiosondeHumidityCount.Text = data.HumidityFrequency.ToString("0");
            labelRadiosondePressureTempCount.Text = data.PressureTempCounts.ToString("0");
            
            //Ref Counts
            labelRadiosondePressureRefferanceCount.Text = data.PressureRefCounts.ToString("0");
            labelRadiosondeATRefCount.Text = data.TemperatureRefCounts.ToString("0");
            labelRadiosondePressureTempRefCount.Text = data.PressureTempRefCounts.ToString("0");


        }

        #endregion



    }
}
