﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testHumidityCalDisplay
{
    #region Delegates for cross thread interaction.

    delegate void updateSensorData(double airTemp, double humidity);
    delegate void updateProgress(int airStat, int humidityStat);

    #endregion

    /// <summary>
    /// Display for Humidity Calibration.
    /// Focused on the Radiosondes Data Display
    /// </summary>
    public class displayHCal
    {
        //Objects that compose the display.
        System.Windows.Forms.Panel panelMain;

        //Data objects
        public List<enclouser> calEnclousers = new List<enclouser>();  //All the enclousers in the calibration.

        public displayHCal()
        {
            panelMain = new System.Windows.Forms.Panel();
            panelMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelMain.BackColor = System.Drawing.Color.White;
            panelMain.Name = "HCalDisplay";

        }

        #region Methods for setting up the display

        public void setDisplaySize(int height, int width)
        {
            panelMain.Size = new System.Drawing.Size(width, height);    //Setting the panel size.
            updateDisplayElements();                                    //Updating other elements on the display.

        }

        public void setDisplayPosition(int X, int Y)
        {
            panelMain.Location = new System.Drawing.Point(X, Y);
        }

        /// <summary>
        /// Method for adjucting display elements to scale with the mainPanel.
        /// </summary>
        public void updateDisplayElements()
        {
            //Adding scrollbar to right side.
            if (calEnclousers.Count > 0)
            {
                if (calEnclousers[0].getEnclouser().Size.Height * calEnclousers.Count > panelMain.Size.Height)
                {

                    panelMain.AutoScroll = true;    //Adding scroll bars.


                    //Adjusting the enclousers size to fit in the scroll bar.
                    foreach (enclouser enc in calEnclousers)
                    {
                        enc.setSize(panelMain.Size.Width - 23, 200);
                    }

                }
            }
        }


        #endregion

        #region Methods for accessing componets

        public System.Windows.Forms.Panel getDisplay()
        {
            return panelMain;
        }

        #endregion

        #region Methods for controlling subcomponts

        public void addEnclouser(string enclouser)
        {
            enclouser tempENC = new enclouser(enclouser);       //Creating the enclouser panel.
            tempENC.setSize(panelMain.Width-1, 200);           //Setting the size to the match the display width.
            tempENC.setPosition(0, 200 * calEnclousers.Count);  //Moving it to center

            calEnclousers.Add(tempENC);                         //Adding to the list of enclousers

            panelMain.Controls.Add(tempENC.getEnclouser());     //Adding the panel to the main display.


            updateDisplayElements();                            //Updating the main display.
        }

        #endregion

    }

    #region Sub-objects of the display

    public class enclouser
    {
        //Enclouser display objects.
        System.Windows.Forms.Panel panelEnclouser;

        //Label for enclouser settings.
        //Moving to it's own class.
        enclouserStatus encStatus;  //Current status of the enclouser conditions.
        System.Windows.Forms.Label labelAirTemp;
        System.Windows.Forms.Label curAirTemp;
        System.Windows.Forms.Label spAirTemp;

        System.Windows.Forms.Label labelHumidity;

        public List<runnerHCal> enclosuerRunners = new List<runnerHCal>(); //Runners in the enclouser.

        //Data objects
        public string enclouserID = "";

        public enclouser(string ID)
        {
            enclouserID = ID;
            panelEnclouser = new System.Windows.Forms.Panel();
            panelEnclouser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelEnclouser.BackColor = System.Drawing.Color.LightGray;

            //Starting size... I most likly will need to remove this.
            panelEnclouser.Size = new System.Drawing.Size(200, 100);

            encStatus = new enclouserStatus();
            panelEnclouser.Controls.Add(encStatus.getStatPanel());
        }

        public void setSize(int width,int height)
        {
            panelEnclouser.Size = new System.Drawing.Size(width, height);

            //Created a pen for boader.
            /*
            System.Drawing.Pen boarderPen = new System.Drawing.Pen(System.Drawing.Color.Black, 4);
            System.Drawing.Graphics outline = this.panelEnclouser.CreateGraphics();
            outline.DrawRectangle(boarderPen, new System.Drawing.Rectangle(4, 4, width-8, height-8));
            */

            updatedSubElements();
        }

        private void updatedSubElements()
        {
            //Updating the runners.
            foreach (runnerHCal run in enclosuerRunners)
            {
                run.setSize(panelEnclouser.Size.Width, (panelEnclouser.Size.Height / 3)); //This might need to be adjust to give status some more room.
            }

            //Setting the status panel location.
            encStatus.setLocation(panelEnclouser.Width - encStatus.getStatPanel().Width, (panelEnclouser.Size.Height / 3));
        }

        public void setPosition(int X, int Y)
        {
            panelEnclouser.Location = new System.Drawing.Point(X, Y);
        }

        public System.Windows.Forms.Panel getEnclouser()
        {
            return panelEnclouser;
        }

        public void addRunner(string runnerID)
        {
            runnerHCal tempRunner = new runnerHCal(runnerID);
            tempRunner.setSize(panelEnclouser.Size.Width, panelEnclouser.Size.Height / 3);


            //Setting the possition.
            if (enclosuerRunners.Count == 0)
            {
                tempRunner.setPosition(0, 0);
            }
            if (enclosuerRunners.Count == 1)
            {
                tempRunner.setPosition(0, panelEnclouser.Size.Height - (panelEnclouser.Size.Height / 3));
            }

            
            enclosuerRunners.Add(tempRunner);
            panelEnclouser.Controls.Add(tempRunner.getRunnerPanel());

            updatedSubElements();       //Update the sub comp;
        }

        public enclouserStatus getStatus()
        {
            return encStatus;
        }
    }

    public class runnerHCal
    {
        //Runner display objects.
        System.Windows.Forms.Panel panelRunner;

        //Data items.
        public List<radiosondePort> radiosondePorts = new List<radiosondePort>();
        public string runnerID = "";

        public runnerHCal(string ID)
        {
            runnerID = ID;
            panelRunner = new System.Windows.Forms.Panel();
            panelRunner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelRunner.BackColor = System.Drawing.Color.PowderBlue;
            panelRunner.Name = ID;
            panelRunner.AutoScroll = true;
        }

        public System.Windows.Forms.Panel getRunnerPanel()
        {
            return panelRunner;
        }

        public void setSize(int width, int height)
        {
            panelRunner.Size = new System.Drawing.Size(width, height);
        }

        public void setPosition(int X, int Y)
        {
            panelRunner.Location = new System.Drawing.Point(X, Y);
        }

        public void addRadiosondePort(string portID, ipRadiosonde.ipUniRadiosonde port)
        {
            radiosondePort tempPort = new radiosondePort(portID, port);

            //Adding directional sorting to reflect the current rig configuration.
            if (Convert.ToInt16(portID) <= 24)
            {
                tempPort.setPosition((panelRunner.Width - 40) - (20 * radiosondePorts.Count), (panelRunner.Height / 2) - 10);
            }
            else
            {
                tempPort.setPosition(20 * radiosondePorts.Count, (panelRunner.Height / 2) - 10);
            }

            tempPort.setSize(20, 20);

            panelRunner.Controls.Add(tempPort.getPortPanel());
            radiosondePorts.Add(tempPort);

        }

    }

    /// <summary>
    /// Radiosonde port display.
    /// </summary>
    public class radiosondePort
    {
        //Port display objects.
        public System.Windows.Forms.Panel panelRadiosondePort;
        public System.Windows.Forms.Label labelPortID;

        //Port data objects.
        public string portID;
        public string connectionInfo;
        private ipRadiosonde.ipUniRadiosonde portConnection;    //Hardware port.... Not sure if I like this.

        //Acceptable Status Settings.
        private double spAirTemp = 25;
        private double spHumidity = 20000;
        private double rangeAirTemp = 1;
        private double rangeHumidity = 3000;
        private double rangeInternalTemp = 5;


        System.Threading.Thread clearStatus;
        System.Threading.AutoResetEvent blinkWait = new System.Threading.AutoResetEvent(false);


        public radiosondePort(string ID, ipRadiosonde.ipUniRadiosonde desiredPort)
        {
            portID = ID;
            portConnection = desiredPort;   //Applying the hardware port.

            //Connecting the radiosonde to graphic elements.
            portConnection.CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
            portConnection.bootingRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(bootingRadiosonde_PropertyChange);
            portConnection.disconnectRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(disconnectRadiosonde_PropertyChange);
            portConnection.error.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(error_PropertyChange);

            panelRadiosondePort = new System.Windows.Forms.Panel();
            panelRadiosondePort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panelRadiosondePort.Size = new System.Drawing.Size(20, 20);
            panelRadiosondePort.BackColor = System.Drawing.Color.White;

            //Setting jup the label for the port.
            labelPortID = new System.Windows.Forms.Label();
            labelPortID.AutoSize = false;
            labelPortID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelPortID.Size = new System.Drawing.Size(20, 20);
            labelPortID.Name = ID;
            labelPortID.Text = ID;
            labelPortID.MouseClick += new System.Windows.Forms.MouseEventHandler(labelPortID_MouseClick);
            labelPortID.MouseHover += new EventHandler(labelPortID_MouseHover);

            panelRadiosondePort.Controls.Add(labelPortID);


            //Setting up a thread to clear status. This makes it look like it is blinking.
            //Clearing status for the next packet.
            clearStatus = new System.Threading.Thread(clearStatusThread);
            clearStatus.Name = "Clear Status " + portID;
            clearStatus.Start();


        }

        #region Radiosonde event methods

        void error_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            setState("errorpacket");
        }

        void disconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            setState("disconnect");
        }

        void bootingRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            setState("booting");
        }

        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Bringing in current data.
            ipRadiosonde.CalData incoming = (ipRadiosonde.CalData)data.NewValue;

            if (incoming.Temperature < spAirTemp + rangeAirTemp &&
                incoming.Temperature > spAirTemp - rangeAirTemp &&
                incoming.HumidityFrequency < spHumidity + rangeHumidity &&
                incoming.HumidityFrequency > spHumidity - rangeHumidity &&
                incoming.InternalTemp < spAirTemp + rangeInternalTemp &&
                incoming.InternalTemp > spAirTemp - rangeInternalTemp)
            {
                setState("calpacketgood");  //Setting the state.
            }
            else
            {
                setState("calpacketbad");  //Setting the state.
            }
        }

        #endregion


        #region Events tied to graphics elements.

        void labelPortID_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip portStatus = new System.Windows.Forms.ToolTip();

            string displayInfo = portConnection.CalData.ProbeID + "\n" +
                                "ATDiff: " + (spAirTemp - portConnection.CalData.Temperature).ToString("0.00") + "\n" +
                                "U Diff: " + (spHumidity - portConnection.CalData.HumidityFrequency).ToString("0") + "\n" +
                                "IT Diff: " + (spAirTemp - portConnection.CalData.InternalTemp).ToString("0.00");

            portStatus.SetToolTip(labelPortID, displayInfo);
        }

        void labelPortID_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //Setting up port option pop up menu.
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                //Creating the menu
                System.Windows.Forms.ContextMenu popUp = new System.Windows.Forms.ContextMenu();

                //Adding menu options.
                System.Windows.Forms.MenuItem resetPort = new System.Windows.Forms.MenuItem();
                resetPort.Name = "resetPort";
                resetPort.Text = "Reset Port";
                resetPort.Click +=new EventHandler(commandPort);

                System.Windows.Forms.MenuItem details = new System.Windows.Forms.MenuItem();
                details.Name = "details";
                details.Text = portConnection.CalData.ProbeID + " Detail";
                details.Click += new EventHandler(commandPort);

                //Adding the item to the menu.
                popUp.MenuItems.Add(resetPort);
                popUp.MenuItems.Add(details);
 
                popUp.Show(this.panelRadiosondePort, e.Location);

            }
        }

        void commandPort(object sender, System.EventArgs e)
        {
            System.Windows.Forms.MenuItem incoming = (System.Windows.Forms.MenuItem)sender;
            
            //reset command input.
            if (incoming.Name == "resetPort")
            {
                portConnection.Reset();
            }

            if (incoming.Name == "details")
            {
                iMetTest.frmRadiosonde detailed = new iMetTest.frmRadiosonde();
                detailed.setRadiosonde(portConnection);
                detailed.Show();
            }

        }

        #endregion

        public void setSize(int width, int height)
        {
            panelRadiosondePort.Size = new System.Drawing.Size(width, height);
            labelPortID.Size = new System.Drawing.Size(width, height);
        }

        public void setPosition(int X, int Y)
        {
            panelRadiosondePort.Location = new System.Drawing.Point(X, Y);
        }

        /// <summary>
        /// Methods sets the acceptable settings.
        /// </summary>
        /// <param name="spAir"></param>
        /// <param name="spAirRange"></param>
        /// <param name="spHum"></param>
        /// <param name="spHumidityRange"></param>
        /// <param name="spInternalRange"></param>
        public void setAPConditions(double spAir, double spAirRange, double spHum, double spHumidityRange, double spInternalRange)
        {
            spAirTemp = spAir;
            spHumidity = spHum;
            rangeAirTemp = spAir;
            rangeHumidity = spHumidityRange;
            rangeInternalTemp = spInternalRange;
        }

        public System.Windows.Forms.Panel getPortPanel()
        {
            return panelRadiosondePort;
        }

        public void setState(string state)
        {
            switch (state.ToLower())
            {
                case "calpacketgood":
                    labelPortID.BackColor = System.Drawing.Color.LightGreen;
                    labelPortID.ForeColor = System.Drawing.Color.Black;
                    blinkWait.Set();
                    break;

                case "calpacketbad":
                    labelPortID.BackColor = System.Drawing.Color.DarkRed;
                    labelPortID.ForeColor = System.Drawing.Color.White;

                    blinkWait.Set();
                    break;

                case "disconnect":
                    labelPortID.BackColor = System.Drawing.Color.Red;
                    break;

                case "errorpacket":
                    labelPortID.BackColor = System.Drawing.Color.DarkRed;

                    break;

                case "booting":
                    labelPortID.BackColor = System.Drawing.Color.Yellow;
                    break;

                case "ready":
                    labelPortID.BackColor = System.Drawing.Color.Green;
                    break;
            }

            
        }

        /// <summary>
        /// Thread for createing a blinking effect.
        /// </summary>
        private void clearStatusThread()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;

            while (true)
            {
                blinkWait.WaitOne();
                System.Threading.Thread.Sleep(750);
                labelPortID.BackColor = System.Drawing.Color.White;
                labelPortID.ForeColor = System.Drawing.Color.Black;

            }
        }


    }

    /// <summary>
    /// Calibration status display. Shows conditions of current enclouser.
    /// </summary>
    public class enclouserStatus
    {
        #region Graphic Objects
        //Panel to hold everything together.
        System.Windows.Forms.Panel panelEncStatus;

        //label for enclouser Air Temp.
        System.Windows.Forms.Label labelHeaderAirTemp;
        System.Windows.Forms.Label labelCurAirTemp;
        System.Windows.Forms.Label labelSPAirTemp;
        System.Windows.Forms.Label labelDiffAirTemp;

        //label for enclouser Humidity
        System.Windows.Forms.Label labelHeaderHumidity;
        System.Windows.Forms.Label labelCurHumidity;
        System.Windows.Forms.Label labelSPHumidity;
        System.Windows.Forms.Label labelDiffHumidity;

        //Ready progress bars.
        System.Windows.Forms.ProgressBar pBarAirTemp;
        System.Windows.Forms.ProgressBar pBarHumidity;


        //Generic Labels.
        System.Windows.Forms.Label labelHeaderCurrent;
        System.Windows.Forms.Label labelHeaderSP;
        System.Windows.Forms.Label labelHeaderDiff;
        System.Windows.Forms.Label labelHeaderReady;
        #endregion

        #region Data Objects

        double curAirTemp;
        double curHumidity;
        double spAirTemp;
        double spHumidity;

        #endregion

        public enclouserStatus()
        {
            //Setting up the enviroment panel.
            panelEncStatus = new System.Windows.Forms.Panel();
            panelEncStatus.Size = new System.Drawing.Size(200, 68);
            panelEncStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            //Setting up labels.
            //Headers running across the top.
            labelHeaderAirTemp = new System.Windows.Forms.Label();
            labelHeaderAirTemp.AutoSize = false;
            labelHeaderAirTemp.Size = new System.Drawing.Size(50, 17);
            //labelHeaderAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelHeaderAirTemp.Name = "labelHeaderAirTemp";
            labelHeaderAirTemp.Text = "AirTemp";
            labelHeaderAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelHeaderAirTemp.Location = new System.Drawing.Point(50, 0);

            labelHeaderHumidity = new System.Windows.Forms.Label();
            labelHeaderHumidity.AutoSize = false;
            labelHeaderHumidity.Size = new System.Drawing.Size(50, 17);
            //labelHeaderHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelHeaderHumidity.Name = "labelHeaderHumidity";
            labelHeaderHumidity.Text = "Humidity";
            labelHeaderHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelHeaderHumidity.Location = new System.Drawing.Point(100, 0);

            labelHeaderReady = new System.Windows.Forms.Label();
            labelHeaderReady.AutoSize = false;
            labelHeaderReady.Size = new System.Drawing.Size(50, 17);
            //labelHeaderReady.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelHeaderReady.Name = "labelHeaderReady";
            labelHeaderReady.Text = "Ready";
            labelHeaderReady.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelHeaderReady.Location = new System.Drawing.Point(150, 0);

            //Headers running down the left side.
            labelHeaderCurrent = new System.Windows.Forms.Label();
            labelHeaderCurrent.AutoSize = false;
            labelHeaderCurrent.Size = new System.Drawing.Size(50, 17);
            //labelHeaderCurrent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelHeaderCurrent.Name = "labelHeaderCurrent";
            labelHeaderCurrent.Text = "Current:";
            labelHeaderCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            labelHeaderCurrent.Location = new System.Drawing.Point(0, 17);

            labelHeaderSP = new System.Windows.Forms.Label();
            labelHeaderSP.AutoSize = false;
            labelHeaderSP.Size = new System.Drawing.Size(50, 17);
            //labelHeaderSP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelHeaderSP.Name = "labelHeaderSP";
            labelHeaderSP.Text = "SPoint:";
            labelHeaderSP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            labelHeaderSP.Location = new System.Drawing.Point(0, 34);

            labelHeaderDiff = new System.Windows.Forms.Label();
            labelHeaderDiff.AutoSize = false;
            labelHeaderDiff.Size = new System.Drawing.Size(50, 17);
            //labelHeaderDiff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelHeaderDiff.Name = "labelHeaderDiff";
            labelHeaderDiff.Text = "Diff:";
            labelHeaderDiff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            labelHeaderDiff.Location = new System.Drawing.Point(0, 51);

            
            //Status labels
            labelCurAirTemp = new System.Windows.Forms.Label();
            labelCurAirTemp.AutoSize = false;
            labelCurAirTemp.Size = new System.Drawing.Size(50, 17);
            labelCurAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelCurAirTemp.Name = "labelCurAirTemp";
            labelCurAirTemp.Text = "20.25";
            labelCurAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelCurAirTemp.Location = new System.Drawing.Point(50, 17);
            labelCurAirTemp.BackColor = System.Drawing.Color.White;

            labelSPAirTemp = new System.Windows.Forms.Label();
            labelSPAirTemp.AutoSize = false;
            labelSPAirTemp.Size = new System.Drawing.Size(50, 17);
            labelSPAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelSPAirTemp.Name = "labelSPAirTemp";
            labelSPAirTemp.Text = "20.25";
            labelSPAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelSPAirTemp.Location = new System.Drawing.Point(50, 34);
            labelSPAirTemp.BackColor = System.Drawing.Color.White;

            labelDiffAirTemp = new System.Windows.Forms.Label();
            labelDiffAirTemp.AutoSize = false;
            labelDiffAirTemp.Size = new System.Drawing.Size(50, 17);
            labelDiffAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelDiffAirTemp.Name = "labelDiffAirTemp";
            labelDiffAirTemp.Text = "20.25";
            labelDiffAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelDiffAirTemp.Location = new System.Drawing.Point(50, 51);
            labelDiffAirTemp.BackColor = System.Drawing.Color.White;

            labelCurHumidity = new System.Windows.Forms.Label();
            labelCurHumidity.AutoSize = false;
            labelCurHumidity.Size = new System.Drawing.Size(50, 17);
            labelCurHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelCurHumidity.Name = "labelCurHumidity";
            labelCurHumidity.Text = "60.25";
            labelCurHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelCurHumidity.Location = new System.Drawing.Point(100, 17);
            labelCurHumidity.BackColor = System.Drawing.Color.White;

            labelSPHumidity = new System.Windows.Forms.Label();
            labelSPHumidity.AutoSize = false;
            labelSPHumidity.Size = new System.Drawing.Size(50, 17);
            labelSPHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelSPHumidity.Name = "labelSPHumidity";
            labelSPHumidity.Text = "60.25";
            labelSPHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelSPHumidity.Location = new System.Drawing.Point(100, 34);
            labelSPHumidity.BackColor = System.Drawing.Color.White;

            labelDiffHumidity = new System.Windows.Forms.Label();
            labelDiffHumidity.AutoSize = false;
            labelDiffHumidity.Size = new System.Drawing.Size(50, 17);
            labelDiffHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            labelDiffHumidity.Name = "labelDiffHumidity";
            labelDiffHumidity.Text = "60.25";
            labelDiffHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            labelDiffHumidity.Location = new System.Drawing.Point(100, 51);
            labelDiffHumidity.BackColor = System.Drawing.Color.White;


            //Readiness progress bars and labels.
            pBarAirTemp = new System.Windows.Forms.ProgressBar();
            pBarAirTemp.Size = new System.Drawing.Size(50, 17);
            pBarAirTemp.Location = new System.Drawing.Point(150, 17);
            pBarAirTemp.BackColor = System.Drawing.Color.White;
            pBarAirTemp.Value = 50;

            pBarHumidity = new System.Windows.Forms.ProgressBar();
            pBarHumidity.Size = new System.Drawing.Size(50, 17);
            pBarHumidity.Location = new System.Drawing.Point(150, 34);
            pBarHumidity.BackColor = System.Drawing.Color.White;
            pBarHumidity.Value = 50;



            //Adding labels to the panel
            panelEncStatus.Controls.Add(labelHeaderAirTemp);
            panelEncStatus.Controls.Add(labelHeaderHumidity);
            panelEncStatus.Controls.Add(labelHeaderReady);
            panelEncStatus.Controls.Add(labelHeaderCurrent);
            panelEncStatus.Controls.Add(labelHeaderSP);
            panelEncStatus.Controls.Add(labelHeaderDiff);

            //Adding current stats labels.
            panelEncStatus.Controls.Add(labelCurAirTemp);
            panelEncStatus.Controls.Add(labelSPAirTemp);
            panelEncStatus.Controls.Add(labelDiffAirTemp);
            panelEncStatus.Controls.Add(labelCurHumidity);
            panelEncStatus.Controls.Add(labelSPHumidity);
            panelEncStatus.Controls.Add(labelDiffHumidity);

            //Adding progress Bars and labels.
            panelEncStatus.Controls.Add(pBarAirTemp);
            panelEncStatus.Controls.Add(pBarHumidity);

        }

        /// <summary>
        /// Sets the location of the curent status display.
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        public void setLocation(int X, int Y)
        {
            panelEncStatus.Location = new System.Drawing.Point(X, Y);
        }

        public System.Windows.Forms.Panel getStatPanel()
        {
            return panelEncStatus;
        }

        //Update items displayed.
        public void updateCurStatus(double refAirTemp, double refHumidity)
        {
            curAirTemp = refAirTemp;
            curHumidity = refHumidity;

            if (labelCurAirTemp.InvokeRequired)
            {
                labelCurAirTemp.BeginInvoke(new updateSensorData(updateCurStatus), new object[] { refAirTemp, refHumidity });
                return;
            }

            labelCurAirTemp.Text = refAirTemp.ToString("0.00");
            labelCurHumidity.Text = refHumidity.ToString("0.00");

            labelDiffAirTemp.Text = (refAirTemp - spAirTemp).ToString("0.000");
            labelDiffHumidity.Text = (refHumidity - spHumidity).ToString("0.000");
        }

        //Updating set points and the display.
        public void updateSP(double spAirTemp, double spHumidity)
        {
            this.spAirTemp = spAirTemp;
            this.spHumidity = spHumidity;

            if (labelSPAirTemp.InvokeRequired)
            {
                labelSPAirTemp.BeginInvoke(new updateSensorData(updateSP), new object[] { spAirTemp, spHumidity });
                return;
            }

            labelSPAirTemp.Text = spAirTemp.ToString("0.00");
            labelSPHumidity.Text = spHumidity.ToString("0.00");
            
        }
    
        public void updateReady(int airTempReady, int humidityReady)
        {

            if (pBarAirTemp.InvokeRequired)
            {
                pBarAirTemp.BeginInvoke(new updateProgress(updateReady), new object[] { airTempReady, humidityReady });
                return;
            }

            if (airTempReady > 100)
            {
                pBarAirTemp.Value = 100;
            }
            else
            {
                pBarAirTemp.Value = airTempReady;
            }

            if (humidityReady > 100)
            {
                pBarHumidity.Value = 100;
            }
            else
            {
                pBarHumidity.Value = humidityReady;
            }
        }
    }

    #endregion

}
