﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace testHumidityCalDisplay
{

    delegate void moveLoc(int X, int Y);

    public partial class frmHCalDisplay : Form
    {
        displayHCal calDisplay;

        public frmHCalDisplay()
        {
            InitializeComponent();
        }

        private void frmHCalDisplay_Load(object sender, EventArgs e)
        {
            calDisplay = new displayHCal();
            calDisplay.setDisplaySize(400, 500);    //This sets the size of the avaiable area for the display.
            //calDisplay.setDisplayPosition(100, 100);
            System.Windows.Forms.Panel display = calDisplay.getDisplay();


            this.Controls.Add(display);

            /*
            //Adding enclouser
            calDisplay.addEnclouser("1");
            //Adding the 2 runners.
            calDisplay.calEnclousers.Last().addRunner("Left Side");
            calDisplay.calEnclousers.Last().addRunner("Right Side");

            //Adding radiosonde ports.
            for (int l = 0; l < Program.sondePorts.Count; l++)
            {
                foreach (runnerHCal runner in calDisplay.calEnclousers.Last().enclosuerRunners)     //Going through the runners in the enclouser.
                {
                    if (runner.runnerID == "Left Side" && l < 24)   //Left side
                    {
                        runner.addRadiosondePort((l + 1).ToString(), Program.sondePorts[l]);
                        runner.radiosondePorts.Last().connectionInfo = Program.sondePorts[l].ipAddress + ":" + Program.sondePorts[l].port.ToString();
                    }

                    if (runner.runnerID == "Right Side" && l>=24)   //Right Side
                    {
                        runner.addRadiosondePort((l + 1).ToString(), Program.sondePorts[l]);
                        runner.radiosondePorts.Last().connectionInfo = Program.sondePorts[l].ipAddress + ":" + Program.sondePorts[l].port.ToString();
                    }

                }
            }

            */

            //Setting up the enclouser display
            //This drives the setup of the enclousers
            for (int en = 0; en < Program.calibrationEnclousers.Count; en++)
            {
                //Setting up the basic elements of the enclouser.
                calDisplay.addEnclouser(Program.calibrationEnclousers[en].encID);
                calDisplay.calEnclousers.Last().addRunner("Left Side");
                calDisplay.calEnclousers.Last().addRunner("Right Side");

                //Setting up the runners.
                //Adding radiosonde ports.
                for (int l = 0; l < Program.calibrationEnclousers[en].sondePort.Count; l++)
                {
                    foreach (runnerHCal runner in calDisplay.calEnclousers.Last().enclosuerRunners)
                    {
                        if (runner.runnerID == "Left Side" && l < 24)   //Left side
                        {
                            runner.addRadiosondePort((l + 1).ToString(), Program.calibrationEnclousers[en].sondePort[l]);
                            runner.radiosondePorts.Last().connectionInfo = Program.calibrationEnclousers[en].sondePort[l].ipAddress + ":" + Program.calibrationEnclousers[en].sondePort[l].port.ToString();
                        }

                        if (runner.runnerID == "Right Side" && l >= 24)   //Right Side
                        {
                            runner.addRadiosondePort((l + 1).ToString(), Program.calibrationEnclousers[en].sondePort[l]);
                            runner.radiosondePorts.Last().connectionInfo = Program.calibrationEnclousers[en].sondePort[l].ipAddress + ":" + Program.calibrationEnclousers[en].sondePort[l].port.ToString();
                        }
                    }
                }
            }


            System.Threading.Thread connectPort = new System.Threading.Thread(connectPortsThread);
            connectPort.Name = "ConnectPort";
            connectPort.Start();

            System.Threading.Thread randomData = new System.Threading.Thread(fakeDataThread);
            randomData.Name = "Fake_Data";
            randomData.Start();



        }

        #region Methods relating to radiosonde communication.

        /*
        void error_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate from = (updateCreater.objectUpdate)sender; //Getting the where from.

            radiosondePort curPort = findPort(from.objectID);   //Getting the port.
            curPort.setState("errorpacket");
        }

        void disconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate from = (updateCreater.objectUpdate)sender; //Getting the where from.

            radiosondePort curPort = findPort(from.objectID);   //Getting the port.
            curPort.setState("disconnect");
        }

        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate from = (updateCreater.objectUpdate)sender; //Getting the where from.
                
            radiosondePort curPort = findPort(from.objectID);   //Getting the port.
            curPort.setState("calpacket");

        }

        void bootingRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate from = (updateCreater.objectUpdate)sender; //Getting the where from.

            radiosondePort curPort = findPort(from.objectID);   //Getting the port.
            curPort.setState("booting");
        }

        private radiosondePort findPort(string ID)
        {
            //Looking through the radiosonde displayed for the matching sender.
            foreach (enclouser enc in calDisplay.calEnclousers)
            {
                foreach (runnerHCal runner in enc.enclosuerRunners)
                {
                    foreach (radiosondePort port in runner.radiosondePorts)
                    {
                        if (port.connectionInfo == ID)
                        {
                            return port;
                        }
                    }
                }
            }

            return null;
        }
        */

        #endregion

        int count = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            double[,] settings = new double[2,5];

            settings[0, 0] = 25;
            settings[0, 1] = 1;
            settings[0, 2] = 20000;
            settings[0, 3] = 3000;
            settings[0, 4] = 5;

            settings[1, 0] = 17;
            settings[1, 1] = 1;
            settings[1, 2] = 20000;
            settings[1, 3] = 3000;
            settings[1, 4] = 5;

            

            foreach (enclouser enc in calDisplay.calEnclousers)
            {
                foreach (runnerHCal run in enc.enclosuerRunners)
                {
                    foreach (radiosondePort port in run.radiosondePorts)
                    {
                        port.setAPConditions(settings[count, 0], settings[count, 1], settings[count, 2], settings[count, 3], settings[count, 4]);
                    }
                }
            }

            count++;
            if (count >= 2)
            {
                count = 0;
            }

        }

        #region Methods for testing objest pos to other objects.
        private void moveAroundThread()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            System.Windows.Forms.Panel moveTest = calDisplay.getDisplay();

            while (true)
            {
                //BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { openError.Message });
                BeginInvoke(new moveLoc(move), new object[] { moveTest.Location.X + 1, moveTest.Location.Y + 1 });
                System.Threading.Thread.Sleep(250);
            }
        }

        private void move(int X, int Y)
        {
            System.Windows.Forms.Panel moveTest = calDisplay.getDisplay();
            calDisplay.setDisplayPosition(moveTest.Location.X + 1, moveTest.Location.Y + 1);
        }

        #endregion


        private void connectPortsThread()
        {
            foreach (settingsEnclouser enc in Program.calibrationEnclousers)
            {

                //Connecting to radiosondes, and tieing the radiosondes to their port display.
                foreach (ipRadiosonde.ipUniRadiosonde port in enc.sondePort)
                {
                    port.open();        //Connectiong to port.
                    port.autoStartCalMode(true);    //Setting to auto mfg mode.
                    //port.CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
                    //port.bootingRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(bootingRadiosonde_PropertyChange);
                    //port.disconnectRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(disconnectRadiosonde_PropertyChange);
                    //port.error.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(error_PropertyChange);

                }
            }

        }
        

        //Thread fakes incoming data from sensors.
        private void fakeDataThread()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            
            //Setting test sp for enclouser.
            calDisplay.calEnclousers[0].getStatus().updateSP(13.5, 1);

            Random airTemp = new Random();
            Random humidity = new Random();

            Random Ready = new Random();

            int airTR = 0;
            int UR = 0;

            while (true)
            {
                foreach (enclouser enc in calDisplay.calEnclousers)
                {


                    double curAT = Convert.ToDouble(airTemp.Next(1000, 2000)) / 100;
                    double curHum = Convert.ToDouble(humidity.Next(0, 10000)) / 100;

                    enc.getStatus().updateCurStatus(curAT, curHum);
                    enc.getStatus().updateReady(Ready.Next(0, 100), Ready.Next(0, 100));
                    //calDisplay.calEnclousers[0].getStatus().updateReady(airTR, UR);

                    /*
                    airTR++;
                    UR++;

                    if (airTR > 100)
                    {
                        airTR = 0;
                        UR = 0;
                    }

                    */
                }

                System.Threading.Thread.Sleep(1000);
            }
        }




        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            foreach (enclouser enc in calDisplay.calEnclousers)
            {
                comboBox1.Items.Add(enc.enclouserID);
            }
        }

        private void buttonAddPort_Click(object sender, EventArgs e)
        {
            foreach (enclouser enc in calDisplay.calEnclousers)
            {
                if (enc.enclouserID == comboBox1.Text)
                {
                    //enc.enclosuerRunners[0].addRadiosondePort(enc.enclosuerRunners[0].radiosondePorts.Count.ToString());
                    //enc.enclosuerRunners[1].addRadiosondePort(enc.enclosuerRunners[1].radiosondePorts.Count.ToString());
                }
            }
        }

        private void frmHCalDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*
            foreach (ipRadiosonde.ipUniRadiosonde port in Program.sondePorts)
            {
                port.close();
            }
            */

        }
    }
}
