﻿namespace testHumidityCalDisplay
{
    partial class frmHCalDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddEnc = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonAddPort = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAddEnc
            // 
            this.buttonAddEnc.Location = new System.Drawing.Point(660, 36);
            this.buttonAddEnc.Name = "buttonAddEnc";
            this.buttonAddEnc.Size = new System.Drawing.Size(75, 23);
            this.buttonAddEnc.TabIndex = 0;
            this.buttonAddEnc.Text = "Add Enc";
            this.buttonAddEnc.UseVisualStyleBackColor = true;
            this.buttonAddEnc.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(660, 130);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(74, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            // 
            // buttonAddPort
            // 
            this.buttonAddPort.Location = new System.Drawing.Point(660, 157);
            this.buttonAddPort.Name = "buttonAddPort";
            this.buttonAddPort.Size = new System.Drawing.Size(75, 23);
            this.buttonAddPort.TabIndex = 2;
            this.buttonAddPort.Text = "Add Port";
            this.buttonAddPort.UseVisualStyleBackColor = true;
            this.buttonAddPort.Click += new System.EventHandler(this.buttonAddPort_Click);
            // 
            // frmHCalDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 499);
            this.Controls.Add(this.buttonAddPort);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonAddEnc);
            this.Name = "frmHCalDisplay";
            this.Text = "frmHCalDisplay";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHCalDisplay_FormClosing);
            this.Load += new System.EventHandler(this.frmHCalDisplay_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAddEnc;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonAddPort;
    }
}