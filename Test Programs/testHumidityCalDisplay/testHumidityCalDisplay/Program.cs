﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace testHumidityCalDisplay
{
    static class Program
    {
        //Program Objects
        //public static List<ipRadiosonde.ipUniRadiosonde> sondePorts = new List<ipRadiosonde.ipUniRadiosonde>();     //All radiosonde ports on systems.


        //Building enclousers test items.
        public static List<settingsEnclouser> calibrationEnclousers = new List<settingsEnclouser>();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Loading port settings from file.
            //string[] portSettings = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "portAddress.csv");

            //Connection settings.
            string ipaddress = "10.245.126.140";
            int startingPort = 8000;

            //Starting up radiosonde ports.
            //This would be done at setup/startup.
            
           
            //Creating more the one test enclouser.
            for (int x = 0; x < 2; x++)
            {
                settingsEnclouser tempENC = new settingsEnclouser(ipaddress, startingPort + (48 * x));
                tempENC.encID = x.ToString();

                calibrationEnclousers.Add(tempENC);

                //Creating a sim ref sensor.
                //TestEquipment.epluseEE31 refSensor = new TestEquipment.epluseEE31();
                //tempENC.refSensor = refSensor;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmHCalDisplay());
        }

    }

    public class settingsEnclouser
    {
        public List<ipRadiosonde.ipUniRadiosonde> sondePort = new List<ipRadiosonde.ipUniRadiosonde>();    //Radiosonde ports for the enclouser.
        public object refSensor;
        public string encID = "";

        public settingsEnclouser(string ipaddress, int startingPort)
        {
            //Setting up fake radiosonde connections.
            for (int i = 0; i < 48; i++)
            {
                ipRadiosonde.ipUniRadiosonde tempSonde = new ipRadiosonde.ipUniRadiosonde();

                //Real Hardware.
                //tempSonde.ipAddress = breakSettings[0];
                //tempSonde.port = Convert.ToInt16(breakSettings[1]);

                //Sim Hardware.
                tempSonde.ipAddress = ipaddress;
                tempSonde.port = startingPort;
                startingPort++;

                sondePort.Add(tempSonde);
            }
        }

        public void startRefSim()
        {

        }

    }
}
