﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace testRadiosondeDecode
{
    class Program
    {
        //static Radiosonde.iMet1 iMetTX = new Radiosonde.iMet1("COM8");
        public static txDecoder.decoderBell202 decoderBell;

        public static System.Timers.Timer dataMissingTimer;

        public static System.IO.Ports.SerialPort simPort;

        static crcCheckSum.Crc16Ccitt calc = new crcCheckSum.Crc16Ccitt(crcCheckSum.InitialCrcValue.NonZero2); //CRC-CCITT (0x1D0F)

        static string comPort = "";

        static void Main(string[] args)
        {
            /*
            //Setting up radiosonde.
            iMetTX.ComPort.Open();

            //Timer code to collect data at a given time.
            System.Timers.Timer getRadiosondeData = new System.Timers.Timer(1000);
            getRadiosondeData.Elapsed += new System.Timers.ElapsedEventHandler(getRadiosondeData_Elapsed);
            //getRadiosondeData.Enabled = true;


            //iMetTX.decoderPacketDataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderPacketDataReady_PropertyChange);
            iMetTX.oldFormatReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(oldFormatReady_PropertyChange);
            iMetTX.badDataPacket.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(badDataPacket_PropertyChange);
            iMetTX.packetTimeOut.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetTimeOut_PropertyChange);
            iMetTX.startDecoderCollection();

            while (true)
            {
                string command = Console.ReadLine();

                switch (command)
                {
                    case "stop":
                        iMetTX.stopDecoderCollection();
                        break;

                    case "start":
                        iMetTX.startDecoderCollection();
                        break;
                }
            }
            */

            Console.WriteLine("Enter in the com port of decoder");
            comPort = Console.ReadLine();


            decoderBell = new txDecoder.decoderBell202(comPort);
            //decoderBell = new txDecoder.decoderBell202();

            decoderBell.packetPTUReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUReady_PropertyChange);
            //decoderBell.packetGPSReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSReady_PropertyChange);
            //decoderBell.packetPTUXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUReady_PropertyChange);
            decoderBell.packetGPSXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSReady_PropertyChange);
            decoderBell.decoderMessage.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderError_PropertyChange);
            decoderBell.packetBad.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderError_PropertyChange);
            //decoderBell.packetGPSTimeout.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderError_PropertyChange);
            //decoderBell.packetPTUTimeout.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderError_PropertyChange);

            //decoderBell.setTestMode(true);
            

            while (true)
            {
                string command = Console.ReadLine();

                switch (command)
                {
                    case "inst":
                        Console.Write("Enter the stability>");
                        command = Console.ReadLine();
                        string[] inst = command.Split(',');
                        if (inst.Length == 5)
                        {
                            decoderBell.testRadiosonde.setInstability(Convert.ToInt16(inst[0]), Convert.ToInt16(inst[1]), Convert.ToInt16(inst[2]), Convert.ToInt16(inst[3]), Convert.ToInt16(inst[4]));
                        }
                        break;

                    case "set":
                        Console.Write("Set Condition>");
                        command = Console.ReadLine();
                        string[] con = command.Split(',');
                        if (con.Length == 3)
                        {
                            decoderBell.testRadiosonde.setPTU(Convert.ToDouble(con[0]), Convert.ToDouble(con[1]), Convert.ToDouble(con[2]));
                        }
                        break;

                    case "gps":
                        Console.Write("GPS Mode>");
                        command = Console.ReadLine();
                        if (command.Contains("on"))
                        {
                            decoderBell.packetGPSReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSReady_PropertyChange);
                        }
                        else
                        {
                            decoderBell.packetGPSReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSReady_PropertyChange);
                        }
                        break;

                    case "ptu":
                        Console.Write("PTU Mode>");
                        command = Console.ReadLine();
                        if (command.Contains("on"))
                        {
                            decoderBell.packetPTUXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUReady_PropertyChange);
                        }
                        else
                        {
                            decoderBell.packetPTUXReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUReady_PropertyChange);
                        }
                        break;

                    case "xdata":
                        Console.Write("XData Mode>");
                        command = Console.ReadLine();
                        if (command.Contains("on"))
                        {
                            decoderBell.packetXDataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetXDataReady_PropertyChange);
                        }
                        else
                        {
                            decoderBell.packetXDataReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetXDataReady_PropertyChange);
                        }
                        break;

                    case "rsb":
                        Console.WriteLine("Enter COM Port (COM##)");
                        string port = Console.ReadLine();
                        simPort = new System.IO.Ports.SerialPort(port, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
                        simPort.Open();

                        loadDataFile(@"C:\IMS\iMetOS-II\DATA\IMS_20150226_076_001\IMS_20150226_076_001.dat");

                        break;
                }
            }

        }

        static void packetXDataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            txDecoder.radiosondeXDataData curData = decoderBell.getLatestXData();
            Console.WriteLine(curData.packetDateTime.ToString("HH:mm:ss.ff") + "," + curData.data.Length.ToString());
        }

        static void dataMissingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
        }


        static List<txDecoder.radiosondeBadPacketData> badDataPacket = new List<txDecoder.radiosondeBadPacketData>();

        static void decoderError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            System.IO.IOException incoming = (System.IO.IOException)data.NewValue;
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " " + incoming.Message);
            //writeToFile("Error," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("HH:mm:ss.ffff") + "," + incoming.Message);
            



            //If you have received more then 4 bad packets then disable auto testing.
            if (badDataPacket.Count > 6)
            {
                //Checking to see what the time is between the current bad packet and the last packet.
                if (badDataPacket[badDataPacket.Count-1].packetDateTime - badDataPacket[badDataPacket.Count - 3].packetDateTime < new TimeSpan(0, 0, 3))
                {
                    try
                    {
                        Console.WriteLine("Unstable Radiosonde TX. Disabling Auto testing." );
                    }
                    catch
                    {

                    }
                }

                badDataPacket.Clear();

            }

            badDataPacket.Add(decoderBell.badDataPacket);

        }

        static void packetGPSReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            /*
            byte[] gpsData = ((byte[])data.NewValue);

            //Making checksum.
            byte[] toCheck = new byte[gpsData.Length - 2];

            int localCounter = 0;
            for (int y = 0; y < gpsData.Length - 2; y++)
            {
                toCheck[localCounter] = gpsData[y];
                localCounter++;
            }


            byte[] crc = calc.ComputeChecksumBytes(toCheck);

            Console.Write(DateTime.Now.ToString("HH:mm:ss.ff") + " - " + gpsData.Length + " - ");

            Console.Write(BitConverter.ToString(gpsData));

            Console.Write("\n");
            */
            txDecoder.radiosondeGPSData latest = decoderBell.getLatestGPS();

            Console.WriteLine(latest.gpsTime.ToString("HH:mm:ss") + "," + latest.latitude.ToString("0.000000") + "," + latest.longitude.ToString("0.000000") + "," + latest.alt + "," + latest.satCount);
            writeToFile("GPS," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("HH:mm:ss.ffff") + "," + latest.gpsTime.ToString("HH:mm:ss") + "," + latest.latitude.ToString("0.000000") + "," + latest.longitude.ToString("0.000000") + "," + latest.alt + "," + latest.satCount);
            writeToFile("GPSRaw," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("HH:mm:ss.ffff") + "," + getStringForByteArray(latest.rawData));
            //writeToFile(gpsData, crc);


            if (simPort != null)
            {
                if (simPort.IsOpen)
                {
                    string line = "GPS: " + latest.latitude.ToString("0.00000") + ", " + latest.longitude.ToString("0.00000") + ", " + latest.alt.ToString("0.0") + ", " +
                        latest.satCount.ToString("00") + ", " + DateTime.Now.ToString("HH:mm:ss");
                    simPort.Write(line + "\r\n");
                }
            }

        }

        static void packetPTUReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            /*
            byte[] gpsPTU = ((byte[])data.NewValue);

            //Making checksum.
            byte[] toCheck = new byte[gpsPTU.Length - 2];

            int localCounter = 0;
            for (int y = 0; y < gpsPTU.Length - 2; y++)
            {
                toCheck[localCounter] = gpsPTU[y];
                localCounter++;
            }


            byte[] crc = calc.ComputeChecksumBytes(toCheck);

            Console.Write(DateTime.Now.ToString("HH:mm:ss.ff") + " - " + gpsPTU.Length + " - ");

            Console.Write(BitConverter.ToString(gpsPTU));

            Console.Write("\n");
            */
            txDecoder.radiosondePTUData latest = decoderBell.getLatestPTU();

            Console.WriteLine(latest.packetNumber + "\t" + latest.pressure.ToString("0.00") + "\t" + latest.airTemp.ToString("0.00") + "\t" + latest.humidity.ToString("0.00") + "\t" + latest.internetTemp.ToString("0.00") + "," + latest.pressureTemp.ToString("0.00"));
            writeToFile("PTU," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("HH:mm:ss.ffff") + "," + latest.packetNumber + "," + latest.pressure.ToString("0.00") + "," + latest.airTemp.ToString("0.00") + "," + latest.humidity.ToString("0.00") + "," + latest.internetTemp.ToString("0.00") + "," + latest.pressureTemp.ToString("0.00"));
            writeToFile("PTURaw," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("HH:mm:ss.ffff") + "," + getStringForByteArray(latest.rawData));
            //writeToFile(gpsPTU, crc);

            if (simPort != null)
            {
                if (simPort.IsOpen)
                {
                    string line = "PTUX: " + latest.pressure.ToString("0.00") + ", " + latest.airTemp.ToString("0.00") + ", " + latest.humidity.ToString("0.00") + ", " +
                        latest.internetTemp.ToString("0.00") + " ";
                    simPort.Write(line + "\r\n");
                }
            }
        }

        static string getStringForByteArray(byte[] data)
        {
            string output = "";
            for(int i=0; i< data.Length; i++)
            {
                output += data[i].ToString("X") + "-";
            }

            return output;
        }

        static void writeToFile(byte[] incomingData, byte[] checksum)
        {
            string outputData = BitConverter.ToString(incomingData);

            System.IO.File.AppendAllText("decoderRadioSonde.csv", DateTime.Now.ToString("HH:mm:ss.ff") + "," + incomingData.Length.ToString() + "," + outputData + "," + BitConverter.ToString(checksum) + "\n");
        }

        static void writeToFile(string logData)
        {
            System.IO.File.AppendAllText("decodedData" + comPort + ".csv", logData + "\n");
        }

        static void loadDataFile(string fileName)
        {
            string[] data = System.IO.File.ReadAllLines(fileName);

        }

        /*
        static void packetTimeOut_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Console.WriteLine("Packet Time Out.");
        }

        static void badDataPacket_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            byte[] incomingData = (byte[])data.NewValue;

            string badData = DateTime.Now.ToString("HH:mm:ss.ffff") + "," + BitConverter.ToString(incomingData) + "\n";
            System.IO.File.AppendAllText("BadData.csv", badData);
        }

        static void oldFormatReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {

            string header = DateTime.Now.ToString("HH:mm:ss.ffff") + "," + iMetTX.getPressure().ToString() + "," + iMetTX.getAirTemp().ToString() + "," +
                iMetTX.getRH().ToString() + "," + iMetTX.getGPSlat().ToString() + "," + iMetTX.getGPSLong().ToString() + "," + iMetTX.getGPSAlt().ToString() + "," + iMetTX.sSondeData + "\n";


            System.IO.File.AppendAllText(@"c:\radiosondeDataNew.csv", header);
        }

        static void decoderPacketDataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            /*
            byte[] incomingData = (byte[])data.NewValue;

            //Making a string of the data.
            string stringData = BitConverter.ToString(incomingData);
            
            //Doing some air temp decodeing.
            // double airTemp = Convert.ToUInt32(splitSondeData[88] + splitSondeData[87] + splitSondeData[86], 16) / 1000.0 - 273.15;

            double airTemp = (incomingData[89] + incomingData[88] + incomingData[87]) / 1000.0 - 273.15;
            //Convert.ToUInt32(
            */
        /*

            //string header = DateTime.Now.ToString("HH:mm:ss.ffff") + "," + incomingData.Length.ToString() + "," + airTemp.ToString("0.00000") + "," + stringData + "\n";

            string header = DateTime.Now.ToString("HH:mm:ss.ffff") + "," + iMetTX.getPressure().ToString() + "," + iMetTX.getAirTemp().ToString() + "," + iMetTX.getRH().ToString() + "," + iMetTX.sSondeData;
            System.IO.File.AppendAllText(@"c:\radiosondeDataNew.csv", header);

        }

        static void getRadiosondeData_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string what = iMetTX.getLastDecodedHexData();
            Console.WriteLine(iMetTX.sSondeData);
            writeData(what);
        }

        private static void writeData(string state)
        {
            if (!System.IO.File.Exists(@"c:\radiosondeData.csv"))
            {
                string header = "Date,Time,Packet,Pressure,Temp,RH,Lat,Long,Alt,State\n";
                System.IO.File.AppendAllText(@"c:\radiosondeData.csv", header);
            }

            string theLine = DateTime.Now.ToString("dd/MM/yyyy") + "," + DateTime.Now.ToString("HH:mm:ss.ffff") + "," +
                iMetTX.getPacketCount() + "," + iMetTX.getPressure() + "," + iMetTX.getAirTemp() + "," + iMetTX.getRH() + "," +
                iMetTX.getGPSlat() + "," + iMetTX.getGPSLong() + "," + iMetTX.getGPSAlt() + "," + state + "\n";

            System.IO.File.AppendAllText(@"c:\radiosondeData.csv", theLine);
        }
         */ 
    }


}
