﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace testChamberControl
{

    delegate void UpdateDisplay(string desiredUpdate);

    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            Program.chamber.statusPacket.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPacket_PropertyChange);
        }

        void statusPacket_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //BeginInvoke(new sendABool(this.contorlControls), new object[] { true });
            BeginInvoke(new UpdateDisplay(this.updateDisplay), new object[] { "nothing" });
        }

        void updateDisplay(string nothing)
        {
            labelCurrentTemp.Text = Program.chamber.CurrentChamberC.ToString("0.000");
            labelTHD.Text = Program.chamber.CurrentChamberTempTHTL.ToString("0.000");
            labelStatus.Text = Program.chamber.CurrentChamberMode.ToString();

            labelCurrentPTemp.Text = Program.chamber.CurrentChamberPC.ToString("0.000");
            labelCurrentPThrottle.Text = Program.chamber.CurrentChamberPTHTL.ToString("0.000");

            labelCurrentTempSetPoint.Text = Program.chamber.CurrentChamberSPC.ToString("0.000");
            labelCurrentPTempSetPoint.Text = Program.chamber.CurrentChamberSPPC.ToString("0.000");

            //if(Program.chamber.CurrentChamberMode
        }

        private void buttonSetTemp_Click(object sender, EventArgs e)
        {
            Program.chamber.setChamberTemp(Convert.ToDouble(textBoxSetTemp.Text));
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            Program.chamber.startChamberManualMode();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            Program.chamber.stopChamberManualMode();
        }

        private void buttonSetPTemp_Click(object sender, EventArgs e)
        {
            int options = Program.chamber.getOptions();

            Program.chamber.setOptions(33);
            Program.chamber.setProductTemp(Convert.ToDouble(textBoxSetTemp.Text));
        }

        private void labelCurrentTempSetPoint_Click(object sender, EventArgs e)
        {

        }

        private void labelCurrentPThrottle_Click(object sender, EventArgs e)
        {

        }

        private void labelCurrentPTemp_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void labelTHD_Click(object sender, EventArgs e)
        {

        }

        private void labelCurrentTemp_Click(object sender, EventArgs e)
        {

        }

        private void labelCurrentPTempSetPoint_Click(object sender, EventArgs e)
        {

        }
    }
}
