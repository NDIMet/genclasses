﻿namespace testChamberControl
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSetTemp = new System.Windows.Forms.TextBox();
            this.buttonSetTemp = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCurrentTemp = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelTHD = new System.Windows.Forms.Label();
            this.labelCurrentPThrottle = new System.Windows.Forms.Label();
            this.labelCurrentPTemp = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSetPTemp = new System.Windows.Forms.Button();
            this.labelCurrentPTempSetPoint = new System.Windows.Forms.Label();
            this.labelCurrentTempSetPoint = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxSetTemp
            // 
            this.textBoxSetTemp.Location = new System.Drawing.Point(12, 113);
            this.textBoxSetTemp.Name = "textBoxSetTemp";
            this.textBoxSetTemp.Size = new System.Drawing.Size(100, 20);
            this.textBoxSetTemp.TabIndex = 0;
            // 
            // buttonSetTemp
            // 
            this.buttonSetTemp.Location = new System.Drawing.Point(118, 111);
            this.buttonSetTemp.Name = "buttonSetTemp";
            this.buttonSetTemp.Size = new System.Drawing.Size(75, 23);
            this.buttonSetTemp.TabIndex = 1;
            this.buttonSetTemp.Text = "Set Temp";
            this.buttonSetTemp.UseVisualStyleBackColor = true;
            this.buttonSetTemp.Click += new System.EventHandler(this.buttonSetTemp_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Air Temp:";
            // 
            // labelCurrentTemp
            // 
            this.labelCurrentTemp.BackColor = System.Drawing.Color.White;
            this.labelCurrentTemp.Location = new System.Drawing.Point(170, 23);
            this.labelCurrentTemp.Name = "labelCurrentTemp";
            this.labelCurrentTemp.Size = new System.Drawing.Size(70, 23);
            this.labelCurrentTemp.TabIndex = 3;
            this.labelCurrentTemp.Text = "XXXX.XXXX";
            this.labelCurrentTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCurrentTemp.Click += new System.EventHandler(this.labelCurrentTemp_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(257, 141);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(278, 123);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(35, 13);
            this.labelStatus.TabIndex = 5;
            this.labelStatus.Text = "label2";
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(257, 170);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 6;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // labelTHD
            // 
            this.labelTHD.BackColor = System.Drawing.Color.White;
            this.labelTHD.Location = new System.Drawing.Point(246, 23);
            this.labelTHD.Name = "labelTHD";
            this.labelTHD.Size = new System.Drawing.Size(70, 23);
            this.labelTHD.TabIndex = 7;
            this.labelTHD.Text = "XXXX.XXXX";
            this.labelTHD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTHD.Click += new System.EventHandler(this.labelTHD_Click);
            // 
            // labelCurrentPThrottle
            // 
            this.labelCurrentPThrottle.BackColor = System.Drawing.Color.White;
            this.labelCurrentPThrottle.Location = new System.Drawing.Point(246, 51);
            this.labelCurrentPThrottle.Name = "labelCurrentPThrottle";
            this.labelCurrentPThrottle.Size = new System.Drawing.Size(70, 23);
            this.labelCurrentPThrottle.TabIndex = 10;
            this.labelCurrentPThrottle.Text = "XXXX.XXXX";
            this.labelCurrentPThrottle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCurrentPThrottle.Click += new System.EventHandler(this.labelCurrentPThrottle_Click);
            // 
            // labelCurrentPTemp
            // 
            this.labelCurrentPTemp.BackColor = System.Drawing.Color.White;
            this.labelCurrentPTemp.Location = new System.Drawing.Point(170, 51);
            this.labelCurrentPTemp.Name = "labelCurrentPTemp";
            this.labelCurrentPTemp.Size = new System.Drawing.Size(70, 23);
            this.labelCurrentPTemp.TabIndex = 9;
            this.labelCurrentPTemp.Text = "XXXX.XXXX";
            this.labelCurrentPTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCurrentPTemp.Click += new System.EventHandler(this.labelCurrentPTemp_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Product Temp:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // buttonSetPTemp
            // 
            this.buttonSetPTemp.Location = new System.Drawing.Point(118, 140);
            this.buttonSetPTemp.Name = "buttonSetPTemp";
            this.buttonSetPTemp.Size = new System.Drawing.Size(75, 23);
            this.buttonSetPTemp.TabIndex = 11;
            this.buttonSetPTemp.Text = "Set P Temp";
            this.buttonSetPTemp.UseVisualStyleBackColor = true;
            this.buttonSetPTemp.Click += new System.EventHandler(this.buttonSetPTemp_Click);
            // 
            // labelCurrentPTempSetPoint
            // 
            this.labelCurrentPTempSetPoint.BackColor = System.Drawing.Color.White;
            this.labelCurrentPTempSetPoint.Location = new System.Drawing.Point(94, 51);
            this.labelCurrentPTempSetPoint.Name = "labelCurrentPTempSetPoint";
            this.labelCurrentPTempSetPoint.Size = new System.Drawing.Size(70, 23);
            this.labelCurrentPTempSetPoint.TabIndex = 13;
            this.labelCurrentPTempSetPoint.Text = "XXXX.XXXX";
            this.labelCurrentPTempSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCurrentPTempSetPoint.Click += new System.EventHandler(this.labelCurrentPTempSetPoint_Click);
            // 
            // labelCurrentTempSetPoint
            // 
            this.labelCurrentTempSetPoint.BackColor = System.Drawing.Color.White;
            this.labelCurrentTempSetPoint.Location = new System.Drawing.Point(94, 23);
            this.labelCurrentTempSetPoint.Name = "labelCurrentTempSetPoint";
            this.labelCurrentTempSetPoint.Size = new System.Drawing.Size(70, 23);
            this.labelCurrentTempSetPoint.TabIndex = 12;
            this.labelCurrentTempSetPoint.Text = "XXXX.XXXX";
            this.labelCurrentTempSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCurrentTempSetPoint.Click += new System.EventHandler(this.labelCurrentTempSetPoint_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Set Point";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(188, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Current";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Throttle";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 205);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelCurrentPTempSetPoint);
            this.Controls.Add(this.labelCurrentTempSetPoint);
            this.Controls.Add(this.buttonSetPTemp);
            this.Controls.Add(this.labelCurrentPThrottle);
            this.Controls.Add(this.labelCurrentPTemp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelTHD);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelCurrentTemp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSetTemp);
            this.Controls.Add(this.textBoxSetTemp);
            this.Name = "frmMain";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSetTemp;
        private System.Windows.Forms.Button buttonSetTemp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCurrentTemp;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelTHD;
        private System.Windows.Forms.Label labelCurrentPThrottle;
        private System.Windows.Forms.Label labelCurrentPTemp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSetPTemp;
        private System.Windows.Forms.Label labelCurrentPTempSetPoint;
        private System.Windows.Forms.Label labelCurrentTempSetPoint;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}

