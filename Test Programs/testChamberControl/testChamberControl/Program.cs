﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace testChamberControl
{
    static class Program
    {
        public static TestEquipment.Thermotron chamber;


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            chamber = new TestEquipment.Thermotron("192.168.1.230", 8888);

            System.Threading.Thread getChamber = new System.Threading.Thread(getChamberData);
            getChamber.Start();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        static void getChamberData()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            

            while (true)
            {
                chamber.getChamberStatus();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
