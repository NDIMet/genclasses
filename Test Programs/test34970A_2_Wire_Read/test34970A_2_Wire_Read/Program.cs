﻿//Test Program for Agilent 34401A for 2 wire resistance reads
//William Jones
//4/6/2015
//for InterMet Systems.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test34970A_2_Wire_Read
{
    class Program
    {
        //Setting up the mux.
        public static TestEquipment.Agilent34401A mux;
        static bool runService = true;
        static System.Threading.AutoResetEvent messageRec = new System.Threading.AutoResetEvent(false);

        static double[] ref2Coef = new double[4]{0.001198961099970, 0.000299455978051, -0.000003730397590, 0.000000305166661};

        static void Main(string[] args)
        {

            Console.WriteLine("Test program for the Agilent 34970A. Enter ? for commands.");        

            //Something to bring in user input.
            string command = "";
            while(command.ToLower() != "exit")
            {
                command = Console.ReadLine();

                switch (command.ToLower())
                {
                    case "setup":

                        Console.Write("What communication mode>");
                        string mode = Console.ReadLine();

                        switch (mode.ToLower())
                        {
                            case "comport":


                                //Setting up the comport.
                                Console.WriteLine("Enter in the com port>");
                                string port = Console.ReadLine();
                                mux = new TestEquipment.Agilent34401A(port);

                                //Setting the mux up for 2 wire reads.
                                mux.configureResistance();
                                break;

                            case "tcp":

                                string ip = "10.245.126.224";
                                int tcpPort = 8001;

                                mux = new TestEquipment.Agilent34401A(ip, tcpPort);

                                //Setting the mux up for 2 wire reads.
                                mux.configureResistance();

                                break;

                        }


                        break;


                    case "run":

                        runService = true;

                        if (mux != null)
                        {
                            //Connecting to data stream.
                            mux.dataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(dataReady_PropertyChange);

                            //Setting up thread to read port.
                            System.Threading.Thread threadMux = new System.Threading.Thread(new System.Threading.ThreadStart(serveMuxThread));
                            runService = true;
                            threadMux.Name = "Mux Thread";
                            threadMux.IsBackground = true;
                            threadMux.Start();
                            

                        }
                        else
                        {
                            Console.WriteLine("Mux not configured.");
                        }
                        break;

                    case "stop":
                        runService = false;
                        break;

                    case "?":
                        Console.WriteLine("setup - Sets up the connection from the computer the the mux.");
                        Console.WriteLine("run - Runs a thread to read the resistance.");
                        Console.WriteLine("stop - Stop thread that reads the resistance.");
                        break;
                }
                

            }
            
        }

        static void dataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            TestEquipment.dataPacket incoming = (TestEquipment.dataPacket)data.NewValue;

            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " - " + incoming.packetMessage.Trim() + " = " + 
                calculateReference2Temp(Convert.ToDouble(incoming.packetMessage.Trim())).ToString("0.0000"));

            messageRec.Set();
        }
        

        static void serveMuxThread()
        {
            while (runService)
            {
                mux.getCurrentReading();

                messageRec.WaitOne();


            }
        }

        static private double calculateReference2Temp(double resistance)
        {
            double lnRes = Math.Log(resistance, Math.E);

            //A more dynamic way to calc
            double invTemp = ref2Coef[0];

            for (int i = 1; i < ref2Coef.Length; i++)
            {
                invTemp += ref2Coef[i] * Math.Pow(lnRes, i);
            }

            double kT = 1 / invTemp;
            double output = kT - 273.15;
            return output;
        }
        
    }
}
