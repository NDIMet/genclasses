﻿///Metadata export class.
///Created by: William Jones
///For: InterMet Systems
///Date: 08/27/2015

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace metadataExport
{
    /// <summary>
    /// Directory Interchange Format Metadata Class
    /// See: http://gcmd.gsfc.nasa.gov/add/difguide/index.html For details
    /// </summary>
    public class metadata_DIF
    {
        public string Entry_ID;
        public string Entry_Title;
        public Data_Set_Citation Data_Set_Citation;
        public Personnel Personnel;
        public Discipline Discipline;
        public Parameters Parameters;
        public string ISO_Topic_Category;
        public string Keyword;
        public Sensor_Name Sensor_Name;
        public Source_Name Source_Name;
        public Temporal_Coverage Temporal_Coverage;
        public Paleo_Temporal_Coverage Paleo_Temporal_Coverage;
        public string Data_Set_Progress;
        public Spatial_Coverage Spatial_Coverage;
        public Location Location;
        public Data_Resolution Data_Resolution;
        public Project Project;
        public string Quality;
        public string Access_Constraints;
        public string Use_Constraints;
        public string Data_Set_Language;
        public string Originating_Center;
        public Data_Center Data_Center;
        public Distribution Distribution;
        public Multimedia_Sample Multimedia_Sample;
        public Reference Reference;
        public Summary Summary;
        public Related_URL Related_URL;
        public string Parent_DIF;
        public IDN_Node IDN_Node;
        public string Originating_Metadata_Node;
        public string Metadata_Name;
        public Extended_Metadata Extended_Metadata;
        public string Metadata_Version;
        public string DIF_Creation_Date;
        public string Last_DIF_Revision_Date;
        public string DIF_Revision_History;
        public string Future_DIF_Review_Date;
        public string Private;


        public metadata_DIF()
        {
            //init all data items.
            Entry_ID = "";
            Entry_Title = "";
            Data_Set_Citation = new Data_Set_Citation();
            Personnel = new Personnel();
            Discipline = new Discipline();
            Parameters = new Parameters();
            ISO_Topic_Category = "";
            Keyword = "";
            Sensor_Name = new Sensor_Name();
            Source_Name = new Source_Name();
            Temporal_Coverage = new Temporal_Coverage();
            Paleo_Temporal_Coverage = new Paleo_Temporal_Coverage();
            Data_Set_Progress = "";
            Spatial_Coverage = new Spatial_Coverage();
            Location = new Location();
            Data_Resolution = new Data_Resolution();
            Project = new Project();
            Quality = "";
            Access_Constraints = "";
            Use_Constraints = "";
            Data_Set_Language = "";
            Originating_Center = "";
            Data_Center = new Data_Center();
            Distribution = new Distribution();
            Multimedia_Sample = new Multimedia_Sample();
            Reference = new Reference();
            Summary = new Summary();
            Related_URL = new Related_URL();
            Parent_DIF = "";
            IDN_Node = new IDN_Node();
            Originating_Metadata_Node = "";
            Extended_Metadata = new metadataExport.Extended_Metadata();
            Metadata_Name = "";
            DIF_Creation_Date = "";
            Last_DIF_Revision_Date = "";
            DIF_Revision_History = "";
            Future_DIF_Review_Date = "";
            Private = "";
        }

        public void saveMetadata(string fileName)
        {
            //Container for the file data.
            List<string> fileData = new List<string>();

            //The Header.
            fileData.Add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            fileData.Add("<DIF xmlns=\"http://gcmd.gsfc.nasa.gov/Aboutus/xml/dif/\"");
            fileData.Add("    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
            fileData.Add("    xsi:schemaLocation=\"http://gcmd.gsfc.nasa.gov/Aboutus/xml/dif/ http://gcmd.nasa.gov/Aboutus/xml/dif/dif_v9.8.4.xsd\">");

            //The Metadata
            fileData.Add("\t<Entry_ID>" + Entry_ID + "</Entry_ID>");
            fileData.Add("\t<Entry_Title>" + Entry_Title + "</Entry_Title>");

            fileData.Add("\t<Data_Set_Citation>");
            fileData.Add("\t\t<Dataset_Creator>" + Data_Set_Citation.Dataset_Creator + "</Dataset_Creator>");
            fileData.Add("\t\t<Dataset_Editor>" + Data_Set_Citation.Dataset_Editor + "</Dataset_Editor>");
            fileData.Add("\t\t<Dataset_Title>" + Data_Set_Citation.Dataset_Title + "</Dataset_Title>");
            fileData.Add("\t\t<Dataset_Series_Name>" + Data_Set_Citation.Dataset_Series_Name + "</Dataset_Series_Name>");
            fileData.Add("\t\t<Dataset_Release_Date>" + Data_Set_Citation.Dataset_Release_Date + "</Dataset_Release_Date>");
            fileData.Add("\t\t<Dataset_Release_Place>" + Data_Set_Citation.Dataset_Release_Place + "</Dataset_Release_Place>");
            fileData.Add("\t\t<Dataset_Publisher>" + Data_Set_Citation.Dataset_Publisher + "</Dataset_Publisher>");
            fileData.Add("\t\t<Version>" + Data_Set_Citation.Version + "</Version>");
            fileData.Add("\t\t<Issue_Identification>" + Data_Set_Citation.Issue_Identification + "</Issue_Identification>");
            fileData.Add("\t\t<Data_Presentation_Form>" + Data_Set_Citation.Data_Presentation_Form + "</Data_Presentation_Form>");
            fileData.Add("\t\t<Other_Citation_Details>" + Data_Set_Citation.Other_Citation_Details + "</Other_Citation_Details>");
            fileData.Add("\t\t<Dataset_DOI>" + Data_Set_Citation.Dataset_DOI + "</Dataset_DOI>");
            fileData.Add("\t\t<Online_Resource>" + Data_Set_Citation.Online_Resource + "</Online_Resource>");
            fileData.Add("\t</Data_Set_Citation>");

            fileData.Add("\t<Personnel>");
            fileData.Add("\t\t<Role>" + Personnel.Role + "</Role>");
            fileData.Add("\t\t<First_Name>" + Personnel.First_Name + "</First_Name>");
            fileData.Add("\t\t<Middle_Name>" + Personnel.Middle_Name + "</Middle_Name>");
            fileData.Add("\t\t<Last_Name>" + Personnel.Last_Name + "</Last_Name>");
            fileData.Add("\t\t<Email>" + Personnel.Email + "</Email>");
            fileData.Add("\t\t<Phone>" + Personnel.Phone + "</Phone>");
            fileData.Add("\t\t<Fax>" + Personnel.Fax + "</Fax>");
            fileData.Add("\t\t<Contact_Address>");
            fileData.Add("\t\t\t<Address>" + Personnel.Contact_Address.Address + "</Address>");
            fileData.Add("\t\t\t<City>" + Personnel.Contact_Address.City + "</City>");
            fileData.Add("\t\t\t<Province_or_State>" + Personnel.Contact_Address.Province_or_State + "</Province_or_State>");
            fileData.Add("\t\t\t<Postal_Code>" + Personnel.Contact_Address.Postal_Code + "</Postal_Code>");
            fileData.Add("\t\t\t<Country>" + Personnel.Contact_Address.Country + "</Country>");
            fileData.Add("\t\t</Contact_Address>");
            fileData.Add("\t</Personnel>");

            fileData.Add("\t<Discipline>");
            fileData.Add("\t\t<Discipline_Name>" + Discipline.Discipline_Name + "</Discipline_Name>");
            fileData.Add("\t\t<Subdiscipline>" + Discipline.Subdiscipline + "</Subdiscipline>");
            fileData.Add("\t\t<Detailed_Subdiscipline>" + Discipline.Detailed_Subdiscipline + "</Detailed_Subdiscipline>");
            fileData.Add("\t</Discipline>");

            fileData.Add("\t<Parameters>");
            fileData.Add("\t\t<Category>" + Parameters.Category + "</Category>");
            fileData.Add("\t\t<Topic>" + Parameters.Topic + "</Topic>");
            fileData.Add("\t\t<Term>" + Parameters.Term + "</Term>");
            fileData.Add("\t\t<Variable_Level_1>" + Parameters.Variable_Level_1 + "</Variable_Level_1>");
            fileData.Add("\t\t<Variable_Level_2>" + Parameters.Variable_Level_2 + "</Variable_Level_2>");
            fileData.Add("\t\t<Variable_Level_3>" + Parameters.Variable_Level_3 + "</Variable_Level_3>");
            fileData.Add("\t\t<Detailed_Variable>" + Parameters.Detailed_Variable + "</Detailed_Variable>");
            fileData.Add("\t</Parameters>");

            fileData.Add("\t<ISO_Topic_Category>" + ISO_Topic_Category + "</ISO_Topic_Category>");
            fileData.Add("\t<Keyword>" + Keyword + "</Keyword>");
            
            fileData.Add("\t<Sensor_Name>");
            fileData.Add("\t\t<Short_Name>" + Sensor_Name.Short_Name + "</Short_Name>");
            fileData.Add("\t\t<Long_Name>" + Sensor_Name.Long_Name + "</Long_Name>");
            fileData.Add("\t</Sensor_Name>");

            fileData.Add("\t<Source_Name>");
            fileData.Add("\t\t<Short_Name>" + Source_Name.Short_Name + "</Short_Name>");
            fileData.Add("\t\t<Long_Name>" + Source_Name.Long_Name + "</Long_Name>");
            fileData.Add("\t</Source_Name>");

            fileData.Add("\t<Temporal_Coverage>");
            fileData.Add("\t\t<Start_Date>" + Temporal_Coverage.Start_Date + "</Start_Date>");
            fileData.Add("\t\t<Stop_Date>" + Temporal_Coverage.Stop_Date + "</Stop_Date>");
            fileData.Add("\t</Temporal_Coverage>");

            fileData.Add("\t<Paleo_Temporal_Coverage>");
            fileData.Add("\t\t<Paleo_Start_Date>" + Paleo_Temporal_Coverage.Paleo_Start_Date + "</Paleo_Start_Date>");
            fileData.Add("\t\t<Paleo_Stop_Date>" + Paleo_Temporal_Coverage.Paleo_Stop_Date + "</Paleo_Stop_Date>");
            fileData.Add("\t\t<Chronostratigraphic_Unit>");
            fileData.Add("\t\t\t<Eon>" + Paleo_Temporal_Coverage.Chronostratigraphic_Unit.Eon + "</Eon>");
            fileData.Add("\t\t\t<Era>" + Paleo_Temporal_Coverage.Chronostratigraphic_Unit.Era + "</Era>");
            fileData.Add("\t\t\t<Period>" + Paleo_Temporal_Coverage.Chronostratigraphic_Unit.Period + "</Period>");
            fileData.Add("\t\t\t<Epoch>" + Paleo_Temporal_Coverage.Chronostratigraphic_Unit.Epoch + "</Epoch>");
            fileData.Add("\t\t\t<Stage>" + Paleo_Temporal_Coverage.Chronostratigraphic_Unit.Stage + "</Stage>");
            fileData.Add("\t\t\t<Detailed_Classification>" + Paleo_Temporal_Coverage.Chronostratigraphic_Unit.Detailed_Classification + "</Detailed_Classification>");
            fileData.Add("\t\t</Chronostratigraphic_Unit>");
            fileData.Add("\t</Paleo_Temporal_Coverage>");

            fileData.Add("\t<Data_Set_Progress>" + Data_Set_Progress + "</Data_Set_Progress>");

            fileData.Add("\t<Spatial_Coverage>");
            fileData.Add("\t\t<Southernmost_Latitude>" + Spatial_Coverage.Southernmost_Latitude + "</Southernmost_Latitude>");
            fileData.Add("\t\t<Northernmost_Latitude>" + Spatial_Coverage.Northernmost_Latitude + "</Northernmost_Latitude>");
            fileData.Add("\t\t<Westernmost_Longitude>" + Spatial_Coverage.Westernmost_Longitude + "</Westernmost_Longitude>");
            fileData.Add("\t\t<Easternmost_Longitude>" + Spatial_Coverage.Easternmost_Longitude + "</Easternmost_Longitude>");
            fileData.Add("\t\t<Minimum_Altitude>" + Spatial_Coverage.Minimum_Altitude + "</Minimum_Altitude>");
            fileData.Add("\t\t<Maximum_Altitude>" + Spatial_Coverage.Maximum_Altitude + "</Maximum_Altitude>");
            fileData.Add("\t\t<Minimum_Depth>" + Spatial_Coverage.Minimum_Depth + "</Minimum_Depth>");
            fileData.Add("\t\t<Maximum_Depth>" + Spatial_Coverage.Maximum_Depth + "</Maximum_Depth>");
            fileData.Add("\t</Spatial_Coverage>");

            fileData.Add("\t<Location>");
            fileData.Add("\t\t<Location_Category>" + Location.Location_Category + "</Location_Category>");
            fileData.Add("\t\t<Location_Type>" + Location.Location_Type + "</Location_Type>");
            fileData.Add("\t\t<Location_Subregion1>" + Location.Location_Subregion1 + "</Location_Subregion1>");
            fileData.Add("\t\t<Location_Subregion2>" + Location.Location_Subregion2 + "</Location_Subregion2>");
            fileData.Add("\t\t<Location_Subregion3>" + Location.Location_Subregion3 + "</Location_Subregion3>");
            fileData.Add("\t\t<Detailed_Location>" + Location.Detailed_Location + "</Detailed_Location>");
            fileData.Add("\t</Location>");

            fileData.Add("\t<Data_Resolution>");
            fileData.Add("\t\t<Latitude_Resolution>" + Data_Resolution.Latitude_Resolution + "</Latitude_Resolution>");
            fileData.Add("\t\t<Longitude_Resolution>" + Data_Resolution.Longitude_Resolution + "</Longitude_Resolution>");
            fileData.Add("\t\t<Horizontal_Resolution_Range>" + Data_Resolution.Horizontal_Resolution_Range + "</Horizontal_Resolution_Range>");
            fileData.Add("\t\t<Vertical_Resolution>" + Data_Resolution.Vertical_Resolution + "</Vertical_Resolution>");
            fileData.Add("\t\t<Vertical_Resolution_Range>" + Data_Resolution.Vertical_Resolution_Range + "</Vertical_Resolution_Range>");
            fileData.Add("\t\t<Temporal_Resolution>" + Data_Resolution.Temporal_Resolution + "</Temporal_Resolution>");
            fileData.Add("\t\t<Temporal_Resolution_Range>" + Data_Resolution.Temporal_Resolution_Range + "</Temporal_Resolution_Range>");
            fileData.Add("\t</Data_Resolution>");

            fileData.Add("\t<Project>");
            fileData.Add("\t\t<Short_Name>" + Project.Short_Name + "</Short_Name>");
            fileData.Add("\t\t<Long_Name>" + Project.Long_Name + "</Long_Name>");
            fileData.Add("\t</Project>");

            fileData.Add("\t<Quality>" + Quality + "</Quality>");
            fileData.Add("\t<Access_Constraints>" + Access_Constraints + "</Access_Constraints>");
            fileData.Add("\t<Use_Constraints>" + Use_Constraints + "</Use_Constraints>");
            fileData.Add("\t<Data_Set_Language>" + Data_Set_Language + "</Data_Set_Language>");
            fileData.Add("\t<Originating_Center>" + Originating_Center + "</Originating_Center>");

            fileData.Add("\t<Data_Center>");
            fileData.Add("\t\t<Data_Center_Name>");
            fileData.Add("\t\t\t<Short_Name>" + Data_Center.Data_Center_Name.Short_Name + "</Short_Name>");
            fileData.Add("\t\t\t<Long_Name>" + Data_Center.Data_Center_Name.Long_Name + "</Long_Name>");
            fileData.Add("\t\t</Data_Center_Name>");
            fileData.Add("\t\t<Data_Center_URL>" + Data_Center.Data_Center_URL + "</Data_Center_URL>");
            fileData.Add("\t\t<Data_Set_ID>" + Data_Center.Data_Set_ID + "</Data_Set_ID>");

            fileData.Add("\t\t<Personnel>");
            fileData.Add("\t\t\t<Role>" + Data_Center.Personnal.Role + "</Role>");
            fileData.Add("\t\t\t<First_Name>" + Data_Center.Personnal.First_Name + "</First_Name>");
            fileData.Add("\t\t\t<Middle_Name>" + Data_Center.Personnal.Middle_Name + "</Middle_Name>");
            fileData.Add("\t\t\t<Last_Name>" + Data_Center.Personnal.Last_Name + "</Last_Name>");
            fileData.Add("\t\t\t<Email>" + Data_Center.Personnal.Email + "</Email>");
            fileData.Add("\t\t\t<Phone>" + Data_Center.Personnal.Phone + "</Phone>");
            fileData.Add("\t\t\t<Fax>" + Data_Center.Personnal.Fax + "</Fax>");
            fileData.Add("\t\t\t<Contact_Address>");
            fileData.Add("\t\t\t\t<Address>" + Data_Center.Personnal.Contact_Address.Address + "</Address>");
            fileData.Add("\t\t\t\t<City>" + Data_Center.Personnal.Contact_Address.City + "</City>");
            fileData.Add("\t\t\t\t<Province_or_State>" + Data_Center.Personnal.Contact_Address.Province_or_State + "</Province_or_State>");
            fileData.Add("\t\t\t\t<Postal_Code>" + Data_Center.Personnal.Contact_Address.Postal_Code + "</Postal_Code>");
            fileData.Add("\t\t\t\t<Country>" + Data_Center.Personnal.Contact_Address.Country + "</Country>");
            fileData.Add("\t\t\t</Contact_Address>");
            fileData.Add("\t\t</Personnel>");
            fileData.Add("\t</Data_Center>");

            fileData.Add("\t<Distribution>");
            fileData.Add("\t\t<Distribution_Media>" + Distribution.Distribution_Media + "</Distribution_Media>");
            fileData.Add("\t\t<Distribution_Size>" + Distribution.Distribution_Size + "</Distribution_Size>");
            fileData.Add("\t\t<Distribution_Format>" + Distribution.Distribution_Format + "</Distribution_Format>");
            fileData.Add("\t\t<Fees>" + Distribution.Fees + "</Fees>");
            fileData.Add("\t</Distribution>");

            fileData.Add("\t<Multimedia_Sample>");
            fileData.Add("\t\t<File>" + Multimedia_Sample.File + "</File>");
            fileData.Add("\t\t<URL>" + Multimedia_Sample.URL + "</URL>");
            fileData.Add("\t\t<Format>" + Multimedia_Sample.Format + "</Format>");
            fileData.Add("\t\t<Caption>" + Multimedia_Sample.Caption + "</Caption>");
            fileData.Add("\t\t<Description>" + Multimedia_Sample.Description + "</Description>");
            fileData.Add("\t</Multimedia_Sample>");

            fileData.Add("\t<Reference>");
            fileData.Add("\t\t<Author>" + Reference.Author + "</Author>");
            fileData.Add("\t\t<Publication_Date>" + Reference.Publication_Date + "</Publication_Date>");
            fileData.Add("\t\t<Title>" + Reference.Title + "</Title>");
            fileData.Add("\t\t<Series>" + Reference.Series + "</Series>");
            fileData.Add("\t\t<Edition>" + Reference.Edition + "</Edition>");
            fileData.Add("\t\t<Volume>" + Reference.Volume + "</Volume>");
            fileData.Add("\t\t<Issue>" + Reference.Issue + "</Issue>");
            fileData.Add("\t\t<Report_Number>" + Reference.Report_Number + "</Report_Number>");
            fileData.Add("\t\t<Publication_Place>" + Reference.Publication_Place + "</Publication_Place>");
            fileData.Add("\t\t<Publisher>" + Reference.Publisher + "</Publisher>");
            fileData.Add("\t\t<Pages>" + Reference.Pages + "</Pages>");
            fileData.Add("\t\t<ISBN>" + Reference.ISBN + "</ISBN>");
            fileData.Add("\t\t<DOI>" + Reference.DOI + "</DOI>");
            fileData.Add("\t\t<Online_Resource>" + Reference.Online_Resource + "</Online_Resource>");
            fileData.Add("\t\t<Other_Reference_Details>" + Reference.Other_Reference_Details + "</Other_Reference_Details>");
            fileData.Add("\t</Reference>");

            fileData.Add("\t<Summary>");
            fileData.Add("\t\t<Abstract>" + Summary.Abstract + "</Abstract>");
            fileData.Add("\t\t<Purpose>" + Summary.Purpose + "</Purpose>");
            fileData.Add("\t</Summary>");

            fileData.Add("\t<Related_URL>");
            fileData.Add("\t\t<URL_Content_Type>");
            fileData.Add("\t\t\t<Type>" + Related_URL.URL_Content_Type.Type + "</Type>");
            fileData.Add("\t\t\t<Subtype>" + Related_URL.URL_Content_Type.Subtype + "</Subtype>");
            fileData.Add("\t\t</URL_Content_Type>");
            fileData.Add("\t\t<URL>" + Related_URL.URL + "</URL>");
            fileData.Add("\t\t<Description>" + Related_URL.Description + "</Description>");
            fileData.Add("\t</Related_URL>");

            fileData.Add("\t<Parent_DIF>" + Parent_DIF + "</Parent_DIF>");

            fileData.Add("\t<IDN_Node>");
            fileData.Add("\t\t<Short_Name>" + IDN_Node.Short_Name + "</Short_Name>");
            fileData.Add("\t\t<Long_Name>" + IDN_Node.Long_Name + "</Long_Name>");
            fileData.Add("\t</IDN_Node>");

            fileData.Add("\t<Originating_Metadata_Node>" + Originating_Metadata_Node + "</Originating_Metadata_Node>");
            fileData.Add("\t<Metadata_Name>" + Metadata_Name + "</Metadata_Name>");

            fileData.Add("\t<Extended_Metadata>");
            fileData.Add("\t\t<Group>" + Extended_Metadata.Group + "</Group>");
            fileData.Add("\t\t<Name>" + Extended_Metadata.Name + "</Name>");
            fileData.Add("\t\t<Value>" + Extended_Metadata.Value + "</Value>");
            fileData.Add("\t</Extended_Metadata>");

            fileData.Add("\t<Metadata_Version>" + Metadata_Version + "</Metadata_Version>");
            fileData.Add("\t<DIF_Creation_Date>" + DIF_Creation_Date + "</DIF_Creation_Date>");
            fileData.Add("\t<Last_DIF_Revision_Date>" + Last_DIF_Revision_Date + "</Last_DIF_Revision_Date>");
            fileData.Add("\t<DIF_Revision_History>" + DIF_Revision_History + "</DIF_Revision_History>");
            fileData.Add("\t<Future_DIF_Review_Date>" + Future_DIF_Review_Date + "</Future_DIF_Review_Date>");
            fileData.Add("\t<Private>" + Private + "</Private>");

            //The File end.
            fileData.Add("</DIF>");

            //Creating a writer
            System.IO.TextWriter textWriter = new System.IO.StreamWriter(fileName, false, Encoding.UTF8);

            //Writing all lines.
            for (int i = 0; i < fileData.Count; i++)
            {
                textWriter.WriteLine(fileData[i]);
            }

            //Closing the file.
            textWriter.Close();
        }
    }

    public class Data_Set_Citation
    {
        public string Dataset_Creator;
        public string Dataset_Editor;
        public string Dataset_Title;
        public string Dataset_Series_Name;
        public string Dataset_Release_Date;
        public string Dataset_Release_Place;
        public string Dataset_Publisher;
        public string Version;
        public string Issue_Identification;
        public string Data_Presentation_Form;
        public string Other_Citation_Details;
        public string Dataset_DOI;
        public string Online_Resource;

        public Data_Set_Citation()
        {
            Dataset_Creator = "";
            Dataset_Editor = "";
            Dataset_Title = "";
            Dataset_Series_Name = "";
            Dataset_Release_Date = "";
            Dataset_Release_Place = "";
            Dataset_Publisher = "";
            Version = "";
            Issue_Identification = "";
            Data_Presentation_Form = "";
            Other_Citation_Details = "";
            Dataset_DOI = "";
            Online_Resource = "";
        }

    }

    public class Personnel
    {
        public string Role;
        public string First_Name;
        public string Middle_Name;
        public string Last_Name;
        public string Email;
        public string Phone;
        public string Fax;
        public Contact_Address Contact_Address;

        public Personnel()
        {
            Role = "";
            First_Name = "";
            Middle_Name = "";
            Last_Name = "";
            Email = "";
            Phone = "";
            Fax = "";
            Contact_Address = new Contact_Address();
        }

    }

    public class Contact_Address
    {
        public string Address;
        public string City;
        public string Province_or_State;
        public string Postal_Code;
        public string Country;

        public Contact_Address()
        {
            Address = "";
            City = "";
            Province_or_State = "";
            Postal_Code = "";
            Country = "";
        }

    }

    public class Discipline
    {
        public string Discipline_Name;
        public string Subdiscipline;
        public string Detailed_Subdiscipline;

        public Discipline()
        {
            Discipline_Name = "";
            Subdiscipline = "";
            Detailed_Subdiscipline = "";
        }
    }

    public class Parameters
    {
        public string Category;
        public string Topic;
        public string Term;
        public string Variable_Level_1;
        public string Variable_Level_2;
        public string Variable_Level_3;
        public string Detailed_Variable;

        public Parameters()
        {
            Category = "";
            Topic = "";
            Term = "";
            Variable_Level_1 = "";
            Variable_Level_2 = "";
            Variable_Level_3 = "";
            Detailed_Variable = "";
        }
    }

    public class Sensor_Name
    {
        public string Short_Name;
        public string Long_Name;

        public Sensor_Name()
        {
            Short_Name = "";
            Long_Name = "";
        }
    }

    public class Source_Name
    {
        public string Short_Name;
        public string Long_Name;

        public Source_Name()
        {
            Short_Name = "";
            Long_Name = "";
        }
    }

    public class Temporal_Coverage
    {
        public string Start_Date;
        public string Stop_Date;

        public Temporal_Coverage()
        {
            Start_Date = "";
            Stop_Date = "";
        }
    }

    public class Paleo_Temporal_Coverage
    {
        public string Paleo_Start_Date;
        public string Paleo_Stop_Date;
        public Chronostratigraphic_Unit Chronostratigraphic_Unit;

        public Paleo_Temporal_Coverage()
        {
            Paleo_Start_Date = "";
            Paleo_Stop_Date = "";
            Chronostratigraphic_Unit = new Chronostratigraphic_Unit();
        }
    }

    public class Chronostratigraphic_Unit
    {
        public string Eon;
        public string Era;
        public string Period;
        public string Epoch;
        public string Stage;
        public string Detailed_Classification;

        public Chronostratigraphic_Unit()
        {
            Eon = "";
            Era = "";
            Period = "";
            Epoch = "";
            Stage = "";
            Detailed_Classification = "";
        }
    }

    public class Spatial_Coverage
    {
        public string Southernmost_Latitude;
        public string Northernmost_Latitude;
        public string Westernmost_Longitude;
        public string Easternmost_Longitude;
        public string Minimum_Altitude;
        public string Maximum_Altitude;
        public string Minimum_Depth;
        public string Maximum_Depth;

        public Spatial_Coverage()
        {
            Southernmost_Latitude = "";
            Northernmost_Latitude = "";
            Westernmost_Longitude = "";
            Easternmost_Longitude = "";
            Minimum_Altitude = "";
            Maximum_Altitude = "";
            Minimum_Depth = "";
            Maximum_Depth = "";
        }
    }

    public class Location
    {
        public string Location_Category;
        public string Location_Type;
        public string Location_Subregion1;
        public string Location_Subregion2;
        public string Location_Subregion3;
        public string Detailed_Location;

        public Location()
        {
            Location_Category = "";
            Location_Type = "";
            Location_Subregion1 = "";
            Location_Subregion2 = "";
            Location_Subregion3 = "";
            Detailed_Location = "";
        }
    }

    public class Data_Resolution
    {
        public string Latitude_Resolution;
        public string Longitude_Resolution;
        public string Horizontal_Resolution_Range;
        public string Vertical_Resolution;
        public string Vertical_Resolution_Range;
        public string Temporal_Resolution;
        public string Temporal_Resolution_Range;

        public Data_Resolution()
        {
            Latitude_Resolution = "";
            Longitude_Resolution = "";
            Horizontal_Resolution_Range = "";
            Vertical_Resolution = "";
            Vertical_Resolution_Range = "";
            Temporal_Resolution = "";
            Temporal_Resolution_Range = "";
        }
    }

    public class Project
    {
        public string Short_Name;
        public string Long_Name;

        public Project()
        {
            Short_Name = "";
            Long_Name = "";
        }
    }

    public class Data_Center
    {
        public Data_Center_Name Data_Center_Name;
        public string Data_Center_URL;
        public string Data_Set_ID;
        public Personnel Personnal;

        public Data_Center()
        {
            Data_Center_Name = new Data_Center_Name();
            Data_Center_URL = "";
            Data_Set_ID = "";
            Personnal = new Personnel();
        }
    }

    public class Data_Center_Name
    {
        public string Short_Name;
        public string Long_Name;

        public Data_Center_Name()
        {
            Short_Name = "";
            Long_Name = "";
        }
    }

    public class Distribution
    {
        public string Distribution_Media;
        public string Distribution_Size;
        public string Distribution_Format;
        public string Fees;

        public Distribution()
        {
            Distribution_Media = "";
            Distribution_Size = "";
            Distribution_Format = "";
            Fees = "";
        }
    }

    public class Multimedia_Sample
    {
        public string File;
        public string URL;
        public string Format;
        public string Caption;
        public string Description;

        public Multimedia_Sample()
        {
            File = "";
            URL = "";
            Format = "";
            Caption = "";
            Description = "";
        }
    }

    public class Reference
    {
        public string Author;
        public string Publication_Date;
        public string Title;
        public string Series;
        public string Edition;
        public string Volume;
        public string Issue;
        public string Report_Number;
        public string Publication_Place;
        public string Publisher;
        public string Pages;
        public string ISBN;
        public string DOI;
        public string Online_Resource;
        public string Other_Reference_Details;

        public Reference()
        {
            Author = "";
            Publication_Date = "";
            Title = "";
            Series = "";
            Edition = "";
            Volume = "";
            Issue = "";
            Report_Number = "";
            Publication_Place = "";
            Publisher = "";
            Pages = "";
            ISBN = "";
            DOI = "";
            Online_Resource = "";
            Other_Reference_Details = "";
        }
    }

    public class Summary
    {
        public string Abstract;
        public string Purpose;

        public Summary()
        {
            Abstract = "";
            Purpose = "";
        }
    }

    public class Related_URL
    {
        public URL_Content_Type URL_Content_Type;
        public string URL;
        public string Description;

        public Related_URL()
        {
            URL_Content_Type = new URL_Content_Type();
            URL = "";
            Description = "";
        }
    }

    public class URL_Content_Type
    {
        public string Type;
        public string Subtype;

        public URL_Content_Type()
        {
            Type = "";
            Subtype = "";
        }
    }

    public class IDN_Node
    {
        public string Short_Name;
        public string Long_Name;

        public IDN_Node()
        {
            Short_Name = "";
            Long_Name = "";
        }
    }

    public class Extended_Metadata
    {
        public string Group;
        public string Name;
        public string Value;

        public Extended_Metadata()
        {
            Group = "";
            Name = "";
            Value = "";
        }
    }
}
