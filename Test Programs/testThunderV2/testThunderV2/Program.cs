﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testThunderV2
{
    class Program
    {
        public static thunderComm2.equipmentThunder equipmentThunder;
        public static thunderComm2.equipmentThunderV2 eqThunder;

        public static bool test = false;

        static void Main(string[] args)
        {
            /*
            equipmentThunder = new thunderComm2.equipmentThunder("COM7");
            equipmentThunder.open();
            equipmentThunder.startInternalUpdate();

            equipmentThunder.statusReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusReady_PropertyChange);
            equipmentThunder.setPointReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(setPointReady_PropertyChange);
            */

            eqThunder = new thunderComm2.equipmentThunderV2("192.168.1.224", 8004);
            //eqThunder.dataPacketRecived.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(dataPacketRecived_PropertyChange);
            eqThunder.readySetPoint.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readySetPoint_PropertyChange);
            eqThunder.readyStatus.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyStatus_PropertyChange);

            string input = "";
            System.Threading.Thread loopTest;

            while (input != "exit")
            {
                input = Console.ReadLine();

                switch (input)
                {
                    case "test":
                        //loopTest = new System.Threading.Thread(testTread);
                        //loopTest.Name = "loop Test";
                        //test = true;
                        //loopTest.Start();
                        eqThunder.startCollect();
                        break;

                    case "sc":
                        input = Console.ReadLine();
                        eqThunder.sendCommand(input);
                        break;

                    case "stop":
                        test = false;
                        break;
                }
            }

        }

        static void readyStatus_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            thunderComm2.statusData incomingData = (thunderComm2.statusData)data.NewValue;
            Console.WriteLine(incomingData.timeDataReceived.ToString("HH:mm:ss.ffff") + " |\tATC: " + incomingData.currentTestC.ToString("0.0000"));
        }

        static void readySetPoint_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            thunderComm2.setPointData incomingData = (thunderComm2.setPointData)data.NewValue;
            Console.WriteLine(incomingData.timeDataReceived.ToString("HH:mm:ss.ffff") + " |\tSPC: " + incomingData.setPointTestC.ToString("0.0000"));
        }

        static void testTread()
        {
            while (test)
            {
                eqThunder.sendCommand("?");
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " | Status Sent Command");
                System.Threading.Thread.Sleep(1500);
                eqThunder.sendCommand("?SP");
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " | Set Point Sent Command");
                System.Threading.Thread.Sleep(1500);
            }
        }

        static void dataPacketRecived_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            thunderComm2.dataPacket incoming = (thunderComm2.dataPacket)data.NewValue;
            Console.WriteLine(incoming.packetDate.ToString("HH:mm:ss.ffff") + " | " + incoming.packetMessage);
        }

        /*
        static void setPointReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " SPC: " + equipmentThunder.setPointData.setPointTestC.ToString("0.000"));
        }

        static void statusReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " ATC: " + equipmentThunder.statsData.currentTestC.ToString("0.000"));
        }
        */

    }
}
