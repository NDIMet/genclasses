﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestNCDRelayController
{
    class Program
    {
        static equipmentNCD.equipmentNCDRelay NCDRealy;

        static void Main(string[] args)
        {
            NCDRealy = new equipmentNCD.equipmentNCDRelay("192.168.1.226", 2101);
            Console.WriteLine("Connected");
            NCDRealy.errorNCD.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(errorNCD_PropertyChange);

            Console.Read();     //A little pause.

            //byte[] command = new byte[] { 0xFE, (byte)49, (byte)3 };
            //NCDRealy.addCommand(command);
            NCDRealy.addRelayBank(1, "Bank 1");
            NCDRealy.addRelayBank(2, "Bank 2");
            NCDRealy.addRelayBank(3, "Bank 3");
            NCDRealy.addRelayBank(4, "Bank 4");
            
            Console.WriteLine("Bank Set");
            Console.Read();

            //NCDRealy.startBankStatus(1);
            //NCDRealy.startBankStatus(2);
            //NCDRealy.startBankStatus(3);
            //NCDRealy.startBankStatus(4);

            //int relay = 1;
            //bool state = false;

            NCDRealy.openAllRelays();
            
            while (true)
            {
                
                Console.Write("Command Relay>");
                string input = Console.ReadLine();

                if(input.Contains("all"))
                {
                    NCDRealy.openAllRelays();
                }
                else
                {
                    int relay = Convert.ToInt16(input);
                    Console.Write("True/False>");
                    string stateIn = Console.ReadLine();
                    bool state = false;

                    if (stateIn.Contains("t"))
                    {
                        state = true;
                    }

                    NCDRealy.commandRelay(relay, state);
                }
                
                
                
                

                /*
                relay++;

                if (relay == 33)
                {
                    relay = 0;

                    if (state)
                    {
                        state = false;
                    }
                    else
                    {
                        state = true;
                    }
                }

                System.Threading.Thread.Sleep(100);
                */
            }

            NCDRealy.openAllRelays();

            Console.Read();
        }

        static void errorNCD_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incoming = (Exception)data.NewValue;
            Console.WriteLine(incoming.Message);
        }
    }
}
