﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using updateCreater;


namespace TcpSimpleClient
{

    public class simpleTcpClient
    {
        //
        //Constructers
        //
        public simpleTcpClient()
        {
        }


        public enum clientType { TCP, UDP }; //Type of connection the client is making.
        public const int ANYPORT = 0;
        public const int SAMPLETCPPORT = 4567;
        public const int SAMPLEUDPPORT = 4568;
        public bool readData = false;
        public clientType cliType;
        public bool DONE = false;
        public TcpClient tcpClient;
        public NetworkStream tcpStream;
        public int recBuffer = 512;

        //Update objects
        public objectUpdate recivedMessage = new objectUpdate();
        public updateCreater.objectUpdate error = new objectUpdate();

        public void simpleClientSetupTCP(string ipAddress, int port)
        {
            //try
            //{

            //Create an instance of TcpClient.
            tcpClient = new TcpClient();
            //tcpClient.SendTimeout = 10;
            //tcpClient.ReceiveTimeout = 10;
            tcpClient.Connect(ipAddress, port);

            //Setting timeout
            

            //Create a NetworkStream for this tcpClient instance.
            //This is only required for TCP stream.
            tcpStream = tcpClient.GetStream();
            Thread clientThread = new Thread(new System.Threading.ThreadStart(clientReadBytes));
            clientThread.Name = "TCP Client";
            clientThread.IsBackground = true;
            clientThread.Start();
            //    return "ok";
            //}
            //catch (Exception e)
            //{
            //    return e.Message;
            //}

        }

        /// <summary>
        /// Method send out the string in byte format.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string clientWriteMessageTCP(string message)
        {
            try
            {
                if (tcpStream.CanWrite)
                {
                    Byte[] inputToBeSent = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());
                    tcpStream.Write(inputToBeSent, 0, inputToBeSent.Length);
                    tcpStream.Flush();
                    return "ok";
                }
                return "error";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Method for sending out a byte message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string clientWriteByteTCP(byte[] message)
        {
            try
            {
                tcpStream.Write(message, 0, message.Length);
                tcpStream.Flush();
                return "ok";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string clientWriteMessageTCP(byte[] message)
        {
            try
            {
                if (tcpStream.CanWrite)
                {
                    tcpStream.Write(message, 0, message.Length);
                    tcpStream.Flush();
                    return "ok";
                }
                return "error";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string clientReadMessageTCP()
        {
            String dataReceived = null;

            while (true)
            {
                try
                {
                    while (tcpStream.CanRead && !DONE)
                    {
                        //We need the DONE condition here because there is possibility that
                        //the stream is ready to be read while there is nothing to be read.
                        if (tcpStream.DataAvailable)
                        {
                            Byte[] received = new Byte[512];
                            int nBytesReceived = tcpStream.Read(received, 0, received.Length);
                            dataReceived = System.Text.Encoding.ASCII.GetString(received);
                            DONE = true;
                            return dataReceived;
                        }
                    }
                    return dataReceived;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }

        public void clientReadBytes()
        {
            while (tcpClient.Client.Connected)
            {

                if (tcpStream.DataAvailable)
                {
                    Byte[] received = new Byte[recBuffer];
                    int nBytesReceived = tcpStream.Read(received, 0, received.Length);
                    Byte[] Output = new Byte[nBytesReceived];

                    for (int i = 0; i < Output.Length; i++)
                    {

                        Output[i] = received[i];

                    }

                    recivedMessage.UpdatingObject = (object)Output;
                }
                else
                {
                    //Taking a small brake to make sure the thread doesn't just spin out of control.
                    if (tcpClient.Client.Connected)
                    {
                        System.Threading.Thread.Sleep(50);
                    }


                }
            }
        }

        public void sampleTcpClient2(String serverName, String whatEver)
        {
            try
            {
                //Create an instance of TcpClient.
                tcpClient = new TcpClient(serverName, SAMPLETCPPORT);
                //Create a NetworkStream for this tcpClient instance.
                //This is only required for TCP stream.
                tcpStream = tcpClient.GetStream();
                if (tcpStream.CanWrite)
                {
                    Byte[] inputToBeSent = System.Text.Encoding.ASCII.GetBytes(whatEver.ToCharArray());
                    tcpStream.Write(inputToBeSent, 0, inputToBeSent.Length);
                    tcpStream.Flush();
                }
                while (tcpStream.CanRead && !DONE)
                {
                    //We need the DONE condition here because there is possibility that
                    //the stream is ready to be read while there is nothing to be read.
                    if (tcpStream.DataAvailable)
                    {
                        Byte[] received = new Byte[512];
                        int nBytesReceived = tcpStream.Read(received, 0, received.Length);
                        String dataReceived = System.Text.Encoding.ASCII.GetString(received);
                        Console.WriteLine(dataReceived);
                        DONE = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An Exception has occurred.");
                Console.WriteLine(e.ToString());
            }
        }

        public void close()
        {
            tcpClient.Client.Close();
            tcpClient.Close();
        }

    }
}
