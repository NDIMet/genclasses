﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testTCPIPParoSensor
{
    class Program
    {
        static equipmentTCPIP.equipmentParoTCPIP sensorParo;

        static void Main(string[] args)
        {
            /*
            client = new equipmentTCPIP.tcpClient("63.131.91.82", 9008);
            client.tcpData.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tcpData_PropertyChange);
            client.tcpError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tcpError_PropertyChange);
            */

            sensorParo = new equipmentTCPIP.equipmentParoTCPIP("63.131.91.82", 9008);
            sensorParo.updatedPressureValue.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatedPressureValue_PropertyChange);

            while (true)
            {
                string command = Console.ReadLine();

                switch (command)
                {
                    case "gp":
                        break;

                    case "start":
                        sensorParo.startCollect();
                        break;

                    case "set":
                        break;

                    case "close":
                        break;
                }
            }

        }

        static void updatedPressureValue_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            equipmentTCPIP.dataPacket incoming = (equipmentTCPIP.dataPacket)data.NewValue;

            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " - " + sensorParo.currentPressure.ToString());
        }

        
    }

    
}
