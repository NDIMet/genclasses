﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestKMLExport
{
    class Program
    {
        static void Main(string[] args)
        {
            
            DataExport.data_KML export = new DataExport.data_KML();

            /* Full testing kml

            string file = @"C:\Users\wjones\Documents\loop3.csv";

            //Temp
            List<DataExport.rawData> airTemp = export.loadRawFile(file, new int[] { 6, 7, 8 }, 2, new double[] { 10000000, 10000000, 1000, 100 }, 9, 3);
            //List<DataExport.rawData> airTemp = export.loadRawFile(@"C:\Users\wjones\Documents\export.csv", new int[] { 8, 9, 10 }, 4, new double[] { 10000000, 10000000, 1000, 100 }, 11, 3);

            //Humidity
            List<DataExport.rawData> humidity = export.loadRawFile(file, new int[] { 6, 7, 8 }, 3, new double[] { 10000000, 10000000, 1000, 10 }, 9, 3);
            //List<DataExport.rawData> humidity = export.loadRawFile(@"C:\Users\wjones\Documents\export.csv", new int[] { 8, 9, 10 }, 5, new double[] { 10000000, 10000000, 1000, 10 }, 11, 3);

            //Alt
            List<DataExport.rawData> alt = export.loadRawFile(file, new int[] { 6, 7, 8 }, 8, new double[] { 10000000, 10000000, 1000, 1000 }, 9, 3);

            //export.saveKMLValue(airTemp, System.Drawing.Color.Red, "airTemp");
            //export.saveKMLValue(humidity, System.Drawing.Color.Blue, "humidity");
            //export.saveKMLAlt(airTemp, System.Drawing.Color.Yellow, "alt");

            //export.saveKMLValue(airTemp, System.Drawing.Color.Yellow, "Scale");

            //airTemp = export.filterData(airTemp);
            //humidity = export.filterData(humidity);
            //alt = export.filterData(alt);

            
            List<string> dataLines = new List<string>();
            dataLines.AddRange(export.getFileHeader());
            dataLines.AddRange(export.getPlacemarkData(airTemp, System.Drawing.Color.Red, "Air Temperature", "Air temperature from flight.", true));
            dataLines.AddRange(export.getPlacemarkData(humidity, System.Drawing.Color.Blue, "Humidity", "Humidity", true));
            dataLines.AddRange(export.getPlacemarkData(alt, System.Drawing.Color.Yellow, "Altitude", "How height can I go.", false));
            dataLines.AddRange(export.getFileEnd());

            System.IO.File.WriteAllLines(@"C:\Users\wjones\Documents\loop.kml", dataLines.ToArray());


            */


            //Testing range

            double distance = export.DistanceTo(42.8944048, -85.5698896, 42.8943841, -85.5699576);


            double chk = export.DistanceTo(42.33, -71.02, 40.66, -73.94, 'K');

            Console.ReadLine();

        }
    }
}
