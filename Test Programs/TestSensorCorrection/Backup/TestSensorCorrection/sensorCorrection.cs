﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sensorCorrection
{
    #region HMP234Blk
    class sensorCorHMP234Blk
    {
        public sensorCorHMP234Blk()
        {
            //Latest coef as of 12/21/2012
            //y = 3E-05x3 - 0.0032x2 + 1.1288x + 1.0717
            coefficient[0] = 1.0717;
            coefficient[1] = 1.1288;
            coefficient[2] = -0.0032;
            coefficient[3] = 3E-05;

            //Old Coefs obsolete as of 2013/01/07. Only use for trace.
            /*
            coefficient[0] = 0.4892;
            coefficient[1] = 1.0312;
            coefficient[2] = 0.0002;
            coefficient[3] = 0;
            */
        }

        //class veriables
        //Coefficient for the 2nd degree correction fix
        //y = 0.0003x2 + 1.0232x + 0.6559
        //=0.0002*'Filtered Data'!E3^2+1.0312*'Filtered Data'!E3+0.4892 This is the new one from excel
        
        private double[] coefficient = new double[4];

        public double[] getCoefficient()
        {
            return coefficient;
        }

        public void setCoefficient(double[] desiredCoefficient)
        {
            coefficient = desiredCoefficient;
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            //the calc output of the correction.
            double correctedHumidity = 0;

            //Latest coef as of 12/21/2012
            //y = 3E-05x3 - 0.0032x2 + 1.1288x + 1.0717
            correctedHumidity = (coefficient[3] * Math.Pow(rawHumidity, 3)) + (coefficient[2] * Math.Pow(rawHumidity, 2)) + (coefficient[1] * rawHumidity) + coefficient[0];

            //Old coefs.
            //replaced on
            //correctedHumidity = coefficient[2] * Math.Pow(rawHumidity, 2) + coefficient[1] * rawHumidity + coefficient[0];

            return correctedHumidity;
        }

    }
    #endregion

    #region HMP234SS
    class sensorCorHMP234SS
    {
        //This sensor was destroyed.... 
        public sensorCorHMP234SS()
        {
            coefficient[0] = 1.416;
            coefficient[1] = 1.0233;
            coefficient[2] = 0.0005;
        }

        //class veriables
        //Coefficient for the 2nd degree correction fix
        //y = 0.0005x2 + 1.0233x + 1.416
        private double[] coefficient = new double[3];

 

        public double[] getCoefficient()
        {
            return coefficient;
        }

        public void setCoefficient(double[] desiredCoefficient)
        {
            coefficient = desiredCoefficient;
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            //the calc output of the correction.
            double correctedHumidity = 0;

            correctedHumidity = coefficient[2] * Math.Pow(rawHumidity, 2) + coefficient[1] * rawHumidity + coefficient[0];

            return correctedHumidity;
        }
    }
    #endregion

    #region EE31
    class sensorCorEE31
    {
        public sensorCorEE31()
        {
            //Latest coef as of 12/21/2012
            //y = 3E-05x3 - 0.0025x2 + 1.0587x - 0.1875

            coefficient[0] = -0.1875;
            coefficient[1] = 1.0587;
            coefficient[2] = -0.0025;
            coefficient[3] = 3E-05;


            //Old coef's
            //coefficient[0] = 0.1451;
            //coefficient[1] = 0.9693;
            //coefficient[2] = 0.0008;
        }

        //class veriables

        private double[] coefficient = new double[4];

        public double[] getCoefficient()
        {
            return coefficient;
        }

        public void setCoefficient(double[] desiredCoefficient)
        {
            coefficient = desiredCoefficient;
        }

        public double getCorrectedHumidity(double rawHumidity)
        {
            //the calc output of the correction.
            double correctedHumidity = 0;

            //y = 3E-05x3 - 0.0025x2 + 1.0587x - 0.1875
            correctedHumidity = (coefficient[3] * Math.Pow(rawHumidity, 3)) + (coefficient[2] * Math.Pow(rawHumidity, 2)) + (coefficient[1] * rawHumidity) + coefficient[0];
            //Old calc
            //correctedHumidity = coefficient[2] * Math.Pow(rawHumidity, 2) + coefficient[1] * rawHumidity + coefficient[0];

            return correctedHumidity;
        }
    }
    #endregion
}
