﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using sensorCorrection;
using TestEquipment;

namespace TestSensorCorrection
{
    class Program
    {
        public static sensorCorHMP234Blk testHMP234BlkCor = new sensorCorHMP234Blk();
        public static sensorCorHMP234SS testHMP234SSCor = new sensorCorHMP234SS();
        public static sensorCorEE31 testEE31 = new sensorCorEE31();

        public static System.ComponentModel.BackgroundWorker backgroundWorkerRealTimeCompare = new System.ComponentModel.BackgroundWorker();

        
        public static HMP234 sensorHMP234Blk = new HMP234("COM12");
        public static HMP234 sensorHMP234SS = new HMP234("COM16");
        public static GEHygro sensorGEHydro = new GEHygro("COM17");
        public static epluseEE31 sensorEE31 = new epluseEE31("COM15");
        

        static void Main(string[] args)
        {
            string userSelect = "";
            string userInput = "";

            Console.WriteLine("HMP234 Blk correction");

            while (true)
            {
                Console.WriteLine("Select a sensor.");
                Console.WriteLine("1 - HMP234 Blk");
                Console.WriteLine("2 - HMP234 SS");
                Console.WriteLine("3 - EE31");
                Console.WriteLine("4 - Real time compare");
                Console.Write("Select>");
                userSelect = Console.ReadLine();
                Console.Write("Desired input to correct>");
                userInput = Console.ReadLine();

                processCommand(userSelect, userInput);
                Console.WriteLine("");
            }
        }

        static void processCommand(string userSelect, string userInput)
        {
            switch (userSelect)
            {
                case "1":
                    Console.WriteLine(testHMP234BlkCor.getCorrectedHumidity(Convert.ToDouble(userInput)));
                    break;

                case "2":
                    Console.WriteLine(testHMP234SSCor.getCorrectedHumidity(Convert.ToDouble(userInput)));
                    break;

                case "3":
                    Console.WriteLine(testEE31.getCorrectedHumidity(Convert.ToDouble(userInput)));
                    break;

                case "4":
                    sensorGEHydro.ComPort.Open();
                    sensorHMP234Blk.ComPort.Open();
                    sensorHMP234SS.ComPort.Open();
                    sensorEE31.ComPort.Open();

                    backgroundWorkerRealTimeCompare.WorkerSupportsCancellation = true;
                    backgroundWorkerRealTimeCompare.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerRealTimeCompare_DoWork);
                    backgroundWorkerRealTimeCompare.RunWorkerAsync(userInput);

                    break;
            }
        }

        static void backgroundWorkerRealTimeCompare_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Log File
            string logFile ="c:\\SensorCompare - " + e.Argument.ToString() + ".csv";

            sensorHMP234Blk.startHMP234Sensor();
            sensorHMP234SS.startHMP234Sensor();

            sensorEE31.ComPort.DiscardInBuffer();
            sensorGEHydro.ComPort.DiscardInBuffer();
            sensorHMP234Blk.ComPort.DiscardInBuffer();
            sensorHMP234SS.ComPort.DiscardInBuffer();

            System.Threading.Thread.Sleep(4000);

            int couterLoop = 0;

            while (true)
            {
                string currentEE31AT = "";
                sensorEE31.getAirTemp(ref currentEE31AT);
                sensorEE31.getHumidity(ref currentEE31AT);

                sensorGEHydro.getGEHygroData();

                sensorHMP234Blk.getHMP234Data();
                sensorHMP234SS.getHMP234Data();

                Console.WriteLine("GE>" + sensorGEHydro.CurrentHumidity.ToString("0.000") + " | " +
                                   "HMPBlk>" + sensorHMP234Blk.CurrentHumidity.ToString("0.000") + " : " + testHMP234BlkCor.getCorrectedHumidity(sensorHMP234Blk.CurrentHumidity).ToString("0.000") + " " +
                                   "HMPSS>" + sensorHMP234SS.CurrentHumidity.ToString("0.000") + " : " + testHMP234SSCor.getCorrectedHumidity(sensorHMP234SS.CurrentHumidity).ToString("0.000") + " " +
                                   "EE31>" + sensorEE31.currentHumidity.ToString("0.000") + " : " + testEE31.getCorrectedHumidity(sensorEE31.currentHumidity).ToString("0.000"));

                //System.IO.TextWriter logData = 

                couterLoop++;

                if (couterLoop > 30)
                {
                    sensorEE31.ComPort.DiscardInBuffer();
                    sensorGEHydro.ComPort.DiscardInBuffer();
                    sensorHMP234Blk.ComPort.DiscardInBuffer();
                    sensorHMP234SS.ComPort.DiscardInBuffer();
                    couterLoop = 0;
                }


                System.Threading.Thread.Sleep(4000);

            }
        }


    }
}
