﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace TestEquipment
{
    #region Agilent Multiplexer

    public class Agilent34970A
    {
        //Constructors
        public Agilent34970A()
        {
        }

        public Agilent34970A(string comPort)
        {
            try
            {
                this.muxSerialPort = new SerialPort(comPort, 57600, Parity.None, 8, StopBits.One);
            }
            catch (System.IO.IOException)
            {
                return;
            }
        }

        //Global Variables
        public SerialPort muxSerialPort;

        public int numberOfInputCards;

        public string Card1;
        public string Card2;
        public string Card3;

        //Private Variables


        //Methods
        public string Init()
        {
            try
            {
                muxSerialPort.WriteLine("ROUT:SCAN (@201:240,@301:340)");
                return "Success!";
            }
            catch
            {
                return "Could not initialize mux!";
            }
        }

        /*public double[] Scan()
        {
            muxSerialPort.DiscardInBuffer();
            muxSerialPort.WriteLine("MEASURE:RESISTANCE");
            Thread.Sleep(100);
            
        }*/
    }//end HPMux Class

    #endregion

    #region Paroscientific Pressure Sensor

    public class Paroscientific
    {
        //Constructors
        public Paroscientific()
        { }

        public Paroscientific(string comPort)
        {
            SerialPort paroPort = new SerialPort(comPort, 9600, Parity.None, 8, StopBits.One);
            paroPort.ReceivedBytesThreshold = 8;
            paroPort.NewLine = "\r\n";
            ComPort = paroPort;
        }

        //Fields
        public SerialPort ComPort;

        //Methods

        //Initiates the pressure sensor to continuously send data when ready
        //The sensor returns "OK" or "ERROR"
        public bool Initiate()
        {
            ComPort.ReadTimeout = 3000;
            ComPort.WriteLine("INIT1:CONT 1");
            string response = ComPort.ReadLine();
            if (response.Equals("OK"))
            {
                return true;
            }
            return false;
        }

        //Gets the pressure from the pressure sensor
        public void Fetch()
        {
            ComPort.WriteLine("FETC1?");
            Thread.Sleep(50);
        }

        public void Read() 
        {
            ComPort.ReadTimeout = 2000;
            ComPort.WriteLine("READ1?");
            Thread.Sleep(50);
        }

        //Checks the COM port for a Paro
        public bool detectSensorOnPort(string ComPortName) 
        {
            try
            {
                SerialPort testPort = new SerialPort(ComPortName, 9600, Parity.None, 8, StopBits.One);
                testPort.NewLine = "\r\n";
                testPort.ReadTimeout = 50;

                testPort.Open();

                testPort.WriteLine("*IDN?");
                Thread.Sleep(200);

                string response = testPort.ReadLine();

                if (response.Contains("Paroscientific Inc, Model 785 15A"))
                {
                    testPort.Close();
                    return true;
                }

                else
                {
                    testPort.Close();
                    return false;
                }
            }
            catch 
            {
                return false;
            }
        }
    }
    #endregion

    #region Thermotron Temperature Chamber

    public class Thermotron 
    {
        //Constructors
        public Thermotron() { }

        public Thermotron(string thermotronPort)
        {
            SerialPort comPort = new SerialPort(thermotronPort, 19200, Parity.None, 8, StopBits.One);
            comPort.NewLine = "\r\n";
            comPort.ReadTimeout = 2000;
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;

        public bool LightON;

        //Chamber status.
        public string CurrenttimeTherChamber;
        public double CurrentChamberSPC;
        public double CurrentChamberC;
        public double CurrentChamberSPRH;
        public double CurrentChamberRH;
        public double CurrentChamberPC;
        public double CurrentChamberTempTHTL;
        public double CurrentChamberRHTHTL;
        public int CurrentChamberMode;
        public int CurrentChamberDoor;
        public int CurrentChamberLight;

        //Methods
        public void goToTemp(int degreesCelsius, int stability)
        {
            //ComPort.WriteLine("
        }

        public void Light(bool light) 
        {
            if (light)
            {
                ComPort.WriteLine("LGHT1");
                Thread.Sleep(100);
                if (ComPort.ReadLine().Equals("4")) 
                {
                    LightON = true;
                    return;
                }
            }
            else
            {
                ComPort.WriteLine("LGHT0");
                Thread.Sleep(100);
                Thread.Sleep(100);
                if (ComPort.ReadLine().Equals("4")) 
                {
                    LightON = false;
                    return;
                }
            }
        }

        public string[] getChamberStatus()
        {
            string[] response = new string[9];
            //Asking the chamber for the set point and current status of the temp sensor, humidity sensor, and product temp sensor.
            ComPort.WriteLine("MODE?;SETP1?;PVAR1?;SETP2?;PVAR2?;THTL1?;THTL2?;DOOR?;LGHT?");

            for (int i = 0; i < response.Length; i++)
            {
                response[i] = ComPort.ReadLine();
            }

            this.CurrentChamberMode = int.Parse(response[0]);
            this.CurrentChamberSPC = double.Parse(response[1]);
            this.CurrentChamberC = double.Parse(response[2]);
            this.CurrentChamberSPRH = double.Parse(response[3]);
            this.CurrentChamberRH = double.Parse(response[4]);
            this.CurrentChamberTempTHTL = double.Parse(response[5]);
            this.CurrentChamberRHTHTL = double.Parse(response[6]);
            this.CurrentChamberDoor = int.Parse(response[7]);
            this.CurrentChamberLight = int.Parse(response[8]);
            return response;          
        }

        public int getOptions() 
        {
            ComPort.WriteLine("OPTN?");
            return int.Parse(ComPort.ReadLine());
        }

        public void setOptions(int codedInteger) 
        {
            ComPort.WriteLine("OPTN" + codedInteger.ToString());
            ComPort.ReadLine();
        }

        /*public void getChamberStatus() //This gets the current set points and reports the conditions from the chamber.
        {
            Thread.Sleep(200); //waiting for all the data to enter buffer.

            string timeTherChamber = DateTime.Now.ToString("HH:mm:ss.fffffff"); //Time the data is recived.
            string serialData = ComPort.ReadExisting();
            serialData = serialData.Replace("\n", "");
            string[] dataElements = serialData.Split('\r');
            int amoutofData = dataElements.Count();

            if (amoutofData < 10)
            {
                return;
            }

            if (amoutofData > 11)
            {
                return;
            }

            CurrenttimeTherChamber = timeTherChamber;
            CurrentChamberSPC = Convert.ToDouble(dataElements[0]);
            CurrentChamberC = Convert.ToDouble(dataElements[1]);
            CurrentChamberSPRH = Convert.ToDouble(dataElements[2]);
            CurrentChamberRH = Convert.ToDouble(dataElements[3]);
            CurrentChamberPC = Convert.ToDouble(dataElements[4]);
            CurrentChamberTempTHTL = Convert.ToDouble(dataElements[5]);
            CurrentChamberRHTHTL = Convert.ToDouble(dataElements[6]);
            CurrentChamberMode = Convert.ToInt16(dataElements[7]); // 0 = Program Mode, 1,3 = Stopped, 4 = Manual Mode.
            CurrentChamberDoor = Convert.ToInt16(dataElements[8]); // 0 = Door Open, 1 = Door Closed.
            CurrentChamberLight = Convert.ToInt16(dataElements[9]); // 0 = light off, 1 = light on

        }*/
        public void setChamberTemp(double degreesCelsius) //Set Temp set point
        {
            ComPort.WriteLine("SETP1," + degreesCelsius.ToString());
            Thread.Sleep(50);
        }

        public void setChamberHumidity(double percentRH) //Set RH set point
        {
            ComPort.WriteLine("SETP2," + percentRH.ToString());
            Thread.Sleep(50);
        }

        public void startChamberManualMode() //starts the chamber in manual mode.
        {
            ComPort.WriteLine("RUNM");
            Thread.Sleep(50);
        }

        public void stopChamberManualMode() //Stops the chamber manual mode.
        {
            ComPort.WriteLine("STOP");
            ComPort.ReadLine();
        }
    }
    #endregion

    #region Watlow Temperature Chambers
    /// <summary>
    /// This will making communicating with Watlow controllers easier.
    /// </summary>
    /// 

    public class Watlow
    {
        //Constructors
        public Watlow() { }

        /// <summary>
        /// This method sets up the watlow to 9600,8,n,1 on the provided com port.
        /// </summary>
        /// <param name="watlowPort"></param>
        public Watlow(string watlowPort)
        {
            chamber = new modbus();
            chamber.Open(watlowPort, 9600, 8, Parity.None, StopBits.One);
        }

        //Fields
        public modbus chamber;
        public short currentRawAirTemp;
        public short currentRawSPAirTemp;
        public short currentRawAirTempRamp;

        #region Methods for getting the current status of Air Temp
        /// <summary>
        /// A method for access the current air temp from a watlow controller.
        /// </summary>
        /// <returns></returns>
        public short getAirTemp()
        {
            short[] valuesCurrentTemp = new short[Convert.ToInt32(1)];
            try
            {
                while (!chamber.SendFc3(Convert.ToByte("1"), Convert.ToUInt16("100"), Convert.ToUInt16("1"), ref valuesCurrentTemp)) ;
            }
            catch
            {
            }

            currentRawAirTemp = valuesCurrentTemp[0];
            return valuesCurrentTemp[0];
        }

        /// <summary>
        /// method returns the current air temp set point and retunrs that as a short.
        /// </summary>
        /// <returns></returns>
        public short getAirTempSetPoint()
        {
            short[] valuesCurrentSetPoint = new short[Convert.ToInt32(1)];
            try
            {
                while (!chamber.SendFc3(Convert.ToByte("1"), Convert.ToUInt16("300"), Convert.ToUInt16("1"), ref valuesCurrentSetPoint)) ;
            }
            catch
            {
            }

            currentRawSPAirTemp = valuesCurrentSetPoint[0];
            return valuesCurrentSetPoint[0];
        }

        /// <summary>
        /// Method returns the current power output being used to move/maintain the current air temp.
        /// </summary>
        /// <returns></returns>
        public short getAirTempRamp()
        {
            short[] valuesCurrentPowerOutput = new short[Convert.ToInt32(1)];
            try
            {
                while (!chamber.SendFc3(Convert.ToByte("1"), Convert.ToUInt16("103"), Convert.ToUInt16("1"), ref valuesCurrentPowerOutput)) ;
            }
            catch
            {
            }

            currentRawAirTempRamp = valuesCurrentPowerOutput[0];
            return valuesCurrentPowerOutput[0];
        }
        #endregion

        #region Methods for processing returns from the gets

        public double ProcessCurrentTemp(short currentChamberTemp)
        {
            return(Convert.ToDouble(currentChamberTemp) / 10);
        }

        public double ProcessCurrentSetPoint(short currentChamberSetPoint)
        {
            return(Convert.ToDouble(currentChamberSetPoint) / 10);
        }

        public double ProcessPowerOutput(short currentChamberPowerOutput)
        {
            return(Convert.ToDouble(currentChamberPowerOutput) / 100);
        }

        #endregion
        #region Methods for setting chamber Air Temp

        /// <summary>
        /// Method sets the chamber air temp set point and returns a bool indicating if the command completed or not.
        /// 
        /// </summary>
        /// <param name="newSetPoint"></param>
        /// <returns></returns>
        public bool setAirTemp(double newSetPoint)
        {
            short[] value = new short[1];
            value[0] = Convert.ToInt16(newSetPoint * 10);

            try
            {
                while (!chamber.SendFc16(Convert.ToByte("1"), Convert.ToUInt16("300"), (ushort)1, value)) ;
            }
            catch
            {
                return false;
            }
            return true;
        }

        #endregion

    }

    #endregion

    #region MKS Type 153 Valve

    public class MKS153
    {
        //Constructors
        public MKS153() { }

        public MKS153(string valvePort)
        {
            SerialPort comPort = new SerialPort(valvePort, 9600,Parity.None,8,StopBits.One);
            comPort.NewLine = "\r";
            ComPort = comPort;
            ComPort.Open();
        }

        //Fields
        public SerialPort ComPort;

        //Methods
        public void setValvePosition(int position) 
        {
            if (position > 90 || position < 0) 
            {
                return;
            }

            string valvePosition = position.ToString();

            ComPort.WriteLine("P" + valvePosition + ".0");
        }

        public int getValvePosition() 
        {
            try
            {
                ComPort.WriteLine("R6");
                Thread.Sleep(100);
                string response = ComPort.ReadLine();
                string valvePosition = response.Replace("V",null).Remove(response.IndexOf(".")).Replace(".",null);
                return int.Parse(valvePosition);
            }

            catch
            {
                return -1;
            }
        }

        public void closeValve() 
        {
            ComPort.WriteLine("C");
        }

        public void openValve()
        {
            ComPort.WriteLine("O");
        }

    }
    #endregion 
 
    #region Trimble GPS

    public class Trimble
    {
        //Constructors
        public Trimble() { }

        public Trimble(string TrimblePort)
        {
            SerialPort comPort = new SerialPort(TrimblePort, 9600, Parity.Odd,8, StopBits.One);
            comPort.ReceivedBytesThreshold = 100;
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;

        //GPS Data fields
        public double CurrentGPSTime;
        public double CurrentLat;
        public double CurrentLong;
        public double CurrentAlt;

        //GPS Volcity Data
        public double CurrentEastVelocity;
        public double CurrentNorthVelocity;
        public double CurrentVerticalVelocity;
        public double CurrentRealVelocity;
        public double CurrentSpeed;

        //Methods
        
        public string getDataFromBuffer()
        {
            string serialData = "";
            int CurrentByte = 0;
            int PastByte = 0;
            int ByteCount = 0;
            int Counter = 0;

            string LastTwoBytes = "0";

            try
            {

                ByteCount = ComPort.BytesToRead; //Setting counter;

                while (LastTwoBytes != "1003")
                {
                    CurrentByte = ComPort.ReadByte();
                    LastTwoBytes = PastByte.ToString("X2") + CurrentByte.ToString("X2");

                    //Prevent the double 1010 thing.
                    if (LastTwoBytes == "1010")
                    {

                    }
                    else
                    {
                        serialData = serialData + CurrentByte.ToString("X2");
                    }

                    PastByte = CurrentByte;
                    Counter++;
                }
            }
            catch
            {}
            return serialData + " - " + ByteCount.ToString();
        }

        public void decodeGPSVolcity(string serialData) //Decoding Volocity Packet
        {
            byte[] floatVals = null;
            uint num = 0;

            double EastVelocity = 0;
            double NorthVelocity = 0;
            double VerticalVelocity = 0;
            double Velocity = 0;
            double Speed = 0;

            string EastVelocityHex = "";
            string NorthVelocityHex = "";
            string VerticalVelocityHex = "";

            EastVelocityHex = serialData.Substring(4, 8);
            NorthVelocityHex = serialData.Substring(12, 8);
            VerticalVelocityHex = serialData.Substring(20, 8);

            //Converting from hex
            num = uint.Parse(EastVelocityHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            EastVelocity = BitConverter.ToSingle(floatVals, 0);

            num = uint.Parse(NorthVelocityHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            NorthVelocity = BitConverter.ToSingle(floatVals, 0);

            num = uint.Parse(VerticalVelocityHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            VerticalVelocity = BitConverter.ToSingle(floatVals, 0);

            CurrentEastVelocity = EastVelocity;
            CurrentNorthVelocity = NorthVelocity;
            CurrentVerticalVelocity = VerticalVelocity;

            //Doing Real Distance moves
            Velocity = Math.Sqrt(Math.Pow(EastVelocity, 2) + Math.Pow(NorthVelocity, 2));
            CurrentRealVelocity = Velocity;

            Speed = Velocity * 2.2369363; //converting m/s to MPH
            CurrentSpeed = Speed;
        }

        public void decodeGPSTimeLatLogAlt(string serialData)//Decoding GPS Time, Lat, Long, Alt
        {
            byte[] floatVals = null;
            uint num = 0;

            double Lat = 0;
            double Long = 0;
            double Alt = 0;
            double GPSFixTime = 0;

            string LatHex = "";
            string LongHex = "";
            string AltHex = "";
            string GPSFixTimeHex = "";

            //breaking down the data into its elements.
            LatHex = serialData.Substring(4, 8);
            LongHex = serialData.Substring(12, 8);
            AltHex = serialData.Substring(20, 8);
            GPSFixTimeHex = serialData.Substring(36, 8);

            //Converting from hex data to Single
            num = uint.Parse(LatHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            Lat = BitConverter.ToSingle(floatVals, 0);

            num = uint.Parse(LongHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            Long = BitConverter.ToSingle(floatVals, 0);

            num = uint.Parse(AltHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            Alt = BitConverter.ToSingle(floatVals, 0);

            num = uint.Parse(GPSFixTimeHex, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            GPSFixTime = BitConverter.ToSingle(floatVals, 0);

            //Output of normal decoded data.
            CurrentGPSTime = GPSFixTime;
            CurrentLat = RadianToDegree(Lat);
            CurrentLong = RadianToDegree(Long);
            CurrentAlt = Alt;
        }

        public string getDecodePacketType(string serialData)
        {
            #region Currently not working GPS Packets.
            /*
            if (serialData.Substring(0, 4) == "105C")
            {
                GPSSatStatus(serialData);
            }

            
            if (serialData.Substring(0, 4) == "1041")
            {
                //veriables needed for float point conversion.
                byte[] floatVals = null;
                uint num = 0;
                double timedata = 0;


                num = uint.Parse(serialData.Substring(4, 8), System.Globalization.NumberStyles.AllowHexSpecifier);
                floatVals = BitConverter.GetBytes(num);
                timedata = BitConverter.ToSingle(floatVals, 0);

                textBoxGPSTimeDecode.Text = timedata.ToString();

            }
            */
            #endregion

            if (serialData.Substring(0, 4) == "104A") //Decode GPSTime LAT LONG ALT Packet
            {
                decodeGPSTimeLatLogAlt(serialData);
                             

                //Output of normal decoded data.
                string TimeLatLogAlt = CurrentGPSTime.ToString() + "," + CurrentLat.ToString() + "," +
                    CurrentLong.ToString() + "," + CurrentAlt.ToString();
                return TimeLatLogAlt;

                #region Area to calc Volocity from GPS Doesn't work right now.
                /*
                if (textBoxGPS1Lat.Text != "")
                {

                    DistanceCalcFromGPS = CalcVelocityFromGPS(Lat, Long, LatPast, LongPast);
                    textBoxVelocityCalc.Text = String.Format("{0:0.00000000}", DistanceCalcFromGPS);
                    textBoxSpeedCalc.Text = Convert.ToString(String.Format("{0:0.00000}", DistanceCalcFromGPS * 2.2369363));

                    //Running average for Heading smothing.
                    if (ProgramStatus.HeadingPacketCount < 3)
                    {
                        ProgramStatus.HeadingPacket[ProgramStatus.HeadingPacketCount] = DistanceCalcFromGPS;
                        ProgramStatus.HeadingPacketCount++;
                    }
                    if (ProgramStatus.HeadingPacketCount > 2)
                    {
                        ProgramStatus.HeadingPacketCount = 0;
                    }



                    //Need to stop the heading calc if I slow down to a sertion speed,,, or maybe distance.
                    if (Convert.ToDouble(textBoxVelocity.Text) > 1.0)
                    {


                        //Current Heading was 180 drees wrong so this inverts the last's and long's to see if that fixes it.
                        double LatTemp1 = RadianToDegree(Lat); //current Lat
                        double LongTemp1 = RadianToDegree(Long); //current Long
                        double LatTemp2 = DegreeToRadian(LatPast); //Past Lat
                        double LongTemp2 = DegreeToRadian(LongPast); //Past Long

                        //labelCurrentHeading.Text = CalcHeading(Lat, Long, LatPast, LongPast).ToString();
                        labelCurrentHeading.Text = String.Format("{0:0.0000}", CalcHeading(LatTemp2, LongTemp2, LatTemp1, LongTemp1));

                    }

                }

                //Calcing angel differance.
                if (textBoxGPS1Lat.Text != "" && textBoxLatWayPoint.Text != "" && textBoxLongWayPoint.Text != "" && labelHeadingToWayPoint.Text != "XXXXX") //Calc for angle to way point.
                {
                    labelHeadingDiff.Text = String.Format("{0:0.0000}", (Convert.ToDouble(labelHeadingToWayPoint.Text) - Convert.ToDouble(labelCurrentHeading.Text)));
                }


                if (textBoxGPS1Lat.Text != "" && textBoxLatWayPoint.Text != "")
                {
                    labelDistanceToWayPoint.Text = CalcVelocityFromGPS(Lat, Long, Convert.ToDouble(textBoxLatWayPoint.Text), Convert.ToDouble(textBoxLongWayPoint.Text)).ToString();
                    labelHeadingToWayPoint.Text = CalcHeading(Lat, Long, Convert.ToDouble(textBoxLatWayPoint.Text), Convert.ToDouble(textBoxLongWayPoint.Text)).ToString();
                }


                */
                //Output of normal decoded data.


                //labelGPSDataTime.Text = PacketTime;
                //textBoxGPSTimeFix.Text = GPSFixTime.ToString();
                /*
                if (labelHeadingDiff.Text != "XXXXX")
                {
                    ClearPanel();
                    PaintDirection();
  
                }
                */               
         
                #endregion

            }
            
            if (serialData.Substring(0, 4) == "1056") //Decoding Volocity
            {
                decodeGPSVolcity(serialData);

                string GPSVolocityDecoded = CurrentEastVelocity.ToString() + "," + CurrentNorthVelocity.ToString() +
                    "," + CurrentVerticalVelocity.ToString() + " ---- " + CurrentRealVelocity.ToString() +
                    " m/s," + CurrentSpeed.ToString() + " MPH";

                return GPSVolocityDecoded;

            }

            return null;
        }

        private double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }


    }
    #endregion

    #region Vaisala HMP234 Sensor

    public class HMP234
    {
        //Constructors
        public HMP234() { }

        public HMP234(string HMP234Port)
        {
            SerialPort comPort = new SerialPort(HMP234Port, 9600, Parity.None, 8, StopBits.One);
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;
        public double CurrentHumidity;
        public double CurrentTemp;

        //Methods

        public string getHMP234Data()
        {
            string HMP234SerialData = ComPort.ReadLine();

            //Formating incoming data.
            HMP234SerialData = HMP234SerialData.Replace(" ", "");
            HMP234SerialData = HMP234SerialData.Replace("\r", "");
            string[] HMP234data = HMP234SerialData.Split(',');

            //Protecting the process from failing is some bad data is received.
            try
            {
                CurrentHumidity = Convert.ToDouble(HMP234data[0]);
                CurrentTemp = Convert.ToDouble(HMP234data[1]);
            }
            catch (Exception error)
            {
                return error.Message;
            }
            return HMP234SerialData;
        }

        public void startHMP234Sensor()
        {
            ComPort.WriteLine("R\r");
        }

        public void stopHMP234Sensor()
        {
            ComPort.WriteLine("S\r");
        }

    }
    
    #endregion

    #region GE Hygro M4/E4

    public class GEHygro
    {
        //Constructors
        public GEHygro() { }

        public GEHygro(string GEHygroPort)
        {
            SerialPort comPort = new SerialPort(GEHygroPort, 1200, Parity.None, 8, StopBits.One);
            comPort.NewLine = "\r\n";
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;

        public double CurrentDPC;
        public double CurrentHumidity;
        public double CurrentTemp;
        
        //Methods

        public string getGEHygroData()
        {
            string GEHygroSerialData = ComPort.ReadLine();

            //Sorting line.
            GEHygroSerialData = GEHygroSerialData.Replace(" ", "");
            GEHygroSerialData = GEHygroSerialData.Replace("\r", "");

            string[] GEHygroData = GEHygroSerialData.Split('=');

            string datatype = GEHygroSerialData.Substring(0, 5);

            if (GEHygroData[0] == "DPC")
            {
                //textBoxGETime.Text = timeGEHygro;
                CurrentDPC = Convert.ToDouble(GEHygroData[1]);

            }

            if (GEHygroData[0] == "RH")
            {
                //textBoxGETime.Text = timeGEHygro;
                CurrentHumidity = Convert.ToDouble(GEHygroData[1]);

            }

            if (GEHygroData[0] == "TMPC")
            {
                //textBoxGETime.Text = timeGEHygro;
                CurrentTemp = Convert.ToDouble(GEHygroData[1]);

            }

            return GEHygroSerialData;
        }


    }
    #endregion

    #region B&B Data Acquisition Module

    public class BBModule 
    {
        //Constructors
        public BBModule() { }

        public BBModule(string ComPortName)
        {
            SerialPort comPort = new SerialPort(ComPortName, 1200, Parity.None, 8, StopBits.One);
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;
        public byte[] IOState = new byte[2];
        public byte[] IODefinitions = new byte[2];
        public byte[] powerUpState = new byte[2];

        //Methods
        public bool detectModule() 
        {
            try
            {
                ComPort.ReadTimeout = 300;
                ComPort.DiscardInBuffer();
                ComPort.Write("!0RC");
                //Thread.Sleep(1000);
                //string response = ComPort.ReadExisting();
                IODefinitions[0] = (byte)ComPort.ReadByte();//MSB Ports(0-7)
                IODefinitions[1] = (byte)ComPort.ReadByte();//LSB Ports(8-15)
                powerUpState[0] = (byte)ComPort.ReadByte();//MSB Ports(0-7)
                powerUpState[1] = (byte)ComPort.ReadByte();//LSB Ports(8-15)

                //Check power up state.  0-7 should be outputs, 8-15 should be inputs
                
                if ((IODefinitions[0] == 0x00 && IODefinitions[1] == 0xFF)&&(powerUpState[0]==0x00&&powerUpState[1]==0x00))
                {
                    IOState = powerUpState;
                    ComPort.Write("!0SO");
                    ComPort.Write(IOState, 0, 2);
                    Thread.Sleep(300);
                    return true;
                }
                else 
                {
                    powerUpState[0] = 0x00;//MSB
                    powerUpState[1] = 0x00;//LSB
                    IODefinitions[0] = 0x00;//MSB
                    IODefinitions[1] = 0xFF;//LSB

                    //Set new startup state
                    ComPort.Write("!0SS");
                    ComPort.Write(powerUpState, 0, 2);
                    Thread.Sleep(300);

                    ComPort.Write("!0SD");
                    ComPort.Write(IODefinitions, 0, 2);
                    Thread.Sleep(300);

                    ComPort.Write("!0SO");
                    ComPort.Write(powerUpState, 0, 2);

                    ComPort.Write("!0RC");
                    IODefinitions[0] = (byte)ComPort.ReadByte();//MSB Ports(0-7)
                    IODefinitions[1] = (byte)ComPort.ReadByte();//LSB Ports(8-15)
                    powerUpState[0] = (byte)ComPort.ReadByte();//MSB Ports(0-7)
                    powerUpState[1] = (byte)ComPort.ReadByte();//LSB Ports(8-15)

                    if ((IODefinitions[0] == 0x00 && IODefinitions[1] == 0xFF) && (powerUpState[0] == 0x00 && powerUpState[1] == 0x00))
                    {
                        IOState = powerUpState;
                        return true;
                    }
                    else 
                    {
                        return false;
                    }
                }
            }
            catch 
            {
                return false;
            }
        }

        public void setOutput(int port, bool high) 
        {
            int newState;

            switch (port) 
            {
                case 0:
                    if (high)
                    {
                        newState = IOState[1] | 0x01;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else 
                    {
                        newState = IOState[1] & 0xFE;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;
                case 1:
                    if (high)
                    {
                        newState = IOState[1] | 0x02;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0xFD;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                case 2:
                    if (high)
                    {
                        newState = IOState[1] | 0x04;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0xFB;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                case 3:
                    if (high)
                    {
                        newState = IOState[1] | 0x08;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0xF7;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                case 4:
                    if (high)
                    {
                        newState = IOState[1] | 0x10;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0xEF;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                case 5:
                    if (high)
                    {
                        newState = IOState[1] | 0x20;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0xDF;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                case 6:
                    if (high)
                    {
                        newState = IOState[1] | 0x40;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0xBF;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                case 7:
                    if (high)
                    {
                        newState = IOState[1] | 0x80;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    else
                    {
                        newState = IOState[1] & 0x7F;
                        IOState[1] = (byte)newState;
                        ComPort.Write("!0SO");
                        ComPort.Write(IOState, 0, 2);
                    }
                    break;

                default:
                    return;
            }
        }

        public void bumpOutput(int port, bool high, int milliseconds) 
        {
            if (high) 
            {
                setOutput(port, true);
                Thread.Sleep(milliseconds);
                setOutput(port, false);
            }
            else
            {
                setOutput(port, false);
                Thread.Sleep(milliseconds);
                setOutput(port, true);
            }
        }
        
        
    }
        #endregion

    #region Young Model 61204V Barometric Pressure Sensor

    public class YoungBaro
    {
        //Constructors
        public YoungBaro() { }

        public YoungBaro(string ComPortName)
        {
            SerialPort comPort = new SerialPort(ComPortName, 9600, Parity.None, 8, StopBits.One);
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;
        public double CurrentPressure;

        //Methods
        public string getBaroPressure()
        {
            string BaroSerialData = ComPort.ReadLine();

            //Formating incoming data.
            CurrentPressure = Convert.ToDouble(BaroSerialData);

            return BaroSerialData;
        }
    }
    #endregion

    #region Thunder TS3900 Humidity Generator

    public class TS3900
    {
        //Constructors
        public TS3900() { }

        public TS3900(string ComPortName)
        {
            SerialPort comPort = new SerialPort(ComPortName, 9600, Parity.None, 8, StopBits.One);
            ComPort = comPort;
            //ComPort.ReadTimeout = 6000;
        }

        //Fields
        public SerialPort ComPort;

        public DateTime statusSerialDataDateTime;

        //Element veriables.
        //Current Status of Thunder.
        public double currentFrostPoint;
        public double currentDewPoint;
        public double currentPPMv;
        public double currentPPMw;
        public double currentRH;
        public double currentSaturPSI;
        public double currentSaturC;
        public double currentTestPSI;
        public double currentTestC;
        public double currentFlowRate;
        public int currentStaus;
        public int currentError;
        public string currentErrorMessage;

        //Set Point of the Thunder
        public double setPointFrostPoint;
        public double setPointDewPoint;
        public double setPointPPMv;
        public double setPointPPMw;
        public double setPointRH;
        public double setPointSaturPSI;
        public double setPointSaturC;
        public double setPointTestPSI;
        public double setPointTestC;
        public double setPointFlowRate;
        public int setPointMode;

        //Debug data items
        public string serailData = "";

        //Locker
        static readonly object _dataLock = new object();
        

        //Methods

        /// <summary>
        /// Methods for getting the real time status of the Thunder.
        /// Returns a object array consisting of 0 = the DateTime of when the data was received. 1 = the string return of the serial data.
        /// </summary>
        /// <returns></returns>
        public object[] getStatus()
        {
        retryReadStatus:
            ComPort.DiscardInBuffer();
            ComPort.Write("?\r"); //Sending out request for status.
            string statusSerialData = ComPort.ReadLine(); //Read in the status from the thunder.
            lock (_dataLock)
            {
                this.serailData = statusSerialData;
            }
            statusSerialDataDateTime = DateTime.Now; //Time the status packet came in.

            if (statusSerialData == "\r")
            {
                errorReset();
                goto retryReadStatus;
            }

            //Reporting current status to public veriables.
            string[] TSData = statusSerialData.Split(',');
            if (TSData.Length !=11)
            {
                errorReset();
                goto retryReadStatus;
            }
            try
            {
                currentFrostPoint = Convert.ToDouble(TSData[0]);
                currentDewPoint = Convert.ToDouble(TSData[1]);
                currentPPMv = Convert.ToDouble(TSData[2]);
                currentPPMw = Convert.ToDouble(TSData[3]);
                currentRH = Convert.ToDouble(TSData[4]);
                currentSaturPSI = Convert.ToDouble(TSData[5]);
                currentSaturC = Convert.ToDouble(TSData[6]);
                currentTestPSI = Convert.ToDouble(TSData[7]);
                currentTestC = Convert.ToDouble(TSData[8]);
                currentFlowRate = Convert.ToDouble(TSData[9]);
                currentStaus = Convert.ToInt16(TSData[10]);
            }
            catch 
            {
                errorReset();
                goto retryReadStatus;
            }

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = statusSerialDataDateTime;
            outputData[1] = statusSerialData;
            return outputData;            
        }

        /// <summary>
        /// Method gets the current set points of the Thunder 3900. It returns two objects (1) time, (2) serial string.
        /// </summary>
        /// <returns></returns>
        public object[] getSetPoints()
        {
        retryRead:
            ComPort.DiscardInBuffer();
            ComPort.Write("?SP\r"); //Sending out request for status.
            string statusSerialData = ComPort.ReadLine(); //Read in the status from the thunder.
            lock (_dataLock)
            {
                this.serailData = statusSerialData;
            }
            DateTime setPointStatusSerialDataDateTime = DateTime.Now; //Time the status packet came in.

            if (statusSerialData == "\r")
            {
                errorReset();
                goto retryRead;
            }

            //Reporting current status to public veriables.
            string[] TSData = statusSerialData.Split(',');
            //Checking to make sure the data is good.
            if (TSData.Length != 11)
            {
                errorReset();
                goto retryRead;
            }
            try
            {
                setPointFrostPoint = Convert.ToDouble(TSData[0]);
                setPointDewPoint = Convert.ToDouble(TSData[1]);
                setPointPPMv = Convert.ToDouble(TSData[2]);
                setPointPPMw = Convert.ToDouble(TSData[3]);
                setPointRH = Convert.ToDouble(TSData[4]);
                setPointSaturPSI = Convert.ToDouble(TSData[5]);
                setPointSaturC = Convert.ToDouble(TSData[6]);
                setPointTestPSI = Convert.ToDouble(TSData[7]);
                setPointTestC = Convert.ToDouble(TSData[8]);
                setPointFlowRate = Convert.ToDouble(TSData[9]);
                setPointMode = Convert.ToInt16(TSData[10]);
            }
            catch 
            {
                errorReset();
                goto retryRead;
            }

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = setPointStatusSerialDataDateTime;
            outputData[1] = statusSerialData;
            return outputData;
        }

        private void errorReset()
        {
            //Finishing up any command
            ComPort.Write("\r");

            //Allowing the tunder to respond
            System.Threading.Thread.Sleep(1000);
            
            //Clearing all the data for this device.
            ComPort.DiscardInBuffer();
            ComPort.DiscardOutBuffer();
        }

        #region Methods for getting and setting settings.
        
        /// <summary>
        /// Method for gettiing the current Dew Point from the Thunder.
        /// Returns and object array 0 = DateTime the data was recived. 1 = the double representing the current Dew Point.
        /// </summary>
        /// <returns></returns>
        public object[] getDewPoint()
        {
            DateTime currentElementTime;
            ComPort.WriteLine("?DP\r");
            currentDewPoint = Convert.ToDouble(ComPort.ReadLine());
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentDewPoint;
            return outputData; 
        }

        /// <summary>
        /// Method sets the desired dew point and sets it as the current setting to be reached.
        /// Requires a double input for the desired Dew Point and the bool. True if you require the setting to be verified.
        /// Returns True if no check or if it is verified.
        /// </summary>
        /// <param name="newDewPoint"></param>
        /// <returns></returns>
        public bool setDewPoint(double newDewPoint, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("DP=" + newDewPoint.ToString()+"\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {

                getSetPoints();
                if (setPointDewPoint == newDewPoint)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            //if no checking the command retuns true for running.
            return true;
        }

        /// <summary>
        /// Gets the current error reports from the thunder.
        /// Returns it as three objects. 0 = DateTime of command responce. 1 = Int of error code. 2 = Error message.
        /// </summary>
        /// <returns></returns>
        public object[] getErrorNumber()
        {
            DateTime currentElementTime;
            ComPort.Write("?ER\r");
            string incomingData = ComPort.ReadLine();
            currentElementTime = DateTime.Now;
            string stringOutput = "";

            incomingData = incomingData.TrimEnd('\r');
            incomingData = incomingData.Trim();

            try
            {
                currentError = Convert.ToInt16(incomingData);
            }
            catch
            {
                currentError = 9999;
            }

            //Processing the string output of the error.
            switch (currentError)
            {
                case 0:
                    stringOutput = "No Error.";
                    break;

                case 1:
                    stringOutput = "Expansion Valve Not Closing.";
                    break;

                case 2:
                    stringOutput = "Flow Valve Not Closing.";
                    break;

                case 4:
                    stringOutput = "Low Supply Pressure.";
                    break;

                case 8:
                    stringOutput = "Cabinet Temperature Overrange.";
                    break;

                case 32:
                    stringOutput = "Referance Temperature Underrange.";
                    break;

                case 48:
                    stringOutput = "Refreance Temperature Overrange.";
                    break;

                case 64:
                    stringOutput = "Test Temperature Underrange.";
                    break;

                case 80:
                    stringOutput = "Test Temperature Overrange.";
                    break;

                case 128:
                    stringOutput = "Saturation Temperature Underrange.";
                    break;

                case 144:
                    stringOutput = "Saturation Temperature Overrange.";
                    break;

                case 512:
                    stringOutput = "Test Pressure Underrange.";
                    break;

                case 768:
                    stringOutput = "Test Pressure Overrange.";
                    break;

                case 1024:
                    stringOutput = "Low Range Saturation Pressure Underrange.";
                    break;

                case 1280:
                    stringOutput = "Low Range Saturation Pressure Overrange.";
                    break;

                case 2048:
                    stringOutput = "High Range Saturation Pressure Underrange.";
                    break;

                case 2304:
                    stringOutput = "High Range Saturation Pressure Overrange.";
                    break;

                case 9999:
                    stringOutput = "Data Error. String index error.";
                    break;
            }

            currentErrorMessage = stringOutput;

            //Compiling the data for output.
            object[] outputData = new object[3];
            outputData[0] = currentElementTime;
            outputData[1] = currentError;
            outputData[2] = stringOutput;

            return outputData;
        }

        /// <summary>
        /// Gets the current flow rate from the Thunder.
        /// Returns is as an object array consisting of two elements. 0 = DataTime of data. 1 = Double consisting of the Flow Rate.
        /// </summary>
        /// <returns></returns>
        public object[] getFlowRate()
        {
            DateTime currentElementTime;
            ComPort.Write("?FL\r");
            currentFlowRate = Convert.ToDouble(ComPort.ReadLine()); //current flow rate in l/min.
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentFlowRate;
            return outputData;
        }

        /// <summary>
        /// Methods sets the desired flow rate. Also has a mode for checking to see the rate was set and return true if it was.
        /// </summary>
        /// <param name="newFlowRate"></param>
        /// <param name="checkForAccept"></param>
        /// <returns></returns>
        public bool setFlowRate(double newFlowRate, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("FL=" + newFlowRate.ToString() + "\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {
                return checkCommandAccepted();
            }

            //if no checking the command retuns true for running.
            return true;
        }

        public object[] getFrostPoint()
        {
            DateTime currentElementTime;
            ComPort.Write("?FP\r");
            currentFrostPoint = Convert.ToDouble(ComPort.ReadLine());
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentFrostPoint;
            return outputData;
        }

        /// <summary>
        /// Method sets Frost Point to desire double. Command checking also avaiable.
        /// </summary>
        /// <param name="newFrostPoint"></param>
        /// <param name="checkForAccept"></param>
        /// <returns></returns>
        public bool setFrostPoint(double newFrostPoint, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("FP=" + newFrostPoint.ToString() + "\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {
                return checkCommandAccepted();
            }

            //if no checking the command retuns true for running.
            return true;
        }

        /// <summary>
        /// Method retuns the current gas line pressure in psiG
        /// </summary>
        /// <returns></returns>
        public object[] getGasSupplyPressure()
        {
            DateTime currentElementTime;
            ComPort.Write("?PG\r");
            double currentGasSupplyPressure = Convert.ToDouble(ComPort.ReadLine());
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentFrostPoint;
            return outputData;
        }

        public object[] getSaturationPressure()
        {
            DateTime currentElementTime;
            ComPort.Write("?PS\r");
            currentSaturPSI = Convert.ToDouble(ComPort.ReadLine());
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentSaturPSI;
            return outputData;
        }


        public bool setSaturationPressure(double newSaturationPressure, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("PS=" + newSaturationPressure.ToString() + "\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {
                return checkCommandAccepted();
            }

            //if no checking the command retuns true for running.
            return true;
        }

        public object[] getTestPressure()
        {
            DateTime currentElementTime;
            ComPort.Write("?PT\r");
            currentTestPSI = Convert.ToDouble(ComPort.ReadLine());
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentTestPSI;
            return outputData;
        }

        /// <summary>
        /// Method set the current test pressure in units of psiA. Test Pressure only needed if the external pressure transducer is not connected.
        /// </summary>
        /// <param name="newTestPressure"></param>
        /// <param name="checkForAccept"></param>
        /// <returns></returns>
        public bool setTestPressure(double newTestPressure, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("PT=" + newTestPressure.ToString() + "\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {
                return checkCommandAccepted();
            }

            //if no checking the command retuns true for running.
            return true;
        }

        public object[] getRH()
        {
            DateTime currentElementTime;
            ComPort.Write("?RH\r");
            currentRH = Convert.ToDouble(ComPort.ReadLine());
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentRH;
            return outputData;
        }

        public bool setRH(double newRH, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("RH=" + newRH.ToString() + "\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {
                return checkCommandAccepted();
            }

            //if no checking the command retuns true for running.
            return true;
        }

        public object[] getSaturationTemp()
        {
            DateTime currentElementTime;
            ComPort.Write("?TS\r");
            string test = ComPort.ReadLine();
            currentSaturC = Convert.ToDouble(test);
            currentElementTime = DateTime.Now;

            //Compiling the data for output.
            object[] outputData = new object[2];
            outputData[0] = currentElementTime;
            outputData[1] = currentSaturC;
            return outputData;
        }

        public bool setSaturationTemp(double newSaturationTemp, bool checkForAccept)
        {
            //sending the out the command.
            ComPort.DiscardInBuffer();
            ComPort.Write("TS=" + newSaturationTemp.ToString() + "\r");

            //If requested checking to make the command has been accepted.
            if (checkForAccept)
            {
                return checkCommandAccepted();
            }

            //if no checking the command retuns true for running.
            return true;
        } 

        private bool checkCommandAccepted()
        {
            bool commandStatus = false;
            string responce = ComPort.ReadExisting();

            if (responce == "\r\n")
            {
                commandStatus = true;
            }

            return commandStatus;
        }


        #endregion

        #region Methods for Starting, Purging, and Stopping.

        /// <summary>
        /// Method for stating the system generating. The System will work to match the set point last entered.
        /// </summary>
        /// <param name="checkForAccept"></param>
        /// <returns></returns>
        public bool systemGenerate(bool checkForAccept)
        {
            try
            {
                //Clearing in buffer and sending out the stop command.
                ComPort.DiscardInBuffer();
                ComPort.Write("GEN\r");

                if (checkForAccept)
                {
                    return checkCommandAccepted();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method used to purge the current output being generated from the system. When complete the system will retun to idle.
        /// </summary>
        /// <param name="checkForAccept"></param>
        /// <returns></returns>
        public bool systemPurge(bool checkForAccept)
        {
            try
            {
                //Clearing in buffer and sending out the stop command.
                ComPort.DiscardInBuffer();
                ComPort.Write("PRG\r");

                if (checkForAccept)
                {
                    return checkCommandAccepted();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method used to stop the system. This method can only be used when the system is operating at idle.
        /// This should only be used when the system is going to be unused for an extended amount of time.
        /// </summary>
        /// <param name="checkForAccept"></param>
        /// <returns></returns>
        public bool systemStop(bool checkForAccept)
        {
            try
            {
                //Clearing in buffer and sending out the stop command.
                ComPort.DiscardInBuffer();
                ComPort.Write("STOP\r");

                if (checkForAccept)
                {
                    return checkCommandAccepted();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
    #endregion

    #region Modbus class for commuicating with Watlow Controler
    public class modbus
    {
        public SerialPort sp = new SerialPort();
        public string modbusStatus;

        #region Constructor / Deconstructor
        public modbus()
        {
        }
        ~modbus()
        {
        }
        #endregion

        #region Open / Close Procedures
        public bool Open(string portName, int baudRate, int databits, Parity parity, StopBits stopBits)
        {
            //Ensure port isn't already opened:
            if (!sp.IsOpen)
            {
                //Assign desired settings to the serial port:
                sp.PortName = portName;
                sp.BaudRate = baudRate;
                sp.DataBits = databits;
                sp.Parity = parity;
                sp.StopBits = stopBits;
                //These timeouts are default and cannot be editted through the class at this point:
                //sp.ReadTimeout = 1000;
                //sp.WriteTimeout = 1000;

                //Shortened the time out in the hopes that it will help stop the system from locking up.
                sp.ReadTimeout = 100;
                sp.WriteTimeout = 100;

                try
                {
                    sp.Open();
                }
                catch (Exception err)
                {
                    modbusStatus = "Error opening " + portName + ": " + err.Message;
                    return false;
                }
                modbusStatus = portName + " opened successfully";
                return true;
            }
            else
            {
                modbusStatus = portName + " already opened";
                return false;
            }
        }
        public bool Close()
        {
            //Ensure port is opened before attempting to close:
            if (sp.IsOpen)
            {
                try
                {
                    sp.Dispose();
                }
                catch (Exception err)
                {
                    modbusStatus = "Error closing " + sp.PortName + ": " + err.Message;
                    return false;
                }
                modbusStatus = sp.PortName + " closed successfully";
                return true;
            }
            else
            {
                modbusStatus = sp.PortName + " is not open";
                return false;
            }
        }
        #endregion

        #region CRC Computation
        private void GetCRC(byte[] message, ref byte[] CRC)
        {
            //Function expects a modbus message of any length as well as a 2 byte CRC array in which to 
            //return the CRC values:

            ushort CRCFull = 0xFFFF;
            byte CRCHigh = 0xFF, CRCLow = 0xFF;
            char CRCLSB;

            for (int i = 0; i < (message.Length) - 2; i++)
            {
                CRCFull = (ushort)(CRCFull ^ message[i]);

                for (int j = 0; j < 8; j++)
                {
                    CRCLSB = (char)(CRCFull & 0x0001);
                    CRCFull = (ushort)((CRCFull >> 1) & 0x7FFF);

                    if (CRCLSB == 1)
                        CRCFull = (ushort)(CRCFull ^ 0xA001);
                }
            }
            CRC[1] = CRCHigh = (byte)((CRCFull >> 8) & 0xFF);
            CRC[0] = CRCLow = (byte)(CRCFull & 0xFF);
        }
        #endregion

        #region Build Message
        private void BuildMessage(byte address, byte type, ushort start, ushort registers, ref byte[] message)
        {
            //Array to receive CRC bytes:
            byte[] CRC = new byte[2];

            message[0] = address;
            message[1] = type;
            message[2] = (byte)(start >> 8);
            message[3] = (byte)start;
            message[4] = (byte)(registers >> 8);
            message[5] = (byte)registers;

            GetCRC(message, ref CRC);
            message[message.Length - 2] = CRC[0];
            message[message.Length - 1] = CRC[1];
        }
        #endregion

        #region Check Response
        private bool CheckResponse(byte[] response)
        {
            //Perform a basic CRC check:
            byte[] CRC = new byte[2];
            GetCRC(response, ref CRC);
            if (CRC[0] == response[response.Length - 2] && CRC[1] == response[response.Length - 1])
                return true;
            else
                return false;
        }
        #endregion

        #region Get Response
        private void GetResponse(ref byte[] response)
        {
            //There is a bug in .Net 2.0 DataReceived Event that prevents people from using this
            //event as an interrupt to handle data (it doesn't fire all of the time).  Therefore
            //we have to use the ReadByte command for a fixed length as it's been shown to be reliable.
            for (int i = 0; i < response.Length; i++)
            {
                    response[i] = (byte)(sp.ReadByte());
            }
        }
        #endregion

        #region Function 16 - Write Multiple Registers
        public bool SendFc16(byte address, ushort start, ushort registers, short[] values)
        {
            //Ensure port is open:
            if (sp.IsOpen)
            {
                //Clear in/out buffers:
                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                //Message is 1 addr + 1 fcn + 2 start + 2 reg + 1 count + 2 * reg vals + 2 CRC
                byte[] message = new byte[9 + 2 * registers];
                //Function 16 response is fixed at 8 bytes
                byte[] response = new byte[8];

                //Add bytecount to message:
                message[6] = (byte)(registers * 2);
                //Put write values into message prior to sending:
                for (int i = 0; i < registers; i++)
                {
                    message[7 + 2 * i] = (byte)(values[i] >> 8);
                    message[8 + 2 * i] = (byte)(values[i]);
                }
                //Build outgoing message:
                BuildMessage(address, (byte)16, start, registers, ref message);

                //Send Modbus message to Serial Port:
                try
                {
                    sp.Write(message, 0, message.Length);
                    GetResponse(ref response);
                }
                catch (Exception err)
                {
                    modbusStatus = "Error in write event: " + err.Message;
                    return false;
                }
                //Evaluate message:
                if (CheckResponse(response))
                {
                    modbusStatus = "Write successful";
                    return true;
                }
                else
                {
                    modbusStatus = "CRC error";
                    return false;
                }
            }
            else
            {
                modbusStatus = "Serial port not open";
                return false;
            }
        }
        #endregion

        #region Function 3 - Read Registers
        public bool SendFc3(byte address, ushort start, ushort registers, ref short[] values)
        {
            //Ensure port is open:
            if (sp.IsOpen)
            {
                //Clear in/out buffers:
                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                //Function 3 request is always 8 bytes:
                byte[] message = new byte[8];
                //Function 3 response buffer:
                byte[] response = new byte[5 + 2 * registers];
                //Build outgoing modbus message:
                BuildMessage(address, (byte)3, start, registers, ref message);
                //Send modbus message to Serial Port:
                try
                {
                    sp.Write(message, 0, message.Length);
                    GetResponse(ref response);
                }
                catch (Exception err)
                {
                    modbusStatus = "Error in read event: " + err.Message;
                    return false;
                }
                //Evaluate message:
                if (CheckResponse(response))
                {
                    //Return requested register values:
                    for (int i = 0; i < (response.Length - 5) / 2; i++)
                    {
                        values[i] = response[2 * i + 3];
                        values[i] <<= 8;
                        values[i] += response[2 * i + 4];
                    }
                    modbusStatus = "Read successful";
                    return true;
                }
                else
                {
                    modbusStatus = "CRC error";
                    return false;
                }
            }
            else
            {
                modbusStatus = "Serial port not open";
                return false;
            }

        }
        #endregion

    }

    #endregion

    #region Communication class for E+E EE31 Temp/Humidity Transmistter
    public class epluseEE31
    {
        //Constructor
        public epluseEE31(){}

        public epluseEE31(string ComPortName)
        {
            SerialPort comPort = new SerialPort(ComPortName, 9600, Parity.None, 8, StopBits.One);
            comPort.RtsEnable = false;
            ComPort = comPort;
        }

        //Fields
        public SerialPort ComPort;
        public double currentAirTemp;
        public double currentHumidity;

        public double getAirTemp(ref string statusAirTemp)
        {
            byte[] messageAirTemp = new byte[6]{0x00, 0x00, 0x67, 0x01, 0x00, 0x68};
            byte[] incomingMessage = new byte[11];

            //Clearing buffers
            ComPort.DiscardInBuffer();
            ComPort.DiscardOutBuffer();

            try
            {
                ComPort.Write(messageAirTemp, 0, messageAirTemp.Length);
                getResponse(ref incomingMessage);
            }
            catch(Exception error)
            {
                statusAirTemp = error.Message;
            }

            currentAirTemp = Convert.ToDouble(getFloatData(incomingMessage));

            return Convert.ToDouble(getFloatData(incomingMessage));
        }

        public double getHumidity(ref string statusHumidity)
        {
            byte[] messageAirTemp = new byte[6] { 0x00, 0x00, 0x67, 0x01, 0x01, 0x69 };
            byte[] incomingMessage = new byte[11];

            //Clearing buffers
            ComPort.DiscardInBuffer();
            ComPort.DiscardOutBuffer();

            try
            {
                ComPort.Write(messageAirTemp, 0, messageAirTemp.Length);
                getResponse(ref incomingMessage);
            }
            catch (Exception error)
            {
                statusHumidity = error.Message;
            }

            currentHumidity = Convert.ToDouble(getFloatData(incomingMessage));

            return Convert.ToDouble(getFloatData(incomingMessage));
        }

        private void getResponse(ref byte[] response)
        {
            //There is a bug in .Net 2.0 DataReceived Event that prevents people from using this
            //event as an interrupt to handle data (it doesn't fire all of the time).  Therefore
            //we have to use the ReadByte command for a fixed length as it's been shown to be reliable.
            for (int i = 0; i < response.Length; i++)
            {
                response[i] = (byte)(ComPort.ReadByte());
            }
        }

        private float getFloatData(byte[] hexData)
        {
            byte[] hexFloatData = new byte[] {hexData[6], hexData[7], hexData[8], hexData[9]};
            return BitConverter.ToSingle(hexFloatData, 0);
        }

    }
    #endregion

}