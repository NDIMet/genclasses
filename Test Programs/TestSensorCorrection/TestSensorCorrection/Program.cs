﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using sensorCorrection;
using TestEquipment;

namespace TestSensorCorrection
{
    class Program
    { 
        /*
        public static sensorCorHMP234Blk testHMP234BlkCor = new sensorCorHMP234Blk();
        public static sensorCorHMP234SS testHMP234SSCor = new sensorCorHMP234SS();
        public static sensorCorEE31 testEE31 = new sensorCorEE31();

        public static System.ComponentModel.BackgroundWorker backgroundWorkerRealTimeCompare = new System.ComponentModel.BackgroundWorker();

        
        public static HMP234 sensorHMP234Blk = new HMP234("COM12");
        public static HMP234 sensorHMP234SS = new HMP234("COM16");
        public static GEHygro sensorGEHydro = new GEHygro("COM17");
        public static epluseEE31 sensorEE31 = new epluseEE31("COM15");
        */

        static void Main(string[] args)
        {
            /*
            string userSelect = "";
            string userInput = "";

            Console.WriteLine("HMP234 Blk correction");

            while (true)
            {
                Console.WriteLine("Select a sensor.");
                Console.WriteLine("1 - HMP234 Blk");
                Console.WriteLine("2 - HMP234 SS");
                Console.WriteLine("3 - EE31");
                Console.WriteLine("4 - Real time compare");
                Console.Write("Select>");
                userSelect = Console.ReadLine();
                Console.Write("Desired input to correct>");
                userInput = Console.ReadLine();

                processCommand(userSelect, userInput);
                Console.WriteLine("");
            }
             */
            //=0.0000009*C2^4-0.0002*C2^3+0.0154*C2^2-0.4008*C2+1.0115  //EE31
            //=0.0005*D2^2-0.0732*D2+1.7828 //HMP233

            //double[] cerCoef = new double[5] { 1.0115, -0.4008, 0.0154, -0.0002, 0.0000009 }; //EE31
            //double[] cerCoef = new double[3] { 1.7828, -0.0732, 0.0005 };   //HMP233
            //double[] cerCoef = new double[5] { 0.4089, -0.0533, 0.0019, -2e-05, 1e-07 }; //EE31-PFTD9052/AB3-T52 SN:135093130623F4

            //2015 test
            double[] cerCoef = new double[5] { -0.2698, 0.8648, 0.0059, -7e-05, 3e-07 };


            //sensorCorrection.sensorCorrection corr = new sensorCorrection.sensorCorrection(cerCoef);

            //sensorCorrection.sensorCorEE31 sensorEE31 = new sensorCorEE31();
            //sensorEE31.setCoefficient(cerCoef);

            sensorCorHMP234Blk sensorHMP = new sensorCorHMP234Blk();
            sensorHMP.setCoefficient(cerCoef);

            while (true)
            {
                Console.Write("Input Humidity>");
                double inputNum = Convert.ToDouble(Console.ReadLine());
                double corAmount = sensorHMP.getCorrectedHumidity(inputNum);
                Console.WriteLine("Corr Amount=" + corAmount.ToString("0.0000") + " Total: " + (inputNum + corAmount).ToString("0.0000"));
            }




        }


        


        /*
        static void processCommand(string userSelect, string userInput)
        {
            switch (userSelect)
            {
                case "1":
                    Console.WriteLine(testHMP234BlkCor.getCorrectedHumidity(Convert.ToDouble(userInput)));
                    break;

                case "2":
                    Console.WriteLine(testHMP234SSCor.getCorrectedHumidity(Convert.ToDouble(userInput)));
                    break;

                case "3":
                    Console.WriteLine(testEE31.getCorrectedHumidity(Convert.ToDouble(userInput)));
                    break;

                case "4":
                    sensorGEHydro.ComPort.Open();
                    sensorHMP234Blk.ComPort.Open();
                    sensorHMP234SS.ComPort.Open();
                    sensorEE31.ComPort.Open();

                    backgroundWorkerRealTimeCompare.WorkerSupportsCancellation = true;
                    backgroundWorkerRealTimeCompare.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerRealTimeCompare_DoWork);
                    backgroundWorkerRealTimeCompare.RunWorkerAsync(userInput);

                    break;
            }
        }

        static void backgroundWorkerRealTimeCompare_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Log File
            string logFile ="c:\\SensorCompare - " + e.Argument.ToString() + ".csv";

            sensorHMP234Blk.startHMP234Sensor();
            sensorHMP234SS.startHMP234Sensor();

            sensorEE31.ComPort.DiscardInBuffer();
            sensorGEHydro.ComPort.DiscardInBuffer();
            sensorHMP234Blk.ComPort.DiscardInBuffer();
            sensorHMP234SS.ComPort.DiscardInBuffer();

            System.Threading.Thread.Sleep(4000);

            int couterLoop = 0;

            while (true)
            {
                string currentEE31AT = "";
                sensorEE31.getAirTemp(ref currentEE31AT);
                sensorEE31.getHumidity(ref currentEE31AT);

                sensorGEHydro.getGEHygroData();

                sensorHMP234Blk.getHMP234Data();
                sensorHMP234SS.getHMP234Data();

                Console.WriteLine("GE>" + sensorGEHydro.CurrentHumidity.ToString("0.000") + " | " +
                                   "HMPBlk>" + sensorHMP234Blk.CurrentHumidity.ToString("0.000") + " : " + testHMP234BlkCor.getCorrectedHumidity(sensorHMP234Blk.CurrentHumidity).ToString("0.000") + " " +
                                   "HMPSS>" + sensorHMP234SS.CurrentHumidity.ToString("0.000") + " : " + testHMP234SSCor.getCorrectedHumidity(sensorHMP234SS.CurrentHumidity).ToString("0.000") + " " +
                                   "EE31>" + sensorEE31.currentHumidity.ToString("0.000") + " : " + testEE31.getCorrectedHumidity(sensorEE31.currentHumidity).ToString("0.000"));

                //System.IO.TextWriter logData = 

                couterLoop++;

                if (couterLoop > 30)
                {
                    sensorEE31.ComPort.DiscardInBuffer();
                    sensorGEHydro.ComPort.DiscardInBuffer();
                    sensorHMP234Blk.ComPort.DiscardInBuffer();
                    sensorHMP234SS.ComPort.DiscardInBuffer();
                    couterLoop = 0;
                }


                System.Threading.Thread.Sleep(4000);

            }
        }

        */
    }
}
