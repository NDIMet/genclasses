﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestRadiosondeMaintance
{
    class Program
    {

        static void Main(string[] args)
        {
            //Universal_Radiosonde.iMet1UV2 sonde = new Universal_Radiosonde.iMet1UV2("COM8"); //home
            Universal_Radiosonde.iMet1UV2 sonde = new Universal_Radiosonde.iMet1UV2("COM4"); //work

            sonde.error.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(error_PropertyChange);
            sonde.cmdProcessComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(cmdProcessComplete_PropertyChange);
            sonde.statusReceived.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusReceived_PropertyChange);
            sonde.readyRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
            sonde.disconnectRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(disconnectRadiosonde_PropertyChange);

            

            //sonde.setManufacturingMode(true);
            //sonde.getFirmwareVersion();
            //Setting a coef and seeing how to handle this.
            /*
            Console.ReadLine();

            //double coef = 1.1234;

            sonde.setSerialNumber("1234");
            sonde.setSondeID("5678");
            sonde.setProbeID("90");

            for (int i = 0; i < 43; i++)
            {
                sonde.setCoefficient(i, i * 0.01);
                //System.Threading.Thread.Sleep(50);
            }

            sonde.saveConfiguration();

            */
            while (true)
            {
                sonde.addCommandToQue(Console.ReadLine());            
            }
        }

        static void disconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Console.WriteLine("Disconnected");
        }

        static void readyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Console.WriteLine("Ready> " + DateTime.Now.ToString("HH:mm:ss.ffff") + " - " + (string)data.NewValue);
        }

        static void statusReceived_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Console.WriteLine("Status> " + DateTime.Now.ToString("HH:mm:ss.ffff") + " - " + (string)data.NewValue);
        }

        static void cmdProcessComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Universal_Radiosonde.command incomingCmd = (Universal_Radiosonde.command)data.NewValue;
            Console.WriteLine("Command> " + DateTime.Now.ToString("HH:mm:ss.ffff") + " - " + incomingCmd.strResponce);
            
        }

        static void error_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incomingData = (Exception)data.NewValue;
            Console.WriteLine("Error> " + incomingData.Message);
        }


    }
}
