﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestIpRadiosonde
{
    class Program
    {
        public static List<string> writeBuffer = new List<string>();

        public static List<ipRadiosonde.ipUniRadiosonde> sondePorts = new List<ipRadiosonde.ipUniRadiosonde>();

        public static System.Timers.Timer timerWriteData;

        static void Main(string[] args)
        {
            timerWriteData = new System.Timers.Timer(5000);
            timerWriteData.Elapsed += TimerWriteData_Elapsed;
            timerWriteData.Enabled = true;

            string command = "";
            while (command.ToLower() !="exit")
            {
                command = Console.ReadLine();

                switch(command)
                {
                    case "setip":
                        ipRadiosonde.ipUniRadiosonde tempSonde = new ipRadiosonde.ipUniRadiosonde();

                        Console.Write("Input IP Address>");
                        string ip = Console.ReadLine();

                        Console.Write("Port Address>");
                        string port = Console.ReadLine();
                        tempSonde.open(ip, Convert.ToInt16(port));
                        

                        tempSonde.bootingRadiosonde.PropertyChange += BootingRadiosonde_PropertyChange;
                        tempSonde.CalDataUpdate.PropertyChange += CalDataUpdate_PropertyChange;
                        tempSonde.disconnectRadiosonde.PropertyChange += DisconnectRadiosonde_PropertyChange;
                        tempSonde.readyRadiosonde.PropertyChange += ReadyRadiosonde_PropertyChange;
                        tempSonde.StatDataUpdate.PropertyChange += StatDataUpdate_PropertyChange;

                        tempSonde.autoStartCalMode(true);

                        sondePorts.Add(tempSonde);

                        break;

                }

            }
        }

        private static void TimerWriteData_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (writeBuffer.Count() > 0)
            {
                string compiled = "";
                foreach (string s in writeBuffer)
                {
                    compiled += s + "\r\n";
                }

                writeBuffer.Clear();

                System.IO.File.AppendAllText("data.csv", compiled);
            }
        }

        private static void StatDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            
        }

        private static void ReadyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string type = sender.GetType().ToString();
            updateCreater.objectUpdate theSender = (updateCreater.objectUpdate)sender;

            string line = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "," + theSender.objectID + "," + "Ready";

            writeBuffer.Add(line);

            Console.WriteLine(line);
        }

        private static void DisconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string type = sender.GetType().ToString();
            updateCreater.objectUpdate theSender = (updateCreater.objectUpdate)sender;

            string line = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "," + theSender.objectID + "," + "Disconnect";

            writeBuffer.Add(line);

            Console.WriteLine(line);
        }

        private static void BootingRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string type = sender.GetType().ToString();
            updateCreater.objectUpdate theSender = (updateCreater.objectUpdate)sender;

            string line = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "," + theSender.objectID + "," + "Booting";

            writeBuffer.Add(line);

            Console.WriteLine(line);
        }

        private static void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.CalData incoming = (ipRadiosonde.CalData)data.NewValue;

            string type = sender.GetType().ToString();
            updateCreater.objectUpdate theSender = (updateCreater.objectUpdate)sender;

            string line = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "," + theSender.objectID +  "," + incoming.Pressure.ToString("0.00") + "," + incoming.Temperature.ToString("0.00") + "," +
                incoming.Humidity.ToString("0.00");

            writeBuffer.Add(line);

            Console.WriteLine(line);
        }
    }
}
