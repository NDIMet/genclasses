﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testKMtronicRelay
{
    class Program
    {
        static KMtronicRelay.equipmentKmtronicRelay relay;
        static bool runToggel = false;

        static void Main(string[] args)
        {
            Console.WriteLine("Enter in com port number (COM##)");
            string cominput = Console.ReadLine();

            relay = new KMtronicRelay.equipmentKmtronicRelay(cominput, 8);

            while (true)
            {
                string command = Console.ReadLine();

                switch (command)
                {
                    case "run":
                        Console.WriteLine("Delay?");
                        Int32 delayinMS = Convert.ToInt32(Console.ReadLine());

                        runToggel = true;

                        System.Threading.Thread toggel = new System.Threading.Thread(threadRun);
                        toggel.IsBackground = true;
                        toggel.Start(delayinMS);
                        break;

                    case "stop":
                        runToggel = false;
                        break;
                }
            }




            

            Console.ReadLine();

        }

        static void threadRun(object time)
        {
            int delay = (Int32)time;
            bool state = false;

            while (runToggel)
            {
                relay.setRelayState(1, state);

                if (state)
                {
                    state = false;
                }
                else
                {
                    state = true;
                }

                System.Threading.Thread.Sleep(delay);

            }
        }
    }
}
