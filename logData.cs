﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogClasses
{
    public class logData
    {


    }

    public class logFinalTestResults
    {
        //Public
        public updateCreater.objectUpdate logError = new updateCreater.objectUpdate();

        //Private
        string logFileName ="";

        public logFinalTestResults() { }

        public logFinalTestResults(string logName)
        {
            logFileName = logName;
        }

        public void appendLogData(FinalTest.testResults testResults, FinalTest.testDataPoint testData, idData curID)
        {
            if (!System.IO.File.Exists(logFileName))
            {
                System.IO.File.AppendAllText(logFileName, "Radiosonde Final Test Results\nLogging Version 3\nTimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,SondeHumidityStable,RefHumidityStable,GPSSolution,PressureTest,TemperatureTest,HumidityTest,Pressure(hPa),AirTemp(degC),Humidity(RH),Latitude(deg),Longitude(deg),Altitude(m),AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,CurrentUser,CurrentWO,SondeType\n");
            }

            string totalLine = "";

            // write a line of text to the file
            totalLine = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.ffff") + "," + curID.sondeSerialNumber + "," +
                curID.sondeTrackingNumber + "," + curID.sondeTUNumber + "," +
                curID.sondeFirmware + ",";

            totalLine += decodeBool(testResults.sondeHumidityStability) + "," + decodeBool(testResults.refHumidityStability) + "," +
                decodeBool(testResults.statusGPSPOS) + "," + decodeBool(testResults.statusPressure) + "," +
                decodeBool(testResults.statusAirTemp) + "," + decodeBool(testResults.statusHumidity) + "," +

                testData.txSondePressure.ToString() + "," +
                testData.txSondeAirTemp.ToString() + "," + testData.txSondeHumidity.ToString() + "," +
                testData.txSondeLat.ToString() + "," + testData.txSondeLong.ToString() + "," +
                testData.txSondeAlt.ToString() + "," +

                testData.refPressure.ToString() + "," +
                testData.refAirTemp.ToString() + "," + testData.refHumidity.ToString() + "," +

                (testData.refPressure - testData.txSondePressure).ToString() + "," + (testData.refAirTemp - testData.txSondeAirTemp).ToString() + "," +
                (testData.refHumidity - testData.txSondeHumidity).ToString() + ",";
            

            totalLine += curID.currentUser + "," +
                curID.currentWorkOrder + "," +
                curID.sondeType + "\n";

            try
            {
                System.IO.File.AppendAllText(logFileName, totalLine);
            }
            catch (Exception e)
            {
                logError.UpdatingObject = e;
            }
        }

        /// <summary>
        /// Something to turn True/False to Pass/Fail. This is something that Joe B. assed for telling me that from a reviewer possition True/False is not descriptive enough.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private string decodeBool(bool status)
        {
            if (status)
            {
                return "Pass";
            }
            else
            {
                return "Fail";
            }
        }

    }

    public class idData
    {
        public string currentUser;
        public string currentWorkOrder;
        public string sondeSerialNumber;
        public string sondeTrackingNumber;
        public string sondeTUNumber;
        public string sondeFirmware;
        public string sondeType;
    }
}
