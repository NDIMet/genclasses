﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPRelay
{
    public struct currentRelayState
    {
        public bool relayState;     //Relay on/off state 0=open
        public bool inputState;     //Input on/off state 0=off;
        public bool rebootState;    //Reboot option
        public int totalReboots;    //Amount of reboots
    }


    public class webRealy
    {
        //Public Var
        public updateCreater.objectUpdate relayStateUpdate = new updateCreater.objectUpdate();     //update of the state of the relay system.
        public currentRelayState systemState = new currentRelayState();

        //Private Var
        string address;
        public updateCreater.objectUpdate messageReceived = new updateCreater.objectUpdate();
        

        public webRealy(){}

        public webRealy(string webAddress)
        {
            address = webAddress;
            messageReceived.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(messageReceived_PropertyChange);
        }

        public void triggerRelay(bool state)
        {
            string outputData = "GET /state.xml?relayState=" + Convert.ToInt16(state).ToString() + " HTTP/1.0 \r\n\r\n";
            System.Threading.Thread sendData = new System.Threading.Thread(this.sendData);
            sendData.Name = "Relay Trigger Thread";
            sendData.Start(outputData);
        }

        public void getRelayState()
        {
            string outputData = "GET /state.xml" + " HTTP/1.0 \r\n\r\n";
            System.Threading.Thread sendData = new System.Threading.Thread(this.sendData);
            sendData.Name = "Relay Status Thread";
            sendData.Start(outputData);
        }

        void messageReceived_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string message = (string)data.NewValue;

            message.Trim();

            char[] splitters = { '<', '>' };
            string[] strs = message.Split(splitters);

            for(int i = 0; i< strs.Length; i++)
            {
                if (strs[i] == "relaystate")
                {
                    systemState.relayState = Convert.ToBoolean(Convert.ToInt16(strs[i + 1]));
                    System.Diagnostics.Debug.WriteLine( "relaystate=" + Convert.ToBoolean(Convert.ToInt16(strs[i+1])).ToString());
                }

                if (strs[i] == "inputstate")
                {
                    systemState.inputState = Convert.ToBoolean(Convert.ToInt16(strs[i + 1]));
                    System.Diagnostics.Debug.WriteLine( "inputstate=" + Convert.ToBoolean(Convert.ToInt16(strs[i + 1])).ToString());
                }

                if (strs[i] == "rebootstate")
                {
                    systemState.rebootState = Convert.ToBoolean(Convert.ToInt16(strs[i + 1]));
                    System.Diagnostics.Debug.WriteLine("rebootstate=" + Convert.ToBoolean(Convert.ToInt16(strs[i + 1])).ToString());
                }

                if (strs[i] == "totalreboot")
                {
                    systemState.totalReboots = Convert.ToInt16(strs[i + 1]);
                    System.Diagnostics.Debug.WriteLine("totalreboot=" +Convert.ToInt16(strs[i + 1]).ToString());
                }

            }

            relayStateUpdate.UpdatingObject = systemState;      //Sending out the updated state of the relay system.

        }

        private void sendData(object message)
        {
            System.Net.Sockets.TcpClient tcpClient;    //Client to make a connecton to the relay
            System.Net.Sockets.NetworkStream stream;    //The TCPIP Stream

            tcpClient = new System.Net.Sockets.TcpClient(address, 80);

            
            stream = tcpClient.GetStream();

            byte[] send = Encoding.ASCII.GetBytes((string)message);
            stream.Write(send, 0, send.Length);

            //Getting the return data
            byte[] bytes = new byte[tcpClient.ReceiveBufferSize];
            int count = stream.Read(bytes, 0, (int)tcpClient.ReceiveBufferSize);

            String data = Encoding.ASCII.GetString(bytes);
            messageReceived.UpdatingObject = data;      //Sending update out.

            stream.Close();
            tcpClient.Close();
    
        }

             
    }
}
