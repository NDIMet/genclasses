﻿using System;
using System.Collections.Generic;
using System.Text;

namespace txDecoder
{
    public class decoderBell202
    {
        //Public veriables
        public System.IO.Ports.SerialPort COMPORT;     //Decoder com port connection.
        public updateCreater.objectUpdate decoderMessage = new updateCreater.objectUpdate();      //Decoder Error
        public updateCreater.objectUpdate packetPTUReady = new updateCreater.objectUpdate();    //PTU packet ready.
        public updateCreater.objectUpdate packetPTUXReady = new updateCreater.objectUpdate();   //PTUX packet ready.
        public updateCreater.objectUpdate packetGPSReady = new updateCreater.objectUpdate();    //GPS packet ready.
        public updateCreater.objectUpdate packetGPSXReady = new updateCreater.objectUpdate();   //GPSX packet ready.
        public updateCreater.objectUpdate packetXDataReady = new updateCreater.objectUpdate();  //XData packet ready.
        public updateCreater.objectUpdate packetBad = new updateCreater.objectUpdate();     //Bad Packet detected.
        public updateCreater.objectUpdate packetPTUTimeout = new updateCreater.objectUpdate(); //PTU decoding timeout.
        public updateCreater.objectUpdate packetGPSTimeout = new updateCreater.objectUpdate(); //GPS decoding timeout.
        public simRadiosonde testRadiosonde;       //Test sonde for testing decoding.

        static readonly object _locker = new object();  //Controls data being written to items.

        //Data packets
        public radiosondeBadPacketData badDataPacket;   //Last bad packet for reference.

        //Private veribles
        List<byte> receivedData = new List<byte>();     //The incoming data from the decoder.
        System.ComponentModel.BackgroundWorker backgroundWorkerDecodeData;  //Background worker for collecting and sorting data.
        crcCheckSum.Crc16Ccitt calc = new crcCheckSum.Crc16Ccitt(crcCheckSum.InitialCrcValue.NonZero2); //CRC-CCITT (0x1D0F) For checksum... Bitches love checksum's
        List<radiosondePTUData> dataPTU = new List<radiosondePTUData>();    //List array of the decoded PTU Data.
        List<radiosondeGPSData> dataGPS = new List<radiosondeGPSData>();    //List array of the decoded GPS Data.
        List<radiosondeXDataData> dataXData = new List<radiosondeXDataData>();  //List array of the decoded Xdata.
        System.Timers.Timer timerPTUTimeOut;    //Timer for missing PTU data
        System.Timers.Timer timerGPSTimeOut;    //Timer for missing GPS data


        public decoderBell202() { }

        public decoderBell202(string comPort)
        {
            this.COMPORT = new System.IO.Ports.SerialPort(comPort, 1200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);     //Starting up comport communication

            //This option didn'r work. For some reason is drags the entrie system down.
            //this.COMPORT.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(COMPORT_DataReceived);      //Subscribing to the com port data received to collect data from the serial port.

            this.COMPORT.Open();

            //Starting the background worker to process incoming data.
            backgroundWorkerDecodeData = new System.ComponentModel.BackgroundWorker();  
            backgroundWorkerDecodeData.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerDecodeData_DoWork);
            backgroundWorkerDecodeData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerDecodeData_RunWorkerCompleted);
            backgroundWorkerDecodeData.RunWorkerAsync();

            timerPTUTimeOut = new System.Timers.Timer(1200);
            timerPTUTimeOut.Elapsed += new System.Timers.ElapsedEventHandler(timerPTUTimeOut_Elapsed);
            timerPTUTimeOut.Enabled = true;

            timerGPSTimeOut = new System.Timers.Timer(1200);
            timerGPSTimeOut.Elapsed += new System.Timers.ElapsedEventHandler(timerGPSTimeOut_Elapsed);
            timerGPSTimeOut.Enabled = true;
        }

        #region Background workers and events to handle data.

        void timerGPSTimeOut_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.IO.IOException timeout = new System.IO.IOException("GPS Packet Time Out", 2000);
            packetGPSTimeout.UpdatingObject = (object)timeout;
        }

        void timerPTUTimeOut_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.IO.IOException timeout = new System.IO.IOException("PTU Packet Time Out", 2001);
            packetPTUTimeout.UpdatingObject = (object)timeout;
        }

        void backgroundWorkerDecodeData_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            List<byte> incomingData;    //Local incoming data.
            while (!backgroundWorkerDecodeData.CancellationPending)     //Runs till the background Worker is canceled.
            {
                if (receivedData.Count > 10000) //If 10000 bytes are in buffer clear buffer and start over.
                {
                    System.IO.IOException bufferFull = new System.IO.IOException("Decoder buffer has reached " + receivedData.Count.ToString() + ". Clearing buffer.", 10000);  //Creating buffer full message.
                    decoderMessage.UpdatingObject = (object)bufferFull; //Sending out the decoder status message
                    receivedData.Clear();   //Clearing buffer
                }

                if (COMPORT != null)
                {
                    if (COMPORT.IsOpen && COMPORT.BytesToRead > 0)  //Processing data if port open and data to read.
                    {
                        lock (_locker)
                        {
                            for (int i = 0; i < COMPORT.BytesToRead; i++)
                            {
                                receivedData.Add((byte)COMPORT.ReadByte()); //Reading in all the data in the buffer.
                            }
                        }
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(150); //Taking a break if no data is in the system.
                    }
                }
                else
                {
                    System.Threading.Thread.Sleep(150); //Taking a break if no data is in the system.
                }

                //Cleaning up the stored packets.
                if (dataGPS.Count > 300)
                {
                    dataGPS.Remove(dataGPS[0]); //Removing the first gps packet.
                }
                if (dataPTU.Count > 300)
                {
                    dataPTU.Remove(dataPTU[0]); //Removing the first put packet.
                }

                if (receivedData.Count >= 2)     //If there is data process it.
                {
                    incomingData = new List<byte>();    //Making the local byte container ready. 

                    for (int curByte = 0; curByte < receivedData.Count; curByte++)
                    {
                        if (receivedData[curByte] == 01)  //If the first byte is 01 then start processing
                        {
                            if ((curByte + 1) >= receivedData.Count)
                            {
                                System.Threading.Thread.Sleep(100);
                                break;
                            }

                            switch ((byte)receivedData[curByte + 1])    //Looking for packet type PTU, GPS and so on.
                            {
                                case 01:    //PTU packet (14 bytes)
                                    if (receivedData.Count >= (curByte + 14))   //If there is enough data for a PTU packet in the main buffer load it into the incoming list array
                                    {
                                        for (int i = 0; i < 14; i++)
                                        {
                                            incomingData.Add(receivedData[curByte + i]);  //Loading the incoming Data buffer.
                                        }

                                        lock (_locker)
                                        {
                                            receivedData.RemoveRange(curByte, 14);    //Removing the located packet from the main buffer.
                                        }

                                        if (checkChecksum(incomingData.ToArray()))
                                        {
                                            processData(incomingData.ToArray());    //Sending out data to be processed.
                                        }
                                    }
                                    break;

                                case 02:    //GPS packet (18 bytes)
                                    if (receivedData.Count >= (curByte + 18))   //If there is enough data for a GPS packet in the main buffer load it into the incoming list array
                                    {
                                        for (int i = 0; i < 18; i++)
                                        {
                                            incomingData.Add(receivedData[curByte + i]);  //Loading the incoming Data buffer.
                                        }

                                        lock (_locker)
                                        {
                                            receivedData.RemoveRange(curByte, 18);    //Removing the located packet from the main buffer.
                                        }

                                        if (checkChecksum(incomingData.ToArray()))
                                        {
                                            processData(incomingData.ToArray());    //Sending out data to be processed.
                                        }
                                    }
                                    break;

                                case 03:    //XData Packet
                                    if (receivedData.Count >= (curByte + 10))   
                                    {
                                        int dataCount = (receivedData[curByte + 2] + 2);  //The amount of data.
                                        if ((receivedData.Count + dataCount) <= receivedData.Count)     //Check to make sure there is enoug data to process the message.
                                        {
                                            for (int i = 0; i < dataCount; i++)
                                            {
                                                incomingData.Add(receivedData[curByte + i]);    //Loading the incoming Data buffer.
                                            }

                                            lock (_locker)
                                            {
                                                receivedData.RemoveRange(curByte, dataCount);    //Removing the located packet from the main buffer.
                                            }

                                            if (checkChecksum(incomingData.ToArray()))
                                            {
                                                processData(incomingData.ToArray());    //Sending out data to be processed.
                                            }
                                        }

                                    }
                                    break;

                                case 04:    //PTUX Packet (20 bytes)
                                    if (receivedData.Count >= (curByte + 20))   //If there is enough data for a PTUX packet in the main buffer load it into the incoming list array
                                    {
                                        for (int i = 0; i < 20; i++)
                                        {
                                            incomingData.Add(receivedData[curByte + i]);  //Loading the incoming Data buffer.
                                        }

                                        lock (_locker)
                                        {
                                            receivedData.RemoveRange(curByte, 20);    //Removing the located packet from the main buffer.
                                        }

                                        if (checkChecksum(incomingData.ToArray()))
                                        {
                                            processData(incomingData.ToArray());    //Sending out data to be processed.
                                        }
                                    }
                                    break;

                                case 05:    //GPSX Packet (30 bytes)
                                    if (receivedData.Count >= (curByte + 30))   //If there is enough data for a GPS packet in the main buffer load it into the incoming list array
                                    {
                                        for (int i = 0; i < 30; i++)
                                        {
                                            incomingData.Add(receivedData[curByte + i]);  //Loading the incoming Data buffer.
                                        }

                                        lock (_locker)
                                        {
                                            receivedData.RemoveRange(curByte, 30);    //Removing the located packet from the main buffer.
                                        }

                                        if (checkChecksum(incomingData.ToArray()))
                                        {
                                            processData(incomingData.ToArray());    //Sending out data to be processed.
                                        }
                                    }
                                    break;
                            }
                        }

                        //Checking for IMS Data.
                        if ((curByte + 6) < receivedData.Count) //Looking for there to be enough data.   
                        {
                            if (receivedData[curByte] == 08)    //Found a perpencity for 08's in Bell202 decodeing of IMS TX.
                            {
                                int repeatingEights = 0;
                                for (int re = 0; re < 6; re++)  //Checking for repeating 8's.
                                {
                                    if (receivedData[curByte + re] == 08)
                                    {
                                        repeatingEights++;
                                    }
                                }

                                if (repeatingEights == 6)
                                {
                                    lock (_locker)
                                    {
                                        System.IO.IOException imsTX = new System.IO.IOException("Detected IMS Transmittion", 1030); //Creating new exception to pass error message.
                                        decoderMessage.UpdatingObject = (object)imsTX;    //Triggering decoder error to inform others.
                                        receivedData.Clear();    //Clearing current buffer.
                                    }
                                }
                                
                            }
                        }
                        else
                        {
                            lock (_locker)
                            {   
                                //receivedData.RemoveAt(0);   //Removing the data if not a start packet byte.
                            }
                        }
                    }
                }
                else  //If no data then hold off and wait.
                {
                    System.Threading.Thread.Sleep(200);
                }
            }
        }

        void backgroundWorkerDecodeData_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)    //If there was an error that causes the processing to shutdown then it triggers the error the error event.
            {
                decoderMessage.UpdatingObject = e.Error;  //Passing the error out.
            }
        }

        public bool checkChecksum(byte[] message)
        {
            bool messageStatus = false;

            //Making checksum.
            byte[] toCheck = new byte[message.Length - 2];

            int localCounter = 0;
            for (int y = 0; y < message.Length - 2; y++)
            {
                toCheck[localCounter] = message[y];
                localCounter++;
            }
            byte[] crc = calc.ComputeChecksumBytes(toCheck);

            //Checking calced to the received message.
            if (message[message.Length - 2] == crc[0] && message[message.Length - 1] == crc[1])
            {
                messageStatus = true;
            }
            else
            {
                //Creating bad data packet.
                radiosondeBadPacketData tempPacket = new radiosondeBadPacketData();
                tempPacket.packetDateTime = DateTime.Now;
                tempPacket.data = message;
                badDataPacket = tempPacket;

                System.IO.IOException badPacket = new System.IO.IOException("Bad data packet decoded.", 1001);
                packetBad.UpdatingObject = (object)badPacket;
            }

            return messageStatus;
        }

        #endregion

        /// <summary>
        /// Method for passing logged data into the class to process as if it was decoded.
        /// </summary>
        /// <param name="data"></param>
        public object postProcessData(byte[] data)
        {
            return processData(data);
        }

        private object processData(byte[] data)
        {
            radiosondePTUData tempPacket;
            radiosondeGPSData tempGPSPacket;
            radiosondeXDataData tempXData;

            byte[] currentDecode; //Container for holding current to decode.

            switch ((byte)data[1])
            {
                case 01:    //PTU packet (14 bytes)
                    tempPacket = new radiosondePTUData();     //Temp data container.
                    tempPacket.packetDateTime = DateTime.Now;   //System time packet receive.

                    //PacketCount
                    currentDecode = new byte[2] { data[3], data[2] };
                    tempPacket.packetNumber = Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16); //Formats the data to HEX then convers it all to a 16 bit int.

                    //Pressure
                    currentDecode = new byte[3] { data[6], data[5], data[4] };
                    tempPacket.pressure = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100; //Pressure ,,, I like it

                    //Air Temp
                    currentDecode = new byte[2] { data[8], data[7] };
                    tempPacket.airTemp = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;  //Converting the hex to air temp

                    //Humidity
                    currentDecode = new byte[2] { data[10], data[9] };
                    tempPacket.humidity = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;    //Wet or dry

                    //Vbat
                    tempPacket.batteryVoltage = Convert.ToDouble(Convert.ToInt16(data[11].ToString("X"), 16)) / 10;    //The current battery voltage.

                    tempPacket.rawData = data;

                    dataPTU.Add(tempPacket);    //Adding the decocder packet to the list array.
                    packetPTUReady.UpdatingObject = (object)data;   //Triggering event from PTU packet.
                    resetPTUTimer();
                    return tempPacket;

                case 02:    //GPS packet (18 bytes)
                    tempGPSPacket = new radiosondeGPSData();
                    //Local decode time.
                    tempGPSPacket.packetDateTime = DateTime.Now;

                    //Latitude
                    currentDecode = new byte[4] { data[5], data[4], data[3], data[2] };
                    tempGPSPacket.latitude = BitConverter.ToSingle(BitConverter.GetBytes(uint.Parse(BitConverter.ToString(currentDecode).Replace("-", string.Empty), System.Globalization.NumberStyles.AllowHexSpecifier)), 0);

                    //Longitude
                    currentDecode = new byte[4] { data[9], data[8], data[7], data[6] };
                    tempGPSPacket.longitude = BitConverter.ToSingle(BitConverter.GetBytes(uint.Parse(BitConverter.ToString(currentDecode).Replace("-", string.Empty), System.Globalization.NumberStyles.AllowHexSpecifier)), 0);

                    //Altitude
                    currentDecode = new byte[2] { data[11], data[10] };
                    tempGPSPacket.alt = uint.Parse(BitConverter.ToString(currentDecode).Replace("-", string.Empty), System.Globalization.NumberStyles.AllowHexSpecifier) - 5000;
                    //Sat Count
                    tempGPSPacket.satCount = (int)data[12];
                    //Time(hr,mm,ss)
                    tempGPSPacket.gpsTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, (int)data[13], (int)data[14], (int)data[15]);

                    tempGPSPacket.rawData = data;

                    dataGPS.Add(tempGPSPacket); //Adding decoding gps packet to list.
                    packetGPSReady.UpdatingObject = (object)data;   //Triggering gps event.
                    resetGPSTimer();
                    return tempGPSPacket;

                case 03:    //XData Packet
                    tempXData = new radiosondeXDataData();  //Init temp packet.
                    tempXData.packetDateTime = DateTime.Now;    //Getting the processed local time.
                    tempXData.data = data;

                    dataXData.Add(tempXData);   //Adding sorted data to the memory.

                    packetXDataReady.UpdatingObject = (object)data; //Triggering xdata event.

 
                    break;

                case 04:    //PTUX Packet (20 bytes)
                    tempPacket = new radiosondePTUData();     //Temp data container.
                    tempPacket.packetDateTime = DateTime.Now;   //System time packet receive.

                    //PacketCount
                    currentDecode = new byte[2] { data[3], data[2] };
                    tempPacket.packetNumber = Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16); //Formats the data to HEX then convers it all to a 16 bit int.

                    //Pressure
                    currentDecode = new byte[3] { data[6], data[5], data[4] };
                    tempPacket.pressure = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100; //Pressure ,,, I like it
                    
                    //Air Temp
                    currentDecode = new byte[2] { data[8], data[7] };
                    tempPacket.airTemp = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;  //Converting the hex to air temp

                    //Humidity
                    currentDecode = new byte[2] { data[10], data[9] };
                    tempPacket.humidity = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;    //Wet or dry

                    //Vbat
                    tempPacket.batteryVoltage = Convert.ToDouble(Convert.ToInt16(data[11].ToString("X"), 16)) / 10;    //The current battery voltage.

                    //Internal Temp
                    currentDecode = new byte[2] { data[13], data[12] };
                    tempPacket.internetTemp = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;   //Decoding internal air temp.

                    //Pressure Temp
                    currentDecode = new byte[2] { data[15], data[14] };
                    tempPacket.pressureTemp = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;   //Decoding pressure air temp.

                    //Humidity Temp
                    currentDecode = new byte[2] { data[17], data[16] };
                    tempPacket.humidityTemp = Convert.ToDouble(Convert.ToInt32(BitConverter.ToString(currentDecode).Replace("-", string.Empty), 16)) / 100;   //Decoding humidity air temp.

                    tempPacket.rawData = data;

                    dataPTU.Add(tempPacket);    //Adding the decocder packet to the list array.
                    packetPTUXReady.UpdatingObject = (object)data;   //Triggering event from PTU packet.
                    resetPTUTimer();
                    return tempPacket;

                case 05:    //GPSX Packet (30 bytes)
                    tempGPSPacket = new radiosondeGPSData();
                    //Local decode time.
                    tempGPSPacket.packetDateTime = DateTime.Now;

                    //Latitude
                    currentDecode = new byte[4] { data[5], data[4], data[3], data[2] };
                    tempGPSPacket.latitude = BitConverter.ToSingle(BitConverter.GetBytes(uint.Parse(BitConverter.ToString(currentDecode).Replace("-", string.Empty), System.Globalization.NumberStyles.AllowHexSpecifier)), 0);

                    //Longitude
                    currentDecode = new byte[4] { data[9], data[8], data[7], data[6] };
                    tempGPSPacket.longitude = BitConverter.ToSingle(BitConverter.GetBytes(uint.Parse(BitConverter.ToString(currentDecode).Replace("-", string.Empty), System.Globalization.NumberStyles.AllowHexSpecifier)), 0);

                    //Altitude
                    currentDecode = new byte[2] { data[11], data[10] };
                    tempGPSPacket.alt = uint.Parse(BitConverter.ToString(currentDecode).Replace("-", string.Empty), System.Globalization.NumberStyles.AllowHexSpecifier) - 5000;
                    //Sat Count
                    tempGPSPacket.satCount = (int)data[12];
                    //Time(hr,mm,ss)
                    tempGPSPacket.gpsTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, (int)data[13], (int)data[14], (int)data[15]);

                    tempGPSPacket.rawData = data;

                    dataGPS.Add(tempGPSPacket); //Adding decoding gps packet to list.
                    packetGPSReady.UpdatingObject = (object)data;   //Triggering gps event.
                    resetGPSTimer();
                    return tempGPSPacket;

            }

            return null;
        }

        private void resetPTUTimer()
        {
            if (timerPTUTimeOut != null)
            {
                timerPTUTimeOut.Enabled = false;
                timerPTUTimeOut.Enabled = true;
            }
        }

        private void resetGPSTimer()
        {
            if (timerGPSTimeOut != null)
            {
                timerGPSTimeOut.Enabled = false;
                timerGPSTimeOut.Enabled = true;
            }
        }

        public radiosondePTUData getLatestPTU()
        {
            return dataPTU[dataPTU.Count - 1];
        }

        public radiosondeGPSData getLatestGPS()
        {
            return dataGPS[dataGPS.Count - 1];
        }

        public radiosondeXDataData getLatestXData()
        {
            return dataXData[dataXData.Count - 1];
        }

        public radiosondePTUData getPTUPacket(int desiredPacket)
        {
            if (desiredPacket < dataPTU.Count)
            {
                return dataPTU[desiredPacket];
            }
            radiosondePTUData newOne = new radiosondePTUData();
            return newOne;
        }

        public radiosondeGPSData getGPSPacket(int desiredPacket)
        {
            if (desiredPacket < dataGPS.Count)
            {
                return dataGPS[desiredPacket];
            }
            radiosondeGPSData newOne = new radiosondeGPSData();
            return newOne;
        }


        #region Test Methods for inputing data into the object for testing.

        /// <summary>
        /// Method used to set the object into and cancel test mode. This will sim decoding a bell202 radiosonde.
        /// </summary>
        /// <param name="currentMode"></param>
        public void setTestMode(bool mode)
        {
            if (mode && !checkSimStatus())
            {
                //Setting up sim bell202 sonde.
                testRadiosonde = new simRadiosonde();       //Creating the sim radiosonde.
                testRadiosonde.ptuReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(ptuReady_PropertyChange);    //Connecting to sim ptu trigger.
                testRadiosonde.gpsReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(gpsReady_PropertyChange);    //Connecting to sim GPS trigger.

                //Starting the decoder processor
                backgroundWorkerDecodeData = new System.ComponentModel.BackgroundWorker();
                backgroundWorkerDecodeData.WorkerSupportsCancellation = true;
                backgroundWorkerDecodeData.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerDecodeData_DoWork);
                backgroundWorkerDecodeData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerDecodeData_RunWorkerCompleted);
                backgroundWorkerDecodeData.RunWorkerAsync();

                //Setting up and starting timout timers.
                timerPTUTimeOut = new System.Timers.Timer(1200);
                timerPTUTimeOut.Elapsed += new System.Timers.ElapsedEventHandler(timerPTUTimeOut_Elapsed);
                timerPTUTimeOut.Enabled = true;

                timerGPSTimeOut = new System.Timers.Timer(1200);
                timerGPSTimeOut.Elapsed += new System.Timers.ElapsedEventHandler(timerGPSTimeOut_Elapsed);
                timerGPSTimeOut.Enabled = true;

                testRadiosonde.start();     //Starting the sim.

            }
            else
            {
                testRadiosonde.stop();
                timerGPSTimeOut.Elapsed -= new System.Timers.ElapsedEventHandler(timerGPSTimeOut_Elapsed);
                timerGPSTimeOut = null;

                timerPTUTimeOut.Elapsed -= new System.Timers.ElapsedEventHandler(timerPTUTimeOut_Elapsed);
                timerPTUTimeOut = null;

                backgroundWorkerDecodeData.CancelAsync();

                testRadiosonde.gpsReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(gpsReady_PropertyChange);
                testRadiosonde.ptuReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(ptuReady_PropertyChange);

                testRadiosonde = null;
            }
        }

        void gpsReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            byte[] incomingData = (byte[])data.NewValue;
            //processData(incomingData);


            for (int r = 0; r < incomingData.Length; r++)
            {
                receivedData.Add(incomingData[r]);
            }
        }

        void ptuReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            byte[] incomingData = (byte[])data.NewValue;
            //processData(incomingData);

            
            for (int r = 0; r < incomingData.Length; r++)
            {
                receivedData.Add(incomingData[r]);
            }
            
        }

        bool checkSimStatus()
        {
            if (testRadiosonde == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Method for adjusting the instability of the sim radiosonde.
        /// </summary>
        /// <param name="desirePressure"></param>
        /// <param name="desireAirTemp"></param>
        /// <param name="desireHumidity"></param>
        /// <param name="desireGpsLatLong"></param>
        /// <param name="desireGpsAlt"></param>
        public void adjustInstability(int desirePressure, int desireAirTemp, int desireHumidity, int desireGpsLatLong, int desireGpsAlt)
        {
            testRadiosonde.setInstability(desirePressure, desireAirTemp, desireHumidity, desireGpsLatLong, desireGpsAlt);
        }


        #endregion

    }



    public struct radiosondePTUData
    {
        public DateTime packetDateTime;
        public int packetNumber;
        public double pressure;
        public double airTemp;
        public double humidity;
        public double batteryVoltage;
        public double internetTemp;
        public double pressureTemp;
        public double humidityTemp;
        public byte[] rawData;

    }

    public struct radiosondeGPSData
    {
        public DateTime packetDateTime;
        public double latitude;
        public double longitude;
        public double alt;
        public int satCount;
        public DateTime gpsTime;
        public byte[] rawData;
    }

    /// <summary>
    /// Data packet for the bad packets.
    /// </summary>
    public class radiosondeBadPacketData
    {
        public DateTime packetDateTime;
        public byte[] data;
    }

    public struct radiosondeXDataData
    {
        public DateTime packetDateTime;
        public byte[] data;
    }


    public class simRadiosonde
    {
        //Public items.
        public updateCreater.objectUpdate ptuReady = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate gpsReady = new updateCreater.objectUpdate();

        //Private settings.
        //PTU
        int packetCount = 1;
        double currentPressure = 990;
        int pressureInstability = 1;    //Instability factor for pressure. *Note it will need to be incressed as pressure is decressed to sim normal operation.
        double currentAirTemp = 20.0;
        int airTempInstability = 1;
        double currentHumidity = 30.0;
        int humidityInstability = 1;

        //Radiosonde stuff
        double vbat = 5.0;
        double internalAirTemp = 25.0;

        //GPS
        double currentLat = 42.8937776852398;
        double currentLong = -85.5701756570488;
        int gpsLatLongInstability = 1;
        double currentAlt = 210;
        int gpsAltInstability = 1;
        int gpsSatCount = 6;

        private System.Timers.Timer ptuPacketTimer;
        private System.Timers.Timer gpsPacketTimer;
        private Random instability = new Random();

        public string mode = "bell";

        public simRadiosonde() { }

        public void start()
        {
            if (ptuPacketTimer == null)
            {
                ptuPacketTimer = new System.Timers.Timer(1000);
                ptuPacketTimer.Elapsed += new System.Timers.ElapsedEventHandler(ptuPacketTimer_Elapsed);
                
            }
            if (gpsPacketTimer == null)
            {
                gpsPacketTimer = new System.Timers.Timer(1000);
                gpsPacketTimer.Elapsed += new System.Timers.ElapsedEventHandler(gpsPacketTimer_Elapsed);
                
            }

            ptuPacketTimer.Enabled = true;
            System.Threading.Thread.Sleep(100);
            gpsPacketTimer.Enabled = true;
        }

        public void stop()
        {
            if (ptuPacketTimer != null)
            {
                ptuPacketTimer.Enabled = false;
                ptuPacketTimer.Elapsed -= new System.Timers.ElapsedEventHandler(ptuPacketTimer_Elapsed);
                ptuPacketTimer = null;
            }
            if (gpsPacketTimer != null)
            {
                gpsPacketTimer.Enabled = false;
                gpsPacketTimer.Elapsed -= new System.Timers.ElapsedEventHandler(gpsPacketTimer_Elapsed);
                gpsPacketTimer = null;
            }
        }

        #region Methods for creating/formating radiosdones sim data

        void gpsPacketTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            switch (mode)
            {
                case "bell":

                    byte[] gpsPaketData = new byte[18];     //GPS packet (18 bytes)
                    double eventReading = 0;    //Veriable for holding effected event numbers.

                    gpsPaketData[0] = 1;
                    gpsPaketData[1] = 2;

                    //GPSLat
                    byte[] gpsLat = BitConverter.GetBytes(BitConverter.ToSingle(BitConverter.GetBytes(currentLat), 0));
                    long longValue = BitConverter.DoubleToInt64Bits(currentLat);

                    //Can not seems to get Lat and Long to work as of 04/10/2013 I am hard coding it to the location I am testing to.
                    //0x42 2b 93 3a
                    gpsPaketData[2] = 0x3A;
                    gpsPaketData[3] = 0x93;
                    gpsPaketData[4] = 0x2B;
                    gpsPaketData[5] = 0x42;

                    //GPSLong
                    //0xc2 ab 23 ee
                    byte[] gpsLong = BitConverter.GetBytes(BitConverter.ToSingle(BitConverter.GetBytes(currentLong), 0));
                    gpsPaketData[6] = 0xEE;
                    gpsPaketData[7] = 0x23;
                    gpsPaketData[8] = 0xAB;
                    gpsPaketData[9] = 0xC2;

                    //GPSAlt
                    gpsPaketData[10] = (byte)(currentAlt + 5000);
                    gpsPaketData[11] = (byte)(Convert.ToInt32(currentAlt + 5000) >> 8);

                    //GPSSat
                    gpsPaketData[12] = (byte)gpsSatCount;

                    //GPSTime
                    gpsPaketData[13] = (byte)DateTime.UtcNow.Hour;
                    gpsPaketData[14] = (byte)DateTime.UtcNow.Minute;
                    gpsPaketData[15] = (byte)DateTime.UtcNow.Second;

                    byte[] dataForChecksum = new byte[16];
                    for (int i = 0; i < dataForChecksum.Length; i++)
                    {
                        dataForChecksum[i] = gpsPaketData[i];
                    }

                    crcCheckSum.Crc16Ccitt calc = new crcCheckSum.Crc16Ccitt(crcCheckSum.InitialCrcValue.NonZero2); //CRC-CCITT (0x1D0F) For checksum... Bitches love checksum's
                    byte[] checksum = calc.ComputeChecksumBytes(dataForChecksum);

                    gpsPaketData[16] = checksum[0];
                    gpsPaketData[17] = checksum[1];

                    gpsReady.UpdatingObject = (object)gpsPaketData;

                    break;

                case"RSB":

                    break;

            }

        }

        void ptuPacketTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            switch (mode)
            {
                case "bell":

                    byte[] ptuPacketData = new byte[20];    //PTUX Packet (20 bytes)
                    double eventReading = 0;    //Veriable for holding effected event numbers.

                    ptuPacketData[0] = 1;
                    ptuPacketData[1] = 4;

                    //Packet Count
                    ptuPacketData[2] = (byte)packetCount;
                    ptuPacketData[3] = (byte)(packetCount >> 8);

                    //Pressure
                    eventReading = currentPressure * (((Convert.ToDouble(instability.Next(0, pressureInstability * 10)) / 100000) * currentInstabilityDir()) + 1);
                    ptuPacketData[4] = (byte)Convert.ToInt32(eventReading * 100);
                    ptuPacketData[5] = (byte)(Convert.ToInt32(eventReading * 100) >> 8);
                    ptuPacketData[6] = (byte)(Convert.ToInt32(eventReading * 100) >> 16);

                    //Air Temp
                    eventReading = currentAirTemp * (((Convert.ToDouble(instability.Next(0, airTempInstability * 10)) / 10000) * currentInstabilityDir()) + 1);
                    ptuPacketData[7] = (byte)Convert.ToInt32(eventReading * 100);
                    ptuPacketData[8] = (byte)(Convert.ToInt32(eventReading * 100) >> 8);

                    //Humidity
                    eventReading = currentHumidity * (((Convert.ToDouble(instability.Next(0, humidityInstability * 10)) / 10000) * currentInstabilityDir()) + 1);
                    ptuPacketData[9] = (byte)Convert.ToInt32(eventReading * 100);
                    ptuPacketData[10] = (byte)(Convert.ToInt32(eventReading * 100) >> 8);

                    //Vbat
                    eventReading = vbat * (((Convert.ToDouble(instability.Next(0, 1 * 10)) / 1000) * currentInstabilityDir()) + 1);
                    ptuPacketData[11] = (byte)Convert.ToInt32(eventReading * 10);

                    //Internal Air Temp
                    eventReading = internalAirTemp * (((Convert.ToDouble(instability.Next(0, 1 * 10)) / 10000) * currentInstabilityDir()) + 1);
                    ptuPacketData[12] = (byte)Convert.ToInt32(eventReading * 100);
                    ptuPacketData[13] = (byte)(Convert.ToInt32(eventReading * 100) >> 8);

                    //Pressure Temp
                    eventReading = internalAirTemp * (((Convert.ToDouble(instability.Next(0, 1 * 10)) / 10000) * currentInstabilityDir()) + 1);
                    ptuPacketData[14] = (byte)Convert.ToInt32(eventReading * 100);
                    ptuPacketData[15] = (byte)(Convert.ToInt32(eventReading * 100) >> 8);

                    //Humidity Air Temp
                    eventReading = currentAirTemp * (((Convert.ToDouble(instability.Next(0, airTempInstability * 10)) / 10000) * currentInstabilityDir()) + 1);
                    ptuPacketData[16] = (byte)Convert.ToInt32(eventReading * 100);
                    ptuPacketData[17] = (byte)(Convert.ToInt32(eventReading * 100) >> 8);

                    byte[] dataForChecksum = new byte[18];
                    for (int i = 0; i < dataForChecksum.Length; i++)
                    {
                        dataForChecksum[i] = ptuPacketData[i];
                    }

                    crcCheckSum.Crc16Ccitt calc = new crcCheckSum.Crc16Ccitt(crcCheckSum.InitialCrcValue.NonZero2); //CRC-CCITT (0x1D0F) For checksum... Bitches love checksum's
                    byte[] checksum = calc.ComputeChecksumBytes(dataForChecksum);

                    ptuPacketData[18] = checksum[0];
                    ptuPacketData[19] = checksum[1];

                    packetCount = packetCount + 2;  //Indexing packet counter
                    if (packetCount > 56000)
                    {
                        packetCount = 0;
                    }

                    //Console.Write("Data Test: " + ptuPacketData[6].ToString("X") + " " + ptuPacketData[5].ToString("X") + " " + ptuPacketData[4].ToString("X") + "\n");
                    ptuReady.UpdatingObject = (object)ptuPacketData;

                    break;

                case "RSB":

                    break;
            }
        }

        /// <summary>
        /// Sets the direction of the instability.
        /// </summary>
        /// <returns></returns>
        int currentInstabilityDir()
        {
            int curtDir = -1;
            int cNum = instability.Next(0, 2);
            if (cNum == 1)
            {
                curtDir = 1;
            }
            return curtDir;
        }

        #endregion

        #region Methods for setting settings to the simRadiosonde
        public void setGPSLOC(double gpsLat, double gpsLong, double gpsAlt)
        {
            currentLat = gpsLat;
            currentLong = gpsLong;
            currentAlt = gpsAlt;
        }

        public void setPTU(double pressure, double airTemp, double humidity)
        {
            currentPressure = pressure;
            currentAirTemp = airTemp;
            currentHumidity = humidity;
        }

        public void setGPS(double gpsLat, double gpsLong, double gpsAlt)
        {
            currentLat = gpsLat;
            currentLong = gpsLong;
            currentAlt = gpsAlt;
        }

        public void setInstability(int pressure, int airTemp, int humidity, int gpsLatLong, int gpsAlt)
        {
            pressureInstability = pressure;
            airTempInstability = airTemp;
            humidityInstability = humidity;
            gpsLatLongInstability = gpsLatLong;
            gpsAltInstability = gpsAlt;
        }
        #endregion

    }

}

