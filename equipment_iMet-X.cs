﻿///William Jones
///10/16/2014

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace equipment_Sensors
{
    public class equipment_iMet_X
    {
        //Configuration items
        List<equipment_sensor_board> sensorBoards = new List<equipment_sensor_board>();     //list of all the sensor boards.
        public double dataRate = 1;
        List<SettingsLoader.sensor> settings;   //Settings for the mainboards sub sensors.

        //Communication items.
        System.IO.Ports.SerialPort comPort;     //Sensor com port.
        public Client_TCP.tcpClient clientTCP; //TCP Client Connection.
        public string comMode = "comport"; //Type of communication port.
        bool runComm = false;
        List<string> inBuffer = new List<string>();
        List<string> outBuffer = new List<string>();
        DateTime lastDataReceived = DateTime.Now;

        //Weather the system is in download mode or not.
        public bool downloadingData = false;
        List<string> downloadedData = new List<string>();
        public Int32 downloadedCount = 0;

        //Event operators
        public updateCreater.objectUpdate rawDataToSensor = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate rawDataFromSensor = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate dataReady = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate updateBoardCount = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate updateDataRate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate updateMEMResponce = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate updateCNTResponce = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate newBoardConnected = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate errorEvent = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate downloadComplete = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate updateLoggingState = new updateCreater.objectUpdate();

        //List of command the the iMet-XQ can process.
        string[] iMetXCommands = new string[] { "ERA", "STA", "STP", "RDA", "RDB", "MEM", "WRD", "WRE", "RST", "SRN", "SAV", "DRR", "ST", "CNT", "CPB", "CTB", "CHB", "FMO", "AD1", "AD2", "AD3", "AD4" };

        public string iMetXQSN = "";
        public double iMetXQMEM = 0;
        public Int32 iMetXQPC = 0;
        public int iMetXQFlashMode = -1;

        //Bias shifts
        public double pressureBias = 0;
        public double tempBias = 0;
        public double humidityBias = 0;

        //Is this board a iMet-XQ or not.
        public bool amIaQ = false;


        //iMet-XF Data items
        public string iMetXFSN = "";
        //ADC Settings
        public string AD1 = "";
        public string AD2 = "";
        public string AD3 = "";
        public string AD4 = "";

        //Firmware version number
        public string versionFW = "";

        /// <summary>
        /// Basic setup.
        /// </summary>
        public equipment_iMet_X()
        {
            loadSettings();
        }

        /// <summary>
        /// Create connection to iMet-X Board via a com port.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        public equipment_iMet_X(string port, int baudRate)
        {
            loadSettings();

            if (comPort != null)
            {
                intLogging("Stopping current communication and clearing remaining data.");
                //Shutting down current comms and clearing connection data.
                runComm = false;
                outBuffer.Clear();
                inBuffer.Clear();
                comPort = null;

            }
            else
            {
                //setting up the connection to the incoming data.
                rawDataFromSensor.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(rawDataFromSensor_PropertyChange);
            }


            //Setting up the com port.
            intLogging("Setting up Com port " + port + ":" + baudRate.ToString());
            comPort = new System.IO.Ports.SerialPort(port, baudRate, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            
            //Stating coms threads.
            runComm = true;
            //Setting up the in thread.
            System.Threading.Thread inThread = new System.Threading.Thread(receiveDataThread);
            inThread.Name = "InComThread";
            inThread.IsBackground = true;
            inThread.Start();
            //Setting up the out thread.
            System.Threading.Thread outThread = new System.Threading.Thread(sendDataThread);
            outThread.Name = "OutComThread";
            outThread.IsBackground = true;
            outThread.Start();

        }

        public equipment_iMet_X(string ip, int port, string type, string SN)
        {
            loadSettings(); //Loading the sensor settings.

            runComm = true; //Allowing com threads to run.

            comMode = "tcp";    //Setting the commication port to TCP/IP Network.
            iMetXQSN = SN;      //Passing required Serial Number
            iMetXQFlashMode = 0;    //Overriding the GPS log modes. (It is hack baby!)
            iMetXFSN = SN;      //Passing required Serial Number

            //Pointing out the data is xq.
            if (type.ToLower().Contains("xq"))
            {
                amIaQ = true;
            }
            else
            {
                amIaQ = false;
            }

            //Connecting to the port.
            clientTCP = new Client_TCP.tcpClient(ip, port); //Passing connection information to connection.


            clientTCP.tcpError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tcpError_PropertyChange); //Connecting to Error Events.
            clientTCP.tcpData.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tcpData_PropertyChange);   //Subing to the incoming data stream event.
            
            //setting up the connection to the incoming data.
            rawDataFromSensor.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(rawDataFromSensor_PropertyChange);

            System.Threading.Thread connectionMonitor = new System.Threading.Thread(threadMonitor);
            connectionMonitor.Name = "connectionMonitor";
            connectionMonitor.IsBackground = true;
            connectionMonitor.Start();

        }

        void threadMonitor()
        {
            while (runComm)
            {
                TimeSpan passed = (DateTime.Now - lastDataReceived);
                System.Diagnostics.Debug.WriteLine("Since last: " + passed.TotalMilliseconds.ToString());

                if (passed.TotalMilliseconds > 3000)
                {
                    errorEvent.UpdatingObject = new Exception("Error,TCP/IP,No Sensor Data," + passed.TotalMilliseconds.ToString("0"));
                    System.Threading.Thread.Sleep(30000);
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                }



            }
        }

        void tcpData_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Client_TCP.dataPacket incoming = (Client_TCP.dataPacket)data.NewValue;
            rawDataFromSensor.UpdatingObject = incoming.packetMessage;

        }

        void tcpError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            errorEvent.UpdatingObject = data.NewValue;
        }


        public void disconnect()
        {
            //Stopping all loops.
            runComm = false;

            //Closing TCP connection.
            if(comMode == "tcp")
            {
                clientTCP.disconnectConnection();
                clientTCP.client.Close();
            }
        }

        //Method returns the serial port to the user.
        public System.IO.Ports.SerialPort getSerialPort()
        {
            return comPort;
        }

        public List<equipment_sensor_board> getSensorsBoards()
        {
            return sensorBoards;
        }

        /// <summary>
        /// Gets the current device firmware versiona and formats it to a double.
        /// </summary>
        /// <returns>double</returns>
        public double getFirmwareVersion()
        {
            string fwBreak = versionFW.Remove(0, 1);
            fwBreak = fwBreak.Substring(0, 4);

            return Convert.ToDouble(fwBreak);
        }

        /// <summary>
        /// XML Loader for iMet-XF settings.
        /// </summary>
        public void loadSettings(string fileName = "SensorSettings.xml")
        {
                SettingsLoader.loadXMLSettings loader = new SettingsLoader.loadXMLSettings();
                settings = loader.loadSessionXML(AppDomain.CurrentDomain.BaseDirectory + fileName);

        }

        /// <summary>
        /// Gets the current sensor settings.
        /// </summary>
        /// <returns></returns>
        public List<SettingsLoader.sensor> getCurSensorSettings()
        {
            return settings;
        }

        #region Methods for handeling i/o to sensor plateform

        //Thread for receiving data from sensor main board.
        void receiveDataThread()
        {
            while (runComm)
            {
                if (!comPort.IsOpen)
                {
                    try
                    {
                        comPort.Open();
                        addCommandToQue("/RST");    //Resetting the board.
                    }
                    catch (Exception error)
                    {
                        errorEvent.UpdatingObject = error;
                        break;
                    }
                }

                try
                {
                    string incomingData = comPort.ReadLine();
                    rawDataFromSensor.UpdatingObject = incomingData;
                }
                catch (System.IO.IOException comError)
                {
                    runComm = false;
                    comPort.Close();
                }
            }

            //Communication has been canceled. Closing port.
            comPort.Close();
        }

        /// <summary>
        /// Method adds command to outgoing que to be processed as soon as the system can.
        /// </summary>
        /// <param name="command"></param>
        public void addCommandToQue(string command)
        {
            outBuffer.Add(command);
        }

        //Thread for sending data.
        void sendDataThread()
        {
            while(runComm)
            {
                //Looking to see if anything need to be sent to the sensor plateform.
                if (outBuffer.Count > 0)
                {
                    comPort.DiscardOutBuffer();
                    comPort.Write(outBuffer[0] + "\r\n");
                    rawDataToSensor.UpdatingObject = outBuffer[0];
                    outBuffer.RemoveAt(0);

                    //A small pause to prevent buffer overrun in the device.
                    System.Threading.Thread.Sleep(100);
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// Broken do to the XF multi line sensor messages.
        /// </summary>
        void calcDataRate()
        {
            TimeSpan diff = DateTime.Now - lastDataReceived;

            dataRate = 1 / diff.TotalSeconds;

            /*
            if (secDiff > 0)    //Protecting divide by 0
            {

                if (Convert.ToInt16(1 / secDiff) > dataRate + 3 || Convert.ToInt16(1 / secDiff) < dataRate - 3)
                {
                    dataRate = Convert.ToInt16(1 / secDiff);

                    if (dataRate > 20)
                    {
                        dataRate = 20;
                    }

                    if (dataRate <= 0)  //Protection from 0
                    {
                        dataRate = 1;
                    }

                    
                    updateDataRate.UpdatingObject = dataRate;
                    //System.Diagnostics.Debug.WriteLine("Sample Rate:" + dataRate.ToString());
                }
            }
             * */

        }

        #endregion

        /// <summary>
        /// Method for starting the processing of data incoming data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void rawDataFromSensor_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Time stamping and packaging the data into a data packet.
            dataPacket newData = new dataPacket();
            newData.dateReceived = DateTime.Now;
            newData.rawData = (string)data.NewValue;


            //lastDataReceived = DateTime.Now;    //Updating the last time we heard from the sensor.

            string dataCheck = newData.rawData.TrimEnd();

            if (dataCheck.Length < 1) //Checking to make sure the data packet contains data.
            {
                return; //stopping the process if there is no new data.
            }

            //Checking to see if this board is a XQ.
            if (newData.rawData.Contains("XQ") && iMetXQSN == "" && !outBuffer.Contains("/SRN?"))
            {
                getSerialNumber();
            }

            //Getting the iMet-XF serial number.
            if (newData.rawData.Substring(0, 1) == "0" && iMetXFSN == "" && !outBuffer.Contains("/SRN?"))
            {
                getSerialNumber();
            }

            if (versionFW != "" && !amIaQ )
            {
                if (Convert.ToDouble(versionFW.Substring(1, versionFW.Length - 1)) >= 2.00)
                {

                    //Getting the iMet-XF ADC1
                    if (newData.rawData.Substring(0, 1) == "0" && AD1 == "" && !outBuffer.Contains("/AD1?"))
                    {
                        getADCSetting("AD1");
                    }

                    //Getting the iMet-XF ADC2
                    if (newData.rawData.Substring(0, 1) == "0" && AD2 == "" && !outBuffer.Contains("/AD2?"))
                    {
                        getADCSetting("AD2");
                    }

                    //Getting the iMet-XF ADC3
                    if (newData.rawData.Substring(0, 1) == "0" && AD3 == "" && !outBuffer.Contains("/AD3?"))
                    {
                        getADCSetting("AD3");
                    }

                    //Getting the iMet-XF ADC4
                    if (newData.rawData.Substring(0, 1) == "0" && AD4 == "" && !outBuffer.Contains("/AD4?"))
                    {
                        getADCSetting("AD4");
                    }
                }
            }

            if (newData.rawData.Contains("XQ") && iMetXQFlashMode == -1 && !outBuffer.Contains("/FMO?"))
            {
                getFlashMode();
            }


            //Checking to see if a new board is connected.
            if (newData.rawData.Contains("iMet-X Control Board") | newData.rawData.Contains("iMet-XQ"))
            {
                //Resetting everything and telling the sub that I am doing so.
                resetForNewBoard(newData.rawData);

            }

            //Erasing the data has concluded. Restarting data if needed.
            if (newData.rawData.Contains("Starting to erase...OK"))
            {

            }

            //Checking for iMet-X command responces.
            for (int i = 0; i < iMetXCommands.Length; i++)
            {
                if(newData.rawData.Contains(iMetXCommands[i]))
                {
                    processiMetXResponce(newData);
                }
            }

            if (newData.rawData.Contains("XQ"))
            {
                amIaQ = true;
            }


            //Calculateing the data rate.
            calcDataRate();
            lastDataReceived = DateTime.Now;

            processData(newData);

        }

        private void processiMetXResponce(dataPacket data)
        {
            if (data.rawData.Contains("MEM="))
            {
                string[] lineBreak = data.rawData.Split('=');
                iMetXQMEM = Convert.ToDouble(lineBreak[1])/10;
                updateMEMResponce.UpdatingObject = (object)iMetXQMEM;

            }

            if (data.rawData.Contains("CNT="))
            {
                string[] lineBreak = data.rawData.Split('=');
                iMetXQPC = Convert.ToInt32(lineBreak[1]);
                updateCNTResponce.UpdatingObject = (object)iMetXQPC;

            }

            if (data.rawData.Contains("SRN="))
            {
                string[] lineBreak = data.rawData.Split('=');
                iMetXQSN = lineBreak[1];
                iMetXFSN = lineBreak[1];
            }

            if (data.rawData.Contains("ERR="))
            {

            }

            if (data.rawData.Contains("CPB="))
            {
                string[] lineBreak = data.rawData.Split('=');
                pressureBias = Convert.ToDouble(lineBreak[1]) / 100;
            }

            if (data.rawData.Contains("CTB="))
            {
                string[] lineBreak = data.rawData.Split('=');
                tempBias = Convert.ToDouble(lineBreak[1]) / 100;
            }

            if (data.rawData.Contains("CHB="))
            {
                string[] lineBreak = data.rawData.Split('=');
                humidityBias = Convert.ToDouble(lineBreak[1]) / 10;
            }

            if (data.rawData.Contains("FMO="))
            {
                string[] lineBreak = data.rawData.Split('=');
                iMetXQFlashMode = Convert.ToInt16(lineBreak[1]);
                updateLoggingState.UpdatingObject = iMetXQFlashMode;
            }

            if (data.rawData.Contains("AD1="))
            {
                string[] lineBreak = data.rawData.Split('=');
                AD1 = lineBreak[1].TrimEnd();
            }

            if (data.rawData.Contains("AD2="))
            {
                string[] lineBreak = data.rawData.Split('=');
                AD2 = lineBreak[1].TrimEnd();
            }

            if (data.rawData.Contains("AD3="))
            {
                string[] lineBreak = data.rawData.Split('=');
                AD3 = lineBreak[1].TrimEnd();
            }

            if (data.rawData.Contains("AD4="))
            {
                string[] lineBreak = data.rawData.Split('=');
                AD4 = lineBreak[1].TrimEnd();
            }
        }

        void processData(dataPacket data)
        {
            //Breaking down data.
            string[] dataItems = data.rawData.Trim().Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);



            //Check to see if sensor board is present or needs to be added.
            if (!checkForBoard(dataItems))
            {
                addSensorBoard(dataItems[0]);
            }

            //Bringing in board to be updated.
            equipment_sensor_board updating = getSensorBoard(dataItems[0]);

            //Check to see if sensors on the board are present or need to be added.
            if (updating != null)   //Double check error protection.
            {
                //Checking to make sure all sensors data has a place to go.
                if (updating.sensors.Count != dataItems.Length)
                {
                    //Getting the number missing.
                    int missingCount = (dataItems.Length - 1) - updating.sensors.Count;

                    for(int i = 0; i<missingCount; i++)
                    {
                        //Creating new sensor and adding it to the list of sensors on this board.
                        equipment_sensor_element tempSensor = new equipment_sensor_element();
                        tempSensor.sensorDataPos = updating.sensors.Count;
                        tempSensor.sensorSampleRate = (int)dataRate;
                        updating.sensors.Add(tempSensor);
                    }
                }

                
                    updateSensorElement(updating, dataItems);
                    


                if (downloadingData)
                {
                    downloadedData.Add(data.rawData.Trim());
                    downloadedCount++;
                }



            }

            //Sending out the update ready event
            dataReady.objectID = comPort.PortName;
            dataReady.UpdatingObject = data;

        }


        void updateSensorElement(equipment_sensor_board updating, string[] dataItems)
        {
            foreach (string check in dataItems)
            {
                if (check.Contains("iMet-X"))
                {
                    return;
                }
            }


            //Aligning the sensor settings to the sensor being updated.
            foreach (SettingsLoader.sensor s in settings)
            {
                if (dataItems[0] == s.id) //Found a match.
                {
                    for (int i = 1; i < dataItems.Length; i++)
                    {
                        if (i <= s.element.Count)
                        {
                            if (s.element[i - 1].format == -1)      //Checking to see if the formatting is for a string.
                            {
                                updating.sensors[i - 1].curDataTime = DateTime.Now;     //Setting the current time of posting.
                                updating.sensors[i - 1].curSReading = dataItems[s.element[i - 1].pos];      //Updating the string value with the string data.
                            }
                            else
                            {
                                try
                                {
                                    updating.sensors[i - 1].curDataTime = DateTime.Now; //Setting the current time of posting.
                                    updating.sensors[i - 1].curReading = Convert.ToDouble(dataItems[s.element[i - 1].pos]) / s.element[i - 1].format;   //Updating and converting to double.
                                }
                                catch (Exception error)
                                {
                                    System.Diagnostics.Debug.WriteLine(error.Message);
                                    errorEvent.UpdatingObject = error;
                                }
                            }

                            updating.sensors[i - 1].sensorType = s.element[i - 1].name;
                        }
                    }
                }
            }
        }




        /// <summary>
        /// Method resets sensors for a new board connected.
        /// </summary>
        void resetForNewBoard(string startMessage)
        {
            //Triggering event to alert that a change has happen.
            newBoardConnected.objectID = comPort.PortName;
            newBoardConnected.UpdatingObject = DateTime.Now;

            //Resetting the attached sensor boards.
            sensorBoards = new List<equipment_sensor_board>();
            
            //Resetting the data buffers.
            inBuffer = new List<string>();
            outBuffer = new List<string>();

            downloadingData = false;
            downloadedData = new List<string>();
            downloadedCount = 0;

            iMetXQSN = "";
            iMetXQFlashMode = -1;

            //Resetting iMetXF
            iMetXFSN = "";

            //Resetting iMet-XQ marker.
            amIaQ = false;

            string[] startSplit = startMessage.Trim().Split(' ');

            versionFW = startSplit.Last();

            //Checking for a iMet-XQ and setting the system for XQ mode and loading the settings for the version.
            if (startSplit[0].Contains("XQ"))
            {
                amIaQ = true;   //Hardware is a XQ.
                double firmwareVersion = getFirmwareVersion();

                loadSettings("XQSettings\\XQ" + (firmwareVersion * 100).ToString() + "_Settings.xml");

            }
            else
            {
                loadSettings(); //Loading the default.
            }

            AD1 = "";
            AD2 = "";
            AD3 = "";
            AD4 = "";

        }

        bool checkForBoard(string[] curData)
        {
            foreach (equipment_sensor_board sb in sensorBoards)
            {
                if (curData.Length > 0)
                {
                    if (sb.boardID == curData[0])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        void addSensorBoard(string id)
        {
            //Creating temp board.
            equipment_sensor_board tempBoard = new equipment_sensor_board();
            tempBoard.boardID = id;
            sensorBoards.Add(tempBoard);
        }

        void removeSensorBoard(string id)
        {
            foreach (equipment_sensor_board sb in sensorBoards)
            {
                if (sb.boardID == id)
                {
                    sensorBoards.Remove(sb);
                }
            }
        }

        /// <summary>
        /// Method gets the requested sensor board id. Returns null if not found.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public equipment_sensor_board getSensorBoard(string id)
        {
            foreach (equipment_sensor_board sb in sensorBoards)
            {
                if (sb.boardID == id)
                {
                    return sb;
                }
            }

            return null;
        }

        void addSensorElement(string id, equipment_sensor_element sensor)
        {
            foreach (equipment_sensor_board sb in sensorBoards)
            {
                if (sb.boardID == id)
                {
                    sb.sensors.Add(sensor);
                }
            }
        }

        void removeSensorElement(string id, equipment_sensor_element sensor)
        {
            foreach (equipment_sensor_board sb in sensorBoards)
            {
                if (sb.boardID == id)
                {
                    sb.sensors.Remove(sensor);
                }
            }
        }

        /// <summary>
        /// Debug/Dianostics logging.
        /// </summary>
        /// <param name="message"></param>
        void intLogging(string message)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fffff") + "- " + message);
        }

        //iMet-XF Save
        public void saveConfig()
        {
            outBuffer.Add("/SAV");
        }

        #region Method related to the iMet-XQ

        /// <summary>
        /// Method for getting data from the iMet-XQ flash memory.
        /// </summary>
        public void getDataFromMemory(string outputFileName)
        {
            //Stopping the iMet-XQ from reporting data.
            outBuffer.Add("/STP");

            //Getting total data in memory
            getPacketCount();

            //Reporting that the downloading it happening.
            downloadingData = true;

            //Reading data from the iMet-XQ
            outBuffer.Add("/RDA");

            System.Threading.Thread checkDownloadStatus = new System.Threading.Thread(threadCheckDownload);
            checkDownloadStatus.Name = "Check download status";
            checkDownloadStatus.Start(outputFileName);
        }

        




        public void clearDataFromMemory()
        {
            outBuffer.Add("/ERA");
        }

        public void startData(string fileName)
        {
            downloadingData = false;
            outBuffer.Add("/STA");

            if (downloadedData.Count > 0)
            {
                downloadedData.RemoveAt(0); //Removing the first line. Sometimes it has a line from the data stream.


                //Reviewing the data for bad data.
                reviewDownloadData();

                downloadComplete.objectID = fileName;
                downloadComplete.UpdatingObject = downloadedData;

                //string f = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-iMet-XQ Export.csv";
                //System.IO.File.WriteAllLines(f, downloadedData.ToArray());

                //System.Windows.Forms.MessageBox.Show("Data exported to:\n" + f, "Data Export", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }

            //Clearing the data in read buffer.
            downloadedData.Clear();
        }

        void reviewDownloadData()
        {
            List<string> filteredData = new List<string>();

            //Looking through all the data.
            for (int i = 0; i < downloadedData.Count; i++)
            {
                //Filtering out anything that might not have the XQ id.
                if (downloadedData[i].Contains("XQ"))
                {
                    //Getting the date.
                    string[] lineBreak = downloadedData[i].Split(',');
                    string[] date = lineBreak[4].Split('/');

                    //Filtering anything with a data less then 2014.
                    if (Convert.ToInt16(date[0]) > 2014)
                    {
                        //Adding the acceptable data to the filtered list.
                        filteredData.Add(iMetXQSN.Trim() + "," + downloadedData[i]);
                    }
                }
            }

            //Appling the filtered data to the main data list.
            downloadedData = filteredData;

        }

        public void setSerialNumber(string sn)
        {
            outBuffer.Add("/SRN=" + sn);

            //Setting local memory.
            iMetXQSN = sn;
            iMetXFSN = sn;

            saveConfig();
        }

        public void getSerialNumber()
        {
            outBuffer.Add("/SRN?");
        }

        /// <summary>
        /// Method to get the ADC settings. AD1-AD4.
        /// </summary>
        /// <param name="ADChannel"></param>
        public void getADCSetting(string ADChannel)
        {
            outBuffer.Add("/" + ADChannel + "?");
        }

        /// <summary>
        /// Method for setting the mode of the ADC channel.
        /// </summary>
        /// <param name="ADChannel"></param>
        /// <param name="mode"></param>
        public void setADCSetting(string ADChannel, string mode)
        {
            outBuffer.Add("/" + ADChannel + "=" + mode);
        }

        /// <summary>
        /// Resets the hardware.
        /// </summary>
        public void resetSystem()
        {
            outBuffer.Add("/RST");
        }

        public void setCoefs(string coefType, string[] coefs)
        {
            if (coefs.Length == 4)
            {
                outBuffer.Add("/" + coefType + "0=" + coefs[0]);
                outBuffer.Add("/" + coefType + "1=" + coefs[1]);
                outBuffer.Add("/" + coefType + "2=" + coefs[2]);
                outBuffer.Add("/" + coefType + "3=" + coefs[3]);
                saveConfig();
            }
            else
            {
                Exception setCoefError = new Exception("Error loading coefficients. Only " + coefs.Length.ToString() + " sent to the process.");
                errorEvent.UpdatingObject = setCoefError;
            }
        }

        public void getMEMPrecent()
        {
            outBuffer.Add("/MEM?");
        }

        public void getPacketCount()
        {
            outBuffer.Add("/CNT?");
        }

        void threadCheckDownload(object incoming)
        {
            string fileName = (string)incoming;

            while(true)
            {
                TimeSpan timePassed = DateTime.Now - lastDataReceived;

                if (timePassed >= new TimeSpan(0, 0, 2))
                {
                    startData(fileName);
                    break;
                }

                System.Threading.Thread.Sleep(2000);

            }
        }

        /// <summary>
        /// Sets the pressure bias settings
        /// Don't forget to save.
        /// </summary>
        /// <param name="bias"></param>
        public void setPressureBias(double bias)
        {
            addCommandToQue("/CPB=" + Convert.ToInt16(bias * 100).ToString());
        }

        public void getPressureBias()
        {
            addCommandToQue("/CPB?");
        }

        /// <summary>
        /// Set a Temperature bias
        /// Don't forget to save.
        /// </summary>
        /// <param name="bias"></param>
        public void setTempBias(double bias)
        {
            addCommandToQue("/CTB=" + Convert.ToInt16(bias * 100).ToString());
        }

        public void getTempBias()
        {
            addCommandToQue("/CTB?");
        }

        /// <summary>
        /// Sets the humidity bias.
        /// Don't forget to save.
        /// </summary>
        /// <param name="bias"></param>
        public void setHumidityBias(double bias)
        {
            addCommandToQue("/CHB=" + Convert.ToInt16(bias * 10).ToString());
        }

        public void getHumidityBias()
        {
            addCommandToQue("/CHB?");
        }

        /// <summary>
        /// Set Flash mode
        ///0 = Save disabled
        ///1 = Save only with valid GPS (default)
        ///2 = Save all data
        /// </summary>
        public void setFlashMode(int mode)
        {
            addCommandToQue("/FMO=" + mode.ToString());
            saveConfig();
        }

        /// <summary>
        /// Gets the currect data logging mode.
        /// </summary>
        public void getFlashMode()
        {
            addCommandToQue("/FMO?");
        }

        #endregion

    }

    #region Sim stuff to get and idea what Joe B. was looking for. Might not match how the real system works.
    /// <summary>
    /// Class to sim iMet-X sensor
    /// </summary>
    public class equipment_iMet_X_Sim
    {
        //Event operators
        public updateCreater.objectUpdate rawDataToSensor;
        public updateCreater.objectUpdate rawDataFromSensor;
        public updateCreater.objectUpdate dataReady;

        //Thread to fake serial comms.
        System.Threading.Thread coms;
        bool comsRun = false;
        List<System.Threading.AutoResetEvent> sensorLock = new List<System.Threading.AutoResetEvent>();

        //Sensors
        public List<equipment_sensor_element_Sim> sensors = new List<equipment_sensor_element_Sim>();

        int reportDelay = 1000;

        string equipmentID;

        public equipment_iMet_X_Sim()
        {
            //equipmentID = Guid.NewGuid().ToString("N");
            equipmentID = "0";

            addSensor("pressure0", "pressure", 95000, 100);
            addSensor("airtemp0", "temperture", 2000, 100);
            addSensor("humidity0", "humidity", 3000, 100);

            

            coms = new System.Threading.Thread(incomingCom);
            coms.Name = "Com Thread";

            rawDataFromSensor = new updateCreater.objectUpdate();
            rawDataToSensor = new updateCreater.objectUpdate();

            rawDataFromSensor.objectID = equipmentID;

            rawDataFromSensor.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(rawDataFromSensor_PropertyChange);

            dataReady = new updateCreater.objectUpdate();
            dataReady.objectID = equipmentID;
        }



        public void connect()
        {
            comsRun = true;
            coms.Start();
        }

        public void disconnect()
        {
            comsRun = false;
        }

        public void setRate(int delay)
        {
            reportDelay = delay;

            //Setting the sensors
            for (int i = 0; i < sensors.Count; i++)
            {
                sensors[i].reportRate = delay;
                outGoingCom("/DRR=" + (delay*1000).ToString());
            }
        }

        public void setSensorSampleRate(string sensorName, int rate)
        {
            foreach (equipment_sensor_element_Sim sen in sensors)
            {
                if (sen.sensorName == sensorName)
                {
                    sen.sampleRate = rate;
                    outGoingCom("/DSR=" + rate.ToString());
                }
            }
        }

        public void addSensor(string name, string type, int sp, int tol)
        {
            equipment_sensor_element_Sim tempSensor = new equipment_sensor_element_Sim(name, type);
            tempSensor.setRandomData(sp, tol);
            tempSensor.startSensor();

            tempSensor.dataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(dataReady_PropertyChange);

            sensors.Add(tempSensor);
            
            //Setting up sim sensor controls.
            System.Threading.AutoResetEvent tempSensorLock = new System.Threading.AutoResetEvent(false);
            sensorLock.Add(tempSensorLock);

        }

        public double getSensorReading(string sensorName)
        {
            foreach (equipment_sensor_element_Sim s in sensors)
            {
                if (s.sensorName == sensorName)
                {
                    return s.curSensorData;
                }
            }

            return 9999.99;
        }

        void dataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate incomingSender = (updateCreater.objectUpdate)sender;

            for (int i = 0; i < sensors.Count; i++)
            {
                if (incomingSender.objectID == sensors[i].sensorName + "," + sensors[i].sensorType)
                {
                    sensorLock[i].Set();
                }
            }

            double incoming = (double)data.NewValue;
        }

        void rawDataFromSensor_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate incomingSender = (updateCreater.objectUpdate)sender;

            //Processing Data
            dataPacket tempIncoming = new dataPacket();
            tempIncoming.dateReceived = DateTime.Now;
            tempIncoming.rawData = (string)data.NewValue;
            tempIncoming.deviceID = incomingSender.objectID;

            dataReady.UpdatingObject = tempIncoming;
        }

        /// <summary>
        /// Thread to fake communication from the sensor mainboard.
        /// </summary>
        void incomingCom()
        {
            while (comsRun)
            {
                foreach (System.Threading.AutoResetEvent auto in sensorLock)
                {
                    if(auto.WaitOne(reportDelay/sensorLock.Count))
                    {
                    }
                }

                string dataLine = equipmentID + ",";

                foreach (equipment_sensor_element_Sim element in sensors)
                {
                    dataLine += element.curSensorData.ToString("0.00") + ",";
                }

                rawDataFromSensor.UpdatingObject = dataLine;

                System.Threading.Thread.Sleep(reportDelay);
            }
        }

        void outGoingCom(string toSend)
        {
            rawDataToSensor.UpdatingObject = toSend;
        }
    }

    public class equipment_sensor_element_Sim
    {
        //Sensor details
        public string sensorName = "";
        public string sensorType = "";

        public int sampleRate = 1;  //sample rate per report.
        public int reportRate = 1000;   //pause report rate per sec.

        public double curSensorData;

        //Random data gen items.
        Random dataGen;
        int dataGenSP = 0;
        int dataGenTol = 0;

        //Threading Objects
        System.Threading.Thread sensorProcessor;
        bool processorActive = false;

        //Events
        public updateCreater.objectUpdate dataReady;

        public equipment_sensor_element_Sim(string name, string type)
        {
            sensorName = name;
            sensorType = type;

            //Setting up the event.
            dataReady = new updateCreater.objectUpdate();
            dataReady.objectID = name + "," + type;

            //Setting up threading.
            sensorProcessor = new System.Threading.Thread(genDataThread);
            sensorProcessor.Name = name + "," + type;

            //Initing the random gen.
            dataGen = new Random(123456);
        }

        public void startSensor()
        {
            processorActive = true;
            sensorProcessor.Start();
        }

        public void stopSensor()
        {
            processorActive = false;
        }

        /// <summary>
        /// Int seeds. The SP and tol should take into account the int
        /// returned will then be devided by 100.
        /// </summary>
        /// <param name="SP"></param>
        /// <param name="Tol"></param>
        public void setRandomData(int SP, int Tol)
        {
            dataGenSP = SP;
            dataGenTol = Tol;
        }

        void genDataThread()
        {
            //Lowing the priority to allow it to close on program closing.
            System.Threading.Thread.CurrentThread.IsBackground = true;

            while (processorActive)
            {
                int genPoint = 0;

                //Looping through the sample rate.
                for (int c = 0; c < sampleRate; c++)
                {
                    genPoint += dataGen.Next(dataGenSP - dataGenTol, dataGenSP + dataGenTol);
                }

                //Getting the sensor data output.
                double outPutData = (Convert.ToDouble(genPoint) / sampleRate) / 100;

                curSensorData = outPutData;

                dataReady.UpdatingObject = outPutData;

                System.Threading.Thread.Sleep(reportRate);
            }
        }

    }

    #endregion

    public class dataPacket
    {
        public DateTime dateReceived;
        public string rawData;
        public string deviceID;
    }

    //Class for the sensor boards be they Main or aux.
    public class equipment_sensor_board
    {
        public string boardID = "";     //ID of the board, 0 for main 1 - 4 for the aux sensor boards
        public List<equipment_sensor_element> sensors = new List<equipment_sensor_element>();   //List of sensors on the board.

        public equipment_sensor_board() { }
    }
    
    //Sensor element on the boards.
    public class equipment_sensor_element
    {
        public DateTime curDataTime; //The time/date this data is from.
        public string sensorType = "";  //String description of the sensor.
        public int sensorDataPos;    //Data position in the incoming communication line. This is also the id for the sensor
        public int sensorSampleRate;    //Current known sample rate.... May not be able to know this yet.
        public double curReading = 0;   //Current reading from the sensor.
        public string curSReading = ""; //Current reading from senesor if it is a string.
    } 

}
