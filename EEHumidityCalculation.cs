﻿using System;
using System.Collections.Generic;
using System.Text;
using MathNet.Numerics;

namespace E_E_Calculation_Project
{
    class EEHumidityCalculation
    {
        public double[] getEECoefficients(string referenceFileName,string rawFileName) 
        {
            List<double[]> timeSyncedData = timeSyncData(referenceFileName, rawFileName);
            
            //Calculate the stray capacitance for each data point
            double[] strayCapacitance = new double[timeSyncedData.Count];
            double[] dataRow;

            for (int i = 0; i < strayCapacitance.Length; i++) 
            {
                dataRow = timeSyncedData[i];
                double sondeAirTemperature = dataRow[2];
                strayCapacitance[i] = calculateStrayCapacitance(sondeAirTemperature);
            }

            //Get the high temperature humidity points
            List<double[]> highTempPointsOnly = new List<double[]>();
            for (int i = 0; i < timeSyncedData.Count; i++) 
            {
                dataRow = timeSyncedData[i];
                if (dataRow[2] > 10) 
                {
                    highTempPointsOnly.Add(dataRow);
                }
            }

            //Set the coefficient for the air temperature and internal temperature at high temp
            double[] airTempArray = new double[highTempPointsOnly.Count];
            double[] internalTempArray = new double[highTempPointsOnly.Count];
            for (int i = 0; i < internalTempArray.Length; i++) 
            {
                dataRow = highTempPointsOnly[i];
                airTempArray[i] = dataRow[2];
                internalTempArray[i] = dataRow[3];
            }

            double coefficent_T0 = calculateMean(airTempArray);
            double coefficient_Tio = calculateMean(internalTempArray);

            //Calculate the nominal capacitance (C_0) and the sensitivity (HC) for the sonde
            
            //First get the measured capacitance of the sonde
            double R_b = 133000;
            double[] measuredCapacitance = new double[timeSyncedData.Count];
            for (int i = 0; i < measuredCapacitance.Length; i++) 
            {
                dataRow = timeSyncedData[i];
                double frequency = dataRow[1];
                measuredCapacitance[i] = 1.44 / (2 * R_b * frequency * 1e-12) - calculateStrayCapacitance(dataRow[2]);
            }

            //Use least squares fit method to get the linear approximation for HtoC
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix mCapacitanceMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(highTempPointsOnly.Count, 1);
            for(int i=0;i<highTempPointsOnly.Count;i++)
            {
                mCapacitanceMatrix[i, 0] = measuredCapacitance[i];
            }

            //Build the observation matrix
            double[,] dObservationMatrix = new double[highTempPointsOnly.Count,2];
            for (int i = 0; i < highTempPointsOnly.Count; i++) 
            {
                dataRow = highTempPointsOnly[i];
                dObservationMatrix[i, 0] = 1;
                dObservationMatrix[i, 1] = dataRow[6];
            }

            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix mObservationMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(dObservationMatrix);
            
            //Perform the linear algebra to calculate the nominal capacitance and sensitivity
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix transposedO;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix step1;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix step2;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix step3;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix coefficientMatrix;
            try
            {
                transposedO = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)mObservationMatrix.Transpose();
                step1 = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)transposedO.Multiply(mObservationMatrix);
                step2 = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)step1.Inverse();
                step3 = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)step2.Multiply(transposedO);
                coefficientMatrix = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)step3.Multiply(mCapacitanceMatrix);
            }
            catch
            {
                return null;
            }

            double y_intercept = coefficientMatrix[0, 0];
            double slope = coefficientMatrix[1, 0];

            double coefficient_Cs = (y_intercept + 76 * slope) * 0.000000000001;    //Cs = Capacitance at 76%RH
            double coefficient_HC = (coefficient_Cs - y_intercept * 0.000000000001) / 76 / coefficient_Cs;  //HC = Sensitivity at 76%RH

            //Compute the humidity for each point
            double[] calculatedHumidity = new double[strayCapacitance.Length];
            for (int i = 0; i < calculatedHumidity.Length; i++)
            {
                calculatedHumidity[i] = CtoH(measuredCapacitance[i], coefficient_Cs, coefficient_HC);
            }

            //Get the average errors for each calibration point
            List<double[]> calTempLowHumPoints = new List<double[]>();
            List<double[]> calTempHighHumPoints = new List<double[]>();
            List<double[]> lowTempLowHumPoints = new List<double[]>();
            List<double[]> lowTempHighHumPoints = new List<double[]>();

            //Sort the data

            for (int i = 0; i < timeSyncedData.Count; i++) 
            {
                dataRow = timeSyncedData[i];
                double humidity = dataRow[6];
                double temperature = dataRow[5];

                if(temperature > 10.0 && humidity < 15.0)
                {
                    calTempLowHumPoints.Add(dataRow);
                }
                if (temperature > 10.0 && humidity > 50.0) 
                {
                    calTempHighHumPoints.Add(dataRow);
                }
                if (temperature < 10.0 && humidity < 15.0) 
                {
                    lowTempLowHumPoints.Add(dataRow);
                }
                if (temperature < 10.0 && humidity > 50.0) 
                {
                    lowTempHighHumPoints.Add(dataRow);
                }
            }


            //Use canned Oscillator Gain
            double starting_Og = 0.0005569;
            double adjustment = 1.0;

            //Adjust the canned Og until the errors are close
            double calTempLowHumError = calculateAverageError(calTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            double calTempHighHumError = calculateAverageError(calTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            double lowTempLowHumError = calculateAverageError(lowTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            double lowTempHighHumError = calculateAverageError(lowTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            
            double calTempAverageError = (calTempLowHumError + calTempHighHumError)/2;
            double lowTempAverageError = (lowTempLowHumError + lowTempHighHumError) / 2;

            double lowHumDifference = lowTempLowHumError - calTempLowHumError;

            double temperatureErrorDifference = lowTempAverageError - calTempAverageError;
            
            double placeholder = 0.0;

            while (Math.Abs(temperatureErrorDifference)>0.05) 
            {
                if (temperatureErrorDifference > 0)
                {
                    adjustment = adjustment - 0.01;
                }
                else 
                {
                    adjustment = adjustment + 0.01;
                }

                calTempLowHumError = calculateAverageError(calTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
                calTempHighHumError = calculateAverageError(calTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
                lowTempLowHumError = calculateAverageError(lowTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
                lowTempHighHumError = calculateAverageError(lowTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            
                calTempAverageError = (calTempLowHumError + calTempHighHumError) / 2;
                lowTempAverageError = (lowTempLowHumError + lowTempHighHumError) / 2;
                temperatureErrorDifference = lowTempAverageError - calTempAverageError;
            }
        
            double coefficient_Og = starting_Og * adjustment;

            double[] coeffientArray = { coefficient_Cs, coefficient_HC, coefficent_T0, coefficient_Og, coefficient_Tio };

            return coeffientArray;
        }

        public double[] getEECoefficientsNoLow(string referenceFileName, string rawFileName)
        {
            List<double[]> timeSyncedData = timeSyncData(referenceFileName, rawFileName);

            //Calculate the stray capacitance for each data point
            double[] strayCapacitance = new double[timeSyncedData.Count];
            double[] dataRow;

            for (int i = 0; i < strayCapacitance.Length; i++)
            {
                dataRow = timeSyncedData[i];
                double sondeAirTemperature = dataRow[2];
                strayCapacitance[i] = calculateStrayCapacitance(sondeAirTemperature);
            }

            //Get the high temperature humidity points
            List<double[]> highTempPointsOnly = new List<double[]>();
            for (int i = 0; i < timeSyncedData.Count; i++)
            {
                dataRow = timeSyncedData[i];
                if (dataRow[2] > 10)
                {
                    highTempPointsOnly.Add(dataRow);
                }
            }

            //Set the coefficient for the air temperature and internal temperature at high temp
            double[] airTempArray = new double[highTempPointsOnly.Count];
            double[] internalTempArray = new double[highTempPointsOnly.Count];
            for (int i = 0; i < internalTempArray.Length; i++)
            {
                dataRow = highTempPointsOnly[i];
                airTempArray[i] = dataRow[2];
                internalTempArray[i] = dataRow[3];
            }

            double coefficent_T0 = calculateMean(airTempArray);
            double coefficient_Tio = calculateMean(internalTempArray);

            //Calculate the nominal capacitance (C_0) and the sensitivity (HC) for the sonde

            //First get the measured capacitance of the sonde
            double R_b = 133000;
            double[] measuredCapacitance = new double[timeSyncedData.Count];
            for (int i = 0; i < measuredCapacitance.Length; i++)
            {
                dataRow = timeSyncedData[i];
                double frequency = dataRow[1];
                measuredCapacitance[i] = 1.44 / (2 * R_b * frequency * 1e-12) - calculateStrayCapacitance(dataRow[2]);
            }

            //Use least squares fit method to get the linear approximation for HtoC
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix mCapacitanceMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(highTempPointsOnly.Count, 1);
            for (int i = 0; i < highTempPointsOnly.Count; i++)
            {
                mCapacitanceMatrix[i, 0] = measuredCapacitance[i];
            }

            //Build the observation matrix
            double[,] dObservationMatrix = new double[highTempPointsOnly.Count, 2];
            for (int i = 0; i < highTempPointsOnly.Count; i++)
            {
                dataRow = highTempPointsOnly[i];
                dObservationMatrix[i, 0] = 1;
                dObservationMatrix[i, 1] = dataRow[6];
            }

            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix mObservationMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(dObservationMatrix);

            //Perform the linear algebra to calculate the nominal capacitance and sensitivity
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix transposedO;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix step1;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix step2;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix step3;
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix coefficientMatrix;
            try
            {
                transposedO = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)mObservationMatrix.Transpose();
                step1 = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)transposedO.Multiply(mObservationMatrix);
                step2 = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)step1.Inverse();
                step3 = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)step2.Multiply(transposedO);
                coefficientMatrix = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)step3.Multiply(mCapacitanceMatrix);
            }
            catch
            {
                return null;
            }

            double y_intercept = coefficientMatrix[0, 0];
            double slope = coefficientMatrix[1, 0];

            double coefficient_Cs = (y_intercept + 76 * slope) * 0.000000000001;    //Cs = Capacitance at 76%RH
            double coefficient_HC = (coefficient_Cs - y_intercept * 0.000000000001) / 76 / coefficient_Cs;  //HC = Sensitivity at 76%RH

            //Compute the humidity for each point
            double[] calculatedHumidity = new double[strayCapacitance.Length];
            for (int i = 0; i < calculatedHumidity.Length; i++)
            {
                calculatedHumidity[i] = CtoH(measuredCapacitance[i], coefficient_Cs, coefficient_HC);
            }

            //Get the average errors for each calibration point
            List<double[]> calTempLowHumPoints = new List<double[]>();
            List<double[]> calTempHighHumPoints = new List<double[]>();
            //List<double[]> lowTempLowHumPoints = new List<double[]>();
            List<double[]> lowTempHighHumPoints = new List<double[]>();

            //Sort the data

            for (int i = 0; i < timeSyncedData.Count; i++)
            {
                dataRow = timeSyncedData[i];
                double humidity = dataRow[6];
                double temperature = dataRow[5];

                if (temperature > 10.0 && humidity < 15.0)
                {
                    calTempLowHumPoints.Add(dataRow);
                }
                if (temperature > 10.0 && humidity > 50.0)
                {
                    calTempHighHumPoints.Add(dataRow);
                }
                if (temperature < 10.0 && humidity < 15.0)
                {
                    //lowTempLowHumPoints.Add(dataRow);
                }
                if (temperature < 10.0 && humidity > 50.0)
                {
                    lowTempHighHumPoints.Add(dataRow);
                }
            }


            //Use canned Oscillator Gain
            double starting_Og = 0.0005569;
            double adjustment = 1.0;

            //Adjust the canned Og until the errors are close
            double calTempLowHumError = calculateAverageError(calTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            double calTempHighHumError = calculateAverageError(calTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            //double lowTempLowHumError = calculateAverageError(lowTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
            double lowTempHighHumError = calculateAverageError(lowTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);

            double calTempAverageError = (calTempLowHumError + calTempHighHumError) / 2;
            //double lowTempAverageError = (lowTempLowHumError + lowTempHighHumError) / 2;
            double lowTempAverageError = lowTempHighHumError;

            //double lowHumDifference = lowTempLowHumError - calTempLowHumError;
            double lowHumDifference = calTempLowHumError;

            double temperatureErrorDifference = lowTempAverageError - calTempAverageError;

            double placeholder = 0.0;

            while (Math.Abs(temperatureErrorDifference) > 0.05)
            {
                if (temperatureErrorDifference > 0)
                {
                    adjustment = adjustment - 0.01;
                }
                else
                {
                    adjustment = adjustment + 0.01;
                }

                calTempLowHumError = calculateAverageError(calTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
                calTempHighHumError = calculateAverageError(calTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
                //lowTempLowHumError = calculateAverageError(lowTempLowHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);
                lowTempHighHumError = calculateAverageError(lowTempHighHumPoints, starting_Og * adjustment, coefficient_Tio, coefficient_Cs, coefficient_HC, coefficent_T0);

                calTempAverageError = (calTempLowHumError + calTempHighHumError) / 2;
                //lowTempAverageError = (lowTempLowHumError + lowTempHighHumError) / 2;
                lowTempAverageError = lowTempHighHumError;
                temperatureErrorDifference = lowTempAverageError - calTempAverageError;
            }

            double coefficient_Og = starting_Og * adjustment;

            double[] coeffientArray = { coefficient_Cs, coefficient_HC, coefficent_T0, coefficient_Og, coefficient_Tio };

            return coeffientArray;
        }

        public double calculateAverageError(List<double[]> dataSet,double Og,double coefficient_Tio,double coefficient_Cs,double coefficient_HC,double coefficient_T0) 
        {
            double[] strayC = new double[dataSet.Count];
            double[] temperature = new double[dataSet.Count];
            double[] RH = new double[dataSet.Count];
            double[] TcorH = new double[dataSet.Count];
            double[] Error = new double[dataSet.Count];
            
            double[] dataRow;
            for (int i = 0; i < dataSet.Count; i++)
            {
                dataRow = (double[])dataSet[i];
                strayC[i] = calculateStrayCapacitance(dataRow[2]);
                double correctedCount = correctHumCounts(dataRow[1], Og, dataRow[3], coefficient_Tio);
                temperature[i] = calculate555Capacitance(correctedCount) - strayC[i];
                RH[i] = CtoH(temperature[i], coefficient_Cs, coefficient_HC);
                TcorH[i] = TCorH(RH[i], dataRow[2], coefficient_T0);
                Error[i] = TcorH[i] - dataRow[6];
            }
            return calculateMean(Error);
        }

        public void writeEECalFiles(double[] coefficients,string writeNameAndDirectory) 
        {
            if (!writeNameAndDirectory.EndsWith(".u_cal")) 
            {
                Exception ex = new Exception("Invalid file extension.");
                throw ex;
            }

            System.IO.TextWriter tw = new System.IO.StreamWriter(writeNameAndDirectory);

            int endDirectory = writeNameAndDirectory.LastIndexOf('\\') + 2;
            string sondeName = writeNameAndDirectory.Remove(0, endDirectory).Replace(".u_cal", null);

            tw.WriteLine(sondeName);
            tw.WriteLine(coefficients[0].ToString("0.00000000000000E+00;-0.00000000000000E+00"));
            tw.WriteLine(coefficients[1].ToString("0.00000000000000E+00;-0.00000000000000E+00"));
            tw.WriteLine(coefficients[2].ToString("#.#######"));
            tw.WriteLine(coefficients[3].ToString("0.00000000000000E+00;-0.00000000000000E+00"));
            tw.WriteLine(coefficients[4].ToString("#.#######"));
            tw.Close();
        }

        public void compareCalibrationCoefficients(string firstCalFileName, string secondCalFileName, string delimitedResultFileName,double airTemperature,double internalTemperature) 
        {
            //Get the coefficients from the files
            string[] firstCalFileAllLines = System.IO.File.ReadAllLines(firstCalFileName);
            string[] secondCalAllLines = System.IO.File.ReadAllLines(secondCalFileName);
            double[] firstCalFileCoefficients = new double[5];
            double[] secondCalFileCoefficients = new double[5];

            for (int i = 0; i < firstCalFileCoefficients.Length; i++) 
            {
                firstCalFileCoefficients[i] = double.Parse(firstCalFileAllLines[i + 1]);
                secondCalFileCoefficients[i] = double.Parse(secondCalAllLines[i + 1]);
            }

            System.Data.DataTable firstCalTable = new System.Data.DataTable();
            System.Data.DataTable secondCalTable = new System.Data.DataTable();

            //Calculate the stray capacitance
            double strayCapacitance = -0.000379 * Math.Pow(airTemperature, 2) + 0.057019 * airTemperature + 95.357639;

            //Increment the frequencies and add them to the tables
            firstCalTable.Columns.Add("Frequency",typeof(int));
            secondCalTable.Columns.Add("Frequency",typeof(int));

            for (int i = 22500; i > 18000; i--) 
            {
                firstCalTable.Rows.Add(i);
                secondCalTable.Rows.Add(i);
            }

            //Calculate the corrected counts
            firstCalTable.Columns.Add("Corrected_HCount",typeof(int));
            secondCalTable.Columns.Add("Corrected_HCount",typeof(int));

            for (int i = 0; i < firstCalTable.Rows.Count; i++) 
            {
                System.Data.DataRow firstRow = firstCalTable.Rows[i];
                firstRow["Corrected_HCount"] = (int)firstRow["Frequency"] * (1 + firstCalFileCoefficients[3] * (internalTemperature - firstCalFileCoefficients[4]));
                System.Data.DataRow secondRow = secondCalTable.Rows[i];
                secondRow["Corrected_HCount"] = (int)secondRow["Frequency"] * (1 + secondCalFileCoefficients[3] * (internalTemperature - secondCalFileCoefficients[4]));
            }

            //Compute the humidity sensor capacitance from the internal temperature corrected humidity count and stray capacitance
            firstCalTable.Columns.Add("Sensor_Capacitance", typeof(double));
            secondCalTable.Columns.Add("Sensor_Capacitance", typeof(double));

            for (int i = 0; i < firstCalTable.Rows.Count; i++) 
            {
                System.Data.DataRow row = firstCalTable.Rows[i];
                double correctedHCount = (int)row["Corrected_HCount"];
                row["Sensor_Capacitance"] = 1.44 / (2 * 133000 * correctedHCount * 1e-12) - strayCapacitance;
                row = secondCalTable.Rows[i];
                correctedHCount = (int)row["Corrected_HCount"];
                row["Sensor_Capacitance"] = 1.44 / (2 * 133000 *correctedHCount* 1e-12) - strayCapacitance;
            }

            //Compute the raw humidity
            firstCalTable.Columns.Add("Raw_Humidity", typeof(double));
            secondCalTable.Columns.Add("Raw_Humidity", typeof(double));
            
            for (int i = 0; i < firstCalTable.Rows.Count; i++)
            {
                System.Data.DataRow row = firstCalTable.Rows[i];
                row["Raw_Humidity"] = CtoH((double)row["Sensor_Capacitance"], firstCalFileCoefficients[0], firstCalFileCoefficients[1]);
                row = secondCalTable.Rows[i];
                row["Raw_Humidity"] = CtoH((double)row["Sensor_Capacitance"], secondCalFileCoefficients[0], secondCalFileCoefficients[1]);

            }

            //Set up the output file
            System.IO.TextWriter tw = new System.IO.StreamWriter(delimitedResultFileName);

            //Write Header Information
            tw.WriteLine("Freq,FirstCoefficientRH,SecondCoefficientRH,RH_Error");

            //Create error table and populate with data
            System.Data.DataTable errorTable = new System.Data.DataTable();
            errorTable.Columns.Add("Frequency", typeof(int));
            errorTable.Columns.Add("FirstCoefficientRH", typeof(double));
            errorTable.Columns.Add("SecondCoefficientRH", typeof(double));
            errorTable.Columns.Add("RH_Error", typeof(double));
            for (int i = 0; i < firstCalTable.Rows.Count; i++) 
            {
                int freq = (int)firstCalTable.Rows[i]["Frequency"];
                double firstHumidity = (double)firstCalTable.Rows[i]["Raw_Humidity"];
                double secondHumidity = (double)secondCalTable.Rows[i]["Raw_Humidity"];
                double error = secondHumidity - firstHumidity;
                errorTable.Rows.Add(firstHumidity, secondHumidity, error);
                tw.WriteLine(freq.ToString() + "," + firstHumidity.ToString("0.00") + "," + secondHumidity.ToString("0.00") + "," + error.ToString("0.00"));
            }

            tw.Close();
        }

        public double calcRH(double freq, double airTemperature, double internalTemperature, double[] coef)
        {
            double strayCapacitance = -0.000379 * Math.Pow(airTemperature, 2) + 0.057019 * airTemperature + 95.357639;
            double correctedHCount = freq * (1 + coef[3] * (internalTemperature - coef[4]));
            double sensorCapacitance = 1.44 / (2 * 133000 * correctedHCount * 1e-12) - strayCapacitance;
            double rawHumidity = CtoH(sensorCapacitance, coef[0], coef[1]);

            return rawHumidity;
        }

        public double[] analyzeEECalibration(double[] coefficients, string referenceFileName, string rawFileName) 
        {
            List<double[]> timeSyncedData = timeSyncData(referenceFileName, rawFileName);
            double[] dataRow;

            //Correct the counts for each data point
            double[] correctedCounts = new double[timeSyncedData.Count];
            for (int i = 0; i < correctedCounts.Length; i++) 
            {
                dataRow = timeSyncedData[i];
                double sondeCount = dataRow[1];
                double sondeIT = dataRow[3];
                double Og = coefficients[3];
                double Tio = coefficients[4];
                correctedCounts[i] = sondeCount * (1 + Og * (sondeIT - Tio));
            }

            //Calculate the stray capacitance for each data point
            double[] strayCapacitance = new double[timeSyncedData.Count];
            for (int i = 0; i < strayCapacitance.Length; i++)
            {
                dataRow = timeSyncedData[i];
                double sondeAirTemperature = dataRow[2];
                strayCapacitance[i] = calculateStrayCapacitance(sondeAirTemperature);
            }

            //Compute the humidity sensor capacitance
            double[] sensorCapacitance = new double[timeSyncedData.Count];
            for (int i = 0; i < sensorCapacitance.Length; i++) 
            {
                sensorCapacitance[i] = 1.44 / (2 * 133000 * correctedCounts[i] * 1e-12) - strayCapacitance[i];
            }

            //Compute the humidity for each point
            double[] calculatedHumidity = new double[sensorCapacitance.Length];
            for (int i = 0; i < calculatedHumidity.Length; i++) 
            {
                calculatedHumidity[i] = CtoH(sensorCapacitance[i], coefficients[0], coefficients[1]);
            }

            //Get the error for each point
            double[] errorArray = new double[calculatedHumidity.Length];
            for (int i = 0; i < errorArray.Length; i++) 
            {
                dataRow = timeSyncedData[i];
                double referenceHumidity = dataRow[6];
                errorArray[i] = calculatedHumidity[i] - referenceHumidity;
            }
            return errorArray;
        }

        public double[] analyzeEECalibrationOutPut(double[] coefficients, string referenceFileName, string rawFileName)
        {
            List<double[]> timeSyncedData = timeSyncData(referenceFileName, rawFileName);
            double[] dataRow;

            //Correct the counts for each data point
            double[] correctedCounts = new double[timeSyncedData.Count];
            for (int i = 0; i < correctedCounts.Length; i++)
            {
                dataRow = timeSyncedData[i];
                double sondeCount = dataRow[1];
                double sondeIT = dataRow[3];
                double Og = coefficients[3];
                double Tio = coefficients[4];
                correctedCounts[i] = sondeCount * (1 + Og * (sondeIT - Tio));
            }

            //Calculate the stray capacitance for each data point
            double[] strayCapacitance = new double[timeSyncedData.Count];
            for (int i = 0; i < strayCapacitance.Length; i++)
            {
                dataRow = timeSyncedData[i];
                double sondeAirTemperature = dataRow[2];
                strayCapacitance[i] = calculateStrayCapacitance(sondeAirTemperature);
            }

            //Compute the humidity sensor capacitance
            double[] sensorCapacitance = new double[timeSyncedData.Count];
            for (int i = 0; i < sensorCapacitance.Length; i++)
            {
                sensorCapacitance[i] = 1.44 / (2 * 133000 * correctedCounts[i] * 1e-12) - strayCapacitance[i];
            }

            //Compute the humidity for each point
            double[] calculatedHumidity = new double[sensorCapacitance.Length];
            for (int i = 0; i < calculatedHumidity.Length; i++)
            {
                calculatedHumidity[i] = CtoH(sensorCapacitance[i], coefficients[0], coefficients[1]);
            }

            //Get the error for each point
            double[] errorArray = new double[calculatedHumidity.Length];
            for (int i = 0; i < errorArray.Length; i++)
            {
                dataRow = timeSyncedData[i];
                double referenceHumidity = dataRow[6];
                errorArray[i] = calculatedHumidity[i] - referenceHumidity;
            }

            //Compile everything into a new output file.
            string[] outPutData = new string[timeSyncedData.Count + 1];
            outPutData[0] = "STime,SUFreq,SAT,SIT,RefTime,RefAT,RefU,CorCount,StrayC,SensorC,CalcU,UError";

            for (int u = 1; u < timeSyncedData.Count+1; u++)
            {
                double[] curData = timeSyncedData[u-1];
                outPutData[u] = curData[0].ToString() + "," + curData[1].ToString() + "," + curData[2].ToString() + "," + curData[3].ToString() + "," + curData[4].ToString() + "," +
                    curData[5].ToString() + "," + curData[6].ToString() + "," + correctedCounts[u - 1].ToString() + "," + strayCapacitance[u - 1].ToString() + "," +
                    sensorCapacitance[u - 1].ToString() + "," + calculatedHumidity[u - 1].ToString() + "," + errorArray[u - 1].ToString();
            }

            System.IO.File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + "output.csv", outPutData);


            return errorArray;
        }


        public double calculateStDev(double[] numberList)
        {
            double arraySum = 0;
            foreach (double number in numberList)
            {
                arraySum += number;
            }
            double mean = arraySum / numberList.Length;
            double sumOfDevSquared = 0;
            foreach (double number in numberList)
            {
                sumOfDevSquared += Math.Pow((number - mean), 2);
            }
            return Math.Sqrt(sumOfDevSquared / (numberList.Length - 1));
        }

        public double calculateMean(double[] numberList)
        {
            double arraySum = 0;

            foreach (double number in numberList)
            {
                arraySum += number;
            }

            return arraySum / numberList.Length;
        }

        private List<double[]> timeSyncData(string referenceFileName, string rawSondeFileName)
        {
            string[] referenceFileAllLines = System.IO.File.ReadAllLines(referenceFileName);
            string[] rawProbeFileAllLines = System.IO.File.ReadAllLines(rawSondeFileName);
            int numberOfHeaderLines = 5;

            //Read the reference data file
            List<string> sReferenceData = new List<string>();
            for (int i = numberOfHeaderLines; i < referenceFileAllLines.Length; i++)
            {
                sReferenceData.Add(referenceFileAllLines[i]);
            }
            double[,] dReferenceData = new double[sReferenceData.Count, 3];
            for (int i = 0; i < sReferenceData.Count; i++)
            {
                string[] splitLine = sReferenceData[i].Split(',');
                dReferenceData[i, 0] = double.Parse(splitLine[0]);//Column '0' for time stamp
                dReferenceData[i, 1] = double.Parse(splitLine[1]);//Column '1' for temperature
                dReferenceData[i, 2] = double.Parse(splitLine[2]);//Column '2' for %RH
            }

            //Read the probe data file
            List<string> sSondeRawData = new List<string>();
            for (int i = numberOfHeaderLines; i < rawProbeFileAllLines.Length; i++)
            {
                sSondeRawData.Add(rawProbeFileAllLines[i]);
            }
            double[,] dSondeRawData = new double[sSondeRawData.Count, 4];
            for (int i = 0; i < sSondeRawData.Count; i++)
            {
                string[] splitLine = sSondeRawData[i].Split(',');
                dSondeRawData[i, 0] = double.Parse(splitLine[0]);//Column '0' for time stamp
                dSondeRawData[i, 1] = double.Parse(splitLine[1]);//Column '1' for humidity count (freq)
                dSondeRawData[i, 2] = double.Parse(splitLine[2]);//Column '2' for air temperature
                dSondeRawData[i, 3] = double.Parse(splitLine[3]);//Column '3' for interenal temperature
            }

            //Time sync the data
            List<double[]> timeSyncedData = new List<double[]>();

            for (int i = 0; i < dSondeRawData.Length / 4; i++)
            {
                for (int j = 0; j < dReferenceData.Length / 3; j++)
                {
                    if (j > dSondeRawData.Length)
                    {
                        break;//Break if we run out of sonde raw data
                    }
                    if (dSondeRawData[i, 0] >= dReferenceData[j, 0] && dSondeRawData[i, 0] <= dReferenceData[j + 1, 0])
                    {
                        if ((Math.Abs(dSondeRawData[i, 0] - dReferenceData[j, 0])) < Math.Abs(dSondeRawData[i, 0] - dReferenceData[j + 1, 0]))
                        {
                            double[] newPoint = new double[] { dSondeRawData[i, 0], dSondeRawData[i, 1], dSondeRawData[i, 2], dSondeRawData[i, 3], dReferenceData[j, 0], dReferenceData[j, 1], dReferenceData[j, 2] };
                            timeSyncedData.Add(newPoint);
                        }
                        else
                        {
                            double[] newPoint = new double[] { dSondeRawData[i, 0], dSondeRawData[i, 1], dSondeRawData[i, 2], dSondeRawData[i, 3], dReferenceData[j + 1, 0], dReferenceData[j + 1, 1], dReferenceData[j + 1, 2] };
                            timeSyncedData.Add(newPoint);
                        }
                        break;
                    }
                }
            }

            return timeSyncedData;
        }

        private double CtoH(double C_h,double C_s,double HC_s) 
        {
            C_s = C_s * 1e12;
            double c_ratio = C_h / C_s; //Convert C_s back to pF
            double Hmin = -100.0;
            double Hmax = 150.0;
            double test = 1;
            int count = 0;
            int sign;

            double A1 = 0.0021159;
            double A2 = -0.00076305;
            double A3 = 0.00008947;
            double A4 = -0.000003413;

            //Check values
            if (c_ratio < 0.5 || c_ratio > 1.5 || C_s < 100.0 || C_s > 400.0) 
            {
                return -1000.0;
            }

            double h = -1000.0;   //Iterated Humidity
            double C_i; //Estimated Capacitance for guessed humidity

            while (test > 0.000001 && count < 25) 
            {
                h = (Hmin + Hmax) / 2;
                if (h != 0)
                {
                    double dsign = h / (Math.Abs(h));
                    sign = (int)dsign;
                }
                else 
                {
                    sign = 1;
                }
                
                C_i =1+HC_s*(h-76)+(A1*h+A2*sign*Math.Pow(Math.Abs(h),1.5)+A3*sign*Math.Pow(h,2)+A4*sign*Math.Pow(Math.Abs(h),2.5));

                test = Math.Abs(c_ratio - C_i) / c_ratio;

                if (C_i > c_ratio)
                {
                    Hmax = h;
                }
                else 
                {
                    Hmin = h;
                }

                count++;
            }

            return h;
        }

        private double TCorH(double RH, double airTemp, double calTemp) 
        {
            double a = -0.015;
            double b = 0.00008;
            double c = 0.00000032;
            double h = -0.00321;

            double kt = 1 / (1 + h * (calTemp - 30.0));

            double TCorH = RH-kt*((a+b*(airTemp-30.0)+c*Math.Pow(airTemp-30.0,2))*(airTemp-30.0)-(a+b*(calTemp-30.0)+c*Math.Pow(calTemp-30.0,2))*(calTemp-30.0));

            TCorH = TCorH / (1 - 0.0031 * kt * (airTemp - calTemp));

            if (airTemp < 0) 
            {
                TCorH = TCorH / (1 - 0.0000606 * kt * Math.Pow(airTemp, 2));
            }

            return TCorH;
        }

        private double correctHumCounts(double frequencyHz, double OG, double Ti, double Tio) 
        {
            return frequencyHz * (1 + OG * (Ti - Tio));
        }

        private double calculate555Capacitance(double frequencyHz)
        {
            return 1.44 / (2 * 133000 * frequencyHz * 1e-12);
        }

        private double calculateSVPMH3(double airTemperature)
        {
            double SVPMH3 = 6.1121 * Math.Pow(Math.E,(17.502 * airTemperature) / (240.97 + airTemperature));
            return SVPMH3;
        }

        private double calculateStrayCapacitance(double airTemperature)
        {
                
                double a_5 = 0.0;
                double a_4 = 0.0;
                double a_3 = 0.0;
                double a_2 = -0.000379;
                double a_1 = 0.057019;
                double a_0 = 95.357639;
            
      
                /*
                double a_5 = 2e-9;
                double a_4 = 2e-8;
                double a_3 = -1e-5;
                double a_2 = -0.0004;
                double a_1 = 0.0834;
                double a_0 = 91.055;
                */

            return a_5 * Math.Pow(airTemperature, 5) + a_4 * Math.Pow(airTemperature, 4) + a_3 * Math.Pow(airTemperature, 3) + a_2 * Math.Pow(airTemperature, 2) + a_1 * airTemperature + a_0;
        }

    }


}
