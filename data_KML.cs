﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataExport
{
    public class data_KML
    {
        public data_KML() { }


        /// <summary>
        /// Method loads and exports kml from a data file
        /// dataElementPOS list of int for lat,long,alt
        /// dataElementVeriable int for the element user desired point
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dataElementPOS"></param>
        /// <param name="dataElementVeriable"></param>
        /// <param name="scaleFactor"></param>
        /// <param name="filterElement"></param>
        /// <param name="minFilterQty"></param>
        /// <returns></returns>
        public List<rawData> loadRawFile(string fileName, int[] dataElementPOS, int dataElementVeriable, double[] scaleFactor, int filterElement, double minFilterQty)
        {
            string[] allData = WriteSafeReadAllLines(fileName);

            //Container for the raw compiled data.
            List<rawData> dataToBeExported = new List<rawData>();

            for (int i = 1; i < allData.Length; i++)
            {

                string[] lineData = allData[i].Split(',');

                //Checking the data to make sure the formatters work.
                foreach (int ele in dataElementPOS)
                {
                    if (ele > lineData.Length & ele > dataElementVeriable)
                    {
                        return null;
                    }
                }

                if (Convert.ToDouble(lineData[filterElement]) >= minFilterQty)
                {
                    rawData tempData = new rawData();
                    tempData.latitude = Convert.ToDouble(lineData[dataElementPOS[0]]) / scaleFactor[0];
                    tempData.longitude = Convert.ToDouble(lineData[dataElementPOS[1]]) / scaleFactor[1];
                    tempData.alt = Convert.ToDouble(lineData[dataElementPOS[2]]) / scaleFactor[2];
                    tempData.dataValue = Convert.ToDouble(lineData[dataElementVeriable]) / scaleFactor[3];


                    //Final filtering of data.
                    if (tempData.alt >= 0)
                    {
                        dataToBeExported.Add(tempData);
                    }

                        
                }
                
            }


            //dataToBeExported = filterData(dataToBeExported);

            return dataToBeExported;

        }

        /// <summary>
        /// Method loads and exports kml from a data file that is formated in single line.
        /// dataElementPOS list of int for lat,long,alt
        /// dataElementVeriable int for the element user desired point
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dataElementPOS"></param>
        /// <param name="dataElementVeriable"></param>
        /// <param name="scaleFactor"></param>
        /// <param name="filterElement"></param>
        /// <param name="minFilterQty"></param>
        /// <returns></returns>
        public List<rawData> loadRawFileSingleLine(string fileName, int[] dataElementPOS, int dataElementVeriable, double[] scaleFactor, int filterElement, double minFilterQty)
        {
            string[] allData = WriteSafeReadAllLines(fileName);

            //Container for the raw compiled data.
            List<rawData> dataToBeExported = new List<rawData>();

            for (int i = 1; i < allData.Length; i++)
            {

                string[] lineData = allData[i].Split(',');

                //Checking the data to make sure the formatters work.
                foreach (int ele in dataElementPOS)
                {
                    if (ele > lineData.Length & ele > dataElementVeriable)
                    {
                        return null;
                    }
                }

                if (Convert.ToDouble(lineData[filterElement]) >= minFilterQty)
                {
                    rawData tempData = new rawData();
                    tempData.latitude = Convert.ToDouble(lineData[dataElementPOS[0]]) / scaleFactor[0];
                    tempData.longitude = Convert.ToDouble(lineData[dataElementPOS[1]]) / scaleFactor[1];
                    tempData.alt = Convert.ToDouble(lineData[dataElementPOS[2]]) / scaleFactor[2];
                    tempData.dataValue = Convert.ToDouble(lineData[dataElementVeriable]) / scaleFactor[3];


                    //Final filtering of data.
                    if (tempData.alt >= 0)
                    {
                        dataToBeExported.Add(tempData);
                    }


                }
                
            }


            //dataToBeExported = filterData(dataToBeExported);

            return dataToBeExported;

        }


        /// <summary>
        /// Method takes in coma delimited string array data and returns Raw Lat/Long/Alt/Element data in a list array.
        /// Returns null if there is data requested outside the coma array.
        /// </summary>
        /// <param name="allData"></param>
        /// <param name="dataElementPOS"></param>
        /// <param name="dataElementVeriable"></param>
        /// <param name="scaleFactor"></param>
        /// <param name="filterElement"></param>
        /// <param name="minFilterQty"></param>
        /// <returns></returns>
        public List<rawData> loadRawData(string[] allData, int[] dataElementPOS, int dataElementVeriable, double[] scaleFactor, int filterElement, double minFilterQty)
        {
            //Container for the raw compiled data.
            List<rawData> dataToBeExported = new List<rawData>();

            for (int i = 0; i < allData.Length; i++)
            {
                //Making sure that the data line is XQ formatted.
                if (allData[i].Contains("XQ"))
                {

                    string[] lineData = allData[i].Split(',');

                    //Checking the data to make sure the formatters work.
                    foreach (int ele in dataElementPOS)
                    {
                        if (ele > lineData.Length & ele > dataElementVeriable)
                        {
                            return null;
                        }
                    }


                    if (Convert.ToDouble(lineData[filterElement]) >= minFilterQty)
                    {
                        rawData tempData = new rawData();
                        tempData.latitude = Convert.ToDouble(lineData[dataElementPOS[0]]) / scaleFactor[0];
                        tempData.longitude = Convert.ToDouble(lineData[dataElementPOS[1]]) / scaleFactor[1];
                        tempData.alt = Convert.ToDouble(lineData[dataElementPOS[2]]) / scaleFactor[2];
                        tempData.dataValue = Convert.ToDouble(lineData[dataElementVeriable]) / scaleFactor[3];


                        //Final filtering of data.
                        if (tempData.alt >= 0)
                        {
                            dataToBeExported.Add(tempData);
                        }


                    }
                }
            }

            return dataToBeExported;

        }

        public List<rawData> filterData(List<rawData> fullDataSet)
        {
            //The shorten/avaerage data set.
            List<rawData> filteredData = new List<rawData>();
            filteredData.Add(fullDataSet[0]);

            rawData compiled = new rawData();
            int counter = 1;

            //Going through the data.
            for (int i = 0; i < fullDataSet.Count; i++)
            {
                double tempDistance = DistanceTo(filteredData.Last().latitude, filteredData.Last().longitude, filteredData.Last().alt, fullDataSet[i].latitude, fullDataSet[i].longitude, fullDataSet[i].alt) * 1000;
                if (tempDistance > 2)
                {
                    compiled.latitude = fullDataSet[i].latitude;
                    compiled.longitude = fullDataSet[i].longitude;
                    compiled.alt = fullDataSet[i].alt;
                    compiled.dataValue += fullDataSet[i].dataValue;
                    compiled.dataValue = compiled.dataValue / counter;
                    filteredData.Add(compiled);

                    compiled = new rawData();
                    counter = 1;
                }
                else
                {
                    compiled.dataValue += fullDataSet[i].dataValue;
                    counter++;
                }
            }

            return filteredData;
        }

        public double DistanceTo(double lat1, double lon1, double alt1, double lat2, double lon2, double alt2,  char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            //Vertical element
            double vertical = Math.Abs(alt1 - alt2); // in meters



            switch (unit)
            {
                case 'K': //Kilometers -> default
                    dist *= 1.609344;

                    dist = Math.Sqrt(Math.Pow(vertical / 1000, 2) + Math.Pow(dist, 2));

                    return dist;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            

            return dist;
        }


        public void saveKMLValue(List<rawData> data, System.Drawing.Color lineColor, string fileName)
        {
            List<string> totalData = new List<string>();
            
            //Header
            totalData.Add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            totalData.Add("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
            totalData.Add("  <Document>");
            totalData.Add("    <name>iMet Path " + fileName + "</name>");
            totalData.Add("    <description>IMS " + fileName + " KML Export</description>");

            //Style.
            totalData.Add("    <Style id=\"yellowLineGreenPoly\">");
            
            //Line Style.
            totalData.Add("        <LineStyle>");
            totalData.Add("          <color>7f" + lineColor.B.ToString("X2") + lineColor.G.ToString("X2") + lineColor.R.ToString("X2") + "</color>");
            totalData.Add("          <width>2</width>");
            totalData.Add("        </LineStyle>");

            //Poly Style.
            totalData.Add("         <PolyStyle>");
            totalData.Add("             <color>7f00ff00</color>");
            totalData.Add("         </PolyStyle>");

            //End of style
            totalData.Add("    </Style>");
            totalData.Add("    <Placemark>");
            totalData.Add("        <name>InterMet Systems Sensor Path</name>");
            totalData.Add("        <description>Data Reading from IMS Sensor Plateform</description>");
            totalData.Add("        <styleUrl>#yellowLineGreenPoly</styleUrl>");
            totalData.Add("        <LineString>");
            totalData.Add("            <extrude>1</extrude>");
            totalData.Add("            <tessellate>1</tessellate>");
            totalData.Add("            <altitudeMode>relativeToGround</altitudeMode>");

            //Cordinates
            totalData.Add("              <coordinates>");

            //Loading the  loc and the elelment.
            for (int i = 0; i < data.Count; i++)
            {
                totalData.Add("              " + data[i].longitude.ToString("0.000000") + "," + data[i].latitude.ToString("0.000000") + "," + data[i].dataValue.ToString("0.000"));
            }

            totalData.Add("              </coordinates>");
            totalData.Add("          </LineString>");
            totalData.Add("      </Placemark>");
            totalData.Add("  </Document>");
            totalData.Add("</kml>");


            System.IO.File.WriteAllLines(fileName + DateTime.Now.ToString("HHmmss") + ".kml", totalData.ToArray());
        }

        public void saveKMLAlt(List<rawData> data, System.Drawing.Color lineColor, string fileName)
        {
            List<string> totalData = new List<string>();

            //Header
            totalData.Add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            totalData.Add("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
            totalData.Add("  <Document>");
            totalData.Add("    <name>iMet Path " + fileName + "</name>");
            totalData.Add("    <description>IMS " + fileName + " KML Export</description>");

            //Style.
            totalData.Add("    <Style id=\"yellowLineGreenPoly\">");

            //Line Style.
            totalData.Add("        <LineStyle>");
            totalData.Add("          <color>7f" + lineColor.B.ToString("X2") + lineColor.G.ToString("X2") + lineColor.R.ToString("X2") + "</color>");
            totalData.Add("          <width>2</width>");
            totalData.Add("        </LineStyle>");

            //Poly Style.
            totalData.Add("         <PolyStyle>");
            totalData.Add("             <color>7f00ff00</color>");
            totalData.Add("         </PolyStyle>");

            //End of style
            totalData.Add("    </Style>");
            totalData.Add("    <Placemark>");
            totalData.Add("        <name>InterMet Systems Sensor Path</name>");
            totalData.Add("        <description>Data Reading from IMS Sensor Plateform</description>");
            totalData.Add("        <styleUrl>#yellowLineGreenPoly</styleUrl>");
            totalData.Add("        <LineString>");
            totalData.Add("            <extrude>1</extrude>");
            totalData.Add("            <tessellate>1</tessellate>");
            totalData.Add("            <altitudeMode>absolute</altitudeMode>");

            //Cordinates
            totalData.Add("              <coordinates>");

            //Loading the  loc and the elelment.
            for (int i = 0; i < data.Count; i++)
            {
                totalData.Add("              " + data[i].longitude.ToString("0.000000") + "," + data[i].latitude.ToString("0.000000") + "," + data[i].alt.ToString("0.000"));
            }

            totalData.Add("              </coordinates>");
            totalData.Add("          </LineString>");
            totalData.Add("      </Placemark>");
            totalData.Add("  </Document>");
            totalData.Add("</kml>");


            System.IO.File.WriteAllLines(fileName + DateTime.Now.ToString("HHmmss") + ".kml", totalData.ToArray());
        }

        public List<string> getFileHeader()
        {
            List<string> totalData = new List<string>();

            //Header
            totalData.Add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            totalData.Add("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
            totalData.Add("  <Document>");
            totalData.Add("    <name>iMet Data Path</name>");
            totalData.Add("    <description>IMS KML Export. Exported: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "</description>");

            return totalData;

        }

        public List<string> getFileEnd()
        {
            List<string> totalData = new List<string>();

            totalData.Add("  </Document>");
            totalData.Add("</kml>");

            return totalData;
        }

        public List<string> getPlacemarkData(List<rawData> data, System.Drawing.Color lineColor, string name, string desc, bool fromGround)
        {
            //Checking the data to make sure it is not null.
            if (data == null)
            {
                return new List<string>();
            }


            List<string> totalData = new List<string>();

            //Finding low/high/mean.
            double low = 999999999;
            double high = 0;
            double mean = 0;

            foreach (rawData rd in data)
            {
                if (rd.dataValue < low) { low = rd.dataValue; }
                if (rd.dataValue > high) { high = rd.dataValue; }
                mean += rd.dataValue;
            }

            mean = mean / data.Count;

            //Style.
            totalData.Add("    <Style id=\"" + lineColor.B.ToString("X2") + lineColor.G.ToString("X2") + lineColor.R.ToString("X2") + "\">");

            //Line Style.
            totalData.Add("        <LineStyle>");
            totalData.Add("          <color>7f" + lineColor.B.ToString("X2") + lineColor.G.ToString("X2") + lineColor.R.ToString("X2") + "</color>");
            totalData.Add("          <width>2</width>");
            totalData.Add("        </LineStyle>");

            //Poly Style.
            totalData.Add("         <PolyStyle>");
            totalData.Add("             <color>7f00ff00</color>");
            totalData.Add("         </PolyStyle>");

            //End of style
            totalData.Add("    </Style>");
            totalData.Add("    <Placemark>");
            totalData.Add("        <name>" + name + "</name>");
            totalData.Add("        <description>" + desc + "\n" + "High: " + high.ToString("0.00") + "\n" + "Low: " + low.ToString("0.00") + "\n" + "Mean: " + mean.ToString("0.00") + "</description>");
            totalData.Add("        <styleUrl>#" + lineColor.B.ToString("X2") + lineColor.G.ToString("X2") + lineColor.R.ToString("X2") + "</styleUrl>");
            totalData.Add("        <LineString>");
            totalData.Add("            <extrude>1</extrude>");
            totalData.Add("            <tessellate>1</tessellate>");


            //Controls the elevation of point.
            if (fromGround)
            {
                totalData.Add("            <altitudeMode>relativeToGround</altitudeMode>");
            }
            else
            {
                totalData.Add("            <altitudeMode>absolute</altitudeMode>");
            }

            //Cordinates
            totalData.Add("              <coordinates>");

            //Loading the  loc and the elelment.
            for (int i = 0; i < data.Count; i++)
            {
                totalData.Add("              " + data[i].longitude.ToString("0.000000") + "," + data[i].latitude.ToString("0.000000") + "," + data[i].dataValue.ToString("0.000"));
            }

            totalData.Add("              </coordinates>");
            totalData.Add("          </LineString>");
            totalData.Add("      </Placemark>");

            return totalData;
        }

        public string[] WriteSafeReadAllLines(String path)
        {
            using (var csv = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
            using (var sr = new System.IO.StreamReader(csv))
            {
                List<string> file = new List<string>();
                while (!sr.EndOfStream)
                {
                    file.Add(sr.ReadLine());
                }

                return file.ToArray();
            }
        }


    }

    public class rawData
    {
        public double latitude;
        public double longitude;
        public double alt;
        public double dataValue;
    }

}
