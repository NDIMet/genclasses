﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace ManagerPresssure
{
    public class pressureManager
    {
        //Public elements
        public updateCreater.objectUpdate managerError = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate gotoPressureEvent = new updateCreater.objectUpdate();

        //Pressure enviroment ready.
        public updateCreater.objectUpdate pressureConditionReady = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate pressureStabilityReady = new updateCreater.objectUpdate();

        //Pressure conditons status.
        public updateCreater.objectUpdate statusPressureCondition = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate statusPressureStability = new updateCreater.objectUpdate();

        //Pressure move complete events.
        public updateCreater.objectUpdate pressureMoveRoughComplete = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate pressureMoveFineComplete = new updateCreater.objectUpdate();

        public updateCreater.objectUpdate pressureMoveExtraFineComplete = new updateCreater.objectUpdate();

        
        //Private elements
        private RelayController.relayControllerSystem relayController; //Controller where the valves need to control the pressure enviroment are stored.
        private equipmentTCPIP.equipmentParoTCPIP sensorPressure;

        private List<equipmentTCPIP.dataPacket> pressureData = new List<equipmentTCPIP.dataPacket>();

        private EnviromentStability.stabilityEnviroment desiredEnviroment = new EnviromentStability.stabilityEnviroment();

        private System.Timers.Timer watchDog;

        private bool pressureConditionChanging = false;

        private System.ComponentModel.BackgroundWorker backgroundWorkerGotoPressure;

        private double pressureSystemTemp = 0;

        private double prevRate = 999.99;

        //Auto Reset Events.
        private System.Threading.AutoResetEvent pressureMoveStable = new System.Threading.AutoResetEvent(false);

        //Logging location
        private string logLoc = "pressureManagerLog.csv";

        //Thread Safety Objects
        static readonly object dataLock = new object();

        public pressureManager()
        {
            //Setting up watchDog timer to insure that if the pressure sensor stops running that the system will do something about it.
            watchDog = new System.Timers.Timer(5000);
            watchDog.Elapsed += new System.Timers.ElapsedEventHandler(watchDog_Elapsed);
            watchDog.Enabled = true;
        }

        void watchDog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Closing pressure valves if the system is above 1000mBar.

            if (sensorPressure.getCurrentPressure() >= 1000)
            {
                pressureValvesClose();
            }

            managerError.UpdatingObject = (object)"Pressure Manager has not recieved data in over 5 secs.";
        }


        #region Methods for setting up the pressureManager

        /// <summary>
        /// Sets the relay manager where the valves for up and down pressure control are found.
        /// </summary>
        /// <param name="incomingRelayController"></param>
        public void setPressureManagerRelay(RelayController.relayControllerSystem incomingRelayController)
        {
            relayController = incomingRelayController;
        }

        public void setReferancePressureSensor(equipmentTCPIP.equipmentParoTCPIP incomingReferanceSensor)
        {
            sensorPressure = incomingReferanceSensor;
            sensorPressure.updatedPressureValue.PropertyChange +=new updateCreater.objectUpdate.PropertyChangeHandler(updatedPressureValue_PropertyChange);
        }
        
        /// <summary>
        /// Clears current pressure condition and replaces it with the condition that you want the system to goto.
        /// </summary>
        /// <param name="lowRangePoint"></param>
        /// <param name="highRangePoint"></param>
        /// <param name="desiredSetPoint"></param>
        /// <param name="aboutOfRecords"></param>
        public void setDesiredPressureEnviroment(double lowRangePoint, double highRangePoint, double desiredSetPoint, int aboutOfRecords)
        {
            try
            {
                desiredEnviroment.removeEviromentElement("pressureCondition");
            }
            catch (Exception removeError)
            {
                managerError.UpdatingObject = (object)removeError;
            }

            //Setting up internal move point to compare to. This will help prevent temp effects on pressure pushing the pressure out of range.
            setDesiredMoveEnviroment(((desiredSetPoint - lowRangePoint) * .2 + lowRangePoint), ((desiredSetPoint - highRangePoint) * .2 + highRangePoint), desiredSetPoint, aboutOfRecords);

            desiredEnviroment.addEnviromentElement("pressureCondition", lowRangePoint, highRangePoint, desiredSetPoint, aboutOfRecords);
        }

        public double getDesiredPressureEnviromentSP()
        {
            return desiredEnviroment.getElementSetPoint("pressureCondition");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lowRangePoint"></param>
        /// <param name="highRangePoint"></param>
        /// <param name="desiredSetPoint"></param>
        /// <param name="aboutOfRecords"></param>
        public void setDesiredPressureStability(double lowRangePoint, double highRangePoint, double desiredSetPoint, int aboutOfRecords)
        {
            try
            {
                desiredEnviroment.removeEviromentElement("pressureStability");
            }
            catch (Exception removeError)
            {
                managerError.UpdatingObject = (object)removeError;
            }

            desiredEnviroment.addEnviromentElement("pressureStability", lowRangePoint, highRangePoint, desiredSetPoint, aboutOfRecords);
        }

        public double getDesiredPressureStabilitySP()
        {
            return desiredEnviroment.getElementSetPoint("pressureStability");
        }

        public void setDesiredMoveEnviroment(double lowRangePoint, double highRangePoint, double desiredSetPoint, int aboutOfRecords)
        {
            try
            {
                desiredEnviroment.removeEviromentElement("pressureMoveEnv");
            }
            catch (Exception removeError)
            {
                managerError.UpdatingObject = (object)removeError;
            }

            desiredEnviroment.addEnviromentElement("pressureMoveEnv", lowRangePoint, highRangePoint, desiredSetPoint, aboutOfRecords);
        }

        public double getDesiredMoveEnviromentSP()
        {
            return desiredEnviroment.getElementSetPoint("pressureMoveEnv");
        }

        public void setPressureSystemTemp(double desiredTemp)
        {
            pressureSystemTemp = desiredTemp;
        }

        public double getPressureSystemTemp()
        {
            return pressureSystemTemp;
        }

        public EnviromentStability.stabilityEnviroment getEnviromentalStabilityItem()
        {
            return desiredEnviroment;
        }

        #endregion

        #region Method regarding Pressure information

        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// /////////////////////////////////////////////////////////////////////////////////
        /// This is becoming the main process for the pressure management system.... Not sure if I like this driving the entire
        /// Pressure Manager.
        /// ///////////////////////////////////////////////////////////////////////////
        /// /////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void updatedPressureValue_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Resetting watchDog.
            watchDog.Enabled = false;

            //Adding the new pressure data into the store in memory data.
            pressureData.Add((equipmentTCPIP.dataPacket)data.NewValue);

            checkDataPacketCount();

            //Checking current condition
            bool currentPressureConditionState = desiredEnviroment.checkEnviromentElement("pressureCondition", Convert.ToDouble(pressureData.Last().packetMessage));
            int currentPressureConditionStateCount = desiredEnviroment.checkEnviromentElementCount("pressureCondition");
            object[] statusPressureConditionObject = new object[2] { currentPressureConditionState, currentPressureConditionStateCount};
            statusPressureCondition.UpdatingObject = (object)statusPressureConditionObject;

            //Checking current stability
            bool currentSabilityState = desiredEnviroment.checkEnviromentElement("pressureStability", getPressureDifferancePerSec());
            int currentStabilityStateCount = desiredEnviroment.checkEnviromentElementCount("pressureStability");
            object[] statusStability = new object[2] { currentSabilityState, currentStabilityStateCount };
            statusPressureStability.UpdatingObject = (object)statusStability;

            //Sending out data for debug.
            Console.WriteLine("Diff/Packet: " + getPressureDifferance().ToString("0.000") + " | Diff/Sec: " + getPressureDifferancePerSec().ToString("0.00000") +
                " | Diff Avg / Sec: " + getPressureDiffAvgPerSec(5).ToString("0.0000"));

            //System.Diagnostics.Debug.WriteLine("Diff/Packet: " + getPressureDifferance().ToString("0.000") + " | Diff/Sec: " + getPressureDifferancePerSec().ToString("0.00000") +
            //    " | Diff Avg / Sec: " + getPressureDiffAvgPerSec(5).ToString("0.0000"));

            //Debuging for conditions and stability
            Console.WriteLine("Current Pressure Condition: " + currentPressureConditionState.ToString() + "," + currentPressureConditionStateCount.ToString() + " || || " +
                "Current Stability Condition: " + currentSabilityState.ToString() + "," + currentStabilityStateCount.ToString());

            //System.Diagnostics.Debug.WriteLine("Current Pressure Condition: " + currentPressureConditionState.ToString() + "," + currentPressureConditionStateCount.ToString() + " || || " +
             //   "Current Stability Condition: " + currentSabilityState.ToString() + "," + currentStabilityStateCount.ToString());
            
            //Checking to see if the pressure Conditions are ready and if so trigger that event.
            if (desiredEnviroment.getElementReferancePointCount("pressureCondition") == desiredEnviroment.getDesiredNumberOfRefPoints("pressureCondition").Length)
            {
                pressureConditionReady.UpdatingObject = (object)"Ready"; 
            }

            //Checking to see if the pressure conditon has stabilized and if so trigger that event.
            if (desiredEnviroment.getElementReferancePointCount("pressureStability") == desiredEnviroment.getDesiredNumberOfRefPoints("pressureStability").Length)
            {
                pressureStabilityReady.UpdatingObject = (object)"Ready"; 
            }


            //Starting watchDog
            watchDog.Enabled = true;
        }

        void checkDataPacketCount()
        {
            if (pressureData.Count >= 90)
            {
                
                    pressureData.RemoveAt(0);
                
            }
        }



        void pressureMove_Checking(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Checking current stability
            bool currentSabilityState = desiredEnviroment.checkEnviromentElement("moveStable", getPressureDifferancePerSec());
            int currentStabilityStateCount = desiredEnviroment.checkEnviromentElementCount("moveStable");           

            //Checking to see if the pressure conditon has stabilized and if so trigger that event.
            if (desiredEnviroment.getElementReferancePointCount("moveStable") == desiredEnviroment.getDesiredNumberOfRefPoints("moveStable").Length)
            {
                pressureMoveStable.Set(); //Telling the system that the pressure system is good enough to decide if the pressure needs another move.
            }
        }

        private double getPressureDifferance()
        {
            if (pressureData.Count > 2)
            {
                return Convert.ToDouble(pressureData[pressureData.Count - 1].packetMessage) - Convert.ToDouble(pressureData[pressureData.Count - 2].packetMessage);
            }

            return 9999.99;
        }

        /// <summary>
        /// Method Gets the current differance sec to sec. Method uses the time between point to calculate the differance.
        /// If system is not ready it returns 999.99.
        /// </summary>
        /// <returns></returns>
        public double getPressureDifferancePerSec()
        {
            if (pressureData.Count > 2)
            {
                double differance = Convert.ToDouble(pressureData[pressureData.Count - 1].packetMessage) - Convert.ToDouble(pressureData[pressureData.Count - 2].packetMessage);
                TimeSpan timePassed = pressureData[pressureData.Count - 1].packetDate - pressureData[pressureData.Count - 2].packetDate;

                //To provent data to time problems.
                if (timePassed.TotalSeconds == 0 && pressureData.Count > 3)
                {
                    differance = Convert.ToDouble(pressureData[pressureData.Count - 1].packetMessage) - Convert.ToDouble(pressureData[pressureData.Count - 3].packetMessage);
                    timePassed = pressureData[pressureData.Count - 3].packetDate - pressureData[pressureData.Count - 2].packetDate;

                }
                
                return differance / timePassed.TotalSeconds;
            }

            return 999.99;
        }

        public double getChangeRatePerMin()
        {
            if (pressureData.Count > 50)
            {
                double minDiff = prevRate; //Min Differance
                equipmentTCPIP.dataPacket[] localData = pressureData.ToArray();

                //Finding a packet a min ago.
                foreach (equipmentTCPIP.dataPacket pData in localData)
                {
                    if (pData.packetDate < pressureData.Last().packetDate.AddSeconds(-59) && pData.packetDate > pressureData.Last().packetDate.AddSeconds(-61))
                    {
                        minDiff = Convert.ToDouble(pressureData.Last().packetMessage) - Convert.ToDouble(pData.packetMessage);
                        break;
                    }
                }
                


                prevRate = minDiff;
                return minDiff;
            }

            return 999.99;
        }

        /// <summary>
        /// This method trys to prodict what a min change will be in only 15 sec. It does ok at this.
        /// </summary>
        /// <returns></returns>
        public double getProdChangeRatePerMin()
        {
            if (pressureData.Count > 20)
            {
                double firstDiff = 999.99; //Min Differance
                double secondDiff = 999.99; //Min Differance

                //Finding a packet a min ago.
                lock (dataLock)
                {
                    foreach (equipmentTCPIP.dataPacket pData in pressureData)
                    {
                        if (pData.packetDate < pressureData.Last().packetDate.AddSeconds(-14) && pData.packetDate > pressureData.Last().packetDate.AddSeconds(-16))
                        {
                            firstDiff = (Convert.ToDouble(pressureData.Last().packetMessage) - Convert.ToDouble(pData.packetMessage)) * 4;
                        }

                        if (pData.packetDate < pressureData.Last().packetDate.AddSeconds(-15) && pData.packetDate > pressureData.Last().packetDate.AddSeconds(-17))
                        {
                            secondDiff = (Convert.ToDouble(pressureData.Last().packetMessage) - Convert.ToDouble(pData.packetMessage)) * 4;
                        }
                    }
                }
                return (firstDiff + secondDiff) / 2;
            }

            return 999.99;
        }

        private double getPressureDiffAvg(int avgPacketCount)
        {
            if (pressureData.Count > avgPacketCount + 1)
            {
                double avgDiff = 0;
                for (int x = 0; x < avgPacketCount; x++)
                {
                    avgDiff += Convert.ToDouble(pressureData[pressureData.Count - 1 - x].packetMessage) - Convert.ToDouble(pressureData[pressureData.Count - 2 - x].packetMessage);
                }

                return avgDiff = avgDiff / (avgPacketCount - 1);
            }

            return 9999.999;
        }

        private double getPressureDiffAvgPerSec(int avgPacketCount)
        {
            if (pressureData.Count > avgPacketCount + 1)
            {
                double avgDiff = 0;
                TimeSpan timeAcrossAvg = new TimeSpan();

                for (int x = 0; x < avgPacketCount; x++)
                {
                    avgDiff += Convert.ToDouble(pressureData[pressureData.Count - 1 - x].packetMessage) - Convert.ToDouble(pressureData[pressureData.Count - 2 - x].packetMessage);
                    timeAcrossAvg += (pressureData[pressureData.Count - 1 - x].packetDate) - (pressureData[pressureData.Count - 2 - x].packetDate);
                }

                return avgDiff = avgDiff / timeAcrossAvg.TotalSeconds;
            }

            return 999.999;
        }

        public double getAirDensity()
        {
            //Density = 101325 / (287.05 * (15 + 273.15)) = 1.225 kg/m3
            return (sensorPressure.getCurrentPressure() * 100) / (287.05 * (pressureSystemTemp + 273.15));
        }


        /// <summary>
        /// This logs data into the a file for debuging.
        /// </summary>
        /// <param name="incomingData"></param>
        private void logEvent(string incomingData)
        {
            gotoPressureEvent.UpdatingObject = (object)incomingData;
            DateTime eventTime = DateTime.Now;

            string compiledData = eventTime.ToString("yyyyMMdd") + "," + eventTime.ToString("HH:mm:ss.fff") + "," + pressureSystemTemp.ToString() + "," + incomingData + "\n";

            try
            {
                System.IO.File.AppendAllText(logLoc, compiledData);
            }
            catch (System.IO.IOException logError)
            {
                managerError.UpdatingObject = (object)logError.Message;
            }

        }

        #endregion

        #region Methods relating to the logic of changing pressure
        /// <summary>
        /// Methods commands the pressure system to desired pressure by controlling the relays
        /// called PressureUp and PressureDown
        /// Obsolete.. Do not use unless you have to.
        /// </summary>
        /// <param name="desiredPressure"></param>
        public bool gotoPressure(double desiredPressure)
        {
            //Checking to make sure that not another pressure changes is happening before the system trys to move.
            if (pressureConditionChanging)
            {
                managerError.UpdatingObject = (object)"Unable to do that at this time. Pressure Manager is changeing perssure right now.";
                return false; ;
            }

            pressureConditionChanging = true; //Indicating the pressure is on the move.

            backgroundWorkerGotoPressure = new System.ComponentModel.BackgroundWorker();
            backgroundWorkerGotoPressure.WorkerSupportsCancellation = true;
            backgroundWorkerGotoPressure.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerGotoPressure_DoWork);
            backgroundWorkerGotoPressure.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerGotoPressure_RunWorkerCompleted);

            //Data that will be needed by the monitor to know when to shut things down.
            object[] neededData = new object[2];
            neededData[0] = (object)desiredPressure;

            //Deciding what change needs to be made to reach the desired pressure.
            if (sensorPressure.getCurrentPressure() >= desiredPressure)
            {
                neededData[1] = (string)"Down";
                //pressureDown();
            }
            else
            {
                neededData[1] = (string)"Up";
                //pressureUp();
            }

            //Starting the monitor.
            backgroundWorkerGotoPressure.RunWorkerAsync(neededData);

            return true;
        }

        /// <summary>
        /// Method for moving pressure in stages.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="desiredPressure"></param>
        public bool gotoPressure(int mode, double desiredPressure)
        {
            //Checking to make sure that not another pressure changes is happening before the system trys to move.
            if (pressureConditionChanging)
            {
                managerError.UpdatingObject = (object)"Unable to do that at this time. Pressure Manager is changeing perssure right now.";
                return false;
            }

            pressureConditionChanging = true; //Indicating the pressure is on the move.

            backgroundWorkerGotoPressure = new System.ComponentModel.BackgroundWorker();
            backgroundWorkerGotoPressure.WorkerSupportsCancellation = true;
            backgroundWorkerGotoPressure.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerGotoPressureSteps_DoWork);
            backgroundWorkerGotoPressure.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerGotoPressure_RunWorkerCompleted);
            
            //Compiling the data needed to make the pressure move.
            object[] outgoingData = new object[2] { mode, desiredPressure };
            
            backgroundWorkerGotoPressure.RunWorkerAsync(outgoingData); //Starting the worker and passing the needed data to it.
            return true;
        }

        /// <summary>
        /// Methods reports of the pressure system is changing or not.
        /// </summary>
        /// <returns></returns>
        public bool checkMoveStatus()
        {
            return pressureConditionChanging;
        }


        public void stopGoToPressure()
        {
            if (backgroundWorkerGotoPressure != null)
            {
                if (backgroundWorkerGotoPressure.IsBusy)
                {
                    gotoPressureEvent.UpdatingObject = (object)"Sending cancel to pressure move.";
                    backgroundWorkerGotoPressure.CancelAsync();
                }
            }
            else
            {
                //managerError.UpdatingObject
            }
        }



        void backgroundWorkerGotoPressure_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                gotoPressureEvent.UpdatingObject = (object)"(Pressure Manager Error) " + e.Error.Message;
            }
            //Cleaning up things created for the move.
            try
            {
                //Stopping event setup to check for rough stability.
                sensorPressure.updatedPressureValue.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(pressureMove_Checking);
                System.Threading.Thread.Sleep(100); //A little pause to make sure all events have ended.
                desiredEnviroment.removeEviromentElement("moveStable"); //Removing he check enviroment.
            }
            catch(Exception error)
            {
                managerError.UpdatingObject = (object)error;
            }
            pressureConditionChanging = false;

            //Last chance to close all the pressure change valves.
            pressureValvesClose();

            logEvent("Goto Pressure Processes has ended");
            gotoPressureEvent.UpdatingObject = (object)"Goto Pressure Processes has ended";
        }

        void backgroundWorkerGotoPressureSteps_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            logEvent("Goto Pressure Processes Started," + sensorPressure.getCurrentPressure().ToString());
            

            //Starting the pressure move by getting the data needed for the move.
            object[] incomingData = (object[])e.Argument; //Pulling in needed data for pressure move.
            int mode = (int)incomingData[0]; //Getting the mode.
            double desiredPressure = (double)incomingData[1]; //Getting the desired pressure.
            string direction = "";

            //Creating new internal/temp pressure stability check system to use to decide if the system is stable enough to decide if another move is needed.
            desiredEnviroment.addEnviromentElement("moveStable", -0.008, 0.008, 0, 10);
            //Setting up a pressure updata event for the move.
            sensorPressure.updatedPressureValue.PropertyChange +=new updateCreater.objectUpdate.PropertyChangeHandler(pressureMove_Checking); //This will set the pressure ready.

            //Determing the direction
            if (desiredPressure > sensorPressure.getCurrentPressure())
            {
                direction = "up";
            }
            else
            {
                direction = "down";
            }


            //Need a way to skip rough moves.
            if (Math.Abs(sensorPressure.getCurrentPressure() - desiredPressure) > 11) //This allows for a bypass.
            {
                logEvent("Rough Pressure Move Start");


                if (mode >= 2) //Rough Move.
                {
                    double correctedPressure = 0;
                    //Gettting correction 
                    if (direction == "up")
                    {
                        correctedPressure = getPressureUpCorrection(desiredPressure);
                    }
                    else
                    {
                        correctedPressure = getPressureDownCorrection(desiredPressure);
                    }

                    logEvent("Rough Pressure Move data," + sensorPressure.getCurrentPressure().ToString() + "," + desiredPressure.ToString() + "," + correctedPressure.ToString());
                    while (!backgroundWorkerGotoPressure.CancellationPending)
                    {
                        if (direction == "up")
                        {
                            if (sensorPressure.getCurrentPressure() >= correctedPressure || sensorPressure.getCurrentPressure() > 1043) //Blanket catch statment. //Also proventing over pressure.
                            {
                                pressureValvesClose();
                                break;
                            }
                            else
                            {
                                pressureUp();
                            }

                        }
                        else
                        {
                            if (sensorPressure.getCurrentPressure() <= correctedPressure) //Blanket catch statment.
                            {
                                pressureValvesClose();
                                break;
                            }
                            else
                            {
                                pressureDown();
                            }

                            //If the pressure is getting close to the desire pressure and is predicted to go under between sensor reads then this will catch it.
                            if (perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()) < 0)
                            {
                                if (sensorPressure.getCurrentPressure() - perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()) <= correctedPressure)
                                {
                                    //Calculating the differance from the current pressure to the perdicted pressure.
                                    double diffPerdtoCurrent = sensorPressure.getCurrentPressure() - correctedPressure;
                                    double predictedRateOfChange = perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()); //Getting the prodicted rate of change.

                                    //Findind the mils needed to arive at the desired pressure.
                                    double timeNeededToGetThere = (diffPerdtoCurrent / predictedRateOfChange) * 1000; //Determing the time to hit the desired.

                                    System.Threading.Thread.Sleep(Math.Abs((int)timeNeededToGetThere));
                                    pressureValvesClose();
                                    break;
                                }
                            }
                        }

                        //A break to keep the system from running out of control.
                        System.Threading.Thread.Sleep(100);
                    }

                    logEvent("Rough Pressure Move Complete," + sensorPressure.getCurrentPressure().ToString());

                    //Once canceled make sure the valves are closed.
                    if (backgroundWorkerGotoPressure.CancellationPending)
                    {
                        pressureValvesClose();
                        
                    }

                    mode = mode - 2;
                    pressureMoveRoughComplete.UpdatingObject = (object)"Rough Move Complete.";
                    gotoPressureEvent.UpdatingObject = (object)"Rough Move Complete.";


                }

                //Waiting for the system to become move stable. If it takes to long error out and let people know something is wrong.
                gotoPressureEvent.UpdatingObject = (object)"Waiting for system to become stable";
                if (!pressureMoveStable.WaitOne(300000))
                {
                    logEvent("Pressure move error. System will not stabilize. Waiting another 5 Min.");
                    managerError.UpdatingObject = (object)"Pressure move error. System will not stabilize. Waiting another 5 Min.";

                    if (!pressureMoveStable.WaitOne(300000))
                    {
                        logEvent("Pressure move error. System will not stabilize.");
                        managerError.UpdatingObject = (object)"Pressure move error. System will not stabilize.";
                        return;
                    }

                    
                }

                //Logging stable information
                logEvent("Rough Pressure Move Stable," + sensorPressure.getCurrentPressure().ToString());

                //Checking to see if we need to continue.

                if (desiredEnviroment.checkEnviromentElement("pressureMoveEnv", sensorPressure.getCurrentPressure()))
                {
                    pressureConditionChanging = false;
                    return; //Stopping the system.
                }

            }
            else
            {
                mode = mode - 2;
            }

            if (mode >= 4) //Fine Move.
            {
                

                int numberOfFineLoops = 0;
                while (!backgroundWorkerGotoPressure.CancellationPending || !desiredEnviroment.checkEnviromentElement("pressureMoveEnv", sensorPressure.getCurrentPressure()))
                {

                    logEvent("Fine move started. Loop," + numberOfFineLoops.ToString());

                    //Seeing what direct we need to go.
                    double pressureDifferance = desiredPressure - sensorPressure.getCurrentPressure(); //Getting the differance from desires to current.
                    logEvent("Fine move differance," + pressureDifferance.ToString() + ",SetPoint," + desiredPressure.ToString());

                    if (pressureDifferance < 0) //Moving pressure down
                    {
                        double predictedRateOfChange = perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()); //Getting the prodicted rate of change.
                        double timeNeededToMove = (pressureDifferance / predictedRateOfChange) * 1000; //Determing the time to hit the desired.
                        pressureDown();

                        //This was added to get the system to stop looping at low pressure and kick the pressure system a bit.
                        if (numberOfFineLoops > 5)
                        {
                            timeNeededToMove = timeNeededToMove * 3;

                        }

                        if (Math.Abs(timeNeededToMove) < 100) //Finding that moving down if the time is less then 100 ms then the conditions doesn't change. Also in some cases is goes backwards.
                        {
                            timeNeededToMove = timeNeededToMove * 2;
                        }

                        System.Threading.Thread.Sleep(Math.Abs((int)timeNeededToMove));
                        logEvent("Fine move time spent," + timeNeededToMove.ToString());
                    }
                    else //Moving pressure up.
                    {
                        double predictedRateOfChange = predictedRateOfChangeUp(sensorPressure.getCurrentPressure());
                        double timeNeededToMove = (pressureDifferance / predictedRateOfChange) * 1000; //Determing the time to hit the desired.
                        pressureUp();

                        if (numberOfFineLoops > 5 && sensorPressure.getCurrentPressure() < 300 )
                        {
                            if (sensorPressure.getCurrentPressure() < 1000) //Sensor protection
                            {
                                timeNeededToMove = timeNeededToMove * 3;
                            }
                            else
                            {
                                timeNeededToMove = timeNeededToMove * 2;
                            }

                            numberOfFineLoops = 0; //Resetting the loop count.... not sure if I need to do this.
                        }
                        System.Threading.Thread.Sleep(Math.Abs((int)timeNeededToMove));
                        logEvent("Fine move time spent," + timeNeededToMove.ToString());
                    }

                    pressureValvesClose(); //Closing the valves.

                    //Trying to stop the valves from sticking open... Not sure why that is happening, but this is something I can try.
                    System.Threading.Thread.Sleep(500);
                    pressureValvesClose(); //Closing the valves.

                    pressureMoveFineComplete.UpdatingObject = (object)"Fine Move Complete.";
                    gotoPressureEvent.UpdatingObject = (object)"Fine Move Complete.";
                    logEvent("Fine Move Complete," + sensorPressure.getCurrentPressure().ToString());

                    //Waiting for the system to become move stable. If it takes to long error out and let people know something is wrong.
                    pressureMoveStable = new System.Threading.AutoResetEvent(false); //Resetting the auto reset event.
                    gotoPressureEvent.UpdatingObject = (object)"Waiting for system to become stable";
                    if (!pressureMoveStable.WaitOne(300000))
                    {
                        logEvent("Unable to get stably. Stopping everything.");
                        managerError.UpdatingObject = (object)"Pressure move error. System will not stabilize.";
                        return;
                        //throw new Exception("Pressure Manager, Unable to get to conditions.");
                        
                    }

                    logEvent("Fine move stable," + sensorPressure.getCurrentPressure().ToString());

                    //Checking to see if we need to continue.

                    if (desiredEnviroment.checkEnviromentElement("pressureMoveEnv", sensorPressure.getCurrentPressure()))
                    {
                        pressureConditionChanging = false;
                        return; //Stopping the system.
                    }
                    numberOfFineLoops++;
                }

                mode = mode - 4; //Move fully complete.
            }

            /* This might need to be refine the move.
            if (mode >= 8) //Extra Fine Move.
            {
                mode = mode - 8;
            }
            */

            if (mode != 0) //Error checking the mode.
            {
                managerError.UpdatingObject = (object)"Pressure move error. Mode has more int left.";
            }
            if (backgroundWorkerGotoPressure.CancellationPending)
            {
                gotoPressureEvent.UpdatingObject = (object)"Worker responded to shutdown request.";
            }

            //Double check shutting down.
            pressureConditionChanging = false;
        }

        /// <summary>
        /// This is the control brain. This thread will decide when to shutdown the valves.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void backgroundWorkerGotoPressure_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            /*This is a PID Code..
            //setting up the PID section
            //coefs.    
            double Kp = 1;
            double Ki = 1;
            double Kd = 1;

            //Control code.
            double previousError = desiredEnviroment.getElementSetPoint("pressureCondition") - sensorPressure.getCurrentPressure();
            double integral = 0;
            int dt = 2000;

            //PID code. Not sure if this is going to work.
            while (!backgroundWorkerGotoPressure.CancellationPending)
            {
                System.Threading.Thread.Sleep(dt);
                double error = desiredEnviroment.getElementSetPoint("pressureCondition") - sensorPressure.getCurrentPressure();
                integral = integral + (error * dt);
                double derivative = (error - previousError) / dt;
                double output = (Kp * error) + (Ki * integral) + (Kd * derivative);
                previousError = error;
            }
             * 
            

            //breaking recived event data into a string array.
            object[] incomingData = (object[])e.Argument;
            double desiredPressure = (double)incomingData[0];
            string direction = (string)incomingData[1];
            */
            


          
            //breaking recived event data into a string array.
            object[] incomingData = (object[])e.Argument;
            double desiredPressure = (double)incomingData[0];
            string direction = (string)incomingData[1];

            //Protection from keeping the system moving if it is with the exceptable range
            if(desiredEnviroment.checkEnviromentElement("pressureCondition", sensorPressure.getCurrentPressure()))
            {
                return;
            }

            double correctedPressure = 0;
            //Gettting correction 
            if (direction == "Up")
            {
                correctedPressure = getPressureUpCorrection(desiredPressure);
            }
            else
            {

                ////////////////////////////////////////////////
                //////Remove after testing.
                //correctedPressure = desiredPressure;
                ////////////////////////
                //////////////////////////////

                correctedPressure = getPressureDownCorrection(desiredPressure);
            }

            while (!backgroundWorkerGotoPressure.CancellationPending)
            {
                if (direction == "Up")
                {
                    if (sensorPressure.getCurrentPressure() >= correctedPressure) //Blanket catch statment.
                    {
                        pressureValvesClose();
                        break;
                    }
                    else
                    {
                        pressureUp();
                    }

                }
                else 
                {
                    if (sensorPressure.getCurrentPressure() <= correctedPressure) //Blanket catch statment.
                    {
                        pressureValvesClose();
                        break;
                    }
                    else
                    {
                        pressureDown();
                    }

                    //If the pressure is getting close to the desire pressure and is predicted to go under between sensor reads then this will catch it.
                    if (perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()) < 0)
                    {
                        if (sensorPressure.getCurrentPressure() + perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()) <= correctedPressure)
                        {
                            //Calculating the differance from the current pressure to the perdicted pressure.
                            double diffPerdtoCurrent = sensorPressure.getCurrentPressure() - (sensorPressure.getCurrentPressure() + perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()));

                            //finding the rate of change pre mill.
                            double ratePerMill = perdictedRateOfChangeDown(sensorPressure.getCurrentPressure()) / 1000; //Rate change per mil

                            //Findind the mils needed to arive at the desired pressure.
                            int timeNeededToGetThere = Convert.ToInt16((sensorPressure.getCurrentPressure() - correctedPressure) / ratePerMill);

                            //All of that came before leads the the delay needed to get there.
                            if (timeNeededToGetThere < 0)
                            {
                                timeNeededToGetThere = timeNeededToGetThere * -1;
                            }

                            System.Threading.Thread.Sleep(timeNeededToGetThere);
                            pressureValvesClose();
                            break;
                        }
                    }


                }

                //A break to keep the system from running out of control.
                System.Threading.Thread.Sleep(100);
            }

        }
        


        private void pressureDown()
        {
            relayController.commandRelayBoard("PressureDown", true);
        }

        private void pressureUp()
        {
            relayController.commandRelayBoard("PressureUp", true);
        }

        /// <summary>
        /// Closed both PressureDown and PressureUp valve.
        /// </summary>
        private void pressureValvesClose()
        {
            string downReport = relayController.commandRelayBoard("PressureDown", false);
            string upReport = relayController.commandRelayBoard("PressureUp", false);
            logEvent("Pressure Valves close command sent. Down: " + downReport + " Up: " + upReport);
        }

        /// <summary>
        /// Method calculates the proper shut off point for the system to bounce back to the desired pressure.
        /// Use when going up in pressure.
        /// </summary>
        /// <param name="desiredPressure"></param>
        /// <returns></returns>
        public double getPressureUpCorrection(double desiredPressure)
        {
            //=(-0.00009*J2^2)+(1.1793*J2)-99.334
            return (desiredPressure - ((-0.00009 * (Math.Pow(desiredPressure, 2))) + (1.1793 * desiredPressure) - 99.334)) + desiredPressure;
        }

        /// <summary>
        /// Method calculates the proper shut off point for the system to bounce back to the desired pressure.v
        /// Use when going down in pressure.
        /// </summary>
        /// <param name="desiredPressure"></param>
        /// <returns></returns>
        public double getPressureDownCorrection(double desiredPressure)
        {
            //Rough pressure change calc.
            //=(-0.00005*L3^2)+(1.0754*L3)+0.8969
            //return ((desiredPressure - ((-0.00005 * (Math.Pow(desiredPressure, 2))) + (1.0754 * desiredPressure) + 0.8969)) + desiredPressure);

            //Matrix calc change
            double bounce = getAirDenDown(desiredPressure, pressureSystemTemp);
            return desiredPressure - bounce;

        }

        
        public double getAirDenDown(double pressure, double airTemp)
        {
            double[] pCondition = new double[8]{950,850,650,400,250,100,50,5};
            double[] aCondition = new double[4]{-10,5,20,40};

            
            double[,] dataAP = new double[32, 2];

            //Filling the double array with the conditions I have taken data at.
            for (int con = 0; con * pCondition.Length < dataAP.Length/2; con++)
            {
                int counterA = 0;

                for (int counterP = 0; counterP < pCondition.Length; counterP++)
                {
                    dataAP[counterP + (con * pCondition.Length), 0] = pCondition[counterA];
                    dataAP[counterP + (con * pCondition.Length), 1] = aCondition[con];
                    counterA++;
                }
                
            }

            //Building the oberservation matrix
            
            double[,] obervationMatrix = new double[32,9];

            for (int i = 0; i < 32; i++)
            {
                obervationMatrix[i, 0] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 0);//p0T0
                obervationMatrix[i, 1] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 1);//p0T1
                obervationMatrix[i, 2] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 2);//p0T2

                obervationMatrix[i, 3] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 0);//p1T0
                obervationMatrix[i, 4] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 1);//p1T1
                obervationMatrix[i, 5] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 2);//p1T2

                obervationMatrix[i, 6] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 0);//p2T0
                obervationMatrix[i, 7] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 1);//p2T1
                obervationMatrix[i, 8] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 2);//p2T2

            }

            double[] usedCoef = new double[9];
            
            /*
            double[,] obervationMatrix = new double[32, 12];

            for (int i = 0; i < 32; i++)
            {
                obervationMatrix[i, 0] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 0);//p0T0
                obervationMatrix[i, 1] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 1);//p0T1
                obervationMatrix[i, 2] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 2);//p0T2
                obervationMatrix[i, 3] = Math.Pow(dataAP[i, 0], 0) * Math.Pow(dataAP[i, 1], 3);//p0T3

                obervationMatrix[i, 4] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 0);//p1T0
                obervationMatrix[i, 5] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 1);//p1T1
                obervationMatrix[i, 6] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 2);//p1T2
                obervationMatrix[i, 7] = Math.Pow(dataAP[i, 0], 1) * Math.Pow(dataAP[i, 1], 3);//p1T3

                obervationMatrix[i, 8] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 0);//p2T0
                obervationMatrix[i, 9] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 1);//p2T1
                obervationMatrix[i, 10] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 2);//p2T2
                obervationMatrix[i, 11] = Math.Pow(dataAP[i, 0], 2) * Math.Pow(dataAP[i, 1], 3);//p2T3

            }

            double[] usedCoef = new double[12];
            */

        
            //Matrix ObservationMatrix = Matrix.Create(obervationMatrix);
            //Matrix ReferenceMatrix = new Matrix(dataAP.Length/2, 1);

            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix ObservationMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(obervationMatrix);
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix ReferenceMatrix = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(dataAP.Length / 2, 1);

            //Enter air density time values here in same order as dataAP
            //This is air density bounce
            /*
            ReferenceMatrix[0,0] = 0.01493;
            ReferenceMatrix[1, 0] = 0.02605;
            ReferenceMatrix[2, 0] = 0.03957;
            ReferenceMatrix[3, 0] = 0.02955;
            ReferenceMatrix[4, 0] = 0.01959;
            ReferenceMatrix[5, 0] = 0.01013;
            ReferenceMatrix[6, 0] = 0.00854;
            ReferenceMatrix[7, 0] = 0.00229;
            ReferenceMatrix[8, 0] = 0.01582;
            ReferenceMatrix[9, 0] = 0.02090;
            ReferenceMatrix[10, 0] = 0.03380;
            ReferenceMatrix[11, 0] = 0.02729;
            ReferenceMatrix[12, 0] = 0.01864;
            ReferenceMatrix[13, 0] = 0.01035;
            ReferenceMatrix[14, 0] = 0.00751;
            ReferenceMatrix[15, 0] = 0.00224;
            ReferenceMatrix[16, 0] = 0.01822;
            ReferenceMatrix[17, 0] = 0.02179;
            ReferenceMatrix[18, 0] = 0.03503;
            ReferenceMatrix[19, 0] = 0.02582;
            ReferenceMatrix[20, 0] = 0.01663;
            ReferenceMatrix[21, 0] = 0.00970;
            ReferenceMatrix[22, 0] = 0.00729;
            ReferenceMatrix[23, 0] = 0.00224;
            ReferenceMatrix[24, 0] = 0.01892;
            ReferenceMatrix[25, 0] = 0.01923;
            ReferenceMatrix[26, 0] = 0.03475;
            ReferenceMatrix[27, 0] = 0.02534;
            ReferenceMatrix[28, 0] = 0.01652;
            ReferenceMatrix[29, 0] = 0.00953;
            ReferenceMatrix[30, 0] = 0.00718;
            ReferenceMatrix[31, 0] = 0.00238;
            */
        
            //this is Pressure Bounce
            ReferenceMatrix[0, 0] =  11.28; //1035
            ReferenceMatrix[1, 0] = 19.676; //950
            ReferenceMatrix[2, 0] = 29.887; //850
            ReferenceMatrix[3, 0] = 22.321; //650
            ReferenceMatrix[4, 0] = 13.796; //250
            ReferenceMatrix[5, 0] = 8.00; //100
            ReferenceMatrix[6, 0] = 6.451; //50
            ReferenceMatrix[7, 0] = 2.100; //5

            ReferenceMatrix[8, 0] = 12.629;
            ReferenceMatrix[9, 0] = 16.000; 
            ReferenceMatrix[10, 0] = 25.985;
            ReferenceMatrix[11, 0] = 21.787;
            ReferenceMatrix[12, 0] = 13.883;
            ReferenceMatrix[13, 0] = 7.26;
            ReferenceMatrix[14, 0] = 5.994;
            ReferenceMatrix[15, 0] = 2.100;

            ReferenceMatrix[16, 0] = 15.335;
            ReferenceMatrix[17, 0] = 18.000;
            ReferenceMatrix[18, 0] = 29.477;
            ReferenceMatrix[19, 0] = 21.727;
            ReferenceMatrix[20, 0] = 13.997;
            ReferenceMatrix[21, 0] = 8.159;
            ReferenceMatrix[22, 0] = 6.138;
            ReferenceMatrix[23, 0] = 2.200;

            ReferenceMatrix[24, 0] = 17.003;
            ReferenceMatrix[25, 0] = 17.000;
            ReferenceMatrix[26, 0] = 31.236;
            ReferenceMatrix[27, 0] = 22.78;
            ReferenceMatrix[28, 0] = 14.851;
            ReferenceMatrix[29, 0] = 8.564;
            ReferenceMatrix[30, 0] = 6.45;
            ReferenceMatrix[31, 0] = 2.300;

            //Matrix transposedO = Matrix.Transpose(ObservationMatrix);
            //Matrix coefficientMatrix = (((transposedO.Multiply(ObservationMatrix)).Inverse()).Multiply(transposedO)).Multiply(ReferenceMatrix);

            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix transposedO = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)ObservationMatrix.Transpose();
            MathNet.Numerics.LinearAlgebra.Double.SparseMatrix coefficientMatrix = (MathNet.Numerics.LinearAlgebra.Double.SparseMatrix)(((transposedO.Multiply(ObservationMatrix)).Inverse()).Multiply(transposedO)).Multiply(ReferenceMatrix);

            for (int i = 0; i < usedCoef.Length; i++)
            {
                usedCoef[i] = coefficientMatrix[i, 0];
            }

            double incomingP = pressure;
            double incomingAT = airTemp;

            
            double airDensity = (usedCoef[0] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 0)) + //p0t0
                                (usedCoef[1] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 1)) + //p0t1
                                (usedCoef[2] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 2)) + //p0t2

                                (usedCoef[3] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 0)) + //p1t0
                                (usedCoef[4] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 1)) + //p1t1
                                (usedCoef[5] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 2)) + //p1t2

                                (usedCoef[6] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 0)) + //p2t0
                                (usedCoef[7] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 1)) + //p2t1
                                (usedCoef[8] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 2)); //p2t2
            
            /*
            double airDensity = (usedCoef[0] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 0)) + //p0t0
                                (usedCoef[1] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 1)) + //p0t1
                                (usedCoef[2] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 2)) + //p0t2
                                (usedCoef[3] * Math.Pow(incomingP, 0) * Math.Pow(incomingAT, 3)) + //p0t3

                                (usedCoef[4] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 0)) + //p1t0
                                (usedCoef[5] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 1)) + //p1t1
                                (usedCoef[6] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 2)) + //p1t2
                                (usedCoef[7] * Math.Pow(incomingP, 1) * Math.Pow(incomingAT, 3)) + //p1t3

                                (usedCoef[8] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 0)) + //p2t0
                                (usedCoef[9] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 1)) + //p2t1
                                (usedCoef[10] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 2)) + //p2t2
                                (usedCoef[11] * Math.Pow(incomingP, 2) * Math.Pow(incomingAT, 3)); //p2t3
            */
        
            return airDensity;

        }
        
        public double perdictedRateOfChangeDown(double currentRefReading)
        {
            //y = 4E-11x4 - 5E-08x3 + 2E-05x2 + 0.0113x - 0.0509
            return (4e-11 * Math.Pow(currentRefReading, 4)) + (-5e-08 * Math.Pow(currentRefReading, 3)) + (2e-05 * Math.Pow(currentRefReading, 2)) + (0.0113 * currentRefReading) - 0.0509;
        }

        public double predictedRateOfChangeUp(double currentRefReading)
        {
            if (currentRefReading <= 185.406)
            {
                //y = 0.004x2 - 1.2763x + 112.73
                //return (0.004 * Math.Pow(currentRefReading, 2)) - (1.2763 * currentRefReading) + 112.73;

                //y = -2E-05x3 + 0.0111x2 - 1.9091x + 115.25
                return (-2e-05 * Math.Pow(currentRefReading, 3)) + (0.0111 * Math.Pow(currentRefReading, 2)) + (-1.9091 * currentRefReading) + 115.25;
            }
            else
            {
                //y = -0.0156x + 18.612
                return (-0.0156 * currentRefReading) + 18.612;
            }
        }

        #endregion
    }
}
